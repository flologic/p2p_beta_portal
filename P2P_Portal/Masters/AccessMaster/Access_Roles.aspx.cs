﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Text;
using System.Net.Http;
using log4net;

namespace P2P_Portal.Masters.Other_Masters
{
    public partial class Access_Roles : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txt_Master_URL.Value = System.Configuration.ConfigurationManager.AppSettings["MasterAPI"];
            txt_Client_ID.Value = System.Configuration.ConfigurationManager.AppSettings["Client_ID"];
            txt_Username.Value = Session["User_ADID"].ToString();
        }

        [WebMethod]
        public static string[] GetRoleData(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "AccessRole";
                    //string apiUrl = "http://localhost:59313/api/AccessRole";
                    var input = new
                    {
                        FK_CLIENTID = Client_ID,
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/getAccessRoleInfo", inputContent).Result;
                    jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
                }
                Logger1.Activity("Access Data Role In P2P");
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string SaveData(string ClientID, string Master_API, string rolename, string Created_By)
        {
            string Result = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "AccessRole";
                    //string apiUrl = "http://localhost:59313/api/AccessRole";
                    var input = new
                    {
                        FK_CLIENTID = ClientID,
                        label = rolename,
                        AMD_BY = Created_By
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/InsertAccessRoleInfo", inputContent).Result;
                    //Result = JsonConvert.SerializeObject(response.ReasonPhrase);
                    Result = response.Content.ReadAsStringAsync().Result;                    

                    Logger1.Activity("Access Roles Data " + Result + " In P2P");
                }                
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return Result;
        }

        [WebMethod]
        public static string UpdateData(string ClientID, string Master_API, string PK_ROLEID, string Role_Name, string Updated_By)
        {
            string Result = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "AccessRole";
                    //string apiUrl = "http://localhost:59313/api/AccessRole";
                    var input = new
                    {
                        id = PK_ROLEID,
                        FK_CLIENTID = ClientID,
                        label = Role_Name,
                        AMD_BY = Updated_By
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/UpdateAccessRoleInfo", inputContent).Result;
                    //Result = JsonConvert.SerializeObject(response.ReasonPhrase);
                    Result = response.Content.ReadAsStringAsync().Result;

                    Logger1.Activity("Access Roles Data " + Result + " In P2P");
                }
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return Result;        
        }

        [WebMethod]
        public static string Deletedata(string ClientID, string Master_API, string PK_ROLEID, string Updated_By)
        {
            string Result = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "AccessRole";
                    //string apiUrl = "http://localhost:59313/api/AccessRole";
                    var input = new
                    {
                        id = PK_ROLEID,
                        FK_CLIENTID = ClientID,
                        AMD_BY = Updated_By
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/DeleteAccessRoleInfo", inputContent).Result;
                    //Result = JsonConvert.SerializeObject(response.ReasonPhrase);
                    Result = response.Content.ReadAsStringAsync().Result;

                    Logger1.Activity("Access Roles Data " + Result + " In P2P");
                }
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return Result;
        }
    }
}