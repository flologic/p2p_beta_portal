﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SupplierMaster.aspx.cs" Inherits="P2P_Portal.Masters.Other_Masters.SupplierMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Supplier</title>
    <link href="../../assets/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../assets/plugins/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet" />
    <link href="../../assets/css/style.css" rel="stylesheet" />
    <link href="../../assets/css/jquery-ui.css" rel="stylesheet" />
    <link href="../../assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="../../assets/css/jAlert.css" rel="stylesheet" />

    <link href="../../assets/js/Export/buttons.dataTables.min.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ToolkitScriptManager>
        <section class="row bg-title">
            <div class="col-md-12">
                <div id="Div1" class="panel panel-default panel-border-top" runat="server">
                    <div class="panel-heading">
                        Supplier Master
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Supplier Name:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control input-sm mandatory" id="txt_Supplier" runat="server" placeholder='Enter Supplier Name' />
                                        </div>
                                        <label class="col-md-2 control-label">Supplier Code:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control input-sm mandatory" id="txt_Suppcode" runat="server" placeholder='Enter Supplier Code' />
                                        </div>
                                        <label class="col-md-2 control-label">Supplier Type:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control input-sm mandatory" id="txt_Supptype" runat="server" placeholder='Enter Supplier Type' />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Supplier ShortName:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control input-sm mandatory" id="txt_ShortName" runat="server" placeholder='Enter Supplier Short Name' />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button id="btn_Save" name="btn_Show" type="button" class="fcbtn btn btn-info btn-outline btn-1b">Submit</button>
                                <button id="btn_Upd" name="btn_Show" type="button" class="fcbtn btn btn-info btn-outline btn-1b">Update</button>
                                <button id="btn_Cancel" name="btn_Show" type="button" class="fcbtn btn btn-info btn-outline btn-1b">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div id="Div4" class="panel panel-default panel-border-top" runat="server">
                    <div class="panel-heading">
                        Supplier Details                               
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <div id="div_supplier">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="display: none">
                <asp:TextBox ID="txt_UserID" runat="server"></asp:TextBox>
                <input type="text" id="txt_Master_URL" runat="server" />
                <input type="text" id="txt_Client_ID" runat="server" />
                <input type="text" id="txt_Username" runat="server" />
                <input type="text" id="txt_PKID" runat="server" />
            </div>
        </section>
    </form>
    <script src="../../assets/js/jquery.min.js"></script>
    <script src="../../assets/js/bootstrap.bundle.min.js"></script>
    <script lang="JavaScript" type="text/javascript" src="../../assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script lang="JavaScript" type="text/javascript" src="../../assets/js/jquery-ui.js"></script>
    <script src="../../assets/js/jAlert.js"></script>
    <script src="../../assets/js/jAlert-functions.js"></script>
    <script src="../../assets/js/underscore.js"></script>
    <script src="../../JS/Common/Utility.js"></script>
    <script src="../../JS/Master/SupplierMaster.js"></script>
    <!--export JS -->
    <script src="../../assets/js/Export/dataTables.buttons.min.js"></script>
    <script src="../../assets/js/Export/buttons.flash.min.js"></script>
    <script src="../../assets/js/Export/jszip.min.js"></script>
    <script src="../../assets/js/Export/buttons.html5.min.js"></script>
    <!--export JS -->
</body>
</html>

