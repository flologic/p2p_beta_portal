﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SupplierContact_Master.aspx.cs" Inherits="P2P_Portal.Masters.Other_Masters.SupplierContact_Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Supplier Contact Details</title>
    <link href="../../assets/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../assets/plugins/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet" />
    <link href="../../assets/css/style.css" rel="stylesheet" />
    <link href="../../assets/css/jquery-ui.css" rel="stylesheet" />
    <link href="../../assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="../../assets/css/jAlert.css" rel="stylesheet" />
    <link href="../../assets/js/Export/buttons.dataTables.min.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ToolkitScriptManager>
        <section class="row bg-title">
            <div class="col-md-12">
                <div id="Div1" class="panel panel-default panel-border-top" runat="server">
                    <div class="panel-heading">
                        Supplier Contact Details
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Supplier Name:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control input-sm mandatory" id="txt_supplier" onkeypress="GetSupplier()" runat="server" placeholder='Enter Supplier Name' />
                                        </div>
                                        <label class="col-md-2 control-label">Branch:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control input-sm mandatory" id="txt_branch" runat="server" placeholder='Enter Branch' />
                                        </div>
                                         <label class="col-md-2 control-label">City:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control input-sm" id="txt_city" runat="server" placeholder='Enter City' />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Contact Person:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control input-sm mandatory" id="txt_cnctperson" runat="server" placeholder='Enter Contact Person' />
                                        </div>
                                        <label class="col-md-2 control-label">Contact Email:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control input-sm" id="txt_contemail" runat="server" placeholder='Enter Contact Email' />
                                        </div>
                                        <label class="col-md-2 control-label">Address:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control input-sm" id="txt_Address" runat="server" placeholder='Enter Address' />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Fax:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control input-sm" id="txt_fax" runat="server" placeholder='Enter Fax No' />
                                        </div>
                                        <label class="col-md-2 control-label">Pin Code:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control input-sm" id="txt_pincode" runat="server" placeholder='Enter Pin Code' />
                                        </div>
                                        <label class="col-md-2 control-label">Tel.No.:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control input-sm" id="txt_telno" runat="server" placeholder='Enter Tel.No.' />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">GST:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control input-sm" id="txt_GST" runat="server" placeholder='Enter GST' />
                                        </div>
                                        <label class="col-md-2 control-label">Mob.No.:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control input-sm" id="txt_mobno" runat="server" placeholder='Enter Mob.No.' />
                                        </div>
                                        <label class="col-md-2 control-label">PAN No.:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control input-sm" id="txt_panno" runat="server" placeholder='Enter PAN No.' />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button id="btn_Save" name="btn_Show" type="button" class="fcbtn btn btn-info btn-outline btn-1b">Submit</button>
                                <button id="btn_Upd" name="btn_Update" type="button" class="fcbtn btn btn-info btn-outline btn-1b">Update</button>
                                <button id="btn_Cancel" name="btn_Show" type="button" class="fcbtn btn btn-info btn-outline btn-1b">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div id="Div4" class="panel panel-default panel-border-top" runat="server">
                    <div class="panel-heading">
                        Contact Details                               
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <div id="div_supplier">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="display: none">
                <asp:TextBox ID="txt_UserID" runat="server"></asp:TextBox>
                <input type="text" id="txt_Master_URL" runat="server" />
                <input type="text" id="txt_Client_ID" runat="server" />
                <input type="text" id="txt_Username" runat="server" />
                <input type="text" id="txt_PKID" runat="server" />
            </div>
        </section>
    </form>
    <script src="../../assets/js/jquery.min.js"></script>
    <script src="../../assets/js/bootstrap.bundle.min.js"></script>
    <script lang="JavaScript" type="text/javascript" src="../../assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script lang="JavaScript" type="text/javascript" src="../../assets/js/jquery-ui.js"></script>
    <script src="../../assets/js/jAlert.js"></script>
    <script src="../../assets/js/jAlert-functions.js"></script>
    <script src="../../assets/js/underscore.js"></script>
    <script src="../../JS/Common/Utility.js"></script>
    <script src="../../JS/Common/document-attach.js"></script>
    <script src="../../JS/Master/Supplier_Contact_Details.js"></script>
    <!--export JS -->
    <script src="../../assets/js/Export/dataTables.buttons.min.js"></script>
    <script src="../../assets/js/Export/buttons.flash.min.js"></script>
    <script src="../../assets/js/Export/jszip.min.js"></script>
    <script src="../../assets/js/Export/buttons.html5.min.js"></script>
    <!--export JS -->
</body>
</html>
