﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;


namespace P2P_Portal.Masters.Other_Masters
{
    public partial class DepartmentMaster1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txt_Master_URL.Value = System.Configuration.ConfigurationManager.AppSettings["MasterAPI"];
            txt_Client_ID.Value = System.Configuration.ConfigurationManager.AppSettings["Client_ID"];
            txt_Username.Value = Session["User_ADID"].ToString();
        }

        [WebMethod]
        public static string[] GetDepartmentData(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "Department";
                    var input = new
                    {
                        FK_CLIENTID = Client_ID,
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/getDepartmentInfo", inputContent).Result;
                    jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
                }
                Logger1.Activity("Department Data Return In P2P");
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string SaveData(string ClientID, string Master_API, string departmentname, string departmentdescription, string Created_By)
        {
            string Result = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "Department";
                    //string apiUrl = "http://localhost:59313/api/Department";
                    var input = new
                    {
                        FK_CLIENTID = ClientID,
                        label = departmentname,
                        DEPARTMENT_DESC = departmentdescription,
                        EMP_NAME = Created_By
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/InsertDepartmentInfo", inputContent).Result;
                    Result = response.Content.ReadAsStringAsync().Result;

                    Logger1.Activity("Department Data " + Result + " In P2P");
                }
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return Result;
        }

        [WebMethod]
        public static string UpdateData(string ClientID, string Master_API, string PK_DEPARTMENTD, string departmentname, string departmentdescription, string Updated_By)
        {
            string Result = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "Department";
                    //string apiUrl = "http://localhost:59313/api/Department";
                    var input = new
                    {
                        id = PK_DEPARTMENTD,
                        FK_CLIENTID = ClientID,
                        label = departmentname,
                        DEPARTMENT_DESC = departmentdescription,
                        EMP_NAME = Updated_By
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/UpdateDepartmentInfo", inputContent).Result;
                    Result = response.Content.ReadAsStringAsync().Result;

                    Logger1.Activity("Department Data " + Result + " In P2P");
                }
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return Result;
        }

        [WebMethod]
        public static string Deletedata(string ClientID, string Master_API, string PK_DEPARTMENTD, string Updated_By)
        {
            string Result = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "Department";
                    //string apiUrl = "http://localhost:59313/api/Department";
                    var input = new
                    {
                        id = PK_DEPARTMENTD,
                        FK_CLIENTID = ClientID,
                        EMP_NAME = Updated_By
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/DeleteDepartmentInfo", inputContent).Result;
                    Result = response.Content.ReadAsStringAsync().Result;


                    Logger1.Activity("Department Data " + Result + " In P2P");
                }
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return Result;
        }
    }
}