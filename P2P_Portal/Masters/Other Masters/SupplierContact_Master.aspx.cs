﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace P2P_Portal.Masters.Other_Masters
{
    public partial class SupplierContact_Master : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txt_Master_URL.Value = System.Configuration.ConfigurationManager.AppSettings["MasterAPI"];
            txt_Client_ID.Value = System.Configuration.ConfigurationManager.AppSettings["Client_ID"];
            txt_Username.Value = Session["User_ADID"].ToString();
        }


        [WebMethod]
        public static string[] GetSupp(string Client_ID, string Master_API, string Supplier)
        {
            string[] jsonArray = new string[1];
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Supplier";
                var input = new
                {
                    FK_ClientID = "",
                    label = Supplier
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getSupplierInfo", inputContent).Result;
                jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string[] GetSuppDTL(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Supplier";
                var input = new
                {
                    FK_ClientID = "",
                    FK_SUPPLIER_HDR_ID = 0
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getSupplierDTLInfo", inputContent).Result;
                jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string SaveData(string ClientID, string Master_API, string SupplierID, string Branch, string City, string ContactPerson, string ContactEmail, string Address, string Fax, string Pin, string TelNo, string GST, string MobNo, string PanNo)
        {
            string Result = string.Empty;
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Supplier";
                var input = new
                {
                    CLIENT_ID = ClientID,
                    FK_SUPPLIER_HDR_ID = SupplierID,
                    CONTACT_PERSON = ContactPerson,
                    CONTACT_EMAIL = ContactEmail,
                    ADDRESS = Address,
                    CITY = City,
                    PINCODE = Pin,
                    TELNO = TelNo,
                    FAX = Fax,
                    MBLNO = MobNo,
                    PANNO = PanNo,
                    BRANCH = Branch,
                    GST = GST
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/InsertSupplierContactInfo", inputContent).Result;
                Result = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            return Result;
        }

        [WebMethod]
        public static string UpdateData(string ClientID, string Master_API, string PK_SuppID, string SupplierID, string Branch, string City, string ContactPerson, string ContactEmail, string Address, string Fax, string Pin, string TelNo, string GST, string MobNo, string PanNo)
        {
            string Result = string.Empty;
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Supplier";
                var input = new
                {
                    PK_ID = PK_SuppID,
                    CLIENT_ID = ClientID,
                    FK_SUPPLIER_HDR_ID = SupplierID,
                    CONTACT_PERSON = ContactPerson,
                    CONTACT_EMAIL = ContactEmail,
                    ADDRESS = Address,
                    CITY = City,
                    PINCODE = Pin,
                    TELNO = TelNo,
                    FAX = Fax,
                    MBLNO = MobNo,
                    PANNO = PanNo,
                    BRANCH = Branch,
                    GST = GST
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/UpdateSupplierContactInfo", inputContent).Result;
                Result = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            return Result;
        }

        [WebMethod]
        public static string Deletedata(string ClientID, string Master_API, string PK_ID)
        {
            string Result = string.Empty;
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Supplier";
                var input = new
                {
                    PK_ID = PK_ID,
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/DeleteSupplierContactInfo", inputContent).Result;
                Result = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            return Result;
        }
    }
}