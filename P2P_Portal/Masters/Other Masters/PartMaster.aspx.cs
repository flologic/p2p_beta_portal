﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace P2P_Portal.Masters.Other_Masters
{
    public partial class PartMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Session["User_ADID"] == null)
            //    Response.Write("<script language='javascript'>self.parent.location='../Login.aspx';</script>");
            //else
            //{
            //  txt_UserID.Text = Session["User_ADID"].ToString();
            txt_Master_URL.Value = System.Configuration.ConfigurationManager.AppSettings["MasterAPI"];
            txt_Client_ID.Value = System.Configuration.ConfigurationManager.AppSettings["Client_ID"];
            //  }
        }

        [WebMethod]
        public static string[] getPartAllData(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "Part";
                    var input = new
                    {
                        FK_CLIENTID = Client_ID,
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/getPartInfo", inputContent).Result;
                    jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
                }
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string saveData(string Role, string Master_API, string saveType)
        {
            string Result = string.Empty;

            //string[] jsonArray = new string[1];
            try
            {
                using (var client = new HttpClient())
                {
                    if (saveType == "SAVE")
                    {
                        string apiUrl = Master_API + "Part";
                        var input = new
                        {
                            transactionData = Role

                        };
                        string inputJson = (new JavaScriptSerializer()).Serialize(input);
                        HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                        HttpResponseMessage response = client.PostAsync(apiUrl + "/InsertPartInfo", inputContent).Result;
                        Result = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);


                    }
                    else if (saveType == "EDIT")
                    {
                        string apiUrl = Master_API + "Part";
                        var input = new
                        {
                            transactionData = Role

                        };
                        string inputJson = (new JavaScriptSerializer()).Serialize(input);
                        HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                        HttpResponseMessage response = client.PostAsync(apiUrl + "/UpdatePartInfo", inputContent).Result;
                        Result = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);

                    }
                    else if (saveType == "DELETE")
                    {
                        string apiUrl = Master_API + "Part";
                        var input = new
                        {
                            transactionData = Role

                        };
                        string inputJson = (new JavaScriptSerializer()).Serialize(input);
                        HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                        HttpResponseMessage response = client.PostAsync(apiUrl + "/DeletePartInfo", inputContent).Result;
                        Result = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);

                    }
                }


            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return Result;
        }

    }
}