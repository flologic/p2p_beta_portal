﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Text;
using System.Net.Http;

namespace P2P_Portal.Masters.Other_Masters
{
    public partial class Bill_PaymentMode_Master : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txt_Master_URL.Value = System.Configuration.ConfigurationManager.AppSettings["MasterAPI"];
            txt_Client_ID.Value = System.Configuration.ConfigurationManager.AppSettings["Client_ID"];
            txt_Username.Value = Session["User_ADID"].ToString();
        }

        [WebMethod]
        public static string[] GetPaymentData(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "BillPayMode";
                    //string apiUrl = "http://localhost:59313/api/BillPayMode";
                    var input = new
                    {
                        FK_CLIENT_ID = Client_ID,
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/getBillPayModeInfo", inputContent).Result;
                    jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
                }
                Logger1.Activity("Branch Data Return In P2P");
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string SaveData(string ClientID, string Master_API, string PaymntMode, string Created_By)
        {
            string Result = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "BillPayMode";
                    //string apiUrl = "http://localhost:59313/api/BillPayMode";
                    var input = new
                    {
                        FK_CLIENT_ID = ClientID,
                        label = PaymntMode,
                        EMP_NAME = Created_By
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/InsertBillPayModeInfo", inputContent).Result;
                    Result = response.Content.ReadAsStringAsync().Result;
                    Logger1.Activity("Bill Payment Mode Data " + Result + " In P2P");
                }
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return Result;
        }

        [WebMethod]
        public static string UpdateData(string ClientID, string Master_API, string PK_PAYMODE_ID, string PaymntMode, string Updated_By)
        {
            string Result = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "BillPayMode";
                    //string apiUrl = "http://localhost:59313/api/BillPayMode";
                    var input = new
                    {
                        id = PK_PAYMODE_ID,
                        FK_CLIENT_ID = ClientID,
                        label = PaymntMode,
                        EMP_NAME = Updated_By
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/UpdateBillPayModeInfo", inputContent).Result;
                    Result = response.Content.ReadAsStringAsync().Result;
                    Logger1.Activity("Bill Payment Mode Data " + Result + " In P2P");
                }
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return Result;
        }


        [WebMethod]
        public static string Deletedata(string ClientID, string Master_API, string PK_PAYMODE_ID, string Updated_By)
        {
            string Result = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "BillPayMode";
                    var input = new
                    {
                        id = PK_PAYMODE_ID,
                        FK_CLIENT_ID = ClientID,
                        EMP_NAME = Updated_By
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/DeleteBillPayModeInfo", inputContent).Result;
                    Result = response.Content.ReadAsStringAsync().Result;
                    Logger1.Activity("Bill Payment Mode Data " + Result + " In P2P");
                }
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return Result;
        }

    }
}