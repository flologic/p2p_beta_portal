﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmployeeMaster.aspx.cs" Inherits="P2P_Portal.Masters.Other_Masters.EmployeeMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Employee Master</title>
    <link href="../../assets/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../assets/plugins/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet" />
    <link href="../../assets/css/style.css" rel="stylesheet" />
    <link href="../../assets/css/jquery-ui.css" rel="stylesheet" />
    <link href="../../assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="../../assets/css/jAlert.css" rel="stylesheet" />
    <link href="../../assets/js/Export/buttons.dataTables.min.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ToolkitScriptManager>
        <section class="row bg-title">
            <div class="col-md-12">
                <div id="Div1" class="panel panel-default panel-border-top" runat="server">
                    <div class="panel-heading">
                        Employee Master
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Employee Name:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control input-sm mandatory" id="txt_Empname" runat="server" placeholder='Enter Employee Name' />
                                        </div>
                                        <label class="col-md-2 control-label">Employee Code:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control input-sm mandatory" id="txt_empcode" runat="server" placeholder='Enter Employee Code' />
                                        </div>
                                        <label class="col-md-2 control-label">Mobile No:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control input-sm mandatory number" id="txt_Mobno" runat="server" maxlength="10" placeholder='Enter Mobile Number'/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Address:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control input-sm mandatory" id="txt_address" runat="server" placeholder='Enter Address' />
                                        </div>
                                        <label class="col-md-2 control-label">Email Id:</label>
                                        <div class="col-md-2">
                                            <input type="email" class="form-control input-sm mandatory" id="txt_emailid" runat="server" placeholder='Enter Your Email Id'  onchange="checkValid()"/>
                                        </div>
                                        <label class="col-md-2 control-label">Designation:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control input-sm mandatory" id="txt_designation" runat="server" placeholder='Please Enter 3 Characters' onkeyup="bindDesignation()" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Branch:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control input-sm mandatory " id="txt_branchnm" runat="server" placeholder='Please Enter 3 Characters' onkeyup="bindBranch()" />
                                        </div>
                                        <label class="col-md-2 control-label">Department:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control input-sm mandatory" id="txt_Dept_name" runat="server" placeholder='Please Enter 3 Characters' onkeyup="bindDepartment()" />
                                        </div>
                                        <label class="col-md-2 control-label">Date of Birth:</label>
                                        <div class="col-md-2">
                                            <div class='input-group'>
                                                <input class='form-control input-sm datepicker-rtl' type='text' id='txt_dob' readonly />
                                                <span class='input-group-addon'><i class='fa fa-calendar text-info'></i></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Date of Joining:</label>
                                        <div class="col-md-2">
                                            <div class='input-group'>
                                                <input class='form-control input-sm datepicker-rtl' type='text' id='txt_dojin' readonly />
                                                <span class='input-group-addon'><i class='fa fa-calendar text-info'></i></span>
                                            </div>
                                        </div>
                                        <label class="col-md-2 control-label">Date of Last Working Day:</label>
                                        <div class="col-md-2">
                                            <div class='input-group'>
                                                <input class='form-control input-sm datepicker-rtl' type='text' id='txt_lwd' readonly />
                                                <span class='input-group-addon'><i class='fa fa-calendar text-info'></i></span>
                                            </div>
                                        </div>
                                        <label class="col-md-2 control-label">Cost Centre:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control input-sm mandatory" id="txt_Costcenter" runat="server" placeholder='Enter Select Center' />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">IFSC Code:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control input-sm mandatory" id="txt_ifsccode" runat="server" placeholder='Enter IFSC Code' />
                                        </div>
                                        <label class="col-md-2 control-label">Grade:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control input-sm mandatory" id="txt_grade" runat="server" placeholder='Please Enter 3 Characters' onkeyup="bindGrade()" />
                                        </div>
                                        <label class="col-md-2 control-label">Admin Reporting:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control input-sm mandatory" id="txt_Manager" runat="server" placeholder='Please Enter 3 Characters' onkeyup="bindUser()" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">HR Admin:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control input-sm mandatory" id="txt_HR" runat="server" placeholder='Please Enter 3 Characters' onkeyup="bindHR()" />
                                        </div>
                                        <label class="col-md-2 control-label">Company Name:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control input-sm mandatory" id="txt_Company" runat="server" placeholder='Please Enter 3 Characters' onkeyup="bindCompany()" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-md-8 text-center">
                                <button id="btn_Save" name="btn_Show" type="button" class="fcbtn btn btn-info btn-outline btn-1b">Save</button>
                                <button id="btn_Upd" style="display: none;" name="btn_Show" type="button" class="fcbtn btn btn-info btn-outline btn-1b">Update</button>
                                <button id="btn_Cancel" name="btn_Show" type="button" class="fcbtn btn btn-info btn-outline btn-1b">Cancel</button>
                                <button id="btn_Show" name="btn_Show" type="button" class="fcbtn btn btn-info btn-outline btn-1b">Show</button>
                            </div>
                            <div class="form-group">
                                <label class="col-md-1 control-label"><b>Search:</b></label>
                                <div class="col-md-3">
                                    <input id="txt_Subject" type="text" class="form-control input-sm" onkeyup="GetEmpData()" placeholder='Enter Employee Name or Code' />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id="Div_emp" class="panel panel-default panel-border-top" runat="server">
                        <div class="panel-heading">
                            <i class="fa fa-2x fa-dribbble"></i>Employee DETAILS                      
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <div id="div_Employee" style="height: 250px; overflow-y: scroll">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="display: none">
                <input type="text" id="txt_PK_EMP_ID" runat="server" />
                <input type="text" id="txt_Master_URL" runat="server" />
                <input type="text" id="txt_Client_ID" runat="server" />
                <input type="text" id="txt_Username" runat="server" />
            </div>
        </section>
    </form>
    <!-- ================== BEGIN BASE JS ================== -->
    <script lang="JavaScript" type="text/javascript" src="../../assets/js/jquery.min.js"></script>
    <script lang="JavaScript" type="text/javascript" src="../../assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    <script lang="JavaScript" type="text/javascript" src="../../assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script lang="JavaScript" type="text/javascript" src="../../assets/js/jquery-ui.js"></script>
    <script lang="JavaScript" type="text/javascript" src="../../assets/js/jAlert.js"></script>
    <script lang="JavaScript" type="text/javascript" src="../../assets/js/jAlert-functions.js"></script>
    <script src="../../JS/Common/document-attach.js"></script>
    <script src="../../JS/Common/Utility.js"></script>
    <script src="../../JS/Master/EmployeeMaster.js"></script>
    <script src="../../assets/js/underscore.js"></script>
    <script src="../../JS/Common/Vaildation.js"></script>
    <!--export JS -->
    <script src="../../assets/js/Export/dataTables.buttons.min.js"></script>
    <script src="../../assets/js/Export/buttons.flash.min.js"></script>
    <script src="../../assets/js/Export/jszip.min.js"></script>
    <script src="../../assets/js/Export/buttons.html5.min.js"></script>
    <!--export JS -->

</body>
</html>

