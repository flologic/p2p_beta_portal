﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Text;
using System.Net.Http;

namespace P2P_Portal.Masters.Other_Masters
{
    public partial class ApprovalMatrix : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txt_Master_URL.Value = System.Configuration.ConfigurationManager.AppSettings["MasterAPI"];
            txt_Client_ID.Value = System.Configuration.ConfigurationManager.AppSettings["Client_ID"];
            txt_Username.Value = Session["User_ADID"].ToString();
        }

        [WebMethod]
        public static string[] GetApproverData(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "Approvar";
                    //string apiUrl = "http://localhost:59313/api/Approvar";
                    var input = new
                    {
                        FK_CLIENTID = Client_ID,

                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/getApprovarMatrixInfo", inputContent).Result;
                    jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
                }
                Logger1.Activity("Approvar Data Return In P2P");
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return jsonArray;

        }


        [WebMethod]
        public static string[] GetUser(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "Employee";
                    //string apiUrl = "http://localhost:59313/api/Employee";
                    var input = new
                    {
                        AD_ID = "",
                        label = ""
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/getEmpInfo", inputContent).Result;
                    jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
                }

                Logger1.Activity("Emp Data Return In P2P");
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return jsonArray;
        }


        [WebMethod]
        public static string[] GetCatType(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "ItemCategory";
                    //string apiUrl = "http://localhost:59313/api/ItemCategory";// +"Area";
                    var input = new
                    {
                        FK_CLIENT_ID = Client_ID,
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/getItemCategoryInfo", inputContent).Result;
                    jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
                }
                Logger1.Activity("Category Data Return In P2P");
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return jsonArray;
        }


        [WebMethod]
        public static string SaveData(string ClientID, string Master_API, string CattypeID, string ApprovarId, string seq_no, string Created_By)
        {
            string Result = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "Approvar";
                    //string apiUrl = "http://localhost:59313/api/Approvar";
                    var input = new
                    {
                        FK_CLIENTID = ClientID,
                        FK_CAT_ID = CattypeID,
                        APPROVARID = ApprovarId,
                        Created_by = Created_By,
                        LINENUMBER = seq_no
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/InsertApprovarInfo", inputContent).Result;
                    Result = response.Content.ReadAsStringAsync().Result;


                    Logger1.Activity("Approvar Data " + Result + " In P2P");
                }
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return Result;
        }

        [WebMethod]
        public static string UpdateData(string ClientID, string Master_API, string PK_ID, string CattypeID, string ApprovarId, int seq_no, string Updated_By)
        {
            string Result = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "Approvar";
                    //string apiUrl = "http://localhost:59313/api/Approvar";
                    var input = new
                    {
                        PK_ID = PK_ID,
                        FK_CLIENTID = ClientID,
                        FK_CAT_ID = CattypeID,
                        APPROVARID = ApprovarId,
                        LINENUMBER = seq_no,
                        Created_by = Updated_By
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/UpdateApproverInfo", inputContent).Result;
                    Result = response.Content.ReadAsStringAsync().Result;
                    Logger1.Activity("Approvar Data " + Result + " In P2P");
                }
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return Result;
        }

        [WebMethod]
        public static string Deletedata(string ClientID, string Master_API, string PK_ID, string Updated_By)
        {
            string Result = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "Branch";
                    var input = new
                    {
                        PK_ID = PK_ID,
                        FK_CLIENTID = ClientID,
                        Created_by = Updated_By
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/DeleteApproverInfo", inputContent).Result;
                    Result = response.Content.ReadAsStringAsync().Result;
                    Logger1.Activity("Approvar Data " + Result + " In P2P");
                }
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return Result;
        }

    }
}