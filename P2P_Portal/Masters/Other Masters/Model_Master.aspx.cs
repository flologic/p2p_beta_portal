﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Text;
using System.Net.Http;

namespace P2P_Portal.Masters.Other_Masters
{
    public partial class Model_Master : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txt_Master_URL.Value = System.Configuration.ConfigurationManager.AppSettings["MasterAPI"];
            txt_Client_ID.Value = System.Configuration.ConfigurationManager.AppSettings["Client_ID"];
            txt_UserName.Value = ((string)Session["User_ADID"]);
        }

        [WebMethod]
        public static string[] GetModelData(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Model";
                var input = new
                {
                    FK_CLIENT_ID = Client_ID,
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getModelInfo", inputContent).Result;
                jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string[] GetItemCatData(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "ItemCategory";
                var input = new
                {
                    FK_CLIENT_ID = Client_ID,
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getItemCategoryInfo", inputContent).Result;
                jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string[] GetItemSubCatData(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "ItemSubCategory";
                var input = new
                {
                    FK_CLIENT_ID = Client_ID,
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getItemSubCategoryInfo", inputContent).Result;
                jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string SaveData(string ItemCatId, string ItemSubId, string ModelName, string Desc, string EmpName, string Client_ID, string Master_API)
        {
            string Result = string.Empty;
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Model";
                var input = new
                {
                    FK_ITMCATID = Convert.ToInt32(ItemCatId),
                    FK_ITMSUBCATID = Convert.ToInt32(ItemSubId),
                    label = ModelName,
                    DESCRIPTION=Desc,
                    EMP_NAME = EmpName,
                    FK_CLIENT_ID = Client_ID
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/InsertModelInfo", inputContent).Result;
                Result = JsonConvert.SerializeObject(response.ReasonPhrase);
            }
            return Result;
        }

        [WebMethod]
        public static string UpdateData(string Id, string ItemCatId, string ItemSubId, string ModelName, string Desc, string Client_ID, string Master_API)
        {
            string Result = string.Empty;
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Model";
                var input = new
                {
                    id = Id,
                    FK_ITMCATID = Convert.ToInt32(ItemCatId),
                    FK_ITMSUBCATID = Convert.ToInt32(ItemSubId),
                    label = ModelName,
                    DESCRIPTION = Desc,
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/UpdateModelInfo", inputContent).Result;
                Result = JsonConvert.SerializeObject(response.ReasonPhrase);
            }
            return Result;
        }

        [WebMethod]
        public static string Deletedata(string Id, string Client_ID, string Master_API)
        {
            string Result = string.Empty;
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Model";
                var input = new
                {
                    id = Id
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/DeleteModelInfo", inputContent).Result;
                Result = JsonConvert.SerializeObject(response.ReasonPhrase);
            }
            return Result;
        }

    }
}