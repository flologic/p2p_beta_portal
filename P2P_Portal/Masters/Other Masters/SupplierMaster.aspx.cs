﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace P2P_Portal.Masters.Other_Masters
{
    public partial class SupplierMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txt_Master_URL.Value = System.Configuration.ConfigurationManager.AppSettings["MasterAPI"];
            txt_Client_ID.Value = System.Configuration.ConfigurationManager.AppSettings["Client_ID"];
            txt_Username.Value = Session["User_ADID"].ToString();
        }

        [WebMethod]
        public static string[] GetSuppHDR(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Supplier";
                var input = new
                {
                    FK_ClientID = "",
                    label = ""
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getSupplierInfo", inputContent).Result;
                jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string SaveData(string ClientID, string Master_API, string Supplier, string Code, string Type, string ShortName, string Created_By)
        {
            string Result = string.Empty;
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Supplier";
                var input = new
                {
                    Client_ID = ClientID,
                    label = Supplier,
                    Supp_Code = Code,
                    Supp_Type = Type,
                    Short_Name = ShortName,
                    Created_By = Created_By
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/InsertSupplierInfo", inputContent).Result;
                Result = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            return Result;
        }

        [WebMethod]
        public static string UpdateData(string ClientID, string Master_API, string PK_SuppID, string Supplier, string Code, string Type, string ShortName, string Updated_By)
        {
            string Result = string.Empty;
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Supplier";
                var input = new
                {
                    Client_ID = ClientID,
                    id = PK_SuppID,
                    label = Supplier,
                    Supp_Code = Code,
                    Supp_Type = Type,
                    Short_Name = ShortName,
                    Created_By = Updated_By
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/UpdateSupplierInfo", inputContent).Result;
                Result = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            return Result;
        }

        [WebMethod]
        public static string Deletedata(string ClientID, string Master_API, string PK_AreaID, string Updated_By)
        {
            string Result = string.Empty;
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Supplier";
                var input = new
                {
                    id = PK_AreaID,
                    Client_ID = ClientID,
                    Created_By = Updated_By
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/DeleteSupplierInfo", inputContent).Result;
                Result = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            return Result;
        }
    }
}