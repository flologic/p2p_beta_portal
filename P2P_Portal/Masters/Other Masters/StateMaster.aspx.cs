﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Text;
using System.Net.Http;
using log4net;

namespace P2P_Portal.Masters.Other_Masters
{
    public partial class StateMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txt_Master_URL.Value = System.Configuration.ConfigurationManager.AppSettings["MasterAPI"];
            txt_Client_ID.Value = System.Configuration.ConfigurationManager.AppSettings["Client_ID"];
            txt_Username.Value = Session["User_ADID"].ToString();
        }

        [WebMethod]
        public static string[] GetStateData(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "State";
                    //string apiUrl = "http://localhost:59313/api/State";
                    var input = new
                    {
                        FK_CLIENTID = Client_ID,
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/getStateInfo", inputContent).Result;
                    jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
                }
                Logger1.Activity("State Data return in P2P");
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string[] GetUser(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "Employee";
                    var input = new
                    {
                        AD_ID = "",
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/getEmpInfo", inputContent).Result;
                    jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
                }
                Logger1.Activity("Employee Data Return In P2P");
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string SaveData(string ClientID, string Master_API, string statename, string Created_By, string stateadminid)
        {
            string Result = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "State";
                    //string apiUrl = "http://localhost:59313/api/State";
                    var input = new
                    {
                        FK_CLIENTID = ClientID,
                        label = statename,
                        EMP_NAME = Created_By,
                        STATE_ADMINID = stateadminid
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/InsertStateInfo", inputContent).Result;
                    Result = response.Content.ReadAsStringAsync().Result;                    

                    Logger1.Activity("State Data " + Result + " P2P");
                }
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return Result;
        }

        [WebMethod]
        public static string UpdateData(string ClientID, string Master_API, string PK_STATEID, string statename, string Updated_By, string stateadminid)
        {
            string Result = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "State";
                    //string apiUrl = "http://localhost:59313/api/State";
                    var input = new
                    {
                        id = PK_STATEID,
                        FK_CLIENTID = ClientID,
                        label = statename,
                        STATE_ADMINID = stateadminid,
                        EMP_NAME = Updated_By
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/UpdateStateInfo", inputContent).Result;
                    Result = response.Content.ReadAsStringAsync().Result;

                    Logger1.Activity("State Data " + Result + " P2P");
                }
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return Result;
        }

        [WebMethod]
        public static string Deletedata(string ClientID, string Master_API, string PK_STATEID, string Updated_By)
        {
            string Result = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "State";
                    //string apiUrl = "http://localhost:59313/api/State";
                    var input = new
                    {
                        id = PK_STATEID,
                        FK_CLIENTID = ClientID,
                        EMP_NAME = Updated_By
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/DeleteStateInfo", inputContent).Result;
                    Result = response.Content.ReadAsStringAsync().Result;

                    Logger1.Activity("State Data " + Result + " P2P");
                }
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return Result;
        }
    }
}