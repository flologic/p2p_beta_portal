﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TemplateMaster.aspx.cs" Inherits="P2P_Portal.Masters.Other_Masters.TemplateMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Template Master</title>
    <link href="../../assets/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../assets/plugins/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet" />
    <link href="../../assets/css/style.css" rel="stylesheet" />
    <link href="../../assets/css/jquery-ui.css" rel="stylesheet" />
    <link href="../../assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="../../assets/css/jAlert.css" rel="stylesheet" />
    <link href="../../assets/js/Export/buttons.dataTables.min.css" rel="stylesheet" />
    <style>
        td.details-control {
            background: url('../assets/images/details_open.png') no-repeat center center;
            cursor: pointer;
        }

        tr.shown td.details-control {
            background: url('../assets/images/details_close.png') no-repeat center center;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ToolkitScriptManager>
        <div class="row bg-title">
            <div class="col-md-12">
                <div id="Div1" class="panel panel-default panel-border-top" runat="server">
                    <div class="panel-heading">
                        Template Master
                    </div>
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Item Template
                                        </div>
                                        <div class="panel-wrapper collapse in" aria-expanded="true">
                                            <div class="panel-body" style="height: 450px; overflow-y: scroll">
                                                <div class="form-horizontal">
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Category Name</label>
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control input-sm mandatory" id="sel_Category" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Sub-Category Name</label>
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control input-sm mandatory" id="sel_SubCategoryName" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Item Name</label>
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control input-sm mandatory" id="sel_ItemName" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Template Name</label>
                                                        <div class="col-md-7">
                                                            <input class="form-control input-sm" id="txt_TemplateName" type="text" maxlength="200" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Template Description</label>
                                                        <div class="col-md-7">
                                                            <%--<input class="form-control input-sm" id="txt_TemplateDescription" type="text" />--%>
                                                            <textarea class="form-control input-sm" id="txt_TemplateDescription" cols="9" rows="3" maxlength="1000" readonly="readonly"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-t-10">
                                                        <div class="col-md-4"></div>
                                                        <div id="Div2" class="col-md-6" runat="server">
                                                            <button type="button" class="fcbtn btn btn-info btn-outline btn-1b" id="btn_generate"><b>Generate</b></button>
                                                            <button type="button" class="fcbtn btn btn-info btn-outline btn-1b" id="btn_Save"><b>Save</b></button>
                                                            <button type="button" class="fcbtn btn btn-info btn-outline btn-1b hidden" id="btn_Update"><b>Update</b></button>
                                                            <button type="button" class="fcbtn btn btn-info btn-outline btn-1b" id="btn_Clear"><b>Cancel</b></button>

                                                        </div>
                                                    </div>
                                                    <div class="form-group m-b-10">
                                                        <div class="col-md-1"></div>
                                                        <div class="col-md-10">
                                                            <div class="table-responsive">
                                                                <div id="div_templateDetails" runat="server">
                                                                    <div class="table-responsive" style="overflow-y: scroll; height: 210px" id="div_parts">
                                                                        <table class="table color-bordered-table info-bordered-table" id="tbl_parts">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th style="width: 3%">#</th>
                                                                                    <th style="width: 3%">Select</th>
                                                                                    <th>Details</th>
                                                                                    <th>Description</th>
                                                                                </tr>
                                                                            </thead>

                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Item Template Details
                                        </div>
                                        <div class="panel-wrapper collapse in" aria-expanded="true">
                                            <div class="panel-body" style="height: 450px; overflow-y: scroll">
                                                <div id="div_ItemTemplate" runat="server">
                                                    <table class="table color-bordered-table info-bordered-table" id="tbl_alltemplatedtl">
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div style="display: none">
            <input type="text" id="txt_PK_PArtID" runat="server" />
            <input type="text" id="txt_Master_URL" runat="server" />
            <input type="text" id="txt_Client_ID" runat="server" />
            <input type="text" id="pkID" runat="server" />
        </div>
        <!-- ================== BEGIN BASE JS ================== -->
        <script lang="JavaScript" type="text/javascript" src="../../assets/js/jquery.min.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/js/jquery-ui.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/js/jAlert.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/js/jAlert-functions.js"></script>
        <script src="../../assets/js/underscore.js"></script>
        <script src="../../JS/Common/Utility.js"></script>
        <script src="../../JS/Master/Template.js"></script>
        <!--export JS -->
        <script src="../../assets/js/Export/dataTables.buttons.min.js"></script>
        <script src="../../assets/js/Export/buttons.flash.min.js"></script>
        <script src="../../assets/js/Export/jszip.min.js"></script>
        <script src="../../assets/js/Export/buttons.html5.min.js"></script>
        <script src="../../assets/js/Export/jszip.min.js"></script>
        <script src="../../assets/js/Export/pdfmake.min.js"></script>
        <script src="../../assets/js/Export/vfs_fonts.js"></script>
        <script src="../../assets/js/Export/buttons.colVis.min.js"></script>
        <!--export JS -->
    </form>
</body>
</html>
