﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ApprovalMatrix.aspx.cs" Inherits="P2P_Portal.Masters.Other_Masters.ApprovalMatrix" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">

    <title>Approval Matrix Master</title>
    <link href="../../assets/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../assets/plugins/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet" />
    <link href="../../assets/css/style.css" rel="stylesheet" />
    <link href="../../assets/css/jquery-ui.css" rel="stylesheet" />
    <link href="../../assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="../../assets/css/jAlert.css" rel="stylesheet" />
    <link href="../../assets/js/Export/buttons.dataTables.min.css" rel="stylesheet" />

</head>
<body>
    <form id="form1" runat="server">
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ToolkitScriptManager>
        <div class="row bg-title">
            <div class="col-md-12">
                <div id="Div1" class="panel panel-default panel-border-top" runat="server">
                    <div class="panel-heading">
                        Approval Matrix Master
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label for="zname" class="col-md-2 control-label">Category Type:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control input-sm mandatory" id="Sel_Cat_type" runat="server" placeholder="select Category Type" />
                                        </div>

                                        <label class="col-md-2 control-label">Approvar:</label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control input-sm mandatory" id="sel_Approvar" runat="server" placeholder="Select Approvar" />
                                        </div>

                                        <label for="zname" class="col-md-2 control-label">Sequence Number:</label>
                                        <div class="col-md-2">
                                            <select id="ddlAction" class="form-control input-sm mandatory">
                                                <option>---Select Sequence---</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                                <option>6</option>
                                                <option>7</option>
                                                <option>8</option>
                                                <option>9</option>
                                                <option>10</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button id="btn_Save" name="btn_Show" type="button" class="fcbtn btn btn-info btn-outline btn-1b">Save</button>
                                <button id="btn_Upd" style="display: none;" name="btn_Show" type="button" class="fcbtn btn btn-info btn-outline btn-1b">Update</button>
                                <button id="btn_Cancel" name="btn_Show" type="button" class="fcbtn btn btn-info btn-outline btn-1b">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div id="Div4" class="panel panel-default panel-border-top" runat="server">
                    <div class="panel-heading">
                        Approvar Matrix Details                               
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <div id="div_displayDtl">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="display: none">
            <input type="text" id="txt_PK_APprovar_ID" runat="server" />
            <input type="text" id="txt_Master_URL" runat="server" />
            <input type="text" id="txt_Client_ID" runat="server" />
            <input type="text" id="txt_Username" runat="server" />
        </div>
        <!-- ================== BEGIN BASE JS ================== -->
        <script lang="JavaScript" type="text/javascript" src="../../assets/js/jquery.min.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/js/jquery-ui.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/js/jAlert.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/js/jAlert-functions.js"></script>
        <script src="../../JS/Common/document-attach.js"></script>
        <script src="../../JS/Common/Utility.js"></script>
        <script src="../../JS/Master/ApprovalMatrix.js"></script>
        <!--export JS -->
        <script src="../../assets/js/Export/dataTables.buttons.min.js"></script>
        <script src="../../assets/js/Export/buttons.flash.min.js"></script>
        <script src="../../assets/js/Export/jszip.min.js"></script>
        <script src="../../assets/js/Export/buttons.html5.min.js"></script>
        <!--export JS -->
    </form>
</body>
</html>

