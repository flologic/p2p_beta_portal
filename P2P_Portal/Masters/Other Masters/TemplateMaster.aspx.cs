﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace P2P_Portal.Masters.Other_Masters
{
    public partial class TemplateMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Session["User_ADID"] == null)
            //    Response.Write("<script language='javascript'>self.parent.location='../Login.aspx';</script>");
            //else
            //{
            //  txt_UserID.Text = Session["User_ADID"].ToString();
            txt_Master_URL.Value = System.Configuration.ConfigurationManager.AppSettings["MasterAPI"];
            txt_Client_ID.Value = System.Configuration.ConfigurationManager.AppSettings["Client_ID"];
            //  }
        }

        [WebMethod]
        public static string[] getCatData(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "ItemCategory";
                    var input = new
                    {
                        FK_CLIENT_ID = Client_ID
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/getItemCategoryInfo", inputContent).Result;
                    jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
                }
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string[] getSubCatData(string Client_ID, string Master_API, string catid)
        {
            string[] jsonArray = new string[1];
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "ItemSubCategory";
                    var input = new
                    {
                        FK_CLIENT_ID = Client_ID,
                        FK_ITMCATID = catid
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/getItemSubCategoryInfo", inputContent).Result;
                    jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
                }
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string[] getallitm(string Client_ID, string Master_API, string catid, string subcatid)
        {
            string[] jsonArray = new string[1];
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "Item";
                    var input = new
                    {
                        FK_CLIENT_ID = Client_ID,
                        FK_ITMCATID = catid,
                        FK_ITMSUBCATID = subcatid
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/getItemInfo", inputContent).Result;
                    jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
                }
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string[] getPartDetails(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "Partwise_mapping";
                    var input = new
                    {
                        FK_CLIENTID = Client_ID
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/getPartwise_mappingInfo", inputContent).Result;
                    jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
                }
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string[] getMakeDetail(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "Make";
                    var input = new
                    {
                        FK_CLIENT_ID = Client_ID
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/getmakeInfo", inputContent).Result;
                    jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
                }
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string[] getAllModelData(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "Model";
                    var input = new
                    {
                        FK_CLIENT_ID = Client_ID
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/getModelInfo", inputContent).Result;
                    jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
                }
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string[] gettemplatedtlData(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "Template";
                    var input = new
                    {
                        FK_TMPLHDRID = 0
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/getTempDtlInfo", inputContent).Result;
                    jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
                }
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return jsonArray;
        }


        [WebMethod]
        public static string[] gettemplateData(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "Template";
                    var input = new
                    {
                        FK_CLIENTID = Client_ID
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/getTempHdrInfo", inputContent).Result;
                    jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
                }
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return jsonArray;
        }
        [WebMethod]
        public static string saveData(string Tmplmaping, string Master_API, string saveType)
        {
            string Result = string.Empty;

            try
            {
                using (var client = new HttpClient())
                {
                    if (saveType == "SAVE")
                    {
                        string apiUrl = Master_API + "Template";
                        var input = new
                        {
                            transactionData = Tmplmaping

                        };
                        string inputJson = (new JavaScriptSerializer()).Serialize(input);
                        HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                        HttpResponseMessage response = client.PostAsync(apiUrl + "/InsertTempInfo", inputContent).Result;
                        Result = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);


                    }
                    else if (saveType == "EDIT")
                    {
                        string apiUrl = Master_API + "Template";
                        var input = new
                        {
                            transactionData = Tmplmaping

                        };
                        string inputJson = (new JavaScriptSerializer()).Serialize(input);
                        HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                        HttpResponseMessage response = client.PostAsync(apiUrl + "/UpdateTempInfo", inputContent).Result;
                        Result = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);

                    }
                    else if (saveType == "DELETE")
                    {
                        string apiUrl = Master_API + "Template";
                        var input = new
                        {
                            PK_TMPLHDRID = Tmplmaping

                        };
                        string inputJson = (new JavaScriptSerializer()).Serialize(input);
                        HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                        HttpResponseMessage response = client.PostAsync(apiUrl + "/DeleteTempInfo", inputContent).Result;
                        Result = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);

                    }
                }


            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return Result;
        }




    }
}