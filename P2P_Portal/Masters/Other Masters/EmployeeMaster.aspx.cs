﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace P2P_Portal.Masters.Other_Masters
{
    public partial class EmployeeMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txt_Master_URL.Value = System.Configuration.ConfigurationManager.AppSettings["MasterAPI"];
            txt_Client_ID.Value = System.Configuration.ConfigurationManager.AppSettings["Client_ID"];
            txt_Username.Value = Session["User_ADID"].ToString();
        }


        [WebMethod]
        public static string[] GetEmployeeData(string Client_ID, string Master_API, string EmpNameADID)
        {
            string[] jsonArray = new string[1];
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Employee";
                var input = new
                {
                    AD_ID = "",
                    label = EmpNameADID
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getEmpInfo", inputContent).Result;
                jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string[] GetDesignation(string Client_ID, string Master_API, string Desig)
        {
            string[] jsonArray = new string[1];
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Designation";
                var input = new
                {
                    FK_CLIENT_ID = Client_ID,
                    label = Desig
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getDesignationInfo", inputContent).Result;
                jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string[] GetBranch(int Client_ID, string Master_API, string GetBranch)
        {
            string[] jsonArray = new string[1];
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Branch";
                var input = new
                {
                    FK_CLIENTID = Client_ID,
                    label = GetBranch
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getBranchInfo", inputContent).Result;
                jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string[] GetDepartment(string Client_ID, string Master_API, string Department)
        {
            string[] jsonArray = new string[1];
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Department";
                var input = new
                {
                    FK_CLIENT_ID = Client_ID,
                    label = Department
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getDepartmentInfo", inputContent).Result;
                jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string[] GetCostCenter(int Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "CostCenter";
                var input = new
                {
                    FK_CLIENTID = Client_ID,
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getCostCenterInfo", inputContent).Result;
                jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string[] GetGrade(string Client_ID, string Master_API, string Grade)
        {
            string[] jsonArray = new string[1];
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Grade";
                var input = new
                {
                    FK_CLIENT_ID = Client_ID,
                    label = Grade
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/geGradeInfo", inputContent).Result;
                jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string[] GetUser(string Client_ID, string Master_API, string UserID)
        {
            string[] jsonArray = new string[1];
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Employee";
                var input = new
                {
                    AD_ID = "",
                    label = UserID
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getEmpInfo", inputContent).Result;
                jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string[] GetCompany(string Client_ID, string Master_API, string CompID)
        {
            string[] jsonArray = new string[1];
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Company";
                var input = new
                {
                    FK_CLIENTID = Client_ID,
                    label = CompID
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getCompanyInfo", inputContent).Result;
                jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string submitData(string ClientID, string Master_API, string EmpName, string Empcode, string MobNo, string Addrss, string emailId, string DesigID, string BranchID, string DepartmentID, string dtofbirth, string dtofJoin, string dtofLWD, string Costcenterid, string IfscCode, string GradeID, string Manager, string HR, string CompanyID, string Creator, string Action)
        {
            string Result = string.Empty;
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Employee";
                var input = new
                {
                    Client_ID = ClientID,
                    EMP_NAME = EmpName,
                    EMP_CODE = Empcode,
                    MOBILE_NO = MobNo,
                    ADDRESS = Addrss,
                    EMAILID = emailId,
                    FK_DESIGNATION_ID = DesigID,
                    FK_BRANCH_ID = BranchID,
                    FK_DEPARTMENT_ID = DepartmentID,
                    DATE_BIRTH = dtofbirth,
                    DATE_JOINING = dtofJoin,
                    LAST_WORKING_DATE = dtofLWD,
                    FK_COST_CENTRE_ID = Costcenterid,
                    IFSCODE = IfscCode,
                    FK_GRADE_ID = GradeID,
                    ADMINREPORTING_TO = Manager,
                    CREATED_BY = Creator,
                    HR_NAME = HR,
                    FK_COMPANY_ID = CompanyID
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                if (Action == "Save")
                {
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/InsertEmpInfo", inputContent).Result;
                    Result = JsonConvert.SerializeObject(response.ReasonPhrase);
                }
                else if (Action == "Update")
                {
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/UpdateEmpInfo", inputContent).Result;
                    Result = JsonConvert.SerializeObject(response.ReasonPhrase);
                }
            }
            return Result;
        }

        [WebMethod]
        public static string Deletedata(string ClientID, string Master_API, string ADID, string Updated_By)
        {
            string Result = string.Empty;
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Employee";
                var input = new
                {
                    AD_ID = ADID,
                    Client_ID = ClientID,
                    EMP_NAME = Updated_By
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/DeleteEmpInfo", inputContent).Result;
                Result = JsonConvert.SerializeObject(response.ReasonPhrase);
            }
            return Result;
        }
    }
}