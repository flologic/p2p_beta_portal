﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace P2P_Portal.Masters.Other_Masters
{
    public partial class AreaMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txt_Master_URL.Value = System.Configuration.ConfigurationManager.AppSettings["MasterAPI"];
            txt_Client_ID.Value = System.Configuration.ConfigurationManager.AppSettings["Client_ID"];
            txt_Username.Value = Session["User_ADID"].ToString();
        }

        [WebMethod]
        public static string[] GetAreaData(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Area";
                var input = new
                {
                    id = 0,
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getAreaInfo", inputContent).Result;
                jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string[] GetUser(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Employee";
                var input = new
                {
                    AD_ID = "",
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getEmpInfo", inputContent).Result;
                jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string[] GetRegion(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Region";
                var input = new
                {
                    FK_CLIENT_ID = "",
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getRegionInfo", inputContent).Result;
                jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string SaveData(string ClientID, string Master_API,int regionID, string area, string area_headid, string Created_By)
        {
            string Result = string.Empty;
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Area";
                var input = new
                {
                    Client_ID = ClientID,
                    FK_REGIONID = regionID,
                    label = area,
                    AREA_HEADID = area_headid,
                    EMP_NAME = Created_By
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/InsertAreaInfo", inputContent).Result;
                Result = JsonConvert.SerializeObject(response.ReasonPhrase);
            }
            return Result;
        }

        [WebMethod]
        public static string UpdateData(string ClientID, string Master_API, string PK_AreaID, string regionID, string area, string area_headid, string Updated_By)
        {
            string Result = string.Empty;
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Area";
                var input = new
                {
                    id = PK_AreaID,
                    Client_ID = ClientID,
                    FK_REGIONID = regionID,
                    label = area,
                    AREA_HEADID = area_headid,
                    EMP_NAME = Updated_By
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/UpdateAreaInfo", inputContent).Result;
                Result = JsonConvert.SerializeObject(response.ReasonPhrase);
            }
            return Result;
        }

        [WebMethod]
        public static string Deletedata(string ClientID, string Master_API, string PK_AreaID, string Updated_By)
        {
            string Result = string.Empty;
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Area";
                var input = new
                {
                    id = PK_AreaID,
                    Client_ID = ClientID,
                    EMP_NAME = Updated_By
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/DeleteAreaInfo", inputContent).Result;
                Result = JsonConvert.SerializeObject(response.ReasonPhrase);
            }
            return Result;
        }
    }
}