﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Text;
using System.Net.Http;

namespace P2P_Portal.Masters.Other_Masters
{
    public partial class Location_Master : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txt_Master_URL.Value = System.Configuration.ConfigurationManager.AppSettings["MasterAPI"];
            txt_Client_ID.Value = System.Configuration.ConfigurationManager.AppSettings["Client_ID"];
            txt_UserName.Value = ((string)Session["User_ADID"]);
        }

        [WebMethod]
        public static string[] GetLocationData(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Location";
                var input = new
                {
                    FK_CLIENT_ID = Client_ID,
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getLocationInfo", inputContent).Result;
                jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string[] GetCityData(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "City";
                var input = new
                {
                    FK_CLIENT_ID = Client_ID,
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getCityInfo", inputContent).Result;
                jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string SaveData(string CityId, string Location, string LocCode, string EmpName, string Client_ID, string Master_API)
        {
            string Result = string.Empty;
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Location";
                var input = new
                {
                    FK_CITYID = Convert.ToInt32(CityId),
                    label = Location,
                    LocationCode = LocCode,
                    EMP_NAME = EmpName,
                    FK_CLIENT_ID = Client_ID
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/InsertLocationInfo", inputContent).Result;
                Result = JsonConvert.SerializeObject(response.ReasonPhrase);
            }
            return Result;
        }

        [WebMethod]
        public static string UpdateData(string Id, string CityId, string Location, string LocCode, string Client_ID, string Master_API)
        {
            string Result = string.Empty;
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Location";
                var input = new
                {
                    id = Id,
                    FK_CITYID = Convert.ToInt32(CityId),
                    label = Location,
                    LocationCode = LocCode,
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/UpdateLocationInfo", inputContent).Result;
                Result = JsonConvert.SerializeObject(response.ReasonPhrase);
            }
            return Result;
        }

        [WebMethod]
        public static string Deletedata(string Id, string Client_ID, string Master_API)
        {
            string Result = string.Empty;
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Location";
                var input = new
                {
                    id = Id
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/DeleteLocationInfo", inputContent).Result;
                Result = JsonConvert.SerializeObject(response.ReasonPhrase);
            }
            return Result;
        }
    }
}