﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Text;
using System.Net.Http;

namespace P2P_Portal.Masters.Other_Masters
{
    public partial class ItemType_Master : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txt_Master_URL.Value = System.Configuration.ConfigurationManager.AppSettings["MasterAPI"];
            txt_Client_ID.Value = System.Configuration.ConfigurationManager.AppSettings["Client_ID"];
            txt_UserName.Value = ((string)Session["User_ADID"]);
        }

        [WebMethod]
        public static string[] GetItemType(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "ItemType";
                var input = new
                {
                    FK_CLIENT_ID = Client_ID,
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getItemTypeInfo", inputContent).Result;
                jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string SaveData(string ItemType, string EmpName, string Client_ID, string Master_API)
        {
            string Result = string.Empty;
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "ItemType";
                var input = new
                {
                    label = ItemType,
                    EMP_NAME = EmpName,
                    FK_CLIENT_ID = Client_ID
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/InsertItemTypeInfo", inputContent).Result;
                Result = JsonConvert.SerializeObject(response.ReasonPhrase);
            }
            return Result;
        }

        [WebMethod]
        public static string UpdateData(string Id, string ItemType, string Client_ID, string Master_API)
        {
            string Result = string.Empty;
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "ItemType";
                var input = new
                {
                    id = Id,
                    label = ItemType
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/UpdateItemTypeInfo", inputContent).Result;
                Result = JsonConvert.SerializeObject(response.ReasonPhrase);
            }
            return Result;
        }

        [WebMethod]
        public static string Deletedata(string Id, string Client_ID, string Master_API)
        {
            string Result = string.Empty;
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "ItemType";
                var input = new
                {
                    id = Id
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/DeleteItemTypeInfo", inputContent).Result;
                Result = JsonConvert.SerializeObject(response.ReasonPhrase);
            }
            return Result;
        }
    }
}