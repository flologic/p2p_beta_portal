﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Text;
using System.Net.Http;
using log4net;
namespace P2P_Portal.Masters.Other_Masters
{
    public partial class TaxType : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txt_Master_URL.Value = System.Configuration.ConfigurationManager.AppSettings["MasterAPI"];
            txt_Client_ID.Value = System.Configuration.ConfigurationManager.AppSettings["Client_ID"];
            txt_Username.Value = Session["User_ADID"].ToString();
        }

        public class Portal_TaxType
        {
            public int id { get; set; }
            public string label { get; set; }
            public string EMP_NAME { get; set; }
            public string FK_CLIENT_ID { get; set; }
        }

        [WebMethod]
        public static string[] GetTaxTypeData(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
             try
            {
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "TaxType";
                var input = new
                {
                    FK_CLIENT_ID = Client_ID,
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getTaxTypeInfo", inputContent).Result;
                jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            Logger1.Activity("Tax Type Data Return In P2P");
            }
             catch (Exception ex)
             {
                 Logger1.Error(ex);
             }
            return jsonArray;
        }

       
        [WebMethod]
        public static string SaveData(string ClientID, string Master_API, string taxtypename, string Created_By)
        {
            string Result = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "TaxType";
                    var input = new
                    {
                        FK_CLIENT_ID = ClientID,
                        label = taxtypename,
                        EMP_NAME = Created_By
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/InsertTaxTypeInfo", inputContent).Result;
                    Result = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);

                    Logger1.Activity(" Tax Type data " + Result + " In P2P");
                }
            }
            catch(Exception ex)
            {
                Logger1.Error(ex);
            }
            return Result;
        }

        [WebMethod]
        public static string UpdateData(string ClientID, string Master_API, string PK_TAXTYPEID, string taxtypename, string Updated_By)
        {
            string Result = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "TaxType";
                    var input = new
                    {
                        id = PK_TAXTYPEID,
                        FK_CLIENT_ID = ClientID,
                        label = taxtypename,
                        EMP_NAME = Updated_By
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/UpdateTaxTypeInfo", inputContent).Result;
                    Result = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);

                    Logger1.Activity("Tax Type Data " + Result + " In P2P");
                }
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return Result;
        }

        [WebMethod]
        public static string Deletedata(string ClientID, string Master_API, string PK_TAXTYPEID, string Updated_By)
        {
            string Result = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "TaxType";
                    var input = new
                    {
                        id = PK_TAXTYPEID,
                        Client_ID = ClientID,
                        EMP_NAME = Updated_By
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/DeleteTaxTypeInfo", inputContent).Result;
                    Result = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);

                    Logger1.Activity("Tax Type Data " + Result + " In P2P");
                }
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return Result;
        }
    }
}