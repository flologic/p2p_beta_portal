﻿using FSL.Controller;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace P2P_Portal.Process.PO_CREATION
{
    public partial class POApproval : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ActionController.IsSessionExpired(Page))
                ActionController.RedirctToLogin(Page);
            else
            {
                if (!IsPostBack)
                {
                    txt_Client_ID.Value = System.Configuration.ConfigurationManager.AppSettings["Client_ID"];
                    txt_Master_Url.Value = System.Configuration.ConfigurationManager.AppSettings["MasterAPI"];
                    txt_P2P_Url.Value = System.Configuration.ConfigurationManager.AppSettings["P2PAPI"];
                    txt_Wfe_Url.Value = System.Configuration.ConfigurationManager.AppSettings["WFEAPI"];
                    SessionAdId.Value = Session["User_ADID"].ToString();
                    txt_Email.Value = Session["EmailID"].ToString();
                    txt_ProcessID.Value = Convert.ToString(Request.QueryString["processid"]);
                    txt_InstanceID.Value = Convert.ToString(Request.QueryString["instanceid"]);
                    txt_StepName.Value = Convert.ToString(Request.QueryString["step"]);
                }
            }
        }

        [WebMethod]
        public static string[] getAuditData(string ProcessID, string InstanceID, string MasterUrl, string ProcessUrl, string WfeUrl)
        {
            string[] jsonArray = new string[1];
            HttpClient client = new HttpClient();
            string MasterUrlNew = MasterUrl + "Common";
            //apiUrl = "http://localhost:59313/api/AuditTrail";
            var input = new
            {
                PROCESSID = ProcessID,
                INSTANCEID = InstanceID,
            };
            string inputJson = (new JavaScriptSerializer()).Serialize(input);

            HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync(MasterUrlNew + "/getAudittraildata", inputContent).Result;

            jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            return jsonArray;
        }

        [WebMethod]
        public static string[] getDocumentData(string Requestno, string apiUrl)
        {
            string[] jsonArray = new string[1];
            HttpClient client = new HttpClient();
            apiUrl = apiUrl + "Common";
            //apiUrl = "http://192.168.0.107:81/api/Requisition";
            var input = new
            {
                OBJECT_VALUE = Requestno,
            };
            string inputJson = (new JavaScriptSerializer()).Serialize(input);
            HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync(apiUrl + "/getDocs", inputContent).Result;
            jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            return jsonArray;
        }

        [WebMethod]
        public static string[] getBudgetDetails(string branchId, string Finyear, string apiUrl)
        {
            string[] jsonArray = new string[1];
            HttpClient client = new HttpClient();
            apiUrl = "http://192.168.0.107:81/api/Requisition";
            var input = new
            {
                PROCESSID = branchId,
                INSTANCEID = Finyear,
            };
            string inputJson = (new JavaScriptSerializer()).Serialize(input);
            HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync(apiUrl + "/gethdrdata", inputContent).Result;

            jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            return jsonArray;
        }

        [WebMethod]
        public static string[] GetPODetails(int ProcessID, int InstanceID, string MasterUrl, string ProcessUrl, string WfeUrl)
        {
            string[] jsonArray = new string[1];
            HttpClient client = new HttpClient();
            string ProcessUrlNew = ProcessUrl + "POApproval";//"http://localhost:51092/api/POApproval"; //
            //string apiUrl = "http://192.168.0.107:81/api/Requisition";
            var input = new
            {
                PROCESSID = ProcessID,
                INSTANCEID = InstanceID,
                Master_API = MasterUrl,
                Trans_API = ProcessUrl,
                Wfe_API = WfeUrl
            };
            string inputJson = (new JavaScriptSerializer()).Serialize(input);
            HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync(ProcessUrlNew + "/getPOprocessdata", inputContent).Result;
            jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            return jsonArray;
        }

        [WebMethod]
        public static string[] getPOTAXData(int ProcessID, int InstanceID, string MasterUrl, string ProcessUrl, string WfeUrl)
        {
            string[] jsonArray = new string[1];
            HttpClient client = new HttpClient();
            string ProcessUrlNew = ProcessUrl + "POApproval";//"http://localhost:51092/api/POApproval";
            //string apiUrl = "http://192.168.0.107:81/api/Requisition";
            var input = new
            {
                PROCESSID = ProcessID,
                INSTANCEID = InstanceID,
                MAsterAPI = MasterUrl,
                TransAPI = ProcessUrl,
                WFEAPI = WfeUrl
            };
            string inputJson = (new JavaScriptSerializer()).Serialize(input);
            HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync(ProcessUrlNew + "/getPOtaxdata", inputContent).Result;
            jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            return jsonArray;
        }

        [WebMethod]
        public static string[] getPOTERMData(int ProcessID, int InstanceID, string MasterUrl, string ProcessUrl, string WfeUrl)
        {
            string[] jsonArray = new string[1];
            HttpClient client = new HttpClient();
            string ProcessUrlNew = ProcessUrl + "POApproval";//"http://localhost:51092/api/POApproval"; //
            //string apiUrl = "http://192.168.0.107:81/api/Requisition";
            var input = new
            {
                PROCESSID = ProcessID,
                INSTANCEID = InstanceID,
                MAsterAPI = MasterUrl,
                TransAPI = ProcessUrl,
                WFEAPI = WfeUrl
            };
            string inputJson = (new JavaScriptSerializer()).Serialize(input);
            HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync(ProcessUrlNew + "/getPOTermsdata", inputContent).Result;
            jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            return jsonArray;
        }

        [WebMethod]
        public static string SaveData(string processId, string instanceId, string stepName, string SessionAdId, string SessionUserEmail, string requestNo, string requestInitiator, string requestIni_Email, string action, string approvalremark, string MasterUrl, string ProcessUrl, string WfeUrl)
        {
            string Result = string.Empty;
            try
            {
                HttpClient client = new HttpClient();
                string ProcessUrl1 = ProcessUrl + "POApproval";
                //apiUrl = "http://192.168.0.111:82/api/Requisition";
                var input = new
                {
                    proccessid = processId,
                    instsnceid = instanceId,
                    stepName = stepName,
                    requestPerformer = SessionAdId,
                    approver_mail = SessionUserEmail,
                    Request_No = requestNo,
                    Requester_ADID = requestInitiator,
                    requester_Mail = requestIni_Email,
                    action = action,
                    approvalremark = approvalremark,
                    MasterUrl = MasterUrl,
                    ProcessUrl = ProcessUrl,
                    WfeUrl = WfeUrl

                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(ProcessUrl1 + "/ApprovalSave", inputContent).Result;
                Result = response.Content.ReadAsStringAsync().Result.ToString();
                Result = Result.Replace("\"", "");
            }
            catch (Exception ex)
            {
                throw;
            }
            return Result;
        }

    }
}