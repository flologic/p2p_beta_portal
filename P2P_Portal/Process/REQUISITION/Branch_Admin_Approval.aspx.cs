﻿using FSL.Controller;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace P2P_Portal.Process.Requisition
{
    public partial class Branch_Admin_Approval : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ActionController.IsSessionExpired(Page))
                ActionController.RedirctToLogin(Page);
            else
            {
                if (!IsPostBack)
                {
                    txt_Master_Url.Value = System.Configuration.ConfigurationManager.AppSettings["MasterAPI"];
                    txt_P2P_Url.Value = System.Configuration.ConfigurationManager.AppSettings["P2PAPI"];
                    txt_Wfe_Url.Value = System.Configuration.ConfigurationManager.AppSettings["WFEAPI"];
                    SessionAdId.Value = Session["User_ADID"].ToString();
                    txt_Email.Value = Session["EmailID"].ToString();
                    txt_ProcessID.Value = Convert.ToString(Request.QueryString["processid"]);
                    txt_InstanceID.Value = Convert.ToString(Request.QueryString["instanceid"]);
                    txt_StepName.Value = Convert.ToString(Request.QueryString["step"]);
                }
            }
        }

        [WebMethod]
        public static string[] GetReqDetails(int ProcessID, int InstanceID, string MasterUrl, string ProcessUrl, string WfeUrl)
        {
            string[] jsonArray = new string[1];
            HttpClient client = new HttpClient();
            string ProcessUrlNew = ProcessUrl + "Reporting_Manager_Approval";
            //string apiUrl = "http://192.168.0.107:81/api/Requisition";
            var input = new
            {
                FK_PROCESSID = ProcessID,
                FK_INSTANCEID = InstanceID,
                MasterUrl = MasterUrl,
                ProcessUrl = ProcessUrl,
                WfeUrl = WfeUrl
            };
            string inputJson = (new JavaScriptSerializer()).Serialize(input);
            HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync(ProcessUrlNew + "/getReqData", inputContent).Result;
            jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            return jsonArray;
        }

        [WebMethod]
        public static string[] getAuditData(string ProcessID, string InstanceID, string MasterUrl, string ProcessUrl, string WfeUrl)
        {
            string[] jsonArray = new string[1];
            HttpClient client = new HttpClient();
            string MasterUrlNew = MasterUrl + "Common";
            //apiUrl = "http://localhost:59313/api/AuditTrail";
            var input = new
            {
                PROCESSID = ProcessID,
                INSTANCEID = InstanceID,
            };
            string inputJson = (new JavaScriptSerializer()).Serialize(input);

            HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync(MasterUrlNew + "/getAudittraildata", inputContent).Result;

            jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            return jsonArray;
        }

        [WebMethod]
        public static string[] getDocumentData(string Requestno, string apiUrl)
        {
            string[] jsonArray = new string[1];
            HttpClient client = new HttpClient();
            apiUrl = apiUrl + "Common";
            var input = new
            {
                OBJECT_VALUE = Requestno,
            };
            string inputJson = (new JavaScriptSerializer()).Serialize(input);
            HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync(apiUrl + "/getDocs", inputContent).Result;
            jsonArray[0] = response.Content.ReadAsStringAsync().Result;
            return jsonArray;
        }

        [WebMethod]
        public static string[] getBudgetDetails(string branchId, string Finyear, string apiUrl)
        {
            string[] jsonArray = new string[1];
            HttpClient client = new HttpClient();
            apiUrl = "http://192.168.0.107:81/api/Requisition";
            var input = new
            {
                PROCESSID = branchId,
                INSTANCEID = Finyear,
            };
            string inputJson = (new JavaScriptSerializer()).Serialize(input);
            HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync(apiUrl + "/gethdrdata", inputContent).Result;

            jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            return jsonArray;
        }

        [WebMethod]
        public static string SaveData(string processId, string instanceId, string stepName, string SessionAdId, string SessionUserEmail, string requestNo, string requestInitiator, string requestIni_Email, string action, string POtype, string approvalremark, string CatName, string MasterUrl, string ProcessUrl, string WfeUrl)
        {
            string Result = string.Empty;
            try
            {
                HttpClient client = new HttpClient();
                string ProcessUrl1 = ProcessUrl + "Branch_Admin_Approval";
                var input = new
                {
                    processid = processId,
                    instanceId = instanceId,
                    stepName = stepName,
                    requestPerformer = SessionAdId,
                    approver_mail = SessionUserEmail,
                    Request_No = requestNo,
                    Requester_ADID = requestInitiator,
                    requester_Mail = requestIni_Email,
                    action = action,
                    POType = POtype,
                    approvalremark = approvalremark,
                    CatName = CatName,
                    MasterUrl = MasterUrl,
                    ProcessUrl = ProcessUrl,
                    WfeUrl = WfeUrl

                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(ProcessUrl1 + "/SaveData", inputContent).Result;
                Result = response.Content.ReadAsStringAsync().Result.ToString();
                Result = Result.Replace("\"", "");
            }
            catch (Exception ex)
            {
                throw;
            }
            return Result;
        }
    }
}