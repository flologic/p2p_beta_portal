﻿
using FSL.Controller;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace P2P_Portal.Process.Requisition
{
    public partial class Asset_Request : System.Web.UI.Page
    {
        HttpClient client = new HttpClient();
        string apiUrl = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ActionController.IsSessionExpired(Page))
                ActionController.RedirctToLogin(Page);
            else
            {
                if (!IsPostBack)
                {
                    txt_Master_Url.Value = System.Configuration.ConfigurationManager.AppSettings["MasterAPI"];
                    txt_P2P_Url.Value = System.Configuration.ConfigurationManager.AppSettings["P2PAPI"];
                    txt_Wfe_Url.Value = System.Configuration.ConfigurationManager.AppSettings["WFEAPI"];
                    apiUrl = txt_P2P_Url.Value + "Authenticate";
                    SessionAdId.Value = Session["User_ADID"].ToString();
                    EmailID.Value = Session["EmailID"].ToString();
                    lblrequester.InnerText = Session["USER_NAME"].ToString();// + "_" + Session["User_ADID"].ToString();
                    txt_BranchID.Value = Session["FK_BRANCHID"].ToString();
                    lblreqdate.InnerText = DateTime.Now.ToString("dd-MMM-yyyy");
                    fillDocument_Details();
                }
            }
        }

        private void fillDocument_Details()
        {
            try
            {
                string isData = string.Empty;
                string isValid = string.Empty;
                string DisplayData = string.Empty;

                DisplayData = "<table class='display nowrap dataTable' id='uploadTable'><thead><tr class='grey'><th>Description</th><th>File Name</th><th>Delete</th></tr></thead>";
                DisplayData += "</table>";
                div_Doc.InnerHtml = DisplayData;
                DisplayData = "";
            }
            catch (Exception ex)
            {
                FSL.Logging.Logger.WriteEventLog(false, ex);
            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                string activeDir = ConfigurationManager.AppSettings["DOCPATH"].ToString();
                Int32 flength = FileUpload1.PostedFile.ContentLength;

                string path = string.Empty;
                path = activeDir + "\\" + "REQUISITION\\";
                string filename = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName.ToString());
                filename = filename.Replace("(", "");
                filename = filename.Replace(")", "");
                filename = filename.Replace("&", "");
                filename = filename.Replace("+", "");
                filename = filename.Replace("/", "");
                filename = filename.Replace("\\", "");
                filename = filename.Replace("'", "");
                filename = filename.Replace("  ", "");
                filename = filename.Replace(" ", "");
                filename = filename.Replace("#", "");
                filename = filename.Replace("$", "");
                filename = filename.Replace("~", "");
                filename = filename.Replace("%", "");
                filename = filename.Replace("''", "");
                filename = filename.Replace(":", "");
                filename = filename.Replace("*", "");
                filename = filename.Replace("?", "");
                filename = filename.Replace("<", "");
                filename = filename.Replace(">", "");
                filename = filename.Replace("{", "");
                filename = filename.Replace("}", "");
                filename = filename.Replace(",", "");
                DataTable dt = (DataTable)Session["UploadedFiles"];

                FileUpload1.SaveAs(path + filename);
                //ClearContents(sender as Control);
            }
            catch (Exception Ex)
            {
                //Logger.WriteEventLog(false, Ex);
            }
        }

        [WebMethod]
        public static string[] GetUserData(string Userid, string apiUrl)
        {
            string[] jsonArray = new string[1];
            try
            {
                HttpClient client = new HttpClient();
                apiUrl = apiUrl + "Employee";               
                var input = new
                {
                    AD_ID = Userid,
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getEmpInfo", inputContent).Result;
                jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            catch (Exception Ex)
            {

                Logger1.Error(Ex);
                throw;
            }

            return jsonArray;
        }

        [WebMethod]
        public static string[] GetCategory(string apiUrl)
        {
            string[] jsonArray = new string[1];
            try
            {
                HttpClient client = new HttpClient();
                apiUrl = apiUrl + "ItemCategory";
                //string apiUrl = "http://192.168.0.107:81/api/AssetRequisition";
                var input = new
                {
                    FK_CLIENT_ID = 1,
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getItemCategoryInfo", inputContent).Result;
                jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            catch (Exception Ex)
            {
                Logger1.Error(Ex);
                throw;
            }

            return jsonArray;
        }

        [WebMethod]
        public static string[] GetSubCategory(string catID, string apiUrl)
        {
            string[] jsonArray = new string[1];
            try
            {
                HttpClient client = new HttpClient();
                apiUrl = apiUrl + "ItemSubCategory";
                //string apiUrl = "http://192.168.0.107:81/api/AssetRequisition";
                var input = new
                {
                    FK_ITMCATID = catID,
                    FK_CLIENT_ID = 1,
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getItemSubCategoryInfo", inputContent).Result;
                jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            catch (Exception Ex)
            {
                Logger1.Error(Ex);
                throw;
            }

            return jsonArray;
        }

        [WebMethod]
        public static string[] GetItem(string catID, string subcatID, string apiUrl)
        {
            string[] jsonArray = new string[1];
            try
            {
                HttpClient client = new HttpClient();
                apiUrl = apiUrl + "Item";
                //string apiUrl = "http://192.168.0.107:81/api/AssetRequisition";
                var input = new
                {
                    FK_ITMCATID = catID,
                    FK_ITMSUBCATID = subcatID,
                    FK_CLIENT_ID = 1,
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getItemInfo", inputContent).Result;
                jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            catch (Exception Ex)
            {
                Logger1.Error(Ex);
                throw;
            }
            return jsonArray;
        }

        [WebMethod]
        public static string SaveData(string transactionData, string InitiatorId, string req_Email, string CatName, string Type, string action, string MasterUrl, string ProcessUrl, string WfeUrl)
        {
            string Result = string.Empty;
            try
            {
                HttpClient client = new HttpClient();
                string ProcessUrl1 = ProcessUrl + "Asset_Request";
                //apiUrl = "http://192.168.0.111:82/api/Requisition";
                var input = new
                {
                    Transactiondata = transactionData,
                    Initiatorid = InitiatorId,
                    Requester_mailid = req_Email,
                    CatName = CatName,
                    POType = Type,
                    Action = action,
                    MasterUrl = MasterUrl,
                    ProcessUrl = ProcessUrl,
                    WfeUrl = WfeUrl

                    //FK_CLIENT_ID = 1,
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(ProcessUrl1 + "/SaveData", inputContent).Result;
                Result = response.Content.ReadAsStringAsync().Result.ToString();
                Result = Result.Replace("\"", "");
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
                throw;
            }
            return Result;
        }
    }
}