﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Request_Modification.aspx.cs" Inherits="P2P_Portal.Process.Requisition.Request_Modification" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Request Modification</title>
    <link href="../../assets/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../assets/plugins/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet" />
    <link href="../../assets/css/style.css" rel="stylesheet" />
    <link href="../../assets/css/jquery-ui.css" rel="stylesheet" />
    <link href="../../assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="../../assets/css/jAlert.css" rel="stylesheet" />
</head>
<body style="overflow-x: hidden">
    <form id="form1" runat="server">
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ToolkitScriptManager>
        <div class="row bg-title">
            <div class="col-md-12">
                <div id="Div1" class="panel panel-default panel-border-top" runat="server">
                    <div class="panel-heading">
                        Request Modification
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Request No. :</label>
                                        <div class="col-md-2">
                                            <label class="form-control input-sm label-color" id="lbl_ReqNo" runat="server"></label>
                                        </div>
                                        <label class="col-md-2 control-label">Requester :</label>
                                        <div class="col-md-2">
                                            <label class="form-control input-sm label-color" id="lblrequester" runat="server"></label>
                                        </div>
                                        <label class="col-md-2 control-label">Request Date :</label>
                                        <div class="col-md-2">
                                            <label class="form-control input-sm label-color" id="lblreqdate" runat="server"></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Branch :</label>
                                        <div class="col-md-2">
                                            <label class="form-control input-sm label-color" id="lbl_Branch" runat="server"></label>
                                        </div>
                                        <label class="col-md-2 control-label">Designation :</label>
                                        <div class="col-md-2">
                                            <label class="form-control input-sm label-color" id="lbl_Desi" runat="server"></label>
                                        </div>
                                        <label class="col-md-2 control-label">Grade :</label>
                                        <div class="col-md-2">
                                            <label class="form-control input-sm label-color" id="lbl_Grade" runat="server"></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Department :</label>
                                        <div class="col-md-2">
                                            <label class="form-control input-sm label-color" id="lbl_Department" runat="server"></label>
                                        </div>
                                        <label class="col-md-2 control-label">Category :</label>
                                        <div class="col-md-2">
                                            <label class="form-control input-sm label-color" id="lbl_category" runat="server"></label>
                                        </div>
                                        <label class="col-md-2 control-label m-t-10">Documents:</label>
                                        <div class="col-md-2 m-t-10">
                                            <a href="#div_UploadDocument" data-toggle="modal">
                                                <img id="Img1" src="../../assets/images/attachment_non_sel.png" title="Get Document." alt="Get Document." height="20" width="20" /></a>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label m-t-4">Remark :</label>
                                        <div class="col-md-2 m-t-5">
                                            <textarea id="txt_Remark" class="form-control" placeholder="Enter Remark" rows="2" cols="27" maxlength="300"></textarea>
                                        </div>
                                        <label class="col-md-2 control-label m-t-4">Justification :</label>
                                        <div class="col-md-2 m-t-5">
                                            <textarea id="txt_justification" class="form-control" placeholder='Enter Justification' rows="2" cols="27" maxlength="300"></textarea>
                                        </div>
                                        <%--<label class="col-md-2 control-label m-t-10"></label>
                                        <div class="col-md-2 m-t-10">
                                            <a href="#div_BudgetDetails" data-toggle="modal" class="control-label" title="Click here to view Budget Details.">Budget Details</a>
                                        </div>--%>
                                    </div>
                                    <br />
                                    <hr />
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="Div4" class="panel panel-default panel-border-top" runat="server">
                                                <div class="panel-heading">
                                                    REQUISITION DETAILS                                               
                                               <%-- <div class="panel-action">
                                                    Total Amount : <span id="spn_userTotal" runat="server"></span>
                                                </div>--%>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-horizontal">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="table-responsive">
                                                                            <div id="div_displayReqDtl" runat="server" style="overflow-y: scroll">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button id="btn_Save" name="btn_Show" type="button" runat="server" class="fcbtn btn btn-info btn-outline btn-1b">Submit</button>
                                <%--<button id="btn_SaveAsDraft" name="btn_Draft" type="button" runat="server" class="fcbtn btn btn-info btn-outline btn-1b">Save As Draft</button>--%>
                                <button id="btn_Cancel" name="btn_Show" type="button" runat="server" class="fcbtn btn btn-info btn-outline btn-1b">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="Div5" class="panel panel-default panel-border-top" runat="server">
                    <div class="panel-heading">
                        Audit Trail
                    </div>
                    <div class="panel-body">
                        <div id="div_Audit" runat="server">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div style="display: none;" class="modal" id="div_UploadDocument">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Document Upload</h5>
                        </div>
                        <div class="modal-body">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <div class="col-md-1"></div>
                                    <label class="col-md-2">Description</label>
                                    <div class="col-md-6">
                                        <div id="div_CustomerId">
                                            <input class="form-control input-sm" id="txt_description" name="txt_description" type="text" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-1"></div>
                                    <label class="col-md-2">Attach File</label>
                                    <asp:UpdatePanel ID="upModal" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="col-md-7">
                                                <span class="btn btn-grey fileinput-button">
                                                    <asp:AsyncFileUpload ID="FileUpload1" runat="server" OnClientUploadError="uploadError"
                                                        OnClientUploadStarted="StartUpload" OnClientUploadComplete="UploadComplete" CompleteBackColor="Lime"
                                                        ErrorBackColor="Red" OnUploadedComplete="btnUpload_Click"
                                                        UploadingBackColor="#66CCFF" />
                                                </span>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-1"></div>
                                    <p style="color: red">
                                        <label>Upload .pdf,.jpeg,.jpg,.png formats only.</label>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <div id="div_docs" runat="server"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="javascript:;" class="fcbtn btn btn-info btn-outline btn-1b" data-dismiss="modal">Close</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div id="div_BudgetDetails" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog" style="width: 80%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Budget Details</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-horizontal">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <div id="div_Budget_details" runat="server"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="javascript:;" class="fcbtn btn btn-info btn-outline btn-1b" data-dismiss="modal">Close</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="display: none">
            <input type="text" id="SessionAdId" runat="server" />
            <input type="text" id="txt_ProcessID" runat="server" />
            <input type="text" id="txt_InstanceID" runat="server" />
            <input type="text" id="txt_StepName" runat="server" />
            <input type="text" id="txt_Email" runat="server" />
            <input type="text" id="txt_Master_Url" runat="server" />
            <input type="text" id="txt_P2P_Url" runat="server" />
            <input type="text" id="txt_Wfe_Url" runat="server" />           
        </div>
        <!-- ================== BEGIN BASE JS ================== -->
        <script lang="JavaScript" type="text/javascript" src="../../assets/js/jquery.min.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/js/jquery-ui.js"></script>
        <script src="../../assets/js/underscore.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/js/jAlert.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/js/jAlert-functions.js"></script>
        <script src="../../JS/Common/document-attach.js"></script>
        <script src="../../JS/Common/Utility.js"></script>
        <script src="../../JS/Common/Vaildation.js"></script>
        <script src="../../JS/Process/Asset%20Requisition/Request_Modification.js"></script>
    </form>
</body>
</html>

