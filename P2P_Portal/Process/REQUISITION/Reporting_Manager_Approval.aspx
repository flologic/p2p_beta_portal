﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Reporting_Manager_Approval.aspx.cs" Inherits="P2P_Portal.Process.Requisition.Reporting_Manager_Approval" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Reporting Manager Approval</title>
    <link href="../../assets/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../assets/plugins/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet" />
    <link href="../../assets/css/style.css" rel="stylesheet" />
    <link href="../../assets/css/jquery-ui.css" rel="stylesheet" />
    <link href="../../assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="../../assets/css/jAlert.css" rel="stylesheet" />
</head>
<body style="overflow-x: hidden">
    <form id="form1" runat="server">
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ToolkitScriptManager>
        <div class="row bg-title">
            <div class="col-md-12">
                <div id="Div1" class="panel panel-default panel-border-top" runat="server">
                    <div class="panel-heading">
                        Reporting Manager Approval
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Request No. :</label>
                                        <div class="col-md-2">
                                            <label class="form-control input-sm label-color" id="lbl_ReqNo" runat="server"></label>
                                        </div>
                                        <label class="col-md-2 control-label">Requester :</label>
                                        <div class="col-md-2">
                                            <label class="form-control input-sm label-color" id="lblrequester" runat="server"></label>
                                        </div>
                                        <label class="col-md-2 control-label">Request Date :</label>
                                        <div class="col-md-2">
                                            <label class="form-control input-sm label-color" id="lblreqdate" runat="server"></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Branch :</label>
                                        <div class="col-md-2">
                                            <label class="form-control input-sm label-color" id="lbl_Branch" runat="server"></label>
                                        </div>
                                        <label class="col-md-2 control-label">Designation :</label>
                                        <div class="col-md-2">
                                            <label class="form-control input-sm label-color" id="lbl_Desi" runat="server"></label>
                                        </div>
                                        <label class="col-md-2 control-label">Grade :</label>
                                        <div class="col-md-2">
                                            <label class="form-control input-sm label-color" id="lbl_Grade" runat="server"></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Department :</label>
                                        <div class="col-md-2">
                                            <label class="form-control input-sm label-color" id="lbl_Department" runat="server"></label>
                                        </div>
                                        <label class="col-md-2 control-label">Category :</label>
                                        <div class="col-md-2">
                                            <label class="form-control input-sm label-color" id="lbl_category" runat="server"></label>
                                        </div>
                                        <label class="col-md-2 control-label m-t-10">Documents:</label>
                                        <div class="col-md-2 m-t-10">
                                            <a href="#div_UploadDocument" data-toggle="modal">
                                                <img id="Img1" src="../../assets/images/attachment_non_sel.png" title="Get Document." alt="Get Document." height="20" width="20" /></a>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Remark :</label>
                                        <div class="col-md-2">
                                            <textarea id="txt_Remark" class="form-control" rows="2" cols="27" maxlength="300" disabled></textarea>
                                        </div>
                                        <label class="col-md-2 control-label">Justification :</label>
                                        <div class="col-md-2">
                                            <textarea id="txt_justification" class="form-control" rows="2" cols="27" maxlength="300" disabled></textarea>
                                        </div>
                                        <%--<label class="col-md-2 control-label m-t-10"></label>
                                        <div class="col-md-2 m-t-10">
                                            <a href="#div_BudgetDetails" data-toggle="modal" class="control-label" title="Click here to view Budget Details.">Budget Details</a>
                                        </div>--%>
                                    </div>
                                    <br />
                                    <hr />
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="Div4" class="panel panel-default panel-border-top" runat="server">
                                                <div class="panel-heading">
                                                    REQUISITION DETAILS                                               
                                               <%-- <div class="panel-action">
                                                    Total Amount : <span id="spn_userTotal" runat="server"></span>
                                                </div>--%>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-horizontal">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="table-responsive">
                                                                            <div id="div_displayReqDtl" runat="server" style="overflow-y: scroll">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="Div2" class="panel panel-default panel-border-top" runat="server">
                                                <div class="panel-heading">
                                                    Action
                                                </div>
                                                <div class="panel-body">
                                                    <div class="form-horizontal">
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label m-t-10">Select Action :</label>
                                                            <div class="col-md-2 m-t-10">
                                                                <select id="ddlAction" class="form-control input-sm mandatory">
                                                                    <option value="0">--Select One--</option>
                                                                    <option value="APPROVE">Approve</option>
                                                                    <option value="SENDBACK">Send Back</option>
                                                                    <option value="REJECT">Reject</option>
                                                                </select>
                                                            </div>
                                                            <label class="col-md-2 control-label" id="lbl_rmk">Remark :</label>
                                                            <div class="col-md-2" id="div_remark">
                                                                <textarea class="form-control input-sm" id="app_Remark" rows="2" cols="27" maxlength="300"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button id="btn_Save" name="btn_Show" type="button" class="fcbtn btn btn-info btn-outline btn-1b">Submit</button>
                                <button id="btn_Cancel" name="btn_Show" type="button" class="fcbtn btn btn-info btn-outline btn-1b">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="Div5" class="panel panel-default panel-border-top" runat="server">
                    <div class="panel-heading">
                        Audit Trail
                    </div>
                    <div class="panel-body">
                        <div id="div_Audit" runat="server">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="modal fade" id="div_UploadDocument">
                <div class="modal-dialog" style="width: 45%; margin-left: 25%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">File Attachment</h4>
                        </div>
                        <div class="modal-body">
                            <div class="table-responsive" id="div_docs" runat="server">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="javascript:;" class="fcbtn btn btn-info btn-outline btn-1b" data-dismiss="modal">Close</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div id="div_BudgetDetails" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog" style="width: 80%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Budget Details</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-horizontal">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <div id="div_Budget_details" runat="server"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="javascript:;" class="fcbtn btn btn-info btn-outline btn-1b" data-dismiss="modal">Close</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="display: none">
            <input type="text" id="SessionAdId" runat="server" />
            <input type="text" id="txt_ProcessID" runat="server" />
            <input type="text" id="txt_InstanceID" runat="server" />
            <input type="text" id="txt_StepName" runat="server" />
            <input type="text" id="txt_Email" runat="server" />
            <input type="text" id="txt_Master_Url" runat="server" />
            <input type="text" id="txt_P2P_Url" runat="server" />
            <input type="text" id="txt_Wfe_Url" runat="server" />
        </div>
        <!-- ================== BEGIN BASE JS ================== -->
        <script lang="JavaScript" type="text/javascript" src="../../assets/js/jquery.min.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/js/jquery-ui.js"></script>
        <script src="../../assets/js/underscore.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/js/jAlert.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/js/jAlert-functions.js"></script>
        <script src="../../JS/Common/document-attach.js"></script>
        <script src="../../JS/Common/Utility.js"></script>
        <script src="../../JS/Common/Vaildation.js"></script>
        <script src="../../JS/Process/Asset%20Requisition/Reporting_Manager_Approval.js"></script>
    </form>
</body>
</html>
