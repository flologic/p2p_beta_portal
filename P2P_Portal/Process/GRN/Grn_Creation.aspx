﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Grn_Creation.aspx.cs" Inherits="P2P_Portal.Process.GRN.Grn_Creation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Grn Creation</title>
    <link href="../../assets/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../assets/plugins/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet" />
    <link href="../../assets/css/style.css" rel="stylesheet" />
    <link href="../../assets/css/jquery-ui.css" rel="stylesheet" />
    <link href="../../assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="../../assets/css/jAlert.css" rel="stylesheet" />
</head>
<body style="overflow-x: hidden">
    <form id="form1" runat="server">
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ToolkitScriptManager>
        <div class="row bg-title">
            <div class="col-md-12">
                <div id="Div1" class="panel panel-default panel-border-top" runat="server">
                    <div class="panel-heading">
                        Grn Creation
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Branch :</label>
                                        <div class="col-md-2">
                                            <label class="form-control input-sm label-color" id="txt_Branch" placeholder="Select Branch" runat="server"></label>
                                        </div>
                                        <label class="col-md-2 control-label">GRN Number :</label>
                                        <div class="col-md-2">
                                            <label class="form-control input-sm label-color" id="txt_GrnNO" runat="server"></label>
                                        </div>
                                        <label class="col-md-2 control-label">GRN Entry Date :</label>
                                        <div class="col-md-2">
                                            <input type="text" class='form-control input-sm' id="txt_Grn_Date" placeholder="Select Grn Date" runat="server" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">PO Type :</label>
                                        <div class="col-md-2">
                                            <input type="text" class='form-control input-sm mandatory' id="txt_Po_Type" placeholder="Select Type" runat="server" />
                                        </div>
                                        <label class="col-md-2 control-label">PO Number :</label>
                                        <div class="col-md-2">
                                            <label class="form-control input-sm label-color" id="txt_Po_Number" placeholder="Select PO Number" runat="server"></label>
                                        </div>
                                        <label class="col-md-2 control-label">PO Date :</label>
                                        <div class="col-md-2">
                                            <input type="text" class='form-control input-sm' id="txt_Date"  runat="server" disabled="disabled" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Supplier :</label>
                                        <div class="col-md-2">
                                            <input type="text" class='form-control input-sm mandatory' id="Text2" placeholder="Select Supplier" runat="server" />
                                        </div>
                                        <label class="col-md-2 control-label">Invoice Number :</label>
                                        <div class="col-md-2">
                                            <label class="form-control input-sm label-color" id="Label2"  runat="server"></label>
                                        </div>
                                        <label class="col-md-2 control-label">Invoice Date :</label>
                                        <div class="col-md-2">
                                            <label class="form-control input-sm label-color" id="Label3" runat="server"></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label m-t-4">Remark :</label>
                                        <div class="col-md-2 m-t-5">
                                            <textarea id="txt_Remark" class="form-control" placeholder="Enter Remark" rows="2" cols="27" maxlength="300"></textarea>
                                        </div>
                                        <label class="col-md-2 control-label m-t-4">Documents :</label>
                                        <div class="col-md-2 m-t-10">
                                            <a href="#div_UploadDocument" data-toggle="modal">
                                                <img id="Img1" src="../../assets/images/attachment_non_sel.png" title="Attach file." alt="Attach file." height="20" width="20" /></a>
                                        </div>
                                    </div>
                                    <br />
                                    <hr />
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="Div4" class="panel panel-default panel-border-top" runat="server">
                                                <div class="panel-heading">
                                                    GRN Details                                              
                                               <%-- <div class="panel-action">
                                                    Total Amount : <span id="spn_userTotal" runat="server"></span>
                                                </div>--%>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-horizontal">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="table-responsive">
                                                                            <div id="div_displayGrnDtl" runat="server">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button id="btn_Save" name="btn_Show" type="button" runat="server" class="fcbtn btn btn-info btn-outline btn-1b">Submit</button>                                
                                <button id="btn_Cancel" name="btn_Show" type="button" runat="server" class="fcbtn btn btn-info btn-outline btn-1b">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div style="display: none;" class="modal" id="div_UploadDocument">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Document Upload</h5>
                        </div>
                        <div class="modal-body">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <div class="col-md-1"></div>
                                    <label class="col-md-2">Description</label>
                                    <div class="col-md-6">
                                        <div id="div_CustomerId">
                                            <input class="form-control input-sm" id="txt_description" name="txt_description" type="text" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-1"></div>
                                    <label class="col-md-2">Attach File</label>
                                    <asp:UpdatePanel ID="upModal" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="col-md-7">
                                                <span class="btn btn-grey fileinput-button">
                                                    <asp:AsyncFileUpload ID="FileUpload1" runat="server" OnClientUploadError="uploadError"
                                                        OnClientUploadStarted="StartUpload" OnClientUploadComplete="UploadComplete" CompleteBackColor="Lime"
                                                        ErrorBackColor="Red" OnUploadedComplete="btnUpload_Click"
                                                        UploadingBackColor="#66CCFF" />
                                                </span>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-1"></div>
                                    <p style="color: red">
                                        <label>Upload .pdf,.jpeg,.jpg,.png formats only.</label>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <div id="div_Doc" runat="server"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="javascript:;" class="fcbtn btn btn-info btn-outline btn-1b" data-dismiss="modal">Close</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div style="display: none;" class="modal" id="div_Eligible">
                <div class="modal-dialog" style="width: 65%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title"><b>Asset Description</b></h5>
                        </div>
                        <div class="modal-body">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <%--<div class="form-group">
                                            <div class="col-md-6">
                                                <input type="file" class="fcbtn btn btn-info btn-outline btn-1b" id="excelfile" />
                                            </div>
                                            <div class="col-md-6 right">
                                                <input type="button" id="viewfile" value="Upload Excel" class="fcbtn btn btn-info btn-outline btn-1b" onclick="ExportToTable()" />
                                            </div>
                                        </div>--%>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <div id="div_Asset" runat="server"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="javascript:;" class="fcbtn btn btn-info btn-outline btn-1b" data-dismiss="modal" id="assetpop">Close</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="display: none">
            <input type="text" id="SessionAdId" runat="server" />
            <input type="text" id="EmailID" runat="server" />
            <input type="text" id="txt_approver" runat="server" />
            <input type="text" id="txt_band_Id" runat="server" />
            <input type="text" id="txt_TotReqAmt" runat="server" />
            <input type="text" id="txt_Company_Id" runat="server" />
            <input type="text" id="txt_BranchID" runat="server" />
            <input type="text" id="txt_Master_Url" runat="server" />
            <input type="text" id="txt_P2P_Url" runat="server" />
            <input type="text" id="txt_Wfe_Url" runat="server" />
        </div>
        <!-- ================== BEGIN BASE JS ================== -->
        <script lang="JavaScript" type="text/javascript" src="../../assets/js/jquery.min.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/js/jquery-ui.js"></script>
        <script src="../../assets/js/underscore.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/js/jAlert.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/js/jAlert-functions.js"></script>
        <script src="../../JS/Common/document-attach.js"></script>
        <script src="../../JS/Common/Utility.js"></script>
        <script src="../../JS/Common/Vaildation.js"></script>
        <script src="../../JS/Process/GRN/Grn_Creation.js"></script>
    </form>
</body>
</html>

