﻿using FSL.Controller;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace P2P_Portal.Process.GRN
{
    public partial class Grn_Creation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ActionController.IsSessionExpired(Page))
                ActionController.RedirctToLogin(Page);
            else
            {
                if (!IsPostBack)
                {
                    txt_Master_Url.Value = System.Configuration.ConfigurationManager.AppSettings["MasterAPI"];
                    txt_P2P_Url.Value = System.Configuration.ConfigurationManager.AppSettings["P2PAPI"];
                    txt_Wfe_Url.Value = System.Configuration.ConfigurationManager.AppSettings["WFEAPI"];
                    //apiUrl = txt_P2P_Url.Value + "Authenticate";
                    SessionAdId.Value = Session["User_ADID"].ToString();
                    EmailID.Value = Session["EmailID"].ToString();
                    //lblrequester.InnerText = Session["USER_NAME"].ToString();// + "_" + Session["User_ADID"].ToString();
                    txt_BranchID.Value = Session["FK_BRANCHID"].ToString();
                    //lblreqdate.InnerText = DateTime.Now.ToString("dd-MMM-yyyy");
                    fillDocument_Details();
                }
            }
        }

        private void fillDocument_Details()
        {
            try
            {
                string isData = string.Empty;
                string isValid = string.Empty;
                string DisplayData = string.Empty;

                DisplayData = "<table class='display nowrap dataTable' id='uploadTable'><thead><tr class='grey'><th>Description</th><th>File Name</th><th>Delete</th></tr></thead>";
                DisplayData += "</table>";
                div_Doc.InnerHtml = DisplayData;
                DisplayData = "";
            }
            catch (Exception ex)
            {
                FSL.Logging.Logger.WriteEventLog(false, ex);
            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                string activeDir = ConfigurationManager.AppSettings["DOCPATH"].ToString();
                Int32 flength = FileUpload1.PostedFile.ContentLength;

                string path = string.Empty;
                path = activeDir + "\\" + "REQUISITION\\";
                string filename = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName.ToString());
                filename = filename.Replace("(", "");
                filename = filename.Replace(")", "");
                filename = filename.Replace("&", "");
                filename = filename.Replace("+", "");
                filename = filename.Replace("/", "");
                filename = filename.Replace("\\", "");
                filename = filename.Replace("'", "");
                filename = filename.Replace("  ", "");
                filename = filename.Replace(" ", "");
                filename = filename.Replace("#", "");
                filename = filename.Replace("$", "");
                filename = filename.Replace("~", "");
                filename = filename.Replace("%", "");
                filename = filename.Replace("''", "");
                filename = filename.Replace(":", "");
                filename = filename.Replace("*", "");
                filename = filename.Replace("?", "");
                filename = filename.Replace("<", "");
                filename = filename.Replace(">", "");
                filename = filename.Replace("{", "");
                filename = filename.Replace("}", "");
                filename = filename.Replace(",", "");
                DataTable dt = (DataTable)Session["UploadedFiles"];

                FileUpload1.SaveAs(path + filename);
                //ClearContents(sender as Control);
            }
            catch (Exception Ex)
            {
                //Logger.WriteEventLog(false, Ex);
            }
        }
    }
}