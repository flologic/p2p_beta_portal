﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateRFQ.aspx.cs" Inherits="P2P_Portal.Process.RFQ.CreateRFQ" %>

<!DOCTYPE html>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Create RFQ</title>
    <link rel="stylesheet" href="../../RFQassets/css/lib/font-awesome/font-awesome.min.css" />
    <link rel="stylesheet" href="../../RFQassets/css/lib/bootstrap/bootstrap.min.css" />
    <link rel="stylesheet" href="../../RFQassets/css/main.css" />
    <link href="../../RFQassets/plugins/datatables/datatables.min.css" rel="stylesheet"
        type="text/css" />
    <link href="../../RFQassets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css"
        rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../../RFQassets/css/jquery-ui.css" />
    <link rel="stylesheet" href="../../RFQassets/css/lib/clockpicker/bootstrap-clockpicker.min.css" />
    <link href="../../RFQassets/plugins/datatables/datatables.min.css" rel="stylesheet"
        type="text/css" />
    <link href="../../RFQassets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css"
        rel="stylesheet" type="text/css" />
    <link href="../../RFQassets/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="../../assets/css/jAlert.css" rel="stylesheet" />
</head>
<body class="with-side-menu">
    <form id="form1" runat="server">
        <asp:ToolkitScriptManager ID="ScriptManager2" runat="server" EnablePageMethods="true">
        </asp:ToolkitScriptManager>
        <div class="page-content">
            <div class="container-fluid">
                <div class="row" id="dv_RfqDtl">
                    <div class="col-md-12">
                        <section class="card card-inversed">
                            <header class="card-header">
                                <i class="fa fa-cart-plus text-danger"></i>Create New RFQ  
                            </header>
                            <div class="card-block">
                                <div class="col-md-12 new-rfq">
                                    <button class='btn btn-facebook waves-effect btn-circle waves-light' type='button'><i class="fa fa-indent"></i></button>
                                </div>
                            </div>
                        </section>
                        <section class="card card-inversed">
                            <header class="card-header">
                                <i class="fa fa-cart-plus text-danger"></i>Draft RFQs
                            </header>
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive" id="dv_RfqData">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="card card-inversed">
                            <header class="card-header">
                                <i class="fa fa-cart-plus text-danger"></i>Published RFQs
                            </header>
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive" id="dv_rfqPublish"></div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
                <div class="row" id="dv_Create" style="display: none">
                    <div class="col-md-12">
                        <section class="card card-inversed">
                            <header class="card-header">
                                <i class="fa fa-cart-plus text-danger"></i>Create RFQ
                            </header>
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-md-12">
                                        <fieldset class="form-group">
                                            <label class="col-md-3">RFQ Name</label>
                                            <div class="col-md-3">
                                                <input class="form-control mandatory" id="txt_RFQ_Desc" type="text" runat="server" />
                                            </div>
                                            <label class="col-md-3">Test Project</label>
                                            <div class="col-md-3">
                                                <input class="form-control mandatory" id="txt_Test_Rfq" type="text" runat="server" />
                                            </div>
                                        </fieldset>
                                        <fieldset class="form-group">
                                            <label class="col-md-3">Buyer</label>
                                            <div class="col-md-3">
                                                <input class="form-control mandatory" id="txt_buyer" type="text" onfocus="getbuyer()" />
                                            </div>
                                            <label class="col-md-3">Currency</label>
                                            <div class="col-md-3">
                                                <input class="form-control mandatory" id="txt_Currency" type="text" runat="server" />
                                            </div>
                                        </fieldset>
                                        <fieldset class="form-group">
                                            <label class="col-md-3">Department </label>
                                            <div class="col-md-3">
                                                <input class="form-control mandatory" id="txt_Dept" type="text" runat="server" />
                                            </div>
                                            <label class="col-md-3">Category</label>
                                            <div class="col-md-3">
                                                <input class="form-control mandatory" id="txt_Cat" type="text" runat="server" />
                                            </div>
                                        </fieldset>
                                        <fieldset class="form-group">
                                            <label class="col-md-3">Delivery Location</label>
                                            <div class="col-md-3">
                                                <input class="form-control mandatory" id="txt_Delivery_loc" type="text" runat="server" />
                                            </div>
                                            <label class="col-md-3">Branch</label>
                                            <div class="col-md-3">
                                                <input class="form-control mandatory" id="txt_branch" type="text" runat="server" />
                                            </div>
                                        </fieldset>

                                        <fieldset class="form-group">
                                            <label class="col-md-3">Start Date</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <input class="form-control mandatory datepicker" id="txt_Start_Date" type="text" runat="server" />
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <label class="col-md-3">Start Time</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <input class="form-control mandatory clockpicker" id="txt_Start_Time" type="text" runat="server" />
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-clock-o"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="form-group">
                                            <label class="col-md-3">End Date</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <input class="form-control mandatory datepicker" id="txt_End_Date" type="text" runat="server" />
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <label class="col-md-3">End Time</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <input class="form-control mandatory clockpicker" id="txt_End_Time" type="text" runat="server" />
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-clock-o"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="form-group">
                                            <label class="col-md-3">Region  </label>
                                            <div class="col-md-3">
                                                <input class="form-control mandatory" id="txt_region" type="text" />
                                            </div>

                                        </fieldset>
                                        <fieldset class="form-group">
                                            <div style="display: none">
                                                <label class="col-md-3">Auto Open</label>
                                                <div class="col-md-3">
                                                    <input type='checkbox' class='open-chk' id='txt_Open' runat='server' />
                                                </div>
                                            </div>

                                        </fieldset>
                                        <fieldset class="form-group">
                                            <label class="col-md-3">Documents</label>
                                            <div class="col-md-3">
                                                <a data-toggle="modal" data-target="#rfq_Document"><i class="fa fa-paperclip fa-2x float-left"></i></a>
                                            </div>
                                        </fieldset>
                                        <fieldset class="form-group">
                                            <div class="card-block" id="div10">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div id="div_reminder"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="col-md-12" id="dv_team" style="display: none">
                        <section class="card card-inversed">
                            <header class="card-header">
                                <i class="fa fa-cart-plus text-danger"></i>Terms & Conditions
                            </header>
                            <div class="card-block" id="div_TermsCon">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="dv_teams"></div>
                                        <label class="col-md-3">Documents</label>
                                        <div class="col-md-1">
                                            <a data-toggle="modal" data-target="#term_Document" class="model_img img-responsive"><i class="fa fa-paperclip fa-2x float-left"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="card card-inversed">
                            <header class="card-header">
                                <i class="fa fa-cart-plus text-danger"></i>Supplier
                            </header>
                            <div class="card-block" id="div1">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="div_Supplier"></div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="card card-inversed">
                            <header class="card-header">
                                <i class="fa fa-cart-plus text-danger"></i>Queries
                            </header>
                            <div class="card-block" id="div4">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="div_queries"></div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="col-md-12" id="dvRule" style="display: none">
                        <section class="card card-inversed">
                            <div class="tab-content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="dv_ConfItem">
                                            <section class="card card-inversed">
                                                <header class="card-header">
                                                    <i class="fa fa-cart-plus text-danger"></i>Lots And Items Details
                                                </header>
                                                <fieldset class="form-group" style="margin-top: 10px">

                                                    <div class="col-md-1">
                                                        <input class="form-control mandatory" name="LotItem" id="LotItem" type="radio" onclick="getItem('Lot')">
                                                    </div>
                                                    <label class="col-md-2">Lot with Line Item</label>
                                                    <div class="col-md-1">
                                                        <input class="form-control mandatory" name="Lot" type="radio" id="Lot" onclick="getItem('Item')">
                                                    </div>
                                                    <label class="col-md-2">Lot without Line Item</label>
                                                    <div class="col-md-1">
                                                        <input class="form-control mandatory" name="LineItem" id="LineItem" type="radio" onclick="getItem('Line')">
                                                    </div>
                                                    <label class="col-md-2">Line Item</label>
                                                </fieldset>
                                            </section>
                                        </div>
                                        <div id="dv_LineItem" style="display: none">
                                            <section class="card card-inversed">
                                                <header class="card-header">
                                                    <i class="fa fa-cart-plus text-danger"></i>Lot Details
                                                </header>
                                                <div class="card-block" id="div5">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <fieldset class="form-group">
                                                                <label class="col-md-3">Lot Name:</label>
                                                                <div class="col-md-3">
                                                                    <input class="form-control mandatory" type="text" id="txt_LotName">
                                                                </div>
                                                                <label class="col-md-3">Lot Description:</label>
                                                                <div class="col-md-3">
                                                                    <input class="form-control mandatory" type="text" id="txt_LotDesc">
                                                                </div>

                                                            </fieldset>
                                                            <fieldset class="form-group">
                                                                <label class="col-md-3">Commodity:</label>
                                                                <div class="col-md-3">
                                                                    <input class="form-control mandatory" type="text" id="txt_commodity">
                                                                </div>
                                                                <label class="col-md-3">Historic Price:</label>
                                                                <div class="col-md-3">
                                                                    <input class="form-control mandatory text-right number" type="text" id="txt_HistoPrice" value="0">
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <fieldset class="form-group">
                                                <div class="col-md-12 text-center">
                                                    <button type="button" class="btn btn-inline btn-info ladda-button" id="btn_Add" name="btn_Add">Add</button>
                                                </div>
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <div class="col-md-12">
                                                    <%-- <div class="table-responsive" style="height: 143px">--%>
                                                    <table id="tbl_LineItem" class="table table-bordered table-hover table-sm">
                                                        <thead>
                                                            <tr>
                                                                <th>Lot Name</th>
                                                                <th>Lot Description</th>
                                                                <th>Commodity</th>
                                                                <th>Historic Price</th>
                                                                <th>Delete</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                    <%-- </div>--%>
                                                </div>
                                            </fieldset>
                                        </div>

                                        <div id="dv_LotItem" style="display: none">
                                            <section class="card card-inversed">
                                                <header class="card-header">
                                                    <i class="fa fa-cart-plus text-danger"></i>Lot Details
							               
                                                </header>
                                                <div class="card-block" id="div2">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <fieldset class="form-group">
                                                                <label class="col-md-3">Lot Name:</label>
                                                                <div class="col-md-3">
                                                                    <input class="form-control mandatory" type="text" id="txt_lot">
                                                                </div>
                                                                <label class="col-md-3">Lot Description:</label>
                                                                <div class="col-md-3">
                                                                    <input class="form-control mandatory" type="text" id="txt_lotdesc">
                                                                </div>

                                                            </fieldset>
                                                            <fieldset class="form-group">
                                                                <label class="col-md-3">Commodity:</label>
                                                                <div class="col-md-3">
                                                                    <input class="form-control mandatory" type="text" id="txt_lot_commodity">
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section class="card card-inversed">
                                                <header class="card-header">
                                                    <i class="fa fa-cart-plus text-danger"></i>Line Item
                                                </header>
                                                <div class="card-block" id="div3">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <fieldset class="form-group">
                                                                <label class="col-md-3">Item Name:</label>
                                                                <div class="col-md-3">
                                                                    <div class="input-group">
                                                                        <input class="form-control mandatory" type="text" id="txt_lot_name" />
                                                                        <span class="input-group-addon">
                                                                            <a class='fa fa-pencil font-dark' id='href" + srno + "' data-target='#dateWiseItemPopup' onclick="cleardiv()" data-toggle='modal'></a>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-md-3">Quantity:</label>
                                                                <div class="col-md-3">
                                                                    <input class="form-control mandatory text-right number" type="text" value="0" id="txt_lot_qty" />
                                                                </div>
                                                            </fieldset>
                                                            <fieldset class="form-group">
                                                                <label class="col-md-3">Unit:</label>
                                                                <div class="col-md-3">
                                                                    <input class="form-control mandatory" type="text" id="txt_lot_unit" />
                                                                </div>
                                                                <label class="col-md-3">Historic Price:</label>
                                                                <div class="col-md-3">
                                                                    <input class="form-control mandatory text-right number" type="text" value="0" id="txt_lot_histoprice" />
                                                                </div>
                                                            </fieldset>
                                                            <fieldset class="form-group">
                                                                <label class="col-md-3">Documents</label>
                                                                <div class="col-md-3">
                                                                    <a data-toggle="modal" data-target="#lot_Document"><i class="fa fa-paperclip fa-2x float-left"></i></a>
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <fieldset class="form-group">
                                                <div class="col-md-12 text-center">
                                                    <button type="button" class="btn btn-inline btn-info ladda-button" id="btn_AddLot">Add</button>
                                                </div>
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <div class="col-md-12">
                                                    <table id="tbl_LotItem" class="table table-bordered table-hover table-sm">
                                                        <thead>
                                                            <tr>
                                                                <th>Lot Name</th>
                                                                <th>Lot Description</th>
                                                                <th>Commodity</th>
                                                                <th>Item Name</th>
                                                                <th>Quantity</th>
                                                                <th>Unit</th>
                                                                <th>Historic Price</th>
                                                                <th>Documents</th>
                                                                <th>Delete</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <div id="dv_LItem" style="display: none">
                                            <section class="card card-inversed">
                                                <header class="card-header">
                                                    <i class="fa fa-cart-plus text-danger"></i>Line Items
							               <%-- <button type="button" class="modal-close">
                                                <i class="font-icon-close-2 text-danger"></i>
                                            </button>--%>
                                                </header>
                                                <div class="card-block" id="div8">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-md-12">
                                                                <fieldset class="form-group">
                                                                    <label class="col-md-3">Item Name:</label>
                                                                    <div class="col-md-3">
                                                                        <div class="input-group">
                                                                            <input class="form-control mandatory" type="text" id="txt_line_item_name" />
                                                                            <span class="input-group-addon">
                                                                                <a class='fa fa-pencil font-dark' id='href1" + srno + "' data-target='#dateWiseItemPopup' data-toggle='modal'></a>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <label class="col-md-3">Quantity:</label>
                                                                    <div class="col-md-3">
                                                                        <input class="form-control mandatory text-right number" type="text" id="txt_line_qty" value="0" />
                                                                    </div>
                                                                </fieldset>
                                                                <fieldset class="form-group">
                                                                    <label class="col-md-3">Unit:</label>
                                                                    <div class="col-md-3">
                                                                        <input class="form-control mandatory" type="text" id="txt_line_unit" />
                                                                    </div>
                                                                    <label class="col-md-3">Historic Price:</label>
                                                                    <div class="col-md-3">
                                                                        <input class="form-control mandatory text-right number " type="text" id="txt_line_histoprice" value="0" />
                                                                    </div>
                                                                </fieldset>
                                                                <fieldset class="form-group">
                                                                    <label class="col-md-3">Documents</label>
                                                                    <div class="col-md-3">
                                                                        <a data-toggle="modal" data-target="#line_Document"><i class="fa fa-paperclip fa-2x float-left"></i></a>
                                                                    </div>
                                                                </fieldset>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <fieldset class="form-group">
                                                <div class="col-md-12 text-center">
                                                    <button type="button" class="btn btn-inline btn-info ladda-button" id="btn_Add_LItem">Add</button>
                                                </div>
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <div class="col-md-12">
                                                    <table id="tbl_LItem" class="table table-bordered table-hover table-sm">
                                                        <thead>
                                                            <tr>
                                                                <th>Item Name</th>
                                                                <th>Quantity</th>
                                                                <th>Unit</th>
                                                                <th>Historic Price</th>
                                                                <th>Documents</th>
                                                                <th>Delete</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div>
                        <div class="col-md-12 text-center">
                            <%--       <asp:Button ID="btn_save" Text="Save" class="btn btn-inline btn-info ladda-button" 
                               OnClick="Save_Click"
                                runat="server" />--%>
                            <button id="btn_save" name="btn_Show" type="button" runat="server" class="btn btn-inline btn-info ladda-button">Save</button>
                            <%-- <asp:button id="btn_AucPublish" text="Save For Auction" style="display: none" class="btn btn-inline btn-info ladda-button"
                                onclientclick="return saveBtn('Auction','');" onclick="Save_Click" runat="server" />--%>
                            <button type="button" class="btn btn-inline btn-info ladda-button" data-toggle='modal'
                                data-target='#div_viewdata' onclick="PreviewData(0,'');" id="preview" runat="server"
                                style="display: none">
                                <i class='fa fa-file-text-o'></i>Preview</button>
                            <button type="button" class="btn btn-inline btn-info ladda-button" id="btn_Back">
                                Back</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="div_viewdata" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="H6">Published RFQs</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <section class="tabs-section">
                                    <div class="tabs-section-nav tabs-section-nav-icons">
                                        <div class="tbl">
                                            <ul class="nav" role="tablist">
                                                <li class="nav-item" id="li_preview">
                                                    <a class="nav-link active" href="#tabPreview" role="tab" data-toggle="tab" aria-expanded="false">
                                                        <span class="nav-link-in">Preview
                                                        </span>
                                                    </a>
                                                </li>
                                                <li class="nav-item" id="li_vendor">
                                                    <a class="nav-link" href="#tabTime" role="tab" data-toggle="tab" aria-expanded="false">
                                                        <span class="nav-link-in">Vendor Tracking
                                                        </span>
                                                    </a>
                                                </li>
                                                <li class="nav-item" id="li_item">
                                                    <a class="nav-link" href="#tabItem" role="tab" data-toggle="tab" aria-expanded="false">
                                                        <span class="nav-link-in">Live Monitor - RFQ
                                                        </span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade in active" id="tabPreview" aria-expanded="false">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="card card-inversed">
                                                        <header class="card-header">
                                                            <i class="fa fa-cart-plus text-danger"></i>RFQ
                                                        </header>
                                                        <div class="card-block">
                                                            <fieldset class="form-group">
                                                                <label class="col-md-3">RFQ Name:</label>
                                                                <div class="col-md-3">
                                                                    <label id="lbl_rfq" type="label" runat="server"></label>
                                                                </div>
                                                                <label class="col-md-3">Department:</label>
                                                                <div class="col-md-3">
                                                                    <label id="lbl_dept" runat="server"></label>
                                                                </div>
                                                            </fieldset>
                                                            <fieldset class="form-group">
                                                                <label class="col-md-3">Category:</label>
                                                                <div class="col-md-3">
                                                                    <label id="lbl_cat" runat="server"></label>
                                                                </div>
                                                                <label class="col-md-3">Delivery Location:</label>
                                                                <div class="col-md-3">
                                                                    <label id="lbl_deliloc" runat="server"></label>
                                                                </div>

                                                            </fieldset>
                                                            <%-- <fieldset class="form-group">
                                                                <%--<label class="col-md-3">Delivery Time:</label>
                                                                <div class="col-md-3">
                                                                    <label id="lbl_delit" runat="server"></label>
                                                                </div>
                                                               
                                                            </fieldset>--%>
                                                            <fieldset class="form-group">
                                                                <label class="col-md-3">Branch:</label>
                                                                <div class="col-md-3">
                                                                    <label id="lbl_branch" runat="server"></label>
                                                                </div>
                                                                <label class="col-md-3">Region</label>
                                                                <div class="col-md-3">
                                                                    <label id="lbl_region" runat="server"></label>
                                                                </div>
                                                            </fieldset>
                                                            <fieldset class="form-group">
                                                                <%-- <label class="col-md-3">Expected Delivery Date:</label>
                                                                <div class="col-md-3">
                                                                    <label id="lbl_expectd" runat="server"></label>
                                                                </div>--%>
                                                                <label class="col-md-3">Buyer</label>
                                                                <div class="col-md-3">
                                                                    <label id="lbl_buyer" runat="server"></label>
                                                                </div>
                                                                <label class="col-md-3">Currency:</label>
                                                                <div class="col-md-3">
                                                                    <label id="lbl_currency" runat="server"></label>
                                                                </div>
                                                            </fieldset>
                                                            <fieldset class="form-group">
                                                                <label class="col-md-3">Start DateTime:</label>
                                                                <div class="col-md-3">
                                                                    <label id="lbl_start" runat="server"></label>
                                                                </div>
                                                                <label class="col-md-3">End DateTime:</label>
                                                                <div class="col-md-3">
                                                                    <label id="lbl_end" runat="server"></label>
                                                                </div>
                                                            </fieldset>
                                                            <fieldset class="form-group">
                                                                <label class="col-md-3">Status:</label>
                                                                <div class="col-md-3">
                                                                    <label id="lbl_status" runat="server"></label>
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                    <div class="card card-inversed">
                                                        <header class="card-header">
                                                            <i class="fa fa-cart-plus text-danger"></i>Supplier
                                                        </header>
                                                        <div class="card-block">
                                                            <div class="table-responsive" id="div_previewpodetails">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card card-inversed">
                                                        <header class="card-header">
                                                            <i class="fa fa-cart-plus text-danger"></i>Queries
                                                        </header>
                                                        <div class="card-block">
                                                            <div class="table-responsive" id="div_prequeries">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="card card-inversed">
                                                        <header class="card-header">
                                                            <i class="fa fa-cart-plus text-danger"></i>Terms & Conditions
                                                        </header>
                                                        <div class="card-block">
                                                            <div class="table-responsive" id="div_previewterms">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card card-inversed">
                                                        <header class="card-header">
                                                            <i class="fa fa-cart-plus text-danger"></i>Lots And Items Details
                                                        </header>
                                                        <div class="card-block">
                                                            <div class="table-responsive" id="div_lotline">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="tabTime" aria-expanded="false">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="table-responsive" id="div_vendordetails">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="tabItem" aria-expanded="false">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="card card-inversed">
                                                        <header class="card-header">
                                                            <i class="fa fa-cart-plus text-danger"></i>Admin Live - RFQ
                                                        </header>
                                                        <div class="card-block">
                                                            <div class="table-responsive" id="div_vendorlotdetail">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card card-inversed">
                                                        <header class="card-header">
                                                            <i class="fa fa-cart-plus text-danger"></i>Pricing History
                                                        </header>
                                                        <div class="card-block">
                                                            <div class="table-responsive" id="div_pricehist">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btn_publish" Text="Publish" class="btn btn-inline btn-info ladda-button"
                            runat="server" OnClientClick="return publishBtn();this.disabled='true';" />
                        <asp:Button ID="btn_aucsave" Text="Save For Auction" class="btn btn-inline btn-info ladda-button" OnClientClick="return aucpublishBtn();this.disabled='true';"
                            runat="server" />
                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="rfq_Document" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="H1">Upload Documents</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset class="form-group">
                                    <div class="col-md-1">
                                    </div>
                                    <label class="col-md-2">
                                        Description</label>
                                    <div class="col-md-9">
                                        <input class="form-control sucess" id="txt_description" name="txt_description" type="text"
                                            style="width: 60%" runat="server" />
                                    </div>
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="col-md-1">
                                    </div>
                                    <label class="col-md-2">
                                        Attach File</label>
                                    <asp:UpdatePanel ID="upModal" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="col-md-9">
                                                <span class="btn btn-grey fileinput-button">
                                                    <asp:AsyncFileUpload ID="FileUpload1" runat="server" OnClientUploadError="uploadError"
                                                        OnClientUploadStarted="StartUpload" OnClientUploadComplete="UploadComplete" CompleteBackColor="Lime"
                                                        ErrorBackColor="Red" OnUploadedComplete="btnUpload_Click" UploadingBackColor="#66CCFF" />
                                                </span>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <div id="div_Doc" runat="server">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="lot_Document" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="H2">Upload Documents</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset class="form-group">
                                    <div class="col-md-1">
                                    </div>
                                    <label class="col-md-2">
                                        Description</label>
                                    <div class="col-md-9">
                                        <input class="form-control sucess" id="txt_description_lot" name="txt_description_lot"
                                            type="text" style="width: 60%" runat="server" />
                                    </div>
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="col-md-1">
                                    </div>
                                    <label class="col-md-2">
                                        Attach File</label>
                                    <asp:UpdatePanel ID="Updatepanel1" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="col-md-9">
                                                <span class="btn btn-grey fileinput-button">
                                                    <asp:AsyncFileUpload ID="FileUploadLot" runat="server" OnClientUploadError="uploadError"
                                                        OnClientUploadStarted="StartUpload" OnClientUploadComplete="UploadCompleteLot"
                                                        CompleteBackColor="Lime" ErrorBackColor="Red" OnUploadedComplete="btnUploadLot_Click"
                                                        UploadingBackColor="#66CCFF" />
                                                </span>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <div id="div_doclot" runat="server">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="line_Document" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="H3">Upload Documents</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset class="form-group">
                                    <div id="div_viewd" style="display: none">
                                        <input class="form-control sucess" id="txt_num" type="text" runat="server" />
                                    </div>
                                    <div class="col-md-1">
                                    </div>
                                    <label class="col-md-2">
                                        Description</label>
                                    <div class="col-md-9">
                                        <input class="form-control sucess" id="txt_description_line" name="txt_description_line"
                                            type="text" style="width: 60%" runat="server" />
                                    </div>
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="col-md-1">
                                    </div>
                                    <label class="col-md-2">
                                        Attach File</label>
                                    <asp:UpdatePanel ID="Updatepanel3" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="col-md-9">
                                                <span class="btn btn-grey fileinput-button">
                                                    <asp:AsyncFileUpload ID="FileUploadLine" runat="server" OnClientUploadError="uploadError"
                                                        OnClientUploadStarted="StartUpload" OnClientUploadComplete="UploadCompleteLine"
                                                        CompleteBackColor="Lime" ErrorBackColor="Red" OnUploadedComplete="btnUploadLine_Click"
                                                        UploadingBackColor="#66CCFF" />
                                                </span>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <div id="div_docline" runat="server">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="term_Document" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="H4">Upload Documents</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset class="form-group">
                                    <div class="col-md-1">
                                    </div>
                                    <label class="col-md-2">
                                        Description</label>
                                    <div class="col-md-9">
                                        <input class="form-control sucess" id="txt_description_terms" name="txt_description_terms"
                                            type="text" style="width: 60%" runat="server" />
                                    </div>
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="col-md-1">
                                    </div>
                                    <label class="col-md-2">
                                        Attach File</label>
                                    <asp:UpdatePanel ID="Updatepanel2" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="col-md-9">
                                                <span class="btn btn-grey fileinput-button">
                                                    <asp:AsyncFileUpload ID="FileUploadTerms" runat="server" OnClientUploadError="uploadError"
                                                        OnClientUploadStarted="StartUpload" OnClientUploadComplete="UploadCompleteTerm"
                                                        CompleteBackColor="Lime" ErrorBackColor="Red" OnUploadedComplete="btnUploadTerms_Click"
                                                        UploadingBackColor="#66CCFF" />
                                                </span>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <div id="div_docterm" runat="server">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="genaralterms_Document" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="H5">Upload Documents</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset class="form-group">
                                    <div class="col-md-1">
                                    </div>
                                    <label class="col-md-2">
                                        Description</label>
                                    <div class="col-md-9">
                                        <input class="form-control sucess" id="txt_description_gterms" name="txt_description_gterms"
                                            type="text" style="width: 60%" runat="server" />
                                    </div>
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="col-md-1">
                                    </div>
                                    <label class="col-md-2">
                                        Attach File</label>
                                    <asp:UpdatePanel ID="Updatepanel4" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="col-md-9">
                                                <span class="btn btn-grey fileinput-button">
                                                    <asp:AsyncFileUpload ID="FileUploadGenTerms" runat="server" OnClientUploadError="uploadError"
                                                        OnClientUploadStarted="StartUpload" OnClientUploadComplete="UploadCompleteGenTerm"
                                                        CompleteBackColor="Lime" ErrorBackColor="Red" OnUploadedComplete="btnUploadGenTerms_Click"
                                                        UploadingBackColor="#66CCFF" />
                                                </span>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <div id="div_generalterms" runat="server">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="lgpaymentterms_Document" tabindex="-1" role="dialog"
            aria-labelledby="modalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="H7">Upload Documents</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset class="form-group">
                                    <div class="col-md-1">
                                    </div>
                                    <label class="col-md-2">
                                        Description</label>
                                    <div class="col-md-9">
                                        <input class="form-control sucess" id="txt_description_pterms" name="txt_description_pterms"
                                            type="text" style="width: 60%" runat="server" />
                                    </div>
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="col-md-1">
                                    </div>
                                    <label class="col-md-2">
                                        Attach File</label>
                                    <asp:UpdatePanel ID="Updatepanel5" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="col-md-9">
                                                <span class="btn btn-grey fileinput-button">
                                                    <asp:AsyncFileUpload ID="FileUploadPayTerms" runat="server" OnClientUploadError="uploadError"
                                                        OnClientUploadStarted="StartUpload" OnClientUploadComplete="UploadCompletePayTerm"
                                                        CompleteBackColor="Lime" ErrorBackColor="Red" OnUploadedComplete="btnUploadPayTerms_Click"
                                                        UploadingBackColor="#66CCFF" />
                                                </span>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <div id="div_paymentterms" runat="server">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="myAuction" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document" id="dvData">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"></span>
                        </button>
                        <h4 class="modal-title" id="modalLabel"></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="dv_TermsCon">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="#" class="btn btn-rounded" onclick="getAuctionDetails()" data-dismiss="modal">
                            <i class="fa fa-cart-plus"></i>Save</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="lgview_Document" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="H8">Uploaded Documents</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <div id="div_viewdoc" runat="server">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="dateWiseItemPopup" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            <i class="fa fa-close"></i>
                        </button>
                        <h5 class="modal-title">Item Name</h5>
                        <div class="col-md-4">
                            <input class="form-control" id="txt_item_name_type" name="txt_item_name_type" type="text" runat="server" />
                        </div>
                        <div class="col-md-1">
                            <button type="button" class="btn btn-inline btn-info ladda-button" id="btn_item_show" onclick='rowvalue(" + srno + ")'>Show</button>
                        </div>

                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 m-t">
                                <div class="table-responsive" id="div_Items">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">
                            Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div style="display: none">
            <input type="text" id="SessionAdId" runat="server" />
            <input type="text" id="processid" runat="server" />
            <input type="text" id="stepid" runat="server" />
             <input type="text" id="txt_Master_Url" runat="server" />
            <input type="text" id="txt_Vendor_Url" runat="server" />
            <input type="text" id="txt_Wfe_Url" runat="server" />
            <input class="form-control" id="txt_Dept_id" type="text" runat="server" />
            <input class="form-control" id="txt_loc_id" type="text" runat="server" />
            <input class="form-control" id="txt_region_id" type="text" runat="server" />
            <input class="form-control" id="txt_branch_id" type="text" runat="server" />
            <input class="form-control" id="txt_buyer_id" type="text" runat="server" />
            <input class="form-control" id="txt_SupplierXML" type="text" runat="server" />
            <input class="form-control" id="txt_DocumentXML" type="text" runat="server" />
            <input class="form-control" id="txt_pk_id" type="text" runat="server" />
            <input class="form-control" id="txt_id" type="text" runat="server" />
            <input class="form-control" id="txt_btn_name" type="text" runat="server" />
            <input class="form-control" id="txt_lot_unit_id" type="text" runat="server" />
            <input class="form-control" id="txt_line_unit_id" type="text" runat="server" />
            <input class="form-control" id="selected_Item" runat="server" text="0">
            <input class="form-control" id="selected_index" runat="server" text="0">
            <input class="form-control" id="txt_suppcnt" type="text" runat="server" />
            <input class="form-control" id="txt_publish" type="text" runat="server" />
            <input class="form-control" id="txt_termscnt" type="text" runat="server" />
            <input class="form-control" id="txt_RFQname" type="text" runat="server" />
            <input class="form-control" id="txt_checksave" type="text" runat="server" />
            <input class="form-control" id="txt_sdate" type="text" runat="server" />
            <input class="form-control" id="txt_stime" type="text" runat="server" />
            <input class="form-control" id="txt_edate" type="text" runat="server" />
            <input class="form-control" id="txt_etime" type="text" runat="server" />
            <input class="form-control" id="txt_itemcnt" type="text" runat="server" text="0" />
        </div>
    </form>
    <!-- ================== BEGIN BASE JS ================== -->
    <script src="../../RFQassets/js/jquery-2.2.0.js"></script>
    <script src="../../RFQassets/js/underscore.js"></script>
    <script src="../../RFQassets/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="../../RFQassets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"
        type="text/javascript"></script>
    <script src="../../RFQassets/js/lib/bootstrap/bootstrap.min.js"></script>
    <script src="../../RFQassets/js/jquery.dataTables.min.js"></script>
    <script src="../../RFQassets/js/jquery-ui.js"></script>
    <script src="../../RFQassets/js/lib/clockpicker/bootstrap-clockpicker.js"></script>
    <script src="../../JS/Common/Utility.js"></script>
    <script src="../../JS/Common/Document.js"></script>
    <script lang="JavaScript" type="text/javascript" src="../../assets/js/jAlert.js"></script>
    <script lang="JavaScript" type="text/javascript" src="../../assets/js/jAlert-functions.js"></script>
    <script src="../../JS/Process/RFQ/CreateRFQ.js"></script>

</body>
</html>

