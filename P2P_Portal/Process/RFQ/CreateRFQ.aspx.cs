﻿using FSL.Controller;
using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using Newtonsoft.Json;
using System.Web.Script.Services;
using System.Web.Services;
using System.Configuration;
using System.IO;
using System.Collections.Generic;
using System.Linq;

using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;


namespace P2P_Portal.Process.RFQ
{
    public partial class CreateRFQ : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Log4Logger.Activity("RFQStart");
            ActionController.DisablePageCaching(this);
            if (ActionController.IsSessionExpired(Page))
                ActionController.RedirctToLogin(Page);
            else
            {
                if (!IsPostBack)
                {
                    ActionController.SetControlAttributes(Page);
                    txt_Master_Url.Value = System.Configuration.ConfigurationManager.AppSettings["MasterAPI"];
                    txt_Wfe_Url.Value = System.Configuration.ConfigurationManager.AppSettings["WFEAPI"];
                    txt_Vendor_Url.Value = System.Configuration.ConfigurationManager.AppSettings["VENDORAPI"];
                    //apiUrl = txt_Vendor_Url.Value + "Authenticate";
                    SessionAdId.Value = Session["User_ADID"].ToString();
                    DateTime date = DateTime.Now;
                    DateTime newDate = date.AddMonths(12);
                    // fillDocument_Details();
                    processid.Value = Request.Params["processid"].ToString();
                    stepid.Value = Request.Params["stepid"].ToString();
                }
            }
        }
        private void fillDocument_Details()
        {
            try
            {
                string isData = string.Empty;
                string isValid = string.Empty;
                string DisplayData = string.Empty;

                DisplayData = "<table class='display nowrap dataTable' id='tbl_DocumentDtl'><thead><tr class='grey'><th>Description</th><th>File Name</th><th>Delete</th></tr></thead>";
                DisplayData += "</table>";
                div_Doc.InnerHtml = DisplayData;
                DisplayData = "";
            }
            catch (Exception ex)
            {
                FSL.Logging.Logger.WriteEventLog(false, ex);
            }
        }
        [WebMethod]
        public static string[] getRegion(int regid, string apiUrl)
        {
            string[] jsonArray = new string[1];
            try
            {
                HttpClient client = new HttpClient();
                apiUrl = apiUrl + "Region";
                var input = new
                {
                    FK_CLIENT_ID = 1,
                    id = regid
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getRegionInfoForId", inputContent).Result;
                List<ObjectMaster> obj = JsonConvert.DeserializeObject<List<ObjectMaster>>(response.Content.ReadAsStringAsync().Result);
                if ((obj != null) && (obj.ToString() != "[]"))
                {
                    jsonArray[0] = JsonConvert.SerializeObject(obj, Formatting.Indented);
                }
            }
            catch (Exception Ex)
            {

                Logger1.Error(Ex);
                throw;
            }
            return jsonArray;
        }
        [WebMethod]
        public static string[] getBranch(string branchid, string apiUrl)
        {
            string[] jsonArray = new string[1];
            try
            {
                HttpClient client = new HttpClient();
                apiUrl = apiUrl + "Branch";
                var input = new
                {
                    FK_CLIENTID = 1,
                    id = branchid
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getBranchInfoForId", inputContent).Result;
                List<ObjectMaster> obj = JsonConvert.DeserializeObject<List<ObjectMaster>>(response.Content.ReadAsStringAsync().Result);
                if ((obj != null) && (obj.ToString() != "[]"))
                {
                    jsonArray[0] = JsonConvert.SerializeObject(obj, Formatting.Indented);
                }
            }
            catch (Exception Ex)
            {

                Logger1.Error(Ex);
                throw;
            }
            return jsonArray;
        }
        [WebMethod]
        public static string[] getUnit(string apiUrl)
        {
            string[] jsonArray = new string[1];
            try
            {
                HttpClient client = new HttpClient();
                apiUrl = apiUrl + "Unit";
                var input = new
                {
                    FK_CLIENT_ID = 1
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getUnitInfo", inputContent).Result;
                List<ObjectMaster> obj = JsonConvert.DeserializeObject<List<ObjectMaster>>(response.Content.ReadAsStringAsync().Result);
                if ((obj != null) && (obj.ToString() != "[]"))
                {
                    jsonArray[0] = JsonConvert.SerializeObject(obj, Formatting.Indented);
                }
            }
            catch (Exception Ex)
            {

                Logger1.Error(Ex);
                throw;
            }
            return jsonArray;
        }
        [WebMethod]
        public static string[] getBuyer(string apiUrl)
        {
            string[] jsonArray = new string[1];
            try
            {
                HttpClient client = new HttpClient();
                apiUrl = apiUrl + "Employee";
                var input = new
                {
                    CLIENT_ID = 1,
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getEmpInfoForDisplayData", inputContent).Result;

                List<BuyerDTL> obj = (new JavaScriptSerializer()).Deserialize<List<BuyerDTL>>(response.Content.ReadAsStringAsync().Result);
                if ((obj != null) && (obj.ToString() != "[]"))
                {
                    jsonArray[0] = JsonConvert.SerializeObject(obj, Formatting.Indented);
                }
            }
            catch (Exception Ex)
            {

                Logger1.Error(Ex);
                throw;
            }
            return jsonArray;
        }
        [WebMethod]
        public static string[] getDeptDetails(string deptid, string apiUrl)
        {
            string[] jsonArray = new string[1];
            try
            {
                HttpClient client = new HttpClient();
                apiUrl = apiUrl + "Department";
                var input = new
                {
                    FK_CLIENTID = 1,
                    id = deptid
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getDepartmentInfoForId", inputContent).Result;
                List<ObjectMaster> obj = JsonConvert.DeserializeObject<List<ObjectMaster>>(response.Content.ReadAsStringAsync().Result);
                if ((obj != null) && (obj.ToString() != "[]"))
                {
                    jsonArray[0] = JsonConvert.SerializeObject(obj, Formatting.Indented);
                }
            }
            catch (Exception Ex)
            {

                Logger1.Error(Ex);
                throw;
            }
            return jsonArray;
        }
        [WebMethod]
        public static string[] getLocation(int locid, string apiUrl)
        {
            string[] jsonArray = new string[1];
            try
            {
                HttpClient client = new HttpClient();
                apiUrl = apiUrl + "Location";
                var input = new
                {
                    FK_CLIENT_ID = 1,
                    id = locid
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getLocationInfoForId", inputContent).Result;
                List<ObjectMaster> obj = JsonConvert.DeserializeObject<List<ObjectMaster>>(response.Content.ReadAsStringAsync().Result);
                if ((obj != null) && (obj.ToString() != "[]"))
                {
                    jsonArray[0] = JsonConvert.SerializeObject(obj, Formatting.Indented);
                }
            }
            catch (Exception Ex)
            {

                Logger1.Error(Ex);
                throw;
            }
            return jsonArray;
        }
        [WebMethod]
        public static string[] getContactdetails(int suppid, string apiUrl)
        {
            string[] jsonArray = new string[1];
            try
            {
                HttpClient client = new HttpClient();
                apiUrl = apiUrl + "rfq";
                var input = new
                {
                    suppid = suppid
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getAPIContactdetails", inputContent).Result;
                List<ObjectMaster> obj = JsonConvert.DeserializeObject<List<ObjectMaster>>(response.Content.ReadAsStringAsync().Result);
                if ((obj != null) && (obj.ToString() != "[]"))
                {
                    jsonArray[0] = JsonConvert.SerializeObject(obj, Formatting.Indented);
                }
            }
            catch (Exception Ex)
            {

                Logger1.Error(Ex);
                throw;
            }
            return jsonArray;
        }
        [WebMethod]
        public static string[] getItems(string itm_name, string apiUrl)
        {
            string[] jsonArray = new string[1];
            try
            {
                HttpClient client = new HttpClient();
                apiUrl = apiUrl + "Item";
                var input = new
                {
                    FK_CLIENT_ID = 1,
                    label = itm_name
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getItemInfo", inputContent).Result;
                List<ITEMDTL> obj = JsonConvert.DeserializeObject<List<ITEMDTL>>(response.Content.ReadAsStringAsync().Result);
                if ((obj != null) && (obj.ToString() != "[]"))
                {
                    jsonArray[0] = JsonConvert.SerializeObject(obj, Formatting.Indented);
                }
            }
            catch (Exception Ex)
            {

                Logger1.Error(Ex);
                throw;
            }
            return jsonArray;
        }
        [WebMethod]
        public static string[] getRFQDtl(string apimasterUrl, string apivendorUrl)
        {
            string[] jsonArray = new string[3];
            string[] jsonArrayDta = new string[1];
            try
            {
                List<RFQDTL> model = null;
                List<RFQDTL> modeldept = null;
                List<RFQDTL> modeltotal = null;

                //Call Transaction Api for transaction table rfq detail /////
                HttpClient client = new HttpClient();
                apivendorUrl = apivendorUrl + "rfq";
                HttpContent inputContent = new StringContent("", Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apivendorUrl + "/getAPIRFQDtl", inputContent).Result;
                var content = response.Content.ReadAsStringAsync().Result;
                model = JsonConvert.DeserializeObject<List<RFQDTL>>(content);
                if (model.Count != 0)
                {
                    //Call Master Api for Master table details /////
                    HttpClient client1 = new HttpClient();
                    apimasterUrl = apimasterUrl + "Employee";
                    string inputJson = (new JavaScriptSerializer()).Serialize(model);
                    HttpContent inputContent1 = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response1 = client.PostAsync(apimasterUrl + "/getEmpInfoForRFQProcess", inputContent1).Result;
                    modeldept = JsonConvert.DeserializeObject<List<RFQDTL>>(response1.Content.ReadAsStringAsync().Result);
                    jsonArray[0] = JsonConvert.SerializeObject(model, Formatting.Indented);
                    jsonArray[1] = JsonConvert.SerializeObject(modeldept, Formatting.Indented);

                    //Combine both the table details /////
                    HttpClient client2 = new HttpClient();
                    string xmlData = "{\"xmlRfq\": " + jsonArray[0] + ", \"xmlMaster\": " + jsonArray[1] + "}";
                    var input = new
                    {
                        xmlvalue = xmlData,
                    };
                    string inputJson2 = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent2 = new StringContent(inputJson2, Encoding.UTF8, "application/json");
                    HttpResponseMessage response2 = client.PostAsync(apivendorUrl + "/getCombineInfoForRFQ", inputContent2).Result;
                    modeltotal = JsonConvert.DeserializeObject<List<RFQDTL>>(response2.Content.ReadAsStringAsync().Result);
                    jsonArrayDta[0] = JsonConvert.SerializeObject(modeltotal, Formatting.Indented);
                }
            }
            catch (Exception Ex)
            {

                Logger1.Error(Ex);
                throw;
            }
            return jsonArrayDta;
        }
        [WebMethod]
        public static string[] getRFQTermsDetails(int pkid, string apiUrl)
        {
            string[] jsonArray = new string[1];
            try
            {
                HttpClient client = new HttpClient();
                apiUrl = apiUrl + "rfq";
                var input = new
                {
                    pkid = pkid
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent("", Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getAPIRFQTermsDetails", inputContent).Result;
                List<RFQDTL> obj = JsonConvert.DeserializeObject<List<RFQDTL>>(response.Content.ReadAsStringAsync().Result);
                if ((obj != null) && (obj.ToString() != "[]"))
                {
                    jsonArray[0] = JsonConvert.SerializeObject(obj, Formatting.Indented);
                }
            }
            catch (Exception Ex)
            {

                Logger1.Error(Ex);
                throw;
            }
            return jsonArray;
        }
        [WebMethod]
        public static string[] getRFQDtlPublish(string apiUrl)
        {
            string[] jsonArray = new string[1];
            try
            {
                HttpClient client = new HttpClient();
                apiUrl = apiUrl + "rfq";
                HttpContent inputContent = new StringContent("", Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getAPIRFQDtlPublish", inputContent).Result;
                List<RFQDTL> obj = JsonConvert.DeserializeObject<List<RFQDTL>>(response.Content.ReadAsStringAsync().Result);
                if ((obj != null) && (obj.ToString() != "[]"))
                {
                    jsonArray[0] = JsonConvert.SerializeObject(obj, Formatting.Indented);
                }
            }
            catch (Exception Ex)
            {

                Logger1.Error(Ex);
                throw;
            }
            return jsonArray;
        }
        [WebMethod]
        public static string[] getTermsCon(string apiUrl)
        {
            string[] jsonArray = new string[3];
            try
            {
                //string[] byteArray = new string[3];
                HttpClient client = new HttpClient();
                apiUrl = apiUrl + "TermsCondition";
                HttpContent inputContent = new StringContent("", Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getAPITermsCon", inputContent).Result;
                var values = JsonConvert.DeserializeObject<string>(response.Content.ReadAsStringAsync().Result);
                if ((values != null) && (values.ToString() != "[]"))
                {
                    string[] str = values.Split('#');
                    jsonArray[0] = str[0];
                    jsonArray[1] = str[1];
                    jsonArray[2] = str[2];
                }
            }
            catch (Exception Ex)
            {

                Logger1.Error(Ex);
                throw;
            }
            return jsonArray;
        }
        [WebMethod]
        public static string Save_click(string XmlValue, string Pkid, string Rfq_name, string Test_project, int deptid, int locid, string catname, string sdate, string stime, string edate, string etime, string buyer, int branchid, int regid, string curren, string ad_id, string publish_value, int processid, int stepid, string apiUrl, string apiwfeUrl)
        {
            string Result = string.Empty;
            try
            {
                HttpClient client = new HttpClient();
                apiUrl = apiUrl + "rfq";
                var input = new
                {
                    Pkid = Pkid,
                    Rfq_name = Rfq_name,
                    Test_project = Test_project,
                    deptid = deptid,
                    locid = locid,
                    catname = catname,
                    sdate = sdate,
                    stime = stime,
                    edate = edate,
                    etime = etime,
                    buyer = buyer,
                    branchid = branchid,
                    regid = regid,
                    curren = curren,
                    ad_id = ad_id,
                    xmldata = XmlValue,
                    publish_value = publish_value,
                    processid = processid,
                    step_id = stepid,
                    apiUrl= apiwfeUrl
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/APISaveClick", inputContent).Result;
                Result = response.Content.ReadAsStringAsync().Result.ToString();
                Result = Result.Replace("\"", "");
            }
            catch (Exception Ex)
            {

                Logger1.Error(Ex);
                throw;
            }
            return Result;
        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                //Log4Logger.Activity("UploadRFQ");
                string activeDir = ConfigurationManager.AppSettings["DOCPATH"].ToString();
                Int32 flength = FileUpload1.PostedFile.ContentLength;
                string foname = string.Empty;
                string path = string.Empty;
                //if (txt_pk_id.Value == "0")
                //{

                //}
                path = activeDir + "RFQ\\";
                //else
                //{

                //    path = activeDir + "RFQ\\" + txt_pk_id.Value + "\\";
                //}
                string filename = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName.ToString());
                filename = filename.Replace("(", "");
                filename = filename.Replace(")", "");
                filename = filename.Replace("&", "");
                filename = filename.Replace("+", "");
                filename = filename.Replace("/", "");
                filename = filename.Replace("\\", "");
                filename = filename.Replace("'", "");
                filename = filename.Replace("  ", "");
                filename = filename.Replace(" ", "");
                filename = filename.Replace("#", "");
                filename = filename.Replace("$", "");
                filename = filename.Replace("~", "");
                filename = filename.Replace("%", "");
                filename = filename.Replace("''", "");
                filename = filename.Replace(":", "");
                filename = filename.Replace("*", "");
                filename = filename.Replace("?", "");
                filename = filename.Replace("<", "");
                filename = filename.Replace(">", "");
                filename = filename.Replace("{", "");
                filename = filename.Replace("}", "");
                filename = filename.Replace(",", "");
                DataTable dt = (DataTable)Session["UploadedFiles"];

                FileUpload1.SaveAs(path + filename);
                ClearContents(sender as Control);
            }
            catch (Exception Ex)
            {
                FSL.Logging.Logger.WriteEventLog(false, Ex);
            }

        }
        protected void btnUploadLine_Click(object sender, EventArgs e)
        {
            try
            {
                // Log4Logger.Activity("UploadLineRFQ");
                string activeDir = ConfigurationManager.AppSettings["DOCPATH"].ToString();
                Int32 flength = FileUploadLine.PostedFile.ContentLength;

                string path = string.Empty;
                path = activeDir + "RFQ\\";
                string filename = System.IO.Path.GetFileName(FileUploadLine.PostedFile.FileName.ToString());
                filename = filename.Replace("(", "");
                filename = filename.Replace(")", "");
                filename = filename.Replace("&", "");
                filename = filename.Replace("+", "");
                filename = filename.Replace("/", "");
                filename = filename.Replace("\\", "");
                filename = filename.Replace("'", "");
                filename = filename.Replace("  ", "");
                filename = filename.Replace(" ", "");
                filename = filename.Replace("#", "");
                filename = filename.Replace("$", "");
                filename = filename.Replace("~", "");
                filename = filename.Replace("%", "");
                filename = filename.Replace("''", "");
                filename = filename.Replace(":", "");
                filename = filename.Replace("*", "");
                filename = filename.Replace("?", "");
                filename = filename.Replace("<", "");
                filename = filename.Replace(">", "");
                filename = filename.Replace("{", "");
                filename = filename.Replace("}", "");
                filename = filename.Replace(",", "");
                DataTable dt = (DataTable)Session["UploadedFiles"];

                FileUploadLine.SaveAs(path + filename);
                ClearContents(sender as Control);
            }
            catch (Exception Ex)
            {
                FSL.Logging.Logger.WriteEventLog(false, Ex);
            }

        }
        protected void btnUploadLot_Click(object sender, EventArgs e)
        {
            try
            {
                // Log4Logger.Activity("UploadLotRFQ");
                string activeDir = ConfigurationManager.AppSettings["DOCPATH"].ToString();
                Int32 flength = FileUploadLot.PostedFile.ContentLength;

                string path = string.Empty;
                path = activeDir + "RFQ\\";
                string filename = System.IO.Path.GetFileName(FileUploadLot.PostedFile.FileName.ToString());
                filename = filename.Replace("(", "");
                filename = filename.Replace(")", "");
                filename = filename.Replace("&", "");
                filename = filename.Replace("+", "");
                filename = filename.Replace("/", "");
                filename = filename.Replace("\\", "");
                filename = filename.Replace("'", "");
                filename = filename.Replace("  ", "");
                filename = filename.Replace(" ", "");
                filename = filename.Replace("#", "");
                filename = filename.Replace("$", "");
                filename = filename.Replace("~", "");
                filename = filename.Replace("%", "");
                filename = filename.Replace("''", "");
                filename = filename.Replace(":", "");
                filename = filename.Replace("*", "");
                filename = filename.Replace("?", "");
                filename = filename.Replace("<", "");
                filename = filename.Replace(">", "");
                filename = filename.Replace("{", "");
                filename = filename.Replace("}", "");
                filename = filename.Replace(",", "");
                DataTable dt = (DataTable)Session["UploadedFiles"];

                FileUploadLot.SaveAs(path + filename);
                ClearContents(sender as Control);
            }
            catch (Exception Ex)
            {
                FSL.Logging.Logger.WriteEventLog(false, Ex);
            }

        }
        protected void btnUploadTerms_Click(object sender, EventArgs e)
        {
            try
            {
                //Log4Logger.Activity("UploadTermsRFQ");
                string activeDir = ConfigurationManager.AppSettings["DOCPATH"].ToString();
                Int32 flength = FileUploadTerms.PostedFile.ContentLength;

                string path = string.Empty;
                path = activeDir + "RFQ\\";
                string filename = System.IO.Path.GetFileName(FileUploadTerms.PostedFile.FileName.ToString());
                filename = filename.Replace("(", "");
                filename = filename.Replace(")", "");
                filename = filename.Replace("&", "");
                filename = filename.Replace("+", "");
                filename = filename.Replace("/", "");
                filename = filename.Replace("\\", "");
                filename = filename.Replace("'", "");
                filename = filename.Replace("  ", "");
                filename = filename.Replace(" ", "");
                filename = filename.Replace("#", "");
                filename = filename.Replace("$", "");
                filename = filename.Replace("~", "");
                filename = filename.Replace("%", "");
                filename = filename.Replace("''", "");
                filename = filename.Replace(":", "");
                filename = filename.Replace("*", "");
                filename = filename.Replace("?", "");
                filename = filename.Replace("<", "");
                filename = filename.Replace(">", "");
                filename = filename.Replace("{", "");
                filename = filename.Replace("}", "");
                filename = filename.Replace(",", "");
                DataTable dt = (DataTable)Session["UploadedFiles"];

                FileUploadTerms.SaveAs(path + filename);
                ClearContents(sender as Control);
            }
            catch (Exception Ex)
            {
                FSL.Logging.Logger.WriteEventLog(false, Ex);
            }

        }
        protected void btnUploadGenTerms_Click(object sender, EventArgs e)
        {
            try
            {
                //Log4Logger.Activity("UploadGenTermsRFQ");
                string activeDir = ConfigurationManager.AppSettings["DOCPATH"].ToString();
                Int32 flength = FileUploadGenTerms.PostedFile.ContentLength;

                string path = string.Empty;
                path = activeDir + "RFQ\\";
                string filename = System.IO.Path.GetFileName(FileUploadGenTerms.PostedFile.FileName.ToString());
                filename = filename.Replace("(", "");
                filename = filename.Replace(")", "");
                filename = filename.Replace("&", "");
                filename = filename.Replace("+", "");
                filename = filename.Replace("/", "");
                filename = filename.Replace("\\", "");
                filename = filename.Replace("'", "");
                filename = filename.Replace("  ", "");
                filename = filename.Replace(" ", "");
                filename = filename.Replace("#", "");
                filename = filename.Replace("$", "");
                filename = filename.Replace("~", "");
                filename = filename.Replace("%", "");
                filename = filename.Replace("''", "");
                filename = filename.Replace(":", "");
                filename = filename.Replace("*", "");
                filename = filename.Replace("?", "");
                filename = filename.Replace("<", "");
                filename = filename.Replace(">", "");
                filename = filename.Replace("{", "");
                filename = filename.Replace("}", "");
                filename = filename.Replace(",", "");
                DataTable dt = (DataTable)Session["UploadedFiles"];

                FileUploadGenTerms.SaveAs(path + filename);
                ClearContents(sender as Control);
            }
            catch (Exception Ex)
            {
                FSL.Logging.Logger.WriteEventLog(false, Ex);
            }

        }
        protected void btnUploadPayTerms_Click(object sender, EventArgs e)
        {
            try
            {
                //  Log4Logger.Activity("UploadPayTermsRFQ");
                string activeDir = ConfigurationManager.AppSettings["DOCPATH"].ToString();
                Int32 flength = FileUploadPayTerms.PostedFile.ContentLength;

                string path = string.Empty;
                path = activeDir + "RFQ\\";
                string filename = System.IO.Path.GetFileName(FileUploadPayTerms.PostedFile.FileName.ToString());
                filename = filename.Replace("(", "");
                filename = filename.Replace(")", "");
                filename = filename.Replace("&", "");
                filename = filename.Replace("+", "");
                filename = filename.Replace("/", "");
                filename = filename.Replace("\\", "");
                filename = filename.Replace("'", "");
                filename = filename.Replace("  ", "");
                filename = filename.Replace(" ", "");
                filename = filename.Replace("#", "");
                filename = filename.Replace("$", "");
                filename = filename.Replace("~", "");
                filename = filename.Replace("%", "");
                filename = filename.Replace("''", "");
                filename = filename.Replace(":", "");
                filename = filename.Replace("*", "");
                filename = filename.Replace("?", "");
                filename = filename.Replace("<", "");
                filename = filename.Replace(">", "");
                filename = filename.Replace("{", "");
                filename = filename.Replace("}", "");
                filename = filename.Replace(",", "");
                DataTable dt = (DataTable)Session["UploadedFiles"];

                FileUploadPayTerms.SaveAs(path + filename);
                ClearContents(sender as Control);
            }
            catch (Exception Ex)
            {
                FSL.Logging.Logger.WriteEventLog(false, Ex);
            }

        }
        private void ClearContents(Control control)
        {
            for (var i = 0; i < Session.Keys.Count; i++)
            {
                if (Session.Keys[i].Contains(control.ClientID))
                {
                    Session.Remove(Session.Keys[i]);
                    break;
                }
            }
        }
        public class ObjectMaster
        {
            public string label { get; set; }
            public int id { get; set; }
        }

        public class BuyerDTL
        {
            public string label { get; set; }
            public string id { get; set; }
            public int FK_DEPARTMENT_ID { get; set; }
            public int FK_BRANCH_ID { get; set; }
            public int FK_REGION_ID { get; set; }
            public int FK_LOCATION_ID { get; set; }
        }
        public class ITEMDTL
        {
            public int PK_ITMID { get; set; }
            public string label { get; set; }
            public string ITMCODE { get; set; }
            public string ITMCATNAME { get; set; }
            public string ITMSUBCATNAME { get; set; }
        }
        public class RFQDTL
        {
            public int PK_RFQ_ID { get; set; }
            public string RFQ_NAME { get; set; }
            public string TEST_PROJECT { get; set; }
            public string DEPT_NAME { get; set; }
            public string EMP_NAME { get; set; }
            public string BUYER { get; set; }
            public string FK_CATEGORY_ID { get; set; }
            public string REGION { get; set; }
            public string BRANCH_NAME { get; set; }
            public string CURRENCY { get; set; }
            public string LOCATION_NAME { get; set; }
            public string DELIVERY_TIME { get; set; }
            public string EXP_DELIVERY_DATE { get; set; }
            public string START_DATE { get; set; }
            public string START_TIME { get; set; }
            public string END_DATE { get; set; }
            public string END_TIME { get; set; }
            public string AUTO_OPEN { get; set; }
            public int PK_DEPT_ID { get; set; }
            public int PK_BRANCHID { get; set; }
            public int PK_REGIONID { get; set; }
            public int PK_LOCATIONID { get; set; }
            public string STATUS { get; set; }
            public string AD_ID { get; set; }
            public string PUBLISH_DATE { get; set; }
            public int ACCEPT_TERM { get; set; }
            public string EMAIL_ID { get; set; }
        }
        [JsonArray]
        public class RFQDATA { public List<RFQDTL> JSON; }
        public class RFQDEPT
        {
            public int PK_RFQ_ID { get; set; }
            public string RFQ_NAME { get; set; }
            public string TEST_PROJECT { get; set; }
            public string DEPT_NAME { get; set; }
            public string EMP_NAME { get; set; }
            public string BUYER { get; set; }
            public string FK_CATEGORY_ID { get; set; }
            public string REGION { get; set; }
            public string BRANCH_NAME { get; set; }
            public string CURRENCY { get; set; }
            public string LOCATION_NAME { get; set; }
            public string DELIVERY_TIME { get; set; }
            public string EXP_DELIVERY_DATE { get; set; }
            public string START_DATE { get; set; }
            public string START_TIME { get; set; }
            public string END_DATE { get; set; }
            public string END_TIME { get; set; }
            public string AUTO_OPEN { get; set; }
            public int PK_DEPT_ID { get; set; }
            public int PK_BRANCHID { get; set; }
            public int PK_REGIONID { get; set; }
            public int PK_LOCATIONID { get; set; }
            public string STATUS { get; set; }
            public string AD_ID { get; set; }
            public string PUBLISH_DATE { get; set; }
            public int ACCEPT_TERM { get; set; }
            public string EMAIL_ID { get; set; }
        }
        [JsonArray]
        public class RFQDEPTDATA { public List<RFQDEPT> JSON; }
    }
}