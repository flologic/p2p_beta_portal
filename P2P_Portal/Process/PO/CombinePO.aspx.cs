﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace P2P_Portal.Process.PO
{
    public partial class CombinePO : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            txt_Master_URL.Value = System.Configuration.ConfigurationManager.AppSettings["MasterAPI"];
            txt_Client_ID.Value = System.Configuration.ConfigurationManager.AppSettings["Client_ID"];
            txt_P2P_Url.Value = System.Configuration.ConfigurationManager.AppSettings["P2PAPI"];

        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                string activeDir = ConfigurationManager.AppSettings["DOCPATH"].ToString();
                Int32 flength = FileUpload1.PostedFile.ContentLength;

                string path = string.Empty;
                path = activeDir + "\\" + "PO\\";
                string filename = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName.ToString());
                filename = filename.Replace("(", "");
                filename = filename.Replace(")", "");
                filename = filename.Replace("&", "");
                filename = filename.Replace("+", "");
                filename = filename.Replace("/", "");
                filename = filename.Replace("\\", "");
                filename = filename.Replace("'", "");
                filename = filename.Replace("  ", "");
                filename = filename.Replace(" ", "");
                filename = filename.Replace("#", "");
                filename = filename.Replace("$", "");
                filename = filename.Replace("~", "");
                filename = filename.Replace("%", "");
                filename = filename.Replace("''", "");
                filename = filename.Replace(":", "");
                filename = filename.Replace("*", "");
                filename = filename.Replace("?", "");
                filename = filename.Replace("<", "");
                filename = filename.Replace(">", "");
                filename = filename.Replace("{", "");
                filename = filename.Replace("}", "");
                filename = filename.Replace(",", "");
                DataTable dt = (DataTable)Session["UploadedFiles"];

                FileUpload1.SaveAs(path + filename);
                //ClearContents(sender as Control);
            }
            catch (Exception Ex)
            {
                //Logger.WriteEventLog(false, Ex);
            }
        }

        [WebMethod]
        public static string[] getCatData(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "ItemCategory";
                    var input = new
                    {
                        FK_CLIENT_ID = Client_ID
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/getItemCategoryInfo", inputContent).Result;
                    jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
                }
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string[] getSubCatData(string Client_ID, string Master_API, string catid)
        {
            string[] jsonArray = new string[1];
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "ItemSubCategory";
                    var input = new
                    {
                        FK_CLIENT_ID = Client_ID,
                        FK_ITMCATID = catid
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/getItemSubCategoryInfo", inputContent).Result;
                    jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
                }
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string[] getSuplData(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "Supplier";
                    var input = new
                    {
                        FK_ClientID = Client_ID,
                        label = ""
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/getSupplierInfo", inputContent).Result;
                    jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
                }
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string[] getSuplDtlData(string Client_ID, string Master_API, int pksuplid)
        {
            string[] jsonArray = new string[1];
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "Supplier";
                    var input = new
                    {
                        FK_SUPPLIER_HDR_ID = pksuplid
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/getSupplierDTLInfo", inputContent).Result;
                    jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
                }
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return jsonArray;
        }


        [WebMethod]
        public static string[] GetEmployeeData(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Employee";
                var input = new
                {
                    AD_ID = "",
                    label = ""
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getEmpInfo", inputContent).Result;
                jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            return jsonArray;
        }


        [WebMethod]
        public static string[] GetTaxData(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "Tax";
                var input = new
                {
                    FK_CLIENT_ID = Client_ID,
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getTaxInfo", inputContent).Result;
                jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string[] GetPOTermsData(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            using (var client = new HttpClient())
            {
                string apiUrl = Master_API + "TermsCondition";
                var input = new
                {
                    FK_CLIENT_ID = Client_ID,
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getTermsConditionInfo", inputContent).Result;
                jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string[] getReqDetailData(string Client_ID, string Master_API, string Trans_API, string catdata, string subcatdata, int supplierpk)
        {
            string[] jsonArray = new string[1];
            using (var client = new HttpClient())
            {
                string apiUrl = Trans_API + "CombinePO";//"http://localhost:51092/api/CombinePO";
                var input = new
                {
                    Client_ID = Client_ID,
                    Master_API = Master_API,
                    Trans_API = Trans_API,
                    FK_ITEM_CAT = catdata,
                    FK_ITEM_SUB_CAT = subcatdata,
                    FK_SUPPLIER_HDR_ID = supplierpk

                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/getprocessdata", inputContent).Result;
                jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string[] gettemplateData(string Client_ID, string Master_API)
        {
            string[] jsonArray = new string[1];
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = Master_API + "Template";
                    var input = new
                    {
                        FK_CLIENTID = Client_ID
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/getTempHdrInfo", inputContent).Result;
                    jsonArray[0] = JsonConvert.SerializeObject(response.Content.ReadAsStringAsync().Result);
                }
            }
            catch (Exception ex)
            {
                Logger1.Error(ex);
            }
            return jsonArray;
        }
    }

}