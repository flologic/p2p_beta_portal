﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CombinePO.aspx.cs" Inherits="P2P_Portal.Process.PO.CombinePO" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Combine PO</title>
    <link href="../../assets/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../assets/plugins/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet" />
    <link href="../../assets/css/style.css" rel="stylesheet" />
    <link href="../../assets/css/jquery-ui.css" rel="stylesheet" />
    <link href="../../assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="../../assets/css/jAlert.css" rel="stylesheet" />
</head>
<body style="overflow-x: hidden">
    <form id="form1" runat="server">
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ToolkitScriptManager>
        <div class="row bg-title">
            <div class="col-md-12">
                <div id="Div1" class="panel panel-default panel-border-top" runat="server">
                    <div class="panel-heading">
                        Combine PO
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-2" style="display: none">
                                <div class="white-box-report">
                                    <div class="panel-heading">
                                        <span><i class="fa fa-2x fa-search"></i>Category
                                                <input type="text" id="txt_category_srch" class="form-control input-sm" /></span>
                                    </div>
                                    <div class="table-responsive" id="div_cat" style="height: 150px; overflow-y: scroll">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2" style="display: none">
                                <div class="white-box-report">
                                    <div class="panel-heading">
                                        <span><i class="fa fa-2x fa-search"></i>Sub-Category
                                                <input type="text" id="txt_subcat_srch" class="form-control input-sm" /></span>
                                    </div>
                                    <div class="table-responsive" id="div_subcat" style="height: 150px; overflow-y: scroll">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-horizontal">

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Supplier Name:</label>
                                        <div class="col-md-3">
                                            <input id="sel_Supplier" type="text" class="form-control input-sm mandatory" placeholder='Enter Minimum 3 Characters' />
                                        </div>
                                        <label class="col-md-2 control-label">Supplier Branch:</label>
                                        <div class="col-md-3">
                                            <input id="sel_Branch" type="text" class="form-control input-sm mandatory" placeholder='Select Supplier Branch' />
                                        </div>
                                    </div>
                                    <div class="form-group">

                                        <label class="col-md-2 control-label">Contact Person:</label>
                                        <div class="col-md-3">
                                            <input id="txt_contact_person" type="text" class="form-control input-sm" readonly />
                                        </div>
                                        <label class="col-md-2 control-label">Auth.Signatory:</label>
                                        <div class="col-md-3">
                                            <input id="sel_authsign" type="text" class="form-control input-sm mandatory" placeholder='Enter Minimum 3 Characters' />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Quotation Date:</label>
                                        <div class="col-md-3">
                                            <div class='input-group'>
                                                <input class='form-control input-sm datepicker-rtl' type='text' id='txt_Q_date' readonly />
                                                <span class='input-group-addon'><i class='fa fa-calendar text-info'></i></span>
                                            </div>
                                        </div>
                                        <label class="col-md-2 control-label">Quotation No.:</label>
                                        <div class="col-md-3">
                                            <input id="txt_Q_No" type="text" class="form-control input-sm" placeholder='Enter Quatation No.' />
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Subject:</label>
                                        <div class="col-md-3">
                                            <input id="txt_Subject" type="text" class="form-control input-sm" placeholder='Enter Subject' />
                                        </div>
                                        <label class="col-md-2 control-label">Po Date:</label>
                                        <div class="col-md-3">
                                            <div class='input-group'>
                                                <input class='form-control input-sm datepicker-rtl' runat="server" type='text' id='txt_Po_Date' readonly />
                                                <span class='input-group-addon'><i class='fa fa-calendar text-info'></i></span>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label m-t-10">Documents:</label>
                                        <div class="col-md-3 m-t-10">
                                            <a href="#div_UploadDocument" data-toggle="modal">
                                                <img id="Img1" src="../../assets/images/attachment_non_sel.png" title="Attach file." alt="Attach file." height="20" width="20" /></a>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button id="btn_Show" name="btn_Show" type="button" class="fcbtn btn btn-info btn-outline btn-1b">Show</button>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-md-12">
                                <div id="Div4" class="panel panel-default panel-border-top" runat="server">
                                    <div class="panel-heading">
                                        REQUISITION DETAILS
                                        <div class="panel-action" style="float: right; margin-top: -6px">
                                            <span>Distribution  <i class="fa fa-2x fa-share"></i></span>
                                            <a href='#Div_Distribution' data-toggle='modal'>
                                                <img src="../../assets/images/distribution.png" /></a>
                                        </div>
                                    </div>
                                    <div class="panel-body" id="div_Req_Details" style="display: none;">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <div id="div_displayReqDtl" style="height: 180px; overflow-y: scroll">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-md-3">
                                <div class="panel panel-default panel-border-top">
                                    <div class="panel-heading">
                                        <i class="fa fa-2x fa-percent"></i>Taxes / Charges
                                    </div>
                                    <div class="panel-body" style="height: 200px;">
                                        <div class="col-md-12">
                                            <div class="table-responsive" id="div_tax" style="height: 180px; overflow-y: scroll">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="panel panel-default panel-border-top">
                                    <div class="panel-heading">
                                        <i class="fa fa-2x fa-percent"></i>PO Terms
                                    </div>
                                    <div class="panel-body" style="height: 200px;">
                                        <div class="col-md-12">
                                            <div class="table-responsive" id="div_terms" style="height: 180px; overflow-y: scroll">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="panel panel-default panel-border-top">
                                    <div class="panel-heading">
                                        <i class="fa fa-2x fa-pencil"></i>Other Terms
                                    </div>
                                    <div class="panel-body" style="height: 200px;">
                                        <div class="col-md-12">
                                            <textarea id="txt_other_terms" class="form-control" rows="8" placeholder='Enter Other Terms'></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="panel panel-default panel-border-top">
                                    <div class="panel-heading">
                                        <i class="fa fa-2x fa-inr"></i>Charge
                                    </div>
                                    <div class="panel-body" style="height: 200px;">
                                        <div class="col-md-12">
                                            <div class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-md-6">Installation Charges</label>
                                                    <div class="col-md-6">
                                                        <div class="input-group">
                                                            <input type="text" value="0.00" id="inst_charges" class="form-control input-sm text-right" onkeyup="calculateAmount()" />
                                                            <span class="input-group-addon input-sm"><i class="fa fa-inr"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-6">Transportation Charges</label>
                                                    <div class="col-md-6">
                                                        <div class="input-group">
                                                            <input type="text" value="0.00" id="trans_charges" class="form-control input-sm text-right" onkeyup="calculateAmount()" />
                                                            <span class="input-group-addon input-sm"><i class="fa fa-inr"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-6">Discount</label>
                                                    <div class="col-md-6">
                                                        <div class="input-group">
                                                            <input type="text" value="0.00" id="discount" class="form-control input-sm text-right" onkeyup="calculateAmount()" />
                                                            <span class="input-group-addon input-sm"><i class="fa fa-inr"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-6">Total Amount</label>
                                                    <div class="col-md-6">
                                                        <div class="input-group">
                                                            <input type="text" value="0.00" id="total_amount" class="form-control input-sm text-right" onkeyup="calculateAmount()" readonly />
                                                            <span class="input-group-addon input-sm"><i class="fa fa-inr"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-6">Landed Cost</label>
                                                    <div class="col-md-6">
                                                        <div class="input-group">
                                                            <input type="text" value="0.00" id="landed_cost" class="form-control input-sm text-right" onkeyup="calculateAmount()" readonly />
                                                            <span class="input-group-addon input-sm"><i class="fa fa-inr"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="Div2" class="panel panel-default panel-border-top" runat="server">
                                    <div class="panel-heading">
                                        Action
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label m-t-10">Select Action :</label>
                                                <div class="col-md-2 m-t-10">
                                                    <select id="ddlAction" class="form-control input-sm mandatory">
                                                        <option value="0">--Select One--</option>
                                                        <option value="Submit">Submit</option>
                                                        <option value="Save As Draft">Save As Draft</option>
                                                    </select>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button id="btn_Save" name="btn_Show" type="button" class="fcbtn btn btn-info btn-outline btn-1b">Save</button>
                                <button id="btn_Cancel" name="btn_Show" type="button" class="fcbtn btn btn-info btn-outline btn-1b">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div style="display: none;" class="modal" id="div_UploadDocument">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Document Upload</h5>
                        </div>
                        <div class="modal-body">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <div class="col-md-1"></div>
                                    <label class="col-md-2">Description</label>
                                    <div class="col-md-6">
                                        <div id="div_CustomerId">
                                            <input class="form-control input-sm" id="txt_description" name="txt_description" type="text" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-1"></div>
                                    <label class="col-md-2">Attach File</label>
                                    <asp:UpdatePanel ID="upModal" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="col-md-7">
                                                <span class="btn btn-grey fileinput-button">
                                                    <asp:AsyncFileUpload ID="FileUpload1" runat="server" OnClientUploadError="uploadError"
                                                        OnClientUploadStarted="StartUpload" OnClientUploadComplete="UploadComplete" CompleteBackColor="Lime"
                                                        ErrorBackColor="Red" OnUploadedComplete="btnUpload_Click"
                                                        UploadingBackColor="#66CCFF" />
                                                </span>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-1"></div>
                                    <p style="color: red">
                                        <label>Upload .pdf,.jpeg,.jpg,.png formats only.</label>
                                    </p>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <div id="div_Doc" runat="server"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="javascript:;" class="fcbtn btn btn-info btn-outline btn-1b" data-dismiss="modal">Close</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div id="Div_Distribution" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Distribution</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <div id="Div_Distr" runat="server"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:;" class="fcbtn btn btn-info btn-outline btn-1b" data-dismiss="modal">Close</a>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal_Template" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog" style="width: 50%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title"><b>Template Details</b></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <input type="hidden" id="edit_template_id" />
                                        <textarea id="edit_template_data" class='form-control input-sm' rows="5"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="fcbtn btn btn-info btn-outline btn-1b" onclick="updateTemplate()">Save</button>
                        <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div style="display: none">
            <input type="text" id="SessionAdId" runat="server" />
            <input type="text" id="txt_Client_ID" runat="server" />
            <input type="text" id="txt_Master_URL" runat="server" />
            <input type="text" id="txt_P2P_Url" runat="server" />
        </div>
        <script lang="JavaScript" type="text/javascript" src="../../assets/js/jquery.min.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/js/jquery-ui.js"></script>
        <script src="../../assets/js/underscore.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/js/jAlert.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/js/jAlert-functions.js"></script>
        <script src="../../JS/Common/document-attach.js"></script>
        <script src="../../JS/Common/Utility.js"></script>
        <script src="../../JS/Common/Vaildation.js"></script>
        <script src="../../JS/Process/PO/CombinePO.js"></script>
    </form>
</body>
</html>
