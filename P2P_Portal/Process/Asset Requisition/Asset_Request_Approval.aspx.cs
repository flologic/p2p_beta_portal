﻿using FSL.Controller;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace P2P_Portal.Process.Asset_Requisition
{
    public partial class Asset_Request_Approval : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ActionController.IsSessionExpired(Page))
                ActionController.RedirctToLogin(Page);
            else
            {
                if (!IsPostBack)
                {
                    txt_Master_Url.Value = System.Configuration.ConfigurationManager.AppSettings["MasterAPI"];
                    txt_P2P_Url.Value = System.Configuration.ConfigurationManager.AppSettings["P2PAPI"];
                    SessionAdId.Value = Session["User_ADID"].ToString();
                    txt_Email.Value = Session["EmailID"].ToString();
                    txt_ProcessID.Value = Convert.ToString(Request.QueryString["processid"]);
                    txt_InstanceID.Value = Convert.ToString(Request.QueryString["instanceid"]);
                    txt_StepName.Value = Convert.ToString(Request.QueryString["step"]);
                }
            }
        }

        [WebMethod]
        public static string[] GetReqDetails(int ProcessID, int InstanceID, string apiUrl)
        {
            string[] jsonArray = new string[1];
            HttpClient client = new HttpClient();
            apiUrl = apiUrl + "Requisition";
            //string apiUrl = "http://192.168.0.107:81/api/Requisition";
            var input = new
            {
                PROCESSID = ProcessID,
                INSTANCEID = InstanceID,
            };
            string inputJson = (new JavaScriptSerializer()).Serialize(input);
            HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync(apiUrl + "/getprocessdata", inputContent).Result;

            List<Requisition_HDR> output = (new JavaScriptSerializer()).Deserialize<List<Requisition_HDR>>(response.Content.ReadAsStringAsync().Result);
            if ((output != null) && (output.ToString() != "[]"))
            {
                jsonArray[0] = JsonConvert.SerializeObject(output, Formatting.Indented);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string[] getAuditData(string ProcessID, string InstanceID, string apiUrl)
        {
            string[] jsonArray = new string[1];
            HttpClient client = new HttpClient();
            apiUrl = apiUrl + "AuditTrail";
            //apiUrl = "http://localhost:59313/api/AuditTrail";
            var input = new
            {
                PROCESSID = ProcessID,
                INSTANCEID = InstanceID,
            };
            string inputJson = (new JavaScriptSerializer()).Serialize(input);

            HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync(apiUrl + "/getAudittraildata", inputContent).Result;

            List<audittrail> output = (new JavaScriptSerializer()).Deserialize<List<audittrail>>(response.Content.ReadAsStringAsync().Result);
            if ((output != null) && (output.ToString() != "[]"))
            {
                jsonArray[0] = JsonConvert.SerializeObject(output, Formatting.Indented);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string[] getDocumentData(string Requestno, string apiUrl)
        {
            string[] jsonArray = new string[1];
            HttpClient client = new HttpClient();
            apiUrl = "http://localhost:59313/api/Document";
            //apiUrl = "http://192.168.0.107:81/api/Requisition";
            var input = new
            {
                PROCESSID = Requestno,
            };
            string inputJson = (new JavaScriptSerializer()).Serialize(input);
            HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync(apiUrl + "/getDocData", inputContent).Result;

            List<Requisition_HDR> output = (new JavaScriptSerializer()).Deserialize<List<Requisition_HDR>>(response.Content.ReadAsStringAsync().Result);
            if ((output != null) && (output.ToString() != "[]"))
            {
                jsonArray[0] = JsonConvert.SerializeObject(output, Formatting.Indented);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string[] getBudgetDetails(string branchId, string Finyear, string apiUrl)
        {
            string[] jsonArray = new string[1];
            HttpClient client = new HttpClient();
            apiUrl = "http://192.168.0.107:81/api/Requisition";
            var input = new
            {
                PROCESSID = branchId,
                INSTANCEID = Finyear,
            };
            string inputJson = (new JavaScriptSerializer()).Serialize(input);
            HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync(apiUrl + "/gethdrdata", inputContent).Result;

            List<Requisition_HDR> output = (new JavaScriptSerializer()).Deserialize<List<Requisition_HDR>>(response.Content.ReadAsStringAsync().Result);
            if ((output != null) && (output.ToString() != "[]"))
            {
                jsonArray[0] = JsonConvert.SerializeObject(output, Formatting.Indented);
            }
            return jsonArray;
        }

        [WebMethod]
        public static string SaveData(string processId, string instanceId, string stepName, string SessionAdId, string SessionUserEmail, string requestNo, string requestInitiator, string requestIni_Email, string action, string approvalremark, string apiUrl)
        {
            string Result = string.Empty;
            try
            {
                HttpClient client = new HttpClient();
                apiUrl = apiUrl + "Requisition";
                //apiUrl = "http://192.168.0.111:82/api/Requisition";
                var input = new
                {
                    processid = processId,
                    instanceId = instanceId,
                    stepName = stepName,
                    requestPerformer = SessionAdId,
                    approver_mail = SessionUserEmail,
                    Request_No = requestNo,
                    Requester_ADID = requestInitiator,
                    requester_Mail = requestIni_Email,
                    action = action,
                    approvalremark = approvalremark
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/approvalsave", inputContent).Result;
                Result = response.Content.ReadAsStringAsync().Result.ToString();
                Result = Result.Replace("\"", "");
            }
            catch (Exception ex)
            {
                throw;
            }
            return Result;
        }
    }

    public class Requisition_HDR
    {
        public int PK_HDR_ID { get; set; }
        public int PROCESSID { get; set; }
        public int INSTANCEID { get; set; }
        public string HEADER_INFO { get; set; }
        public string REQ_RAISED_BY { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATION_DATE { get; set; }
        public int FK_BRANCH_ID { get; set; }
        public string BRANCH_NAME { get; set; }
        public string REMARK { get; set; }
        public string STATUS { get; set; }
        public string APPROVE_DATE { get; set; }
        public string APPROVER { get; set; }
        public string APPROVER_REMARK { get; set; }
        public int FK_PO_TYPE { get; set; }
        public string REQUEST_FOR { get; set; }
        public string USER_ADID { get; set; }
        public string USER_GRADE { get; set; }
        public string REQUSTFOR_USERNAME { get; set; }
        public string APPROVER_ACTION { get; set; }
        public string EMAILID { get; set; }
        public int ELIGIBILITY { get; set; }
        public string Branch_Address { get; set; }
        public string Contact_Details { get; set; }
        public string Department { get; set; }
        public int PK_DEPTID { get; set; }

        public int PK_DTL_ID { get; set; }
        public int FK_REQ_HDR_ID { get; set; }
        public string FK_ITEM_CAT { get; set; }
        public string ITEM_CAT { get; set; }
        public string FK_ITEM_SUB_CAT { get; set; }
        public string ITEM_SUB_CAT { get; set; }
        public string FK_ITMID { get; set; }
        public string ITMNAME { get; set; }
        public double QUANTITY { get; set; }
        public string ASSET_TYPE { get; set; }
        public string REFERENCE_ASSET_NO { get; set; }
        public double BASE_RATE { get; set; }
        public double TOTAL_AMOUNT { get; set; }

        public int FK_GRADE_ID { get; set; }
        public int FK_DESIGNATION_ID { get; set; }
        public string DESIGNATION_NAME { get; set; }
    }

    public class audittrail
    {
        public int PK_ID { get; set; }
        public string STEPNAME { get; set; }
        public string PERFORMERTYPE { get; set; }
        public string ACTIONBYUSER { get; set; }
        public string ACTIONDATE { get; set; }
        public string ACTION { get; set; }
        public string REMARK { get; set; }
        public string PROCESSID { get; set; }
        public string INSTANCEID { get; set; }
    }


}