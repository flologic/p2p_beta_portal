﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace P2P_Portal.Portal
{
    public class UpdateLogin
    {
        public static string DataTransaction(string SaveType, string UserName, string newSessionID, string Tokenid)
        {
            if (SaveType.ToUpper() == "UPDATE")
            {
                HttpClient client = new HttpClient();
                string apiUrl = "http://192.168.0.120:81/api/Authenticate";
                var input = new
                {
                    SaveType = "UPDATE",
                    UserName = UserName,
                    newSessionID = newSessionID,
                    Tokenid = Tokenid
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/DataTransaction", inputContent).Result;
                //Result = response.Content.ReadAsStringAsync().Result;
                return "true";
            }
            else
            {
                return "";
            }
        }
    }
}