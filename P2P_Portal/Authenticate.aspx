﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Authenticate.aspx.cs" Inherits="P2P_Portal.Authenticate" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>

    <script src="JS/utility.js" type="text/javascript"></script>
    <script src="JS/validator.js" type="text/javascript"></script>

</head>
<body>
    <form id="form1" runat="server">
        <div id="Div1" style="display: none" runat="server">
            <asp:TextBox ID="txt_UserName" runat="server"></asp:TextBox>
            <asp:TextBox ID="txt_Master_URL" runat="server"></asp:TextBox>
            <asp:TextBox ID="txt_Password" runat="server"></asp:TextBox>
        </div>
    </form>
</body>
</html>
