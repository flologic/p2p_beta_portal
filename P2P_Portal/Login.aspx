﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="P2P_Portal.Login" %>

<!DOCTYPE html>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>P2P Portal Login</title>
    <link rel="icon" href="assets/images/favicon.ico" />
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/animate.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet" />
    <link href="assets/css/colors/blue.css" rel="stylesheet" />
    <script>
        function hide_msg() {
            $("#alert_message").hide();
        }
    </script>
    <style>
        body {
            background: #ffffff url(assets/images/bg.jpg) 0 0 repeat-x;
        }
    </style>
</head>
<body>
    <div class="col-md-8">
        <nav class="">
            <div class="navbar-header">
                <div class="top-left-part m-t-10">
                    <a class="logo" href="#"><b>
                        <img src="assets/images/loginlogo.png" alt="home" /></b></a>
                </div>
            </div>
        </nav>
    </div>

    <section id="wrapper" class="login-register">
        <div class="login-box">
            <div class="white-box">
                <form class="form-horizontal" id="loginform" action="Authenticate.aspx">
                    <div class="sign-avatar text-center">
                        <img src="assets/images/squarelogo1.png" alt="">
                    </div>
                    <div class="form-group m-t-20">
                        <div class="col-md-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input id="Username" class="form-control" type="text" required="" placeholder="Username" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-t-10">
                        <div class="col-md-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-unlock-alt"></i></span>
                                <input id="Password" class="form-control" type="password" required="" placeholder="Password" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="text-center m-t-10">
                        <%-- <img src="assets/images/forgotpassword.png" /><a href="Forgot_Password.aspx" class="text-muted">Forgot Password?</a>--%>
                    </div>
                    <div class="form-group text-center m-t-30 m-b-10">
                        <div class="col-xs-12">
                            <button id="btnLogin" runat="server" class="btn btn-info btn-lg btn-block text-uppercase" type="submit">Log In</button>
                        </div>
                    </div>
                    <div id="alert_message" runat="server" class="alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true" onclick="hide_msg()">
                            &times;</button>
                        <label id="errMsg" class="alert-error" runat="server" />
                    </div>
                </form>
            </div>
        </div>
    </section>
    <%-- <div class="col-md-4">
        <div style="padding-left: 230px;padding-bottom:100px">
            <a href="Documents/P2P_Portal_User_Manual.pdf"><img src="assets/images/UserManual.png" style="height: 90px; width: 160px" /></a>
        </div>
    </div>--%>
</body>
</html>

