﻿using System;
using System.IO;
using System.Collections;
using System.Web;
using FSL.Controller;
using FSL.Logging;
using FSL.Message;
using System.Xml;
//using System.DirectoryServices;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using System.Net.Http;

namespace P2P_Portal
{
    public partial class Authenticate : System.Web.UI.Page
    {
        HttpClient client = new HttpClient();
        string apiUrl = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                txt_Master_URL.Text = System.Configuration.ConfigurationManager.AppSettings["MasterAPI"];
                apiUrl = txt_Master_URL.Text + "Authenticate";
                ActionController.DisablePageCaching(this);
                if (!IsPostBack)
                {
                    string Result = string.Empty;
                    if (Request.QueryString[0].ToString() != null)
                    {
                        //Result = Equitas_Helper.Portal.Authenticate.DataTransaction("GET", Request.QueryString[0].ToString(), Session.SessionID, "");
                        var input = new
                        {
                            SaveType = "GET",
                            UserName = Request.QueryString[0].ToString(),
                            newSessionID = Session.SessionID,
                            Tokenid = ""
                        };
                        string inputJson = (new JavaScriptSerializer()).Serialize(input);
                        HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                        HttpResponseMessage response = client.PostAsync(apiUrl + "/DataTransaction", inputContent).Result;
                        Result = response.Content.ReadAsStringAsync().Result;
                        if (Result == "\"false\"")  //Session["User_ADID"] != null
                        {
                            string Message = DoAuthentication();
                            if (!string.IsNullOrEmpty(Message))
                            {
                                if (!Message.Equals("Duplicate Session"))
                                    Response.SetCookie(new HttpCookie("ASP.NET_SessionId", string.Empty));
                                Response.Redirect("Login.aspx?Message=" + Message, false);
                            }
                        }
                        else
                        {
                            Response.Redirect("Login.aspx?Message=" + "User Already Logged In", false);
                        }
                    }
                }
            }
            catch (Exception Exc) { Logger1.Error(Exc); }
        }

        private string DoAuthentication()
        {
            string AuthenticationStatus = "Invalid user or password!";
            string selected = string.Empty;
            try
            {
                string UserName = string.IsNullOrEmpty(Request.Params.Get("Username")) ? string.Empty : Request.Params.Get("Username").ToLower();
                string Password = string.IsNullOrEmpty(Request.Params.Get("Password")) ? string.Empty : Request.Params.Get("Password");
                string RememberMe = string.IsNullOrEmpty(Request.Params.Get("txt_RememberMe")) ? string.Empty : Request.Params.Get("txt_RememberMe");
                string DomainName = "P2P_Portal";// string.IsNullOrEmpty(Request.Params.Get("cboDomain")) ? string.Empty : Request.Params.Get("cboDomain");
                AuthenticationStatus = string.IsNullOrEmpty(UserName) ? "ValidationErr- Enter value for the 'User Name' field." : string.IsNullOrEmpty(Password) ? "ValidationErr- Enter value for the 'Password' field." : string.Empty;
                if (string.IsNullOrEmpty(AuthenticationStatus))
                {
                    bool isAuthenticated1 = false;
                    //isAuthenticated1 = AuthenticateUser1("LDAP://equitas.in:636", UserName, Password);

                    bool isAuthenticated = AuthenticateUser(UserName, Password);
                    if (UserName == "flologic1")
                    {
                        isAuthenticated = true;
                    }
                    if (isAuthenticated)
                    {
                        //List<P2P_Portal_Helper.Portal.AuthenticateMaster> objWorkmaster = P2P_Portal_API.Controllers.AuthenticationController.GetUserInfos(UserName);
                        //string apiUrl = "http://192.168.0.120:81/api/Authenticate";
                        var input = new
                        {
                            AD_ID = UserName,
                        };
                        string inputJson = (new JavaScriptSerializer()).Serialize(input);
                        HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                        HttpResponseMessage response = client.PostAsync(apiUrl + "/AuthenticateUser", inputContent).Result;
                        AuthenticateMaster objWorkmaster = (new JavaScriptSerializer()).Deserialize<AuthenticateMaster>(response.Content.ReadAsStringAsync().Result);
                        if ((objWorkmaster != null) && (objWorkmaster.ToString() != "[]"))
                        {
                            if (RememberMe == "true")
                            {
                                Response.Cookies["LoginName"].Value = UserName;
                                Response.Cookies["Password"].Value = Password;
                                Response.Cookies["LoginName"].Expires = DateTime.Now.AddDays(7);
                                Response.Cookies["Password"].Expires = DateTime.Now.AddDays(7);
                            }
                            else
                            {
                                Response.Cookies["LoginName"].Value = string.Empty;
                                Response.Cookies["Password"].Value = string.Empty;
                                Response.Cookies["LoginName"].Expires = DateTime.Now.AddMinutes(-1);
                                Response.Cookies["Password"].Expires = DateTime.Now.AddMinutes(-1);
                            }

                            Session["User_ADID"] = UserName;
                            Session["DomainName"] = DomainName;
                            Session["Company"] = DomainName;
                            Session["EmployeeID"] = objWorkmaster.EMP_ID.ToString(); //UserInfo.Rows[0]["emp_id"].ToString();
                            Session["EmailID"] = objWorkmaster.EMAIL_ID.ToString();
                            Session["PHONE_NUMBER"] = Convert.ToString(objWorkmaster.MOBILE_NO);
                            Session["LOGIN_NAME"] = objWorkmaster.EMPLOYEE_NAME.ToString();
                            Session["FK_BRANCHID"] = objWorkmaster.FK_BRANCHID.ToString();
                            Session["FK_DESGID"] = Convert.ToString(objWorkmaster.FK_DESG_ID);
                            Session["USER_NAME"] = objWorkmaster.EMPLOYEE_NAME.ToString();
                            Session["sessionid"] = Session.SessionID;
                            Cryptography.BlowFish bw = new Cryptography.BlowFish();
                            string TID = bw.Encrypt(UserName + Password);
                            Session["Tokenid"] = TID;
                            Logger1.Activity("Login Successfully");
                            Response.Redirect("WorkItem.aspx", false);

                            string ActionResult = string.Empty;
                        }
                        else
                            AuthenticationStatus = "Invalid User";

                    }
                    else
                        AuthenticationStatus = "Invalid User";

                }

            }
            catch (Exception Exc)
            {
                AuthenticationStatus = "Invalid user or password!";
                Logger1.Error(Exc);
            }
            return AuthenticationStatus;
        }

        private bool AuthenticateUser(string UserName, string Password)
        {
            bool result = false;
            try
            {
                var input = new
                {
                    AD_ID = UserName,
                };
                string inputJson = (new JavaScriptSerializer()).Serialize(input);
                HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(apiUrl + "/AuthenticateUser", inputContent).Result;
                var a = response.Content.ReadAsStringAsync().Result;
                if (response.IsSuccessStatusCode)
                {
                    //FSL.Cryptography.BlowFish bw = new FSL.Cryptography.BlowFish();
                    //bw.Initialize("FLOLOGIC");
                    //string pwd = bw.Decrypt(UserInfo[0].PASSWORD.ToString());
                    //if (Password == pwd)
                    //{
                    result = true;
                    //Equitas_Helper.Portal.AuthenticateMaster ls = (new JavaScriptSerializer()).Deserialize<Equitas_Helper.Portal.AuthenticateMaster>(a);
                }
                else
                {
                    result = false;
                }
                //Logger1.Activity("Login User Check");
                //result = true;
            }
            catch (Exception Exc)
            {
                //Logger.WriteEventLog(false, Exc); 
                Logger1.Error(Exc);
                return false;
            }
            return result;
        }

        private bool WriteUserSessionLog(string UserName, string DomainName)
        {
            bool isOK = false;
            try
            {
                Session["IsForceLogin"] = false;
                Hashtable LoggedUserHash = (Hashtable)Application["LOGGEDUSERHASH"];
                LoggedUserHash.Clear();
                if (LoggedUserHash.ContainsKey(UserName))
                {
                    string ActionStatus = string.Empty;
                    ((Hashtable)Application["LOGGEDUSERHASH"]).Remove(UserName);
                    //string Result = P2P_Portal_Helper.Portal.Authenticate.DataTransaction("UPDATE", UserName, Session.SessionID, "");
                    var input = new
                    {
                        SaveType = "UPDATE",
                        UserName = UserName,
                        newSessionID = Session.SessionID,
                        Tokenid = ""
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/DataTransaction", inputContent).Result;
                    string Result = response.Content.ReadAsStringAsync().Result;
                    Logger1.Activity("Login Details Updated");
                    LoggedUserHash.Add(UserName, Session.SessionID);
                    //string Result1 = P2P_Portal_Helper.Portal.Authenticate.DataTransaction("SAVE", UserName, Session.SessionID, "");
                    var input1 = new
                    {
                        SaveType = "SAVE",
                        UserName = UserName,
                        newSessionID = Session.SessionID,
                        Tokenid = ""
                    };
                    string inputJson1 = (new JavaScriptSerializer()).Serialize(input1);
                    HttpContent inputContent1 = new StringContent(inputJson1, Encoding.UTF8, "application/json");
                    HttpResponseMessage response1 = client.PostAsync(apiUrl + "/DataTransaction", inputContent1).Result;
                    string Result1 = response1.Content.ReadAsStringAsync().Result;
                    Logger1.Activity("Login Details Inserted");
                    isOK = true;

                    //Session["IsForceLogin"] = true;
                    //Session["DummyUserName"] = UserName;
                    //Session["DummyDomainName"] = DomainName;
                }
                else
                {
                    string ActionStatus = string.Empty;
                    string Tokenid = string.IsNullOrEmpty(Request.Params.Get("tokenId")) ? string.Empty : Request.Params.Get("tokenId");
                    LoggedUserHash.Add(UserName, Session.SessionID);

                    //string Result = P2P_Portal_Helper.Portal.Authenticate.DataTransaction("SAVE", UserName, Session.SessionID, Tokenid);
                    var input = new
                    {
                        SaveType = "SAVE",
                        UserName = UserName,
                        newSessionID = Session.SessionID,
                        Tokenid = Tokenid
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/DataTransaction", inputContent).Result;
                    string Result = response.Content.ReadAsStringAsync().Result;
                    Logger1.Activity("Login Details Inserted");
                    isOK = true;
                }
                Application["LOGGEDUSERHASH"] = LoggedUserHash;
            }
            catch (Exception Exc)
            {
                //Logger1.Error(Exc);
                throw new Exception(Exc.Message, Exc.InnerException);
            }
            return isOK;
        }

        private void CreateIProcessSession(string UserName)
        {
            Hashtable[] iprocessHash = Application["IPROCESSHASH"] != null ? (Hashtable[])Application["IPROCESSHASH"] : ActionController.GetiProcessConfig(Page);
            if (iprocessHash != null)
            {
                try
                {
                    //vNodeId NodeInfo = new vNodeId(iprocessHash[0]["node-name"].ToString(), iprocessHash[0]["host-name"].ToString(), iprocessHash[0]["ip-address"].ToString(), Int32.Parse(iprocessHash[0]["tcp-port"].ToString()), iprocessHash[0]["Director"].ToString().Equals("0") ? false : true);
                    //Session["UserIPSession"] = new sSession(NodeInfo, UserName, "");
                }
                catch (Exception Exc)
                {
                    // Logger1.Error(Exc);
                    //    Logger.WriteEventLog(false, Exc); 
                }
            }
            if (Application["IPROCESSHASH"] == null) Application["IPROCESSHASH"] = iprocessHash;
        }

        private string getPublickey()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "config\\";
            DirectoryInfo dirinfo = new DirectoryInfo(path);
            FileInfo[] fileInf = dirinfo.GetFiles("PublicKey.config");
            StreamReader sr = new StreamReader(path + "PublicKey.config");
            XmlDocument xdoc = new XmlDocument();
            xdoc.LoadXml(sr.ReadToEnd());
            string strPublicKey = "";
            if (xdoc.HasChildNodes == true)
            {
                XmlNodeList nodes = xdoc.SelectNodes("/configuration/db-key");
                foreach (XmlNode node in nodes)
                {
                    strPublicKey = node.InnerText;
                }

            }
            //strPublicKey = "";
            return strPublicKey;
        }
    }

    public class AuthenticateMaster
    {
        public string AD_ID { get; set; }
        public string EMP_ID { get; set; }
        public string EMPLOYEE_NAME { get; set; }
        public string EMAIL_ID { get; set; }
        public int IS_ACTIVE { get; set; }
        public string PASSWORD { get; set; }
        public string AMD_BY { get; set; }
        public DateTime? AMD_DATE { get; set; }
        public string MOBILE_NO { get; set; }
        public int FK_BRANCHID { get; set; }
        public int FK_DESG_ID { get; set; }
    }
}