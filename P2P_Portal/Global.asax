﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="FSL.Controller" %>
<%@ Import Namespace="System.Web.Script.Serialization" %>

<script RunAt="server">
    
    private static Hashtable LoggedUserHash;
    void Application_Start(object sender, EventArgs e)
    {
        string ActionStatus = string.Empty;
        LoggedUserHash = Hashtable.Synchronized(new Hashtable());
        Application["SERVERPATH"] = "CHOLAMS";
        Application["LOGGEDUSERHASH"] = LoggedUserHash;
        Application["IPROCESSHASH"] = ActionController.GetiProcessConfig(new Page());
        Application["DOMAINHASH"] = ActionController.GetDomains(new Page());
        Application["INBOX"] = (DataTable)ActionController.ExecuteAction("KotakBPM Application", "Global.asax", "select inbox", ref ActionStatus);
        Application["PANEL"] = (DataTable)ActionController.ExecuteAction("KotakBPM Application", "Global.asax", "select panels", ref ActionStatus);
        //ssoDOTNET.StartUp();    
    }
    void Application_End(object sender, EventArgs e)
    {
        //ssoDOTNET.ShutDown();
        Application.Remove("SERVERPATH");
        Application.Remove("LOGGEDUSERHASH");
        Application.Remove("IPROCESSHASH");
        Application.Remove("DOMAINHASH");
        Application.Remove("INBOX");
        Application.Remove("PANEL");
        LoggedUserHash = null;
    }

    protected void Application_BeginRequest()
    {

        //NOTE: Stopping IE from being a caching whore
        HttpContext.Current.Response.Cache.SetAllowResponseInBrowserHistory(false);
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.Cache.SetNoStore();
        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetValidUntilExpires(true);
    }

    void Application_Error(object sender, EventArgs e)
    {

    }
    void Session_Start(object sender, EventArgs e)
    {
        Session["LOGGEDUSERHASH"] = (Hashtable)Application["LOGGEDUSERHASH"];
        Session["FilterExpression"] = string.Empty;
        Session["PageSize"] = 10;
        Session["SearchPageSize"] = 10;
    }

    void Session_End(object sender, EventArgs e)
    {
        try
        {
            Session["Tokenid"] = "";
            Session["lastAccessedDateTime"] = "";

            string ActionStatus = string.Empty;
            string UserName = (string)Session["User_ADID"];
            string DomainName = (string)Session["DomainName"];
            if (UserName != null)
            {
                Hashtable LoggedUserHash = (Hashtable)Application["LOGGEDUSERHASH"];
                if ((LoggedUserHash.ContainsKey(UserName)) && (LoggedUserHash[UserName].ToString().Equals(Session.SessionID)))
                {
                    ((Hashtable)Application["LOGGEDUSERHASH"]).Remove(UserName);
                    // ActionController.ExecuteAction(UserName, "Login.aspx", "write user session log", ref ActionStatus, UserName, "0", Session.SessionID, "0", Dns.GetHostName().ToString(), DomainName, "UPD", "");

                    string Result = P2P_Portal.Portal.UpdateLogin.DataTransaction("UPDATE", UserName, Session.SessionID, "");
                    //HttpClient client = new HttpClient();
                    //string apiUrl = "http://192.168.0.120:81/api/Authenticate";
                    //var input = new
                    //{
                    //    SaveType = "UPDATE",
                    //    UserName = UserName,
                    //    newSessionID = Session.SessionID,
                    //    Tokenid = ""
                    //};
                    //string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    //HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    //HttpResponseMessage response = client.PostAsync(apiUrl + "/DataTransaction", inputContent).Result;
                    //string Result = response.Content.ReadAsStringAsync().Result;


                }
            }
        }
        catch (Exception) { }
    }
           
</script>

