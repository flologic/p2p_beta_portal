﻿
$(document).ready(function () {

});

$("#btnSubmit").click(function () {
    //addmodalpopup();
    $("#loading-div-background").show();
    var CPWD = [];
    var currentpwd = $("#txt_Currentpwd").val();
    var newpwd = $("#txt_newpwd").val();
    var confmnepwd = $("#txt_confnewpwd").val();
    var cretedby = $("#txtCreatedBy").val();

    if (currentpwd == "") {
        $.jAlert({
            'content': 'Please Enter Current Password.',
            'onClose': function () {
                $('#txt_Currentpwd').focus();
            }
        });

        return;
    }
    else if (newpwd == "") {
        $.jAlert({
            'content': 'Please Enter New Password.',
            'onClose': function () {
                $('#txt_newpwd').focus();
            }
        });

        return;
    }
    else if (confmnepwd == "") {
        $.jAlert({
            'content': 'Please Enter Confirm New Password.',
            'onClose': function () {
                $('#txt_confnewpwd').focus();
            }
        });
        return;
    }
    else if (newpwd != confmnepwd) {
        $.jAlert({
            'content': 'New password and Confirm new password Does Not Match.',
            'onClose': function () {
                $('#txt_confnewpwd').focus();
            }
        });

        return;
    }
    $("#loading-div-background").show();
    var objcpwd;

    objcpwd = {
        "LOGIN_NAME": cretedby, "PASSWORD": confmnepwd, "CURRENTPWD": currentpwd
    };

    CPWD.push(objcpwd);

    PageMethods.saveData(JSON.stringify(CPWD), "UPDATE", function (response) {
        //alert(response);
        $.jAlert({
            'content': response,
            'onClose': function () {
                //  $('#txt_confnewpwd').focus();
                showSideMap1("Workitem.aspx")
            }
        });

        //jAlert(response, "alert");
        CPWD = [];
        $("#txt_Currentpwd").val('');
        $("#txt_newpwd").val('');
        $("#txt_confnewpwd").val('');
        //    showSideMap1('Login.aspx');


    }, function (error) {
        //alert(error);
    });
    $("#loading-div-background").hide();

});

function showSideMap1(ObjName) {
    //PageMethods.GetUpdateLogin($('#txtCreatedBy').val(), $('#sessionid').val(), function (response) {
    //    //LoginJson = response;
    //}, function (error) {
    //    alert(error);
    //});

    //document.getElementById("frmset_WorkArea").src = ObjName;
    //document.getElementById('frmset_WorkArea').src = ObjName;
    window.location.href = ObjName;
}


$("#btn_Cancel").click(function () {
    $('#form1').trigger("reset");
    $("#txt_Currentpwd").val('');
    $("#txt_newpwd").val('');
    $("#txt_confnewpwd").val('');

});
