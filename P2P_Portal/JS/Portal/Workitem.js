﻿

var flag = true;
var flag_10 = false;
var flag_15 = false;
var displayed = false;
var displayedConfirm = false;
var cnt = 0;

$(document).ready(function () {
    var height = $(window).height();
    $('iframe').css('height', height * 0.85 | 0);
    DisplaySessionTimeout();
    getMenuDetails();
    showSideMap('HomePage.aspx');
});

function DisplaySessionTimeout() {
    var countDownDate = new Date();
    countDownDate.setMinutes(countDownDate.getMinutes() + parseInt($("#hiddenTimeout").val()));
}

function DisplaySessionTimeout() {
    var countDownDate = new Date();
    countDownDate.setMinutes(countDownDate.getMinutes() + parseInt($("#hiddenTimeout").val()));

    var x = setInterval(function () {

        var now = new Date().getTime();

        var distance = countDownDate - now;

        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        if (hours < 10 && hours >= 0) {
            hours = "0" + hours;
        }
        if (minutes < 10 && minutes >= 0) {
            minutes = "0" + minutes;
        }
        if (seconds < 10 && seconds >= 0) {
            seconds = "0" + seconds;
        }
        document.getElementById("Lbl_SessionState").innerHTML = hours + " : " + minutes + " : " + seconds + "";

        if ($("#ALERTMSG").val() == "" || $("#ALERTMSG").val() == undefined || $("#ALERTMSG").val() == null) {
            $("#ALERTMSG").val("5");
        }

        //if (hours <= 0 && minutes <= 15 && flag_15 == false) {
        //    flag_15 = true;
        //    alert("Your Session will expire within 15 minutes");
        //}
        //if (hours <= 0 && minutes <= 10 && flag_10 == false) {
        //    flag_10 = true;
        //    alert("Your Session will expire within 10 minutes");
        //}
        //if (hours <= 0 && minutes < parseInt($("#ALERTMSG").val())) {
        //    if (flag == true) {
        //        if (confirm("Your Session Will Expire Soon. Are You Sure To Reset?")) {
        //            displayedConfirm = true;
        //            if (distance <= 0) {
        //                flag = false;
        //            }
        //            else {
        //                flag = true;
        //                flag_15 = flag_10 = false;
        //                countDownDate = new Date();
        //                countDownDate.setMinutes((new Date()).getMinutes() + parseInt($("#hiddenTimeout").val()));
        //                PageMethods.updateSession(parseInt($("#hiddenTimeout").val() * 2), function (response) {
        //                    callback_open_detail(response);
        //                }, function (error) {
        //                    alert(error);
        //                });
        //                // WorkItem.updateSession(parseInt($("#hiddenTimeout").val() * 2), callback_open_detail);
        //            }
        //        }
        //        else {
        //            flag = false;
        //        }
        //    }
        //}

        if (hours <= 0 && minutes < parseInt($("#ALERTMSG").val())) {
            if (flag == true) {
                if (cnt == 0) {
                    if (($.jAlert({
                        'title': 'Confirmation',
                        'content': 'Your Session will expire soon. Are you sure to reset?', 'type': 'confirm',
                        'onConfirm': function () {
                        displayedConfirm = true;
                        if (distance <= 0) {
                            flag = false;
                    }
                    else {
                            flag = true;
                            flag_15 = flag_10 = false;
                            cnt = 0;
                            countDownDate = new Date();
                            countDownDate.setMinutes((new Date()).getMinutes() + parseInt($("#hiddenTimeout").val()));
                            PageMethods.updateSession(parseInt($("#hiddenTimeout").val() * 2), function (response) {
                                callback_open_detail(response);
                    }, function (error) {
                                alert(error);
                    });
                    }
                    }
                    }))) {

                        cnt++;
                    }
                    else {
                        flag = false;
                    }
                }

                //$.jAlert({
                //    'title': 'Confirmation',
                //    'content': 'Your Session will expire soon. Are you sure to reset?', 'type': 'confirm', 'onConfirm': function () { ConfirmData() },
                //    'onClose': function () {
                //        flag = false;
                //    }
                //});

            }
        }

        if (distance <= 0) {
            if (flag == false || displayed == false) {
                displayed = true;
                $.jAlert({
                    'content': 'Your Session Timeout Is Expired'
                });
                //alert("Your Session Timeout Is Expired");
                window.location.href = "Login.aspx";
            }
        }

    }, 1000);
}

function callback_open_detail(response) {
}


function getMenuDetails() {
    //var userid = "1";
    var userid = $("#txt_User_id").val();
    var Master_API = $("#txt_Master_URL").val();
    PageMethods.headerDetail(userid, Master_API + "Workitem", function (response) {
        setMasterMenu(response);
    }, function (error) {
        alert(error);
    });
}

function setMasterMenu(response) {

    if (response) {
        var MasterJson = JSON.parse(response[0]);
        var MasterJsonBck = MasterJson;

        var object = {}
        object[String("OBJ_PARENT_ID")] = 0;
        var filteredListParent = _.where(MasterJson, object);

        var tempstring = "";
        var MasterString = "";
        if (filteredListParent.length > 0) {
            for (var i = 0; i < filteredListParent.length; i++) {
                if (filteredListParent[i].OBJ_PARENT_ID == 0) {
                    var parent_id = filteredListParent[i].OBJ_ID;
                    //if (filteredListParent[i].OBJ_TYPE == "L") {
                    MasterString += " <li><a href='javascript:void(0)' class='waves-effect'><b>" + filteredListParent[i].OBJ_NAME + "</b><span class='fa arrow'></span></a><ul class='nav nav-third-level'>";
                    //}
                    //else {
                    //    MasterString += "<li onclick=\"showSideMap('" + filteredListParent[i].OBJ_URL + "');\"> <a href='javascript:;' target='frmset_WorkArea'> " +
                    //    filteredListParent[i].OBJ_NAME + "</a>";
                    //}

                    var object1 = {}
                    object1[String("OBJ_PARENT_ID")] = parseInt(parent_id);
                    var filteredListChild = _.where(MasterJsonBck, object1);

                    for (var j = 0; j < filteredListChild.length; j++) {
                        MasterString += "<li onclick=\"showSideMap('" + filteredListChild[j].OBJ_URL + "');\"> <a href='javascript:;' target='frmset_WorkArea'> " +
                        filteredListChild[j].OBJ_NAME + "</a>";
                        MasterString += "</li>";
                    }
                    MasterString += "</ul></li>";
                }
            }
            $("#masterul").html(MasterString);
            $("#liMaster").show();
        }
        else {
            $("#liMaster").hide();
        }

        /***************************************************************************************/

        var ProcessJson = JSON.parse(response[1]);
        var ProcessJsonBck = ProcessJson;

        var object = {}
        object[String("OBJ_PARENT_ID")] = 0;
        var filteredListParent = _.where(ProcessJson, object);

        var tempstring = "";
        var MasterString = "";
        if (filteredListParent.length > 0) {
            for (var i = 0; i < filteredListParent.length; i++) {
                if (filteredListParent[i].OBJ_PARENT_ID == 0) {
                    var parent_id = filteredListParent[i].OBJ_ID;
                    //if (filteredListParent[i].OBJ_TYPE == "L") {
                    MasterString += " <li><a href='javascript:void(0)' class='waves-effect'><b>" + filteredListParent[i].OBJ_NAME + "</b><span class='fa arrow'></span></a><ul class='nav nav-third-level'>";
                    //}
                    //else {
                    //    MasterString += "<li onclick=\"showSideMap('" + filteredListParent[i].OBJ_URL + "');\"> <a href='javascript:;' target='frmset_WorkArea'> " +
                    //    filteredListParent[i].OBJ_NAME + "</a>";
                    //}
                    var object1 = {}
                    object1[String("OBJ_PARENT_ID")] = parseInt(parent_id);
                    var filteredListChild = _.where(ProcessJsonBck, object1);

                    for (var j = 0; j < filteredListChild.length; j++) {

                        MasterString += "<li onclick=\"showSideMap('" + filteredListChild[j].OBJ_URL + "');\"> <a href='javascript:;' target='frmset_WorkArea'> " +
                        filteredListChild[j].OBJ_NAME + "</a>";

                        MasterString += "</li>";
                    }
                    MasterString += "</ul></li>";
                }
            }
            $("#processul").html(MasterString);
            $("#liprocessul").show();
        }
        else {
            $("#liprocessul").hide();
        }

        /***************************************************************************************/

        var ReportJson = JSON.parse(response[2]);
        var ReportJsonBck = ReportJson;

        var object = {}
        object[String("OBJ_PARENT_ID")] = 0;
        var filteredListParent = _.where(ReportJson, object);

        var tempstring = "";
        var MasterString = "";
        if (filteredListParent.length > 0) {
            for (var i = 0; i < filteredListParent.length; i++) {
                if (filteredListParent[i].OBJ_PARENT_ID == 0) {
                    var parent_id = filteredListParent[i].OBJ_ID;
                    MasterString += " <li><a href='javascript:void(0)' class='waves-effect'><b>" + filteredListParent[i].OBJ_NAME + "</b><span class='fa arrow'></span></a><ul class='nav nav-third-level'>";

                    var object1 = {}
                    object1[String("OBJ_PARENT_ID")] = parseInt(parent_id);
                    var filteredListChild = _.where(ReportJsonBck, object1);

                    for (var j = 0; j < filteredListChild.length; j++) {
                        MasterString += "<li onclick=\"showSideMap('" + filteredListChild[j].OBJ_URL + "');\"> <a href='javascript:;' target='frmset_WorkArea'> " +
                        filteredListChild[j].OBJ_NAME + "</a>";
                        MasterString += "</li>";
                    }
                    MasterString += "</ul></li>";
                }
            }
            $("#reportul").html(MasterString);
            $("#lireport").show();
        }
        else {
            $("#lireport").hide();
        }
    }
}

function showSideMap(ObjName) {
    document.getElementById('frmset_WorkArea').src = ObjName;
}

function menuToggle() {
    $("#side-menu").fadeToggle();
}
