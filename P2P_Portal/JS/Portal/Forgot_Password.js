﻿

$("#btnSubmit").click(function () {
    var CPWD = [];
    var UserName = $("#txt_loginid").val();
    var Email = $("#txt_emailid").val();
    //var cretedby = $("#txtCreatedBy").val();

    if (UserName == "") {
        $.jAlert({
            'content': 'Please Enter Login Id',
            'onClose': function () {
                $('#txt_loginid').focus();
            }
        });
        return false;
    }
    else if (Email == "") {
        $.jAlert({
            'content': 'Please Enter Your Email ID',
            'onClose': function () {
                $('#txt_emailid').focus();
            }
        });
        return false;
    }

    var objcpwd;

    objcpwd = {
        "LOGIN_NAME": UserName, "EMAILID": Email
    };

    CPWD.push(objcpwd);

    PageMethods.saveData(JSON.stringify(CPWD), "UPDATE", function (response) {

        CPWD = [];
        if (response == "Login Id is not Available") {

            $.jAlert({
                'content': response,
                'onClose': function () {
                    $('#txt_loginid').focus();
                }
            });
        }
        else if (response == "Email ID is not correct...!") {
            $.jAlert({
                'content': response,
                'onClose': function () {
                    $('#txt_emailid').focus();
                }
            });
        }
        else {
            $("#txt_loginid").val('');
            $("#txt_emailid").val('');
            $.jAlert({
                'content': response,
                'onClose': function () {
                    window.location.href = 'Login.aspx';
                }
            });

        }
    }, function (error) {
        //alert(error);
    });

});

$("#btn_Cancel").click(function () {
    window.location.href = 'Login.aspx';

});


function validateEmail1(emailField) {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test(emailField.value) == false) {
        alert('Invalid Email Address');
        $('#txt_emailid').focus();
        $('#txt_emailid').val('');

        return false;
    }
    return true;
}
