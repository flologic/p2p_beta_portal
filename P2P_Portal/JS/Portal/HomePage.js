﻿var ProjDtllistJson;
$(document).ready(function () {
    onLoad();

});



function onLoad() {
    PageMethods.GetProjectDetails(function (response) {
        ProjDtllistJson = JSON.parse(response[0]);
        SetProjectDtl(ProjDtllistJson);
    }, function (error) {
        alert(error);
    });
}


function SetProjectDtl(response) {

    if (response) {

        var tableHeader = "";
        var tableBody = "";
        tableHeader += "<tr class='table-th-color-parent'>";
        tableHeader += "<th rowspan='2' style='width: 3%'>#</th>";
        tableHeader += "<th rowspan='2' nowrap>Project Name</th>";
        tableHeader += "<th rowspan='2' nowrap>Status</th>";
        tableHeader += "<th rowspan='2' nowrap>Resource</th>";
        tableHeader += "<th rowspan='2' nowrap>Start</th>";
        tableHeader += "<th rowspan='2' nowrap>Finish</th>";
        var date = new Date();
        var weekdays = ["Sun", "Mon", "Tues", "Wed", "Thu", "Fri", "Sat"];

        var weekday = weekdays[date.getDay()];
        var month = date.getMonth() + 1;
        var year = date.getFullYear();
        for (i = 0; i < 12; i++) {
            getDaysInMonth(i, year);
        }
        function getDaysInMonth(month, year) {
            // Since no month has fewer than 28 days
            var date = new Date(year, month);
            var days = [];
            var numOfMonthDays = new Date(year, month, 0).getDate();
            // console.log(month, 'date.getMonth()', date.getMonth())
            var cnt = 0;
            while (date.getMonth() === month) {
                var Months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                var cmndate = date.getDate() + "-" + Months[date.getMonth()] + "-" + date.getFullYear();

                days.push(cmndate);
                date.setDate(date.getDate() + 7);
                cnt++;
                var diff = numOfMonthDays - date.getDate();
                if (cnt == 4 && diff > 6)
                    tableHeader += "<th nowrap colspan='7'>" + cmndate + "</th>";
            }
        }

        tableHeader += "</tr>";
        tableHeader += "<tr class='table_th_color'>";

        for (j = 0; j < 12; j++) {
            getAllDaysInMonth(j, year);
        }
        function getAllDaysInMonth(month, year) {
            var numOfDays = new Date(year, month, 0).getDate(); //use 0 here and the actual month

            for (var i = 0; i < numOfDays; i++) {
                var date = new Date(year, month, (i + 1));
                var weekdays = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
                var weekday = weekdays[date.getDay()];
                tableHeader += "<th nowrap>" + weekday + "</th>";
            }
        }

        tableHeader += "</tr>";
        $.each(response, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.PROJECT_NAME + "</td>";
            if (val.PROJECT_START_DATE != "" && val.PROJECT_END_DATE != null) {
                tableBody += "<td class='text-center'><img src='assets/img/Inprogress.png' title='Inprogress' alt='Inprogress'></td>";
                //   tableBody += "<td>" + val.PROJECT_STATUS + "</td>";
            }
            else {
                tableBody += "<td class='text-center'><img src='assets/img/Completed.png' title='Inprogress' alt='Completed'></td>";
            }
            tableBody += "<td nowrap>";
            for (l = 0; l < val.resourcecnt; l++) {
                tableBody += "<span class='rating-resource-color'><i class='fa fa-male'></i></span>";  //resource
            }
            tableBody += "</td>";
            tableBody += "<td nowrap>" + val.PROJECT_START_DATE + "</td>";
            tableBody += "<td nowrap>" + val.PROJECT_END_DATE + "</td>";
            var start = val.PROJECT_START_DATE;
            var end = val.PROJECT_END_DATE;
            var str = start;
            var res = str.split("-");
            var res1 = res[2].split(" ");
            var startDay = new Date(res[1] + "/" + res[0] + "/" + res1[0]);

            var str1 = end;
            var res2 = str1.split("-");
            var res3 = res2[2].split(" ");
            var endDay = new Date(res2[1] + "/" + res2[0] + "/" + res3[0]);

            var millisecondsPerDay = 1000 * 60 * 60 * 24;

            var millisBetween = endDay.getTime() - startDay.getTime();
            var days = millisBetween / millisecondsPerDay;
            //alert(Math.floor(days));
            for (k = 0; k <= days; k++) {
                //  tableBody += "<td class='td_activity_day_color'></td>";//Not Started
                tableBody += "<td class='td-assigned-color'></td>"; //Inprogress
                //  tableBody += "<td class='td_skill_past_day_color'></td>";// completed
            }

            tableBody += "</tr>";

        });
        var table = "<table class='table table-bordered table-padding' id='tbl_Activity'>" +
              "<thead>" + tableHeader + "</thead>" +
              "<tbody>" + tableBody + "</tbody>" +
              "</table>";
        $("#div_projectlstdtl").html(table);
        //$("#tbl_pendingMyTask").dataTable();
    }
}
