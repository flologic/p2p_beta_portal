﻿
function fillsubddl(childddlID, childjson, columntosrch, srchvalue, selvalue) {


    var object = {}
    object[String(columntosrch)] = srchvalue;

    var filteredList = _.where(childjson, object);

    var length = filteredList.length;
    if (length > 0) {
        if (selvalue != "") {
            setDropdown(childddlID, filteredList, parseInt(selvalue));
        }
        else {
            setDropdown(childddlID, filteredList, "");
        }
    }
    else {
        setDropdown(childddlID, [], "")
    }
}

function setDropdown(ddlID, JsonData, setvalue) {
    if (ddlID != "" && JsonData != "") {
        var select = $(ddlID);
        $(ddlID).empty();
        select.append('<option value="0">---Select One---</option>');
        $.each(JsonData, function (key, val) {
            if (setvalue != "" && val.id == setvalue) {
                select.append('<option value="' + val.id + '" selected>' + val.label + '</option>');
            }
            else {
                select.append('<option value="' + val.id + '">' + val.label + '</option>');
            }
        })
    }
    else {
        $(ddlID).empty();
        $(ddlID).append('<option value="0">---Select One---</option>');
    }
}

function fillsubddl1(childddlID, childjson, columntosrch, srchvalue, selvalue) {
    var object = {}
    object[String(columntosrch)] = srchvalue;

    //var filteredList = _.where(childjson, object);

    var length = childjson.length;
    if (length > 0) {
        if (selvalue != "") {
            setDropdown1(childddlID, childjson, parseInt(selvalue));
        }
        else {
            setDropdown1(childddlID, childjson, "");
        }
    }
    else {
        setDropdown1(childddlID, [], "")
    }
}

function setDropdown1(ddlID, JsonData, setvalue) {
    if (ddlID != "" && JsonData != "") {
        var select = $(ddlID);
        $(ddlID).empty();
        select.append('<option value="0">---Select One---</option>');
        $.each(JsonData, function (key, val) {
            if (setvalue != "" && val.id == setvalue) {
                select.append('<option value="' + val.id + '" selected>' + val.label + '</option>');
            }
            else {
                select.append('<option value="' + val.id + '">' + val.label + '</option>');
            }
        })
    }
    else {
        $(ddlID).empty();
        $(ddlID).append('<option value="0">---Select One---</option>');
    }
}


function addmodalpopup() {
    processmodalpopup = document.createElement("div");
    processmodalpopup.innerHTML = "<div id='loading-div-background' style='display: none'><div id='loading-div' class='ui-corner-all'> <img style='height: 55px; width: 55px; margin: 30px;' src='../../assets/images/ajax-loader-10.gif' alt='Loading..' /><br>PROCESSING. PLEASE WAIT...</div></div>";
    document.body.appendChild(processmodalpopup);
}

$(".number").keydown(function (e) {
    $(".validation_NumberError").hide();
    $(".validation_DecimalError").hide();
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || (e.keyCode >= 35 && e.keyCode <= 40)) {
        $(".validation_NumberError").hide();
        $(".validation_DecimalError").hide();
        return;
    }
    if (($(this).val().indexOf('.') != -1) && ($(this).val().substring($(this).val().indexOf('.'), $(this).val().indexOf('.').length).length > 2)) {
        event.preventDefault();
        $(this).after('<span class="font-red-thunderbird validation_DecimalError">Only Two Digit Allow</span>');
        $(".validation_NumberError").hide();
    }
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
        $.jAlert({
            'content': 'Enter Numbers Only..!',
            'onClose': function () {
            }
        });
        //jAlert("Enter Numbers Only..!", "Validation");
        // $(this).after('<span class="font-red-thunderbird validation_NumberError">Enter Numbers only</span>');
        $(".validation_DecimalError").hide();
    }
});

function fillsubddl1(childddlID, childjson, columntosrch, srchvalue, selvalue) {
    var object = {}
    object[String(columntosrch)] = srchvalue;

    //var filteredList = _.where(childjson, object);

    var length = childjson.length;
    if (length > 0) {
        if (selvalue != "") {
            setDropdown1(childddlID, childjson, parseInt(selvalue));
        }
        else {
            setDropdown1(childddlID, childjson, "");
        }
    }
    else {
        setDropdown1(childddlID, [], "")
    }
}

function setDropdown1(ddlID, JsonData, setvalue) {
    if (ddlID != "" && JsonData != "") {
        var select = $(ddlID);
        $(ddlID).empty();
        select.append('<option value="0">---Select One---</option>');
        $.each(JsonData, function (key, val) {
            if (setvalue != "" && val.id == setvalue) {
                select.append('<option value="' + val.id + '" selected>' + val.label + '</option>');
            }
            else {
                select.append('<option value="' + val.id + '">' + val.label + '</option>');
            }
        })
    }
    else {
        $(ddlID).empty();
        $(ddlID).append('<option value="0">---Select One---</option>');
    }
}

function isempty(v) // to check values is blank,undefined or null
{
    var type = typeof v;

    if (type === 'undefined')
        return true;

    else if (v === null)
        return true;
    else if (v == undefined)
        return true;
    else if (String(v).trim() == "")
        return true;

    return false;
}
$("#btn_Clear").click(function () {
    location.reload(true);
    //onLoad();
});

function RedirectPage() {
    window.location.replace('../../HomePage.aspx');
}

//function getDocumentDetails() {

//    var pageUrl = '<%= ResolveUrl("~"+Common.TravelRequestFilePath+"/TravelRequestApplication.aspx/GetValidDates") %>';
//    var parameter = { "FromDate": $('#FromDate').val(), "ToDate": $('#ToDate').val(), "pkEmpID": "<%=pkEmpID%>", "RequestNumber": $("#txt_RequestNumber").val() }
//    $.ajax({
//        type: 'POST',
//        url: pageUrl,
//        data: JSON.stringify(parameter),
//        contentType: 'application/json; charset=utf-8',
//        dataType: 'json',
//        success: function (data) {
//            GetValidDates_callback(data);
//        },
//        error: function (data, success, error) {
//            alert("Error : " + error);
//        }
//    });
//    $.ajax({
//        type: "POST",
//        url: "Branch_Admin_Approval.aspx/getDocumentDetail",
//        data: "Requestno":requestNo, "hi",
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: function (msg) {
//            // Do something interesting here.
//        }
//    });
//    //Branch_Admin_Approval..getDocumentDetail(requestNo, "hi", function (response) {
//    //    DocumentJSON = JSON.parse(response[0]);
//    //    displayDocumentData();
//    //}, function (error) {
//    //});
//}


//function displayDocumentData() {
//    if (DocumentJSON) {
//        var tableHeader = "";
//        var tableBody = "";

//        tableHeader += "<th align='center'>SR.No</th>";
//        tableHeader += "<th align='center'>FileName</th>";
//        tableHeader += "<th align='center'>Remark</th>";

//        $.each(DocumentJSON, function (key, val) {
//            tableBody += "<tr>";
//            tableBody += "<td>" + (key + 1) + "</td>";
//            tableBody += "<td><input class='hidden' type='text' name='txt_Region_Add" + (key + 1) + "' id='txt_Document_File" + (key + 1) + "' value=" + val.FILENAME + " readonly ></input><a id='a_downloadfiles" + (key + 1) + "' style='cursor: pointer' onclick=\"return downloadfiles1('" + (key + 1) + "');\" >" + val.FILENAME + "</a><input class='hidden' type='text' name='txt_pono" + (key + 1) + "' id='txt_poreqno" + (key + 1) + "' value=" + val.OBJECT_VALUE + " readonly /></td>";
//            tableBody += "<td>" + val.DOCUMENT_TYPE + "</td>";
//            tableBody += "</tr>";
//        });

//        var table = "<table ID='tblDoc' class='display nowrap dataTable'>" +
//            "<thead><tr class='grey'>" + tableHeader + "</tr></thead>" +
//            "<tbody>" + tableBody + "</tbody>" +
//            "</table>";
//        $("#div_docs").empty();
//        $("#div_docs").html(table);
//        var tbllength = $('#tblDoc tr').length;
//        if (tbllength > 1) {
//            $('#Img1').attr("src", '../../assets/images/attachment_sel.png');
//        }
//        else {
//            $('#Img1').attr("src", '../../assets/images/attachment_non_sel.png');
//        }
//        $("#loading-div-background").hide();
//    }
//}