﻿function isSpecialChar(evt) {
    var k = (evt.which) ? evt.which : event.keyCode
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function getSelectedText(elementId) {
    var elt = document.getElementById(elementId);
    if (elt.selectedIndex == -1)
        return null;
    return elt.options[elt.selectedIndex].text;
}

function getMonthFromString(mon) {
    var month = new Date(Date.parse(mon + " 1, 2012")).getMonth() + 1;
    return month;
}


function validateDecimal(eve, myid) {
    var charCode = (eve.which) ? eve.which : event.keyCode;
    if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode != 46) {
        //alert("Please Enter numeric value.");
        // event.keyCode = 0;
        return false
    }
    if (myid.value.indexOf(".") !== -1) {
        //  var range = document.selection.createRange();
        if (myid.value != "") {
        }
        else {
            var number = myid.value.split('.');
            if (number.length == 2 && number[1].length > 1)
                //    event.keyCode = 0;
                return false;
        }
    }
    return true;
}

function titleCase(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

function GetDatetimeformat() {
    return "dd-mm-yy";
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}



