﻿function uploadError(sender, args) {
    alert(args.get_errorMessage());
    var uploadText = document.getElementById('FileUpload1').getElementsByTagName("input");
    for (var i = 0; i < uploadText.length; i++) {
        if (uploadText[i].type == 'text') {
            uploadText[i].value = '';
        }
    }
}

function StartUpload(sender, args) {

}

function UploadComplete(sender, args) {
    var filename = args.get_fileName();
    var n = filename.replace(/[&\/\\#,+()$~%'":*?<>{} ]/g, "");
    var fileNameSplit = n.split('.');
    var contentType = args.get_contentType();
    var filelength = args.get_length();
    var Document_Name = $("#txt_description").val();
    var fileExt = '.' + filename.split('.').pop();

    if (Document_Name == "") {
        $.jAlert({
            'content': 'Please enter desciption first...!'
        });
        return false;
    }
    if (parseInt(filelength) == 0) {
        $.jAlert({
            'content': 'Sorry cannot upload file ! File is Empty or file does not exist'
        });
    }
    else if (parseInt(filelength) > 20000000) {
        $.jAlert({
            'content': 'Sorry cannot upload file ! File Size Exceeded.'
        });
    }
    else if (contentType == "application/octet-stream" && fileNameSplit[1] == "exe") {
        $.jAlert({
            'content': 'Kindly Check File Type.'
        });
    }
    else {
        if (fileExt.toUpperCase() == ".JPEG" || fileExt.toUpperCase() == ".JPG" || fileExt.toUpperCase() == ".PNG" || fileExt.toUpperCase() == ".PDF") {
            addToClientTable(n, args.get_length());
        }
        else {
            $.jAlert({
                'content': 'Unsupported File'
            });
        }
        var fileInputElement = sender.get_inputFile();
        fileInputElement.value = "";
    }
    var uploadText = document.getElementById('FileUpload1').getElementsByTagName("input");
    for (var i = 0; i < uploadText.length; i++) {
        if (uploadText[i].type == 'text') {
            uploadText[i].value = '';
        }
    }
    document.forms[0].target = "";
}

function addToClientTable(name, size) {
    var Document_Name = $("#txt_description").val();
    var tbl = $("#uploadTable");
    var lastRow = $('#uploadTable tr').length;
    var html1 = "<tr><td><input class='hidden' type='text' id='txt_Document_Name" + lastRow + "' value=" + Document_Name + " readonly ></input>" + Document_Name + "</td>";
    var html2 = "<td><input class='hidden' type='text' name='txt_Region_Add" + lastRow + "' id='txt_Document_File" + lastRow + "' value=" + name + " readonly ></input><a id='a_downloadfiles" + lastRow + "' style='cursor: pointer' onclick=\"return downloadfiles('" + lastRow + "');\" >" + name + "</a></td>";
    var html3 = "<td><i id='del" + lastRow + "' class='glyphicon glyphicon-trash' align='center' onclick=\"return deletefile(" + (lastRow) + ");\" ></td></tr>";
    var htmlcontent = $(html1 + "" + html2 + "" + html3);
    $('#uploadTable').append(htmlcontent);
    $("#txt_description").val('');
    var tbllength = $('#uploadTable tr').length;
    if (tbllength > 1) {
        $('#Img1').attr("src", '../../assets/images/attachment_sel.png');
    }
    else {
        $('#Img1').attr("src", '../../assets/images/attachment_non_sel.png');
    }
}

function downloadfiles(index) {
    // window.open(g_serverpath + '/Common/FileDownload.aspx?indentno=NA&filename=' + $("#uploadTable tr")[index].cells[1].innerText + '&filetag=', 'Download', 'left=150,top=100,width=600,height=300,toolbar=no,menubars=no,status=no,scrollbars=yes,resize=no');
    window.open('/Common/FileDownload.aspx?indentno=NA&filename=' + $("#uploadTable tr")[index].cells[1].innerText + '&filetag=', 'Download', 'left=150,top=100,width=600,height=300,toolbar=no,menubars=no,status=no,scrollbars=yes,resize=no');
}

function deletefile(RowIndex) {
    try {
        //var lastRow = $('#uploadTable tr').length;
        var filename = $('#uploadTable tr')[RowIndex].cells[1].innerText;
        var tbl = document.getElementById("uploadTable");
        var lastRow = tbl.rows.length;
        if (lastRow <= 1)
            return false;
        for (var contolIndex = RowIndex; contolIndex < lastRow - 1; contolIndex++) {
            //$("#del" + (contolIndex + 1)).onclick = new Function("deletefile(" + contolIndex + ")");
            //$("#del" + (contolIndex + 1)).id = "del" + contolIndex;
            document.getElementById("del" + (parseInt(contolIndex) + 1)).onclick = new Function("deletefile(" + contolIndex + ")");
            document.getElementById("del" + (parseInt(contolIndex) + 1)).id = "del" + contolIndex;

            $("#a_downloadfiles" + (contolIndex + 1)).onclick = new Function("downloadfiles(" + contolIndex + ")");
            $("#a_downloadfiles" + (contolIndex + 1)).id = "a_downloadfiles" + contolIndex;

            document.getElementById("a_downloadfiles" + (parseInt(contolIndex) + 1)).onclick = new Function("downloadfiles(" + contolIndex + ")");
            document.getElementById("a_downloadfiles" + (parseInt(contolIndex) + 1)).id = "a_downloadfiles" + contolIndex;

            //$("#txt_Document_Name" + (contolIndex + 1)).value = contolIndex;
            //$("#txt_Document_Name" + (contolIndex + 1)).id = "txt_Document_Name" + contolIndex;
            document.getElementById("txt_Document_Name" + (parseInt(contolIndex) + 1)).id = "txt_Document_Name" + contolIndex;

        }
        tbl.deleteRow(RowIndex);
        //$('#uploadTable tr').eq(RowIndex).remove();
        var tbllength = $('#uploadTable tr').length;
        if (tbllength > 1) {
            $('#Img1').attr("src", '../../assets/images/attachment_sel.png');
        }
        else {
            $('#Img1').attr("src", '../../assets/images/attachment_non_sel.png');
        }
        deletephysicalfile(filename);
    }
    catch (Exc) { }
}

function deletephysicalfile(filename) {
    ContractualInstruction.DeleteFile(filename, callback_deletefile);
}

function callback_deletefile(response) {
    if (response.value != "") {
        alert("Document Removed Successfully..");
    }
}

function CapLetters(id) {
    var txt = document.getElementById(id);
    var start = txt.selectionStart;
    var end = txt.selectionEnd;
    txt.value = txt.value.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
    txt.setSelectionRange(start, end);
}