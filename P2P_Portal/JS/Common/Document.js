﻿
$(document).ready(function () {
    onLoad();

});
function onLoad() {
   // fillDocumentDetails();
    fillDocumentDetailsLot();
    fillDocumentDetailsLine();
    fillDocumentDetailsTerm();
    fillDocumentDetailsGenTerm();
    fillDocumentDetailsPayTerm();
}

function fillDocumentDetails() {
    var DisplayData = "";
    DisplayData = "<table class='table color-bordered-table info-bordered-table' id='tbl_DocumentDtl'><thead><tr class='grey'><th>Description</th><th>File Name</th><th>Delete</th></tr></thead>";
    DisplayData += "</table>";
    $("#div_Doc").html(DisplayData);
}

function uploadError(sender, args) {
    alert(args.get_errorMessage());
    var uploadText = document.getElementById('FileUpload1').getElementsByTagName("input");
    for (var i = 0; i < uploadText.length; i++) {
        if (uploadText[i].type == 'text') {
            uploadText[i].value = '';
        }
    }
}

function StartUpload(sender, args) {

}
function UploadComplete(sender, args) {
    var filename = args.get_fileName();
    var contentType = args.get_contentType();
    var filelength = args.get_length();
    var doctdesc = document.getElementById('txt_description').value;
    if (doctdesc == "") {
        alert("Please Enter Description For Document!")
        var uploadText = document.getElementById('FileUpload1').getElementsByTagName("input");
        for (var i = 0; i < uploadText.length; i++) {
            if (uploadText[i].type == 'file') {
                uploadText[i].value = "";
            }
        }
    }
    else if (parseInt(filelength) == 0) {
        alert("Validation Error:Sorry cannot upload file ! File is Empty or file does not exist");
    }
    else if (parseInt(filelength) > 4000000) {
        alert("Validation Error:Sorry cannot upload file ! File Size Exceeded.");

    }
    else if (contentType == "application/octet-stream") {
        alert("Validation Error:Kindly Check File Type.");
    }
    else {
        filename = filename.replace(" ", "");
        filename = filename.replace("  ", "");
        filename = filename.replace("(", "_");
        filename = filename.replace(")", "_");
        filename = filename.replace("&", "");
        filename = filename.replace("/", "");
        filename = filename.replace("'", "");
        addToClientTable(filename, args.get_length());
    }
    var uploadText = document.getElementById('FileUpload1').getElementsByTagName("input");
    for (var i = 0; i < uploadText.length; i++) {
        if (uploadText[i].type == 'file') {
            uploadText[i].value = '';
        }
    }
    document.forms[0].target = "";
}

function addToClientTable(name, size) {

    var HTML = "";
    var tbl = document.getElementById("tbl_DocumentDtl");
    var lastRow = tbl.rows.length;
    var row = tbl.insertRow(lastRow);
    
    oCell = row.insertCell();
    HTML = "<td style='border: 1px solid #ADBBCA;cursor: pointer' valign='middle'>" + $('#txt_description').val() + "</td>";
    oCell.innerHTML = HTML;

    oCell = row.insertCell();
    HTML = "<td style='border: 1px solid #ADBBCA;cursor: pointer' valign='middle'><a id='a_downloadfiles" + lastRow + "'  style='color:blue;'  onclick=\"return downloadfiles('" + lastRow + "');\" ><u>" + name + "</u></a></td>";
    oCell.innerHTML = HTML;

    oCell = row.insertCell();
    HTML = "<td style='border: 1px solid #ADBBCA;' align='left' width='5%' valign='middle'><img id='del" + lastRow + "' alt='Click Here To Delete The Record.'  onclick=\"return deletefile(" + (lastRow) + ");\" src='/assets/img/ico-delete.gif'  style='vertical-align:middle;'/></td>";
    oCell.innerHTML = HTML;

    changeattchment();
    $('#txt_description').val('');
}

//to delete file from list
function deletefile(RowIndex) {
    try {
        var tbl = document.getElementById("tbl_DocumentDtl");
        var lastRow = tbl.rows.length;
        var filename = tbl.rows[RowIndex].cells[1].innerText;
        if (lastRow <= 1)
            return false;
        for (var contolIndex = RowIndex; contolIndex < lastRow - 1; contolIndex++) {
            document.getElementById("del" + (contolIndex + 1)).onclick = new Function("deletefile(" + contolIndex + ")");
            document.getElementById("del" + (contolIndex + 1)).id = "del" + contolIndex;
            document.getElementById("a_downloadfiles" + (contolIndex + 1)).onclick = new Function("downloadfiles(" + contolIndex + ")");
            document.getElementById("a_downloadfiles" + (contolIndex + 1)).id = "a_downloadfiles" + contolIndex;

        }
        tbl.deleteRow(RowIndex);
        changeattchment();
        //  deletephysicalfile(filename);

    }
    catch (Exc) { }
}
//to delete file from physical location
function deletephysicalfile(filename) {
    Asset_Custody.DeleteFile(filename, document.getElementById("txt_LostAssetNo").value, callback_deletefile);
}
function callback_deletefile(response) {
    if (response.value != "") {
        alert("Document Removed Successfully..");
    }
    $find("MP_Document").show();
}

function changeattchment() {
    var tbl = document.getElementById("tbl_DocumentDtl");
    var lastRow = tbl.rows.length;
    if (lastRow <= 1) {
        $('#doc_img').attr('src', '../../assets/images/attachment.png')
       
    }
    else {
        $('#doc_img').attr('src', '../../assets/images/attchgreen.png')
      
    }
}
///////////////////////////////////////////////////
//////////////////////////////////////////////////

function fillDocumentDetailsLot() {
    var DisplayData = "";
    DisplayData = "<table class='table color-bordered-table info-bordered-table' id='tbl_DocumentDtlLot'><thead><tr class='grey'><th>Description</th><th>File Name</th><th>Delete</th></tr></thead>";
    DisplayData += "<tbody id='tbody_DocumentDtlLot'></tbody></table>";
    $("#div_doclot").html(DisplayData)

}

function uploadError(sender, args) {
    alert(args.get_errorMessage());
    var uploadText = document.getElementById('FileUploadLot').getElementsByTagName("input");
    for (var i = 0; i < uploadText.length; i++) {
        if (uploadText[i].type == 'text') {
            uploadText[i].value = '';
        }
    }
}


function StartUpload(sender, args) {

}
function UploadCompleteLot(sender, args) {
    var filename = args.get_fileName();
    var contentType = args.get_contentType();
    var filelength = args.get_length();
    var doctdesc = document.getElementById('txt_description_lot').value;
    if (doctdesc == "") {
        alert("Please Enter Description For Document!")
        var uploadText = document.getElementById('FileUploadLot').getElementsByTagName("input");
        for (var i = 0; i < uploadText.length; i++) {
            if (uploadText[i].type == 'file') {
                uploadText[i].value = "";
            }
        }
    }
    else if (parseInt(filelength) == 0) {
        alert("Validation Error:Sorry cannot upload file ! File is Empty or file does not exist");
    }
    else if (parseInt(filelength) > 4000000) {
        alert("Validation Error:Sorry cannot upload file ! File Size Exceeded.");

    }
    else if (contentType == "application/octet-stream") {
        alert("Validation Error:Kindly Check File Type.");
    }
    else {
        filename = filename.replace(" ", "");
        filename = filename.replace("  ", "");
        filename = filename.replace("(", "_");
        filename = filename.replace(")", "_");
        filename = filename.replace("&", "");
        filename = filename.replace("/", "");
        filename = filename.replace("'", "");
        addToClientTableLot(filename, args.get_length());
    }
    var uploadText = document.getElementById('FileUploadLot').getElementsByTagName("input");
    for (var i = 0; i < uploadText.length; i++) {
        if (uploadText[i].type == 'file') {
            uploadText[i].value = '';
        }
    }
    document.forms[0].target = "";
}

function addToClientTableLot(name, size) {

    var HTML = "";
    var tbl = document.getElementById("tbl_DocumentDtlLot");
    var lastRow = tbl.rows.length;
    var row = tbl.insertRow(lastRow);

    oCell = row.insertCell();
    HTML = "<td style='border: 1px solid #ADBBCA;cursor: pointer' valign='middle'>" + $('#txt_description_lot').val() + "</td>";
    oCell.innerHTML = HTML;

    oCell = row.insertCell();
    HTML = "<td style='border: 1px solid #ADBBCA;cursor: pointer' valign='middle'><a id='a_downloadfiles" + lastRow + "'  style='color:blue;'  onclick=\"return downloadfilesLot('" + lastRow + "');\" ><u>" + name + "</u></a></td>";
    oCell.innerHTML = HTML;

    oCell = row.insertCell();
    HTML = "<td style='border: 1px solid #ADBBCA;' align='left' width='5%' valign='middle'><img id='del" + lastRow + "' alt='Click Here To Delete The Record.'  onclick=\"return deletefilelot(" + (lastRow) + ");\" src='/assets/img/ico-delete.gif'  style='vertical-align:middle;'/></td>";
    oCell.innerHTML = HTML;

    changeattchment();
    $('#txt_description_lot').val('');
}

//to delete file from list
function deletefilelot(RowIndex) {
    try {
        var tbl = document.getElementById("tbl_DocumentDtlLot");
        var lastRow = tbl.rows.length;
        //var filename = tbl.rows[RowIndex].cells[1].innerText;
        if (lastRow <= 1)
            return false;
        for (var contolIndex = RowIndex; contolIndex < lastRow - 1; contolIndex++) {
            document.getElementById("del" + (contolIndex + 1)).onclick = new Function("deletefilelot(" + contolIndex + ")");
            document.getElementById("del" + (contolIndex + 1)).id = "del" + contolIndex;
            document.getElementById("a_downloadfiles" + (contolIndex + 1)).onclick = new Function("downloadfilesLot(" + contolIndex + ")");
            document.getElementById("a_downloadfiles" + (contolIndex + 1)).id = "a_downloadfiles" + contolIndex;

        }
        tbl.deleteRow(RowIndex);
        changeattchment();
        //  deletephysicalfile(filename);

    }
    catch (Exc) { }
}
///////////////////////////////////////////////////
//////////////////////////////////////////////////

function fillDocumentDetailsLine() {
    var DisplayData = "";
    DisplayData = "<table class='table color-bordered-table info-bordered-table' id='tbl_DocumentDtlLine'><thead><tr class='grey'><th>Description</th><th>File Name</th><th>Delete</th></tr></thead>";
    DisplayData += "<tbody id='tbody_DocumentDtlLine'></tbody></table>";
    $("#div_docline").html(DisplayData)
}

function uploadError(sender, args) {
    alert(args.get_errorMessage());
    var uploadText = document.getElementById('FileUploadLine').getElementsByTagName("input");
    for (var i = 0; i < uploadText.length; i++) {
        if (uploadText[i].type == 'text') {
            uploadText[i].value = '';
        }
    }
}

function StartUpload(sender, args) {

}
function UploadCompleteLine(sender, args) {
    var filename = args.get_fileName();
    var contentType = args.get_contentType();
    var filelength = args.get_length();
    var doctdesc = document.getElementById('txt_description_line').value;
    if (doctdesc == "") {
        alert("Please Enter Description For Document!")
        var uploadText = document.getElementById('FileUploadLine').getElementsByTagName("input");
        for (var i = 0; i < uploadText.length; i++) {
            if (uploadText[i].type == 'file') {
                uploadText[i].value = "";
            }
        }
    }
    else if (parseInt(filelength) == 0) {
        alert("Validation Error:Sorry cannot upload file ! File is Empty or file does not exist");
    }
    else if (parseInt(filelength) > 4000000) {
        alert("Validation Error:Sorry cannot upload file ! File Size Exceeded.");

    }
    else if (contentType == "application/octet-stream") {
        alert("Validation Error:Kindly Check File Type.");
    }
    else {
        filename = filename.replace(" ", "");
        filename = filename.replace("  ", "");
        filename = filename.replace("(", "_");
        filename = filename.replace(")", "_");
        filename = filename.replace("&", "");
        filename = filename.replace("/", "");
        filename = filename.replace("'", "");
        addToClientTableLine(filename, args.get_length());
    }
    var uploadText = document.getElementById('FileUploadLine').getElementsByTagName("input");
    for (var i = 0; i < uploadText.length; i++) {
        if (uploadText[i].type == 'file') {
            uploadText[i].value = '';
        }
    }
    document.forms[0].target = "";
}

function addToClientTableLine(name, size) {
    var cnnt = 0;
    var HTML = "";
    var tbl = document.getElementById("tbl_DocumentDtlLine");
    var lastRow = tbl.rows.length;
    var row = tbl.insertRow(lastRow);

    oCell = row.insertCell();
    HTML = "<td style='border: 1px solid #ADBBCA;cursor: pointer' valign='middle'>" + $('#txt_description_line').val() + "</td>";
    oCell.innerHTML = HTML;

    oCell = row.insertCell();
    HTML = "<td style='border: 1px solid #ADBBCA;cursor: pointer' valign='middle'><a id='a_downloadfiles" + lastRow + "'  style='color:blue;'  onclick=\"return downloadfilesLine('" + lastRow + "');\" ><u>" + name + "</u></a></td>";
    oCell.innerHTML = HTML;

    oCell = row.insertCell();
    HTML = "<td style='border: 1px solid #ADBBCA;' align='left' width='5%' valign='middle'><img id='del" + lastRow + "' alt='Click Here To Delete The Record.'  onclick=\"return deletefileline(" + (lastRow) + ");\" src='/assets/img/ico-delete.gif'  style='vertical-align:middle;'/></td>";
    oCell.innerHTML = HTML;

    changeattchment();
    $('#txt_description_line').val('');
}

//to delete file from list
function deletefileline(RowIndex) {
    try {
        var tbl = document.getElementById("tbl_DocumentDtlLine");
        var lastRow = tbl.rows.length;
        var filename = tbl.rows[RowIndex].cells[1].innerText;
        if (lastRow <= 1)
            return false;
        for (var contolIndex = RowIndex; contolIndex < lastRow - 1; contolIndex++) {
            document.getElementById("del" + (contolIndex + 1)).onclick = new Function("deletefileline(" + contolIndex + ")");
            document.getElementById("del" + (contolIndex + 1)).id = "del" + contolIndex;
            document.getElementById("a_downloadfiles" + (contolIndex + 1)).onclick = new Function("downloadfilesLine(" + contolIndex + ")");
            document.getElementById("a_downloadfiles" + (contolIndex + 1)).id = "a_downloadfiles" + contolIndex;

        }
        tbl.deleteRow(RowIndex);
        changeattchment();
        //  deletephysicalfile(filename);

    }
    catch (Exc) { }
}
///////////////////////////////////////////////////
//////////////////////////////////////////////////

function fillDocumentDetailsTerm() {
    var DisplayData = "";
    DisplayData = "<table class='table color-bordered-table info-bordered-table' id='tbl_DocumentDtlTerms'><thead><tr class='grey'><th>Description</th><th>File Name</th><th>Delete</th></tr></thead>";
    DisplayData += "</table>";
    $("#div_docterm").html(DisplayData)

}

function uploadError(sender, args) {
    alert(args.get_errorMessage());
    var uploadText = document.getElementById('FileUploadTerms').getElementsByTagName("input");
    for (var i = 0; i < uploadText.length; i++) {
        if (uploadText[i].type == 'text') {
            uploadText[i].value = '';
        }
    }
}


function StartUpload(sender, args) {

}
function UploadCompleteTerm(sender, args) {
    var filename = args.get_fileName();
    var contentType = args.get_contentType();
    var filelength = args.get_length();
    var doctdesc = document.getElementById('txt_description_terms').value;
    if (doctdesc == "") {
        alert("Please Enter Description For Document!")
        var uploadText = document.getElementById('FileUploadTerms').getElementsByTagName("input");
        for (var i = 0; i < uploadText.length; i++) {
            if (uploadText[i].type == 'file') {
                uploadText[i].value = "";
            }
        }
    }
    else if (parseInt(filelength) == 0) {
        alert("Validation Error:Sorry cannot upload file ! File is Empty or file does not exist");
    }
    else if (parseInt(filelength) > 4000000) {
        alert("Validation Error:Sorry cannot upload file ! File Size Exceeded.");

    }
    else if (contentType == "application/octet-stream") {
        alert("Validation Error:Kindly Check File Type.");
    }
    else {
        filename = filename.replace(" ", "");
        filename = filename.replace("  ", "");
        filename = filename.replace("(", "_");
        filename = filename.replace(")", "_");
        filename = filename.replace("&", "");
        filename = filename.replace("/", "");
        filename = filename.replace("'", "");
        addToClientTableTerm(filename, args.get_length());
    }
    var uploadText = document.getElementById('FileUploadTerms').getElementsByTagName("input");
    for (var i = 0; i < uploadText.length; i++) {
        if (uploadText[i].type == 'file') {
            uploadText[i].value = '';
        }
    }
    document.forms[0].target = "";
}

function addToClientTableTerm(name, size) {

    var HTML = "";
    var tbl = document.getElementById("tbl_DocumentDtlTerms");
    var lastRow = tbl.rows.length;
    var row = tbl.insertRow(lastRow);


    oCell = row.insertCell();
    HTML = "<td style='border: 1px solid #ADBBCA;cursor: pointer' valign='middle'>" + $('#txt_description_terms').val() + "</td>";
    oCell.innerHTML = HTML;

    oCell = row.insertCell();
    HTML = "<td style='border: 1px solid #ADBBCA;cursor: pointer' valign='middle'><a id='a_downloadfiles" + lastRow + "' style='color:blue;'  onclick=\"return downloadfilesTerms('" + lastRow + "');\" ><u>" + name + "</u></a></td>";
    oCell.innerHTML = HTML;

    oCell = row.insertCell();
    HTML = "<td style='border: 1px solid #ADBBCA;' align='left' width='5%' valign='middle'><img id='del" + lastRow + "' alt='Click Here To Delete The Record.'  onclick=\"return deletefileterms(" + (lastRow) + ");\" src='/assets/img/ico-delete.gif'  style='vertical-align:middle;'/></td>";
    oCell.innerHTML = HTML;

    changeattchment();
    $('#txt_description_terms').val('');
}

//to delete file from list
function deletefileterms(RowIndex) {
    try {
        var tbl = document.getElementById("tbl_DocumentDtlTerms");
        var lastRow = tbl.rows.length;
        var filename = tbl.rows[RowIndex].cells[1].innerText;
        if (lastRow <= 1)
            return false;
        for (var contolIndex = RowIndex; contolIndex < lastRow - 1; contolIndex++) {
            document.getElementById("del" + (contolIndex + 1)).onclick = new Function("deletefileterms(" + contolIndex + ")");
            document.getElementById("del" + (contolIndex + 1)).id = "del" + contolIndex;
            document.getElementById("a_downloadfiles" + (contolIndex + 1)).onclick = new Function("downloadfilesTerms(" + contolIndex + ")");
            document.getElementById("a_downloadfiles" + (contolIndex + 1)).id = "a_downloadfiles" + contolIndex;

        }
        tbl.deleteRow(RowIndex);
        changeattchment();
        //  deletephysicalfile(filename);

    }
    catch (Exc) { }
}
////////////////////////Genaral//////////////////////////

function fillDocumentDetailsGenTerm() {
    var DisplayData = "";
    DisplayData = "<table class='table color-bordered-table info-bordered-table' id='tbl_DocumentDtlGenTerms'><thead><tr class='grey'><th>Description</th><th>File Name</th><th>Delete</th></tr></thead>";
    DisplayData += "</table>";
    $("#div_generalterms").html(DisplayData)

}

function uploadError(sender, args) {
    alert(args.get_errorMessage());
    var uploadText = document.getElementById('FileUploadGenTerms').getElementsByTagName("input");
    for (var i = 0; i < uploadText.length; i++) {
        if (uploadText[i].type == 'text') {
            uploadText[i].value = '';
        }
    }
}


function StartUpload(sender, args) {

}
function UploadCompleteGenTerm(sender, args) {
    var filename = args.get_fileName();
    var contentType = args.get_contentType();
    var filelength = args.get_length();
    var doctdesc = document.getElementById('txt_description_gterms').value;
    if (doctdesc == "") {
        alert("Please Enter Description For Document!")
        var uploadText = document.getElementById('FileUploadGenTerms').getElementsByTagName("input");
        for (var i = 0; i < uploadText.length; i++) {
            if (uploadText[i].type == 'file') {
                uploadText[i].value = "";
            }
        }
    }
    else if (parseInt(filelength) == 0) {
        alert("Validation Error:Sorry cannot upload file ! File is Empty or file does not exist");
    }
    else if (parseInt(filelength) > 4000000) {
        alert("Validation Error:Sorry cannot upload file ! File Size Exceeded.");

    }
    else if (contentType == "application/octet-stream") {
        alert("Validation Error:Kindly Check File Type.");
    }
   else {
        filename = filename.replace(" ", "");
        filename = filename.replace("  ", "");
        filename = filename.replace("(", "_");
        filename = filename.replace(")", "_");
        filename = filename.replace("&", "");
        filename = filename.replace("/", "");
        filename = filename.replace("'", "");
        addToClientTableGenTerm(filename, args.get_length());
    }
    var uploadText = document.getElementById('FileUploadGenTerms').getElementsByTagName("input");
    for (var i = 0; i < uploadText.length; i++) {
        if (uploadText[i].type == 'file') {
            uploadText[i].value = '';
        }
    }
    document.forms[0].target = "";
}

function addToClientTableGenTerm(name, size) {

    var HTML = "";
    var tbl = document.getElementById("tbl_DocumentDtlGenTerms");
    var lastRow = tbl.rows.length;
    var row = tbl.insertRow(lastRow);


    oCell = row.insertCell();
    HTML = "<td style='border: 1px solid #ADBBCA;cursor: pointer' valign='middle'>" + $('#txt_description_gterms').val() + "</td>";
    oCell.innerHTML = HTML;

    oCell = row.insertCell();
    HTML = "<td style='border: 1px solid #ADBBCA;cursor: pointer' valign='middle'><a id='a_downloadfiles" + lastRow + "'  style='color:blue;'  onclick=\"return downloadfilesGenTerms('" + lastRow + "');\" ><u>" + name + "</u></a></td>";
    oCell.innerHTML = HTML;

    oCell = row.insertCell();
    HTML = "<td style='border: 1px solid #ADBBCA;' align='left' width='5%' valign='middle'><img id='del" + lastRow + "' alt='Click Here To Delete The Record.'  onclick=\"return deletefilegenterms(" + (lastRow) + ");\" src='/assets/img/ico-delete.gif'  style='vertical-align:middle;'/></td>";
    oCell.innerHTML = HTML;

  //  changeattchment();
    $('#txt_description_gterms').val('');
}

//to delete file from list
function deletefilegenterms(RowIndex) {
    try {
        var tbl = document.getElementById("tbl_DocumentDtlGenTerms");
        var lastRow = tbl.rows.length;
        var filename = tbl.rows[RowIndex].cells[1].innerText;
        if (lastRow <= 1)
            return false;
        for (var contolIndex = RowIndex; contolIndex < lastRow - 1; contolIndex++) {
            document.getElementById("del" + (contolIndex + 1)).onclick = new Function("deletefilegenterms(" + contolIndex + ")");
            document.getElementById("del" + (contolIndex + 1)).id = "del" + contolIndex;
            document.getElementById("a_downloadfiles" + (contolIndex + 1)).onclick = new Function("downloadfilesGenTerms(" + contolIndex + ")");
            document.getElementById("a_downloadfiles" + (contolIndex + 1)).id = "a_downloadfiles" + contolIndex;

        }
        tbl.deleteRow(RowIndex);
       // changeattchment();
        //  deletephysicalfile(filename);

    }
    catch (Exc) { }
}
//////////////////////////////////////////////////

function fillDocumentDetailsPayTerm() {
    var DisplayData = "";
    DisplayData = "<table class='table color-bordered-table info-bordered-table' id='tbl_DocumentDtlPayTerms'><thead><tr class='grey'><th>Description</th><th>File Name</th><th>Delete</th></tr></thead>";
    DisplayData += "</table>";
    $("#div_paymentterms").html(DisplayData)

}
function uploadError(sender, args) {
    alert(args.get_errorMessage());
    var uploadText = document.getElementById('FileUploadPayTerms').getElementsByTagName("input");
    for (var i = 0; i < uploadText.length; i++) {
        if (uploadText[i].type == 'text') {
            uploadText[i].value = '';
        }
    }
}
function StartUpload(sender, args) {
}
function UploadCompletePayTerm(sender, args) {
    var filename = args.get_fileName();
    var contentType = args.get_contentType();
    var filelength = args.get_length();
    var doctdesc = document.getElementById('txt_description_pterms').value;
    if (doctdesc == "") {
        alert("Please Enter Description For Document!")
        var uploadText = document.getElementById('FileUploadPayTerms').getElementsByTagName("input");
        for (var i = 0; i < uploadText.length; i++) {
            if (uploadText[i].type == 'file') {
                uploadText[i].value = "";
            }
        }
    }
    else if (parseInt(filelength) == 0) {
        alert("Validation Error:Sorry cannot upload file ! File is Empty or file does not exist");
    }
    else if (parseInt(filelength) > 4000000) {
        alert("Validation Error:Sorry cannot upload file ! File Size Exceeded.");

    }
    else if (contentType == "application/octet-stream") {
        alert("Validation Error:Kindly Check File Type.");
    }
    else {
        filename = filename.replace(" ", "");
        filename = filename.replace("  ", "");
        filename = filename.replace("(", "_");
        filename = filename.replace(")", "_");
        filename = filename.replace("&", "");
        filename = filename.replace("/", "");
        filename = filename.replace("'", "");
        addToClientTablePayTerm(filename, args.get_length());
    }
    var uploadText = document.getElementById('FileUploadPayTerms').getElementsByTagName("input");
    for (var i = 0; i < uploadText.length; i++) {
        if (uploadText[i].type == 'file') {
            uploadText[i].value = '';
        }
    }
    document.forms[0].target = "";
}

function addToClientTablePayTerm(name, size) {

    var HTML = "";
    var tbl = document.getElementById("tbl_DocumentDtlPayTerms");
    var lastRow = tbl.rows.length;
    var row = tbl.insertRow(lastRow);


    oCell = row.insertCell();
    HTML = "<td style='border: 1px solid #ADBBCA;cursor: pointer' valign='middle'>" + $('#txt_description_pterms').val() + "</td>";
    oCell.innerHTML = HTML;

    oCell = row.insertCell();
    HTML = "<td style='border: 1px solid #ADBBCA;cursor: pointer' valign='middle'><a id='a_downloadfiles" + lastRow + "'  style='color:blue;' onclick=\"return downloadfilesPayTerms('" + lastRow + "');\" ><u>" + name + "</u></a></td>";
    oCell.innerHTML = HTML;

    oCell = row.insertCell();
    HTML = "<td style='border: 1px solid #ADBBCA;' align='left' width='5%' valign='middle'><img id='del" + lastRow + "' alt='Click Here To Delete The Record.'  onclick=\"return deletefilepayterms(" + (lastRow) + ");\" src='/assets/img/ico-delete.gif'  style='vertical-align:middle;'/></td>";
    oCell.innerHTML = HTML;

    changeattchment();
    $('#txt_description_pterms').val('');
}

//to delete file from list
function deletefilepayterms(RowIndex) {
    try {
        var tbl = document.getElementById("tbl_DocumentDtlPayTerms");
        var lastRow = tbl.rows.length;
        var filename = tbl.rows[RowIndex].cells[1].innerText;
        if (lastRow <= 1)
            return false;
        for (var contolIndex = RowIndex; contolIndex < lastRow - 1; contolIndex++) {
            document.getElementById("del" + (contolIndex + 1)).onclick = new Function("deletefilepayterms(" + contolIndex + ")");
            document.getElementById("del" + (contolIndex + 1)).id = "del" + contolIndex;
            document.getElementById("a_downloadfiles" + (contolIndex + 1)).onclick = new Function("downloadfilesPayTerms(" + contolIndex + ")");
            document.getElementById("a_downloadfiles" + (contolIndex + 1)).id = "a_downloadfiles" + contolIndex;

        }
        tbl.deleteRow(RowIndex);
        changeattchment();
    }
    catch (Exc) { }
}

