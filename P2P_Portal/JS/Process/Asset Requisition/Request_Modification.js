﻿
var DtlJson = [], PK_ID = 0;
var UserID, BranchID, requestIni_Email, requestInitiator, requestNo;
var PK_ID = 0, catid = 0, subcatid = 0;
var DocumentJSON = [];
var AuditJson = "";
var requestNo = "";
var POtype = "";
var deleteXml = [];

$(document).ready(function () {
    $("#form1").trigger("reset");
    addmodalpopup();
    $("#loading-div-background").show();
    $("#spn_userTotal").text(0);
    LoadReqDetails();
    LoadAuditData();
    //getBudgetdata();
});

function LoadReqDetails() {
    var ProID = $("#txt_ProcessID").val();
    var InsID = $("#txt_InstanceID").val();
    var StepName = $("#txt_StepName").val();
    PageMethods.GetReqDetails(ProID, InsID, $("#txt_Master_Url").val(), $("#txt_P2P_Url").val(), $("#txt_Wfe_Url").val(), function (response) {
        if (response[0] != "[]") {
            DtlJson = JSON.parse(JSON.parse(response[0]));
            assignHDRValue();
        }
        else {
            $("#loading-div-background").hide();
            $.jAlert({
                'content': 'No Data Found..!'
            });
            return false;
        }
    },
        function (error) {
        });
}

function assignHDRValue() {
    if (DtlJson.length > 0) {
        PK_ID = DtlJson[0].PK_REQ_HDR_ID;
        requestIni_Email = DtlJson[0].EMAILID;
        requestInitiator = DtlJson[0].CREATED_BY;
        requestNo = DtlJson[0].REQUEST_NO;
        POtype = DtlJson[0].POtype;

        $("#lbl_ReqNo").text(DtlJson[0].REQUEST_NO);
        $("#lblrequester").text(DtlJson[0].EMP_NAME);
        $("#lblreqdate").text(DtlJson[0].CREATION_DATE);
        $("#lbl_category").text(DtlJson[0].CATNAME);
        catid = DtlJson[0].FK_CAT_ID;

        $("#lbl_Branch").text(DtlJson[0].BRANCH_NAME);
        $("#lbl_Desi").text(DtlJson[0].DESIGNATION_NAME);
        $("#lbl_Grade").text(DtlJson[0].GRADE_NAME);
        $("#lbl_Department").text(DtlJson[0].Department);

        $("#txt_Remark").val(DtlJson[0].REMARK);
        $("#txt_justification").val(DtlJson[0].JUSTIFICATION);

        branchID = DtlJson[0].FK_BRANCH_ID;

        LoadSubCategory();
        displayDtlData();

        PageMethods.getDocumentData(requestNo, $("#txt_P2P_Url").val(), function (response) {
            if (response[0] != "Error") {
                DocumentJSON = JSON.parse(response[0]);
                displayDocumentData();
            }
        }, function (error) {
        });
    } else {
        $("#loading-div-background").hide();
        $.jAlert({
            'content': 'No Data Found..!'
        });
        return false;
    }
}

function LoadSubCategory() {
    PageMethods.GetSubCategory(catid, $("#txt_Master_Url").val(), function (response) {
        SubcatJson = JSON.parse(JSON.parse(response[0]));
        if (SubcatJson.Message == "Data Not Found") {
            $.jAlert({
                'content': 'Sub Category Not Found..!'
            });
            $("#loading-div-background").hide();
            return false;
        }
        else {
            BindSubCategory(1);
        }
    },
        function (error) {
        });
}

function BindSubCategory(index) {
    $('#sel_Subcat' + index).autocomplete({
        autoFocus: true,
        minLength: 3,
        source: SubcatJson,
        select: function (event, ui) {
            event.preventDefault();
            subcatid = ui.item.id;
            $('#sel_Subcat' + index).val(ui.item.label);
            $('#txt_Subcat_ID' + index).val(ui.item.id);
            $('#sel_Item' + index).val('');
            $('#txt_Item_ID' + index).val('');
            $("#txtQuantity_" + index).prop('disabled', false);
            LoadItem(index);
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $.jAlert({
                    'content': 'Please Select Valid Sub Category..!'
                });
                $(this).val("");
                $("#txtQuantity_" + index).prop('disabled', true);
                $("#txtQuantity_" + index).val('');
                //$("#Total" + index).val('');
                //CalTotal(index);
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function LoadItem(index) {
    PageMethods.GetItem(catid, subcatid, $("#txt_Master_Url").val(), function (response) {
        ItemJson = JSON.parse(JSON.parse(response[0]));
        if (ItemJson.Message == "Data Not Found") {
            $.jAlert({
                'content': 'Item Not Found..!'
            });
            $("#txtQuantity_" + index).prop('disabled', false);
            $("#loading-div-background").hide();
            return false;
        }

        else {
            BindItem(index);
        }
    },
        function (error) {
        });
}

function LoadItem1(index, subcatid) {
    PageMethods.GetItem(catid, subcatid, $("#txt_Master_Url").val(), function (response) {
        ItemJson = JSON.parse(JSON.parse(response[0]));
        if (ItemJson.Message == "Data Not Found") {
            $.jAlert({
                'content': 'Item Not Found..!'
            });
            $('#sel_Item' + index).val('');
            $("#loading-div-background").hide();
            return false;
        }

        else {
            BindItem(index);
        }
    },
        function (error) {
        });
}

function BindItem(index) {
    $('#sel_Item' + index).autocomplete({
        autoFocus: true,
        minLength: 3,
        source: ItemJson,
        select: function (event, ui) {
            event.preventDefault();
            $('#sel_Item' + index).val(ui.item.label);
            $('#txt_Itm_ID' + index).val(ui.item.id);
            $("#txtQuantity_" + index).prop('disabled', false);
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $.jAlert({
                    'content': 'Please Select Valid Item..!'
                });
                $(this).val("");
                $("#txtQuantity_" + index).prop('disabled', true);
                $("#txtQuantity_" + index).val('');
                $("#Total" + index).val('');
                // CalTotal(index);
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function displayDtlData() {
    if (DtlJson) {
        var tableHeader = "";
        var tableBody = "";
        tableHeader += "<th style='width: 2%; text-align: center'>#</th>";
        tableHeader += "<th>Sub-Category</th>";
        tableHeader += "<th>Item</th>";
        tableHeader += "<th>Quantity</th>";
        tableHeader += "<th style='width: 3%; text-align: center'>Add</th>";
        tableHeader += "<th style='width: 3%; text-align: center'>Delete</th>";
        $.each(DtlJson, function (key, val) {
            tableBody += "<tr><td><span id='index" + (key + 1) + "'>" + (key + 1) + "</span>";
            tableBody += "<input class='form-control input-sm width-xs' type='text' style='display:none;' value=" + val.PK_REQ_DTL_ID + "  id='txt_pkDtlId" + (key + 1) + "'></input></td>";
            tableBody += "<td><input value=" + val.SUB_CATNAME + " class='form-control input-sm mandatory' id='sel_Subcat" + (key + 1) + "' type='text' placeholder='Select Sub-Category' onclick='return BindSubCategory(" + (key + 1) + ")' />";
            tableBody += "<input value=" + val.FK_SUB_CAT_ID + " id='txt_Subcat_ID" + (key + 1) + "'  name='SubHid' style='display:none' type='text'/></td>";
            tableBody += "<td><input value=" + val.ITMNAME + " class='form-control input-sm mandatory' id='sel_Item" + (key + 1) + "' type='text' placeholder='Select Item' />";
            tableBody += "<input value=" + val.FK_ITEM_ID + " id='txt_Itm_ID" + (key + 1) + "' style='display:none' type='text'/></td>";
            tableBody += "<td><input style='text-align: right' value=" + val.QUANTITY + " id='txtQuantity_" + (key + 1) + "' name='quantity' class='form-control input-sm mandatory' type='text' onkeypress='return isNumberKey(event)'  disabled='disabled' ></input></td>";
            tableBody += "<td class='text-center' id='add" + (key + 1) + "'><a id='add_row" + (key + 1) + "' onclick='return initiateAddRowTask(" + (key + 1) + ",true)'> <img src='../../assets/images/add.png' title='Add New Row' /></a></td>";
            tableBody += "<td class='text-center' id='rem" + (key + 1) + "'><a id='del_row" + (key + 1) + "' onclick='delete_row(" + (key + 1) + ")'><img src='../../assets/images/delete.png' title='Delete Row' /></a></td></tr>";
            tableBody += "</tr>";
        });
        var table = "<table ID='tbl_Data' class='display nowrap dataTable'>" +
            "<thead><tr class='grey'>" + tableHeader + "</tr></thead>" +
            "<tbody>" + tableBody + "</tbody>" +
            "</table>";
        $("#div_displayReqDtl").empty();
        $("#div_displayReqDtl").html(table);
        //getspanAmount();

        var lastRow = $('#tbl_Data tr').length;
        for (var q = 0; q < lastRow - 1; q++) {
            LoadItem1(q + 1, $("#txt_Subcat_ID" + (q + 1) + "").val());
        }
        $("#loading-div-background").hide();
    }
}

function initiateAddRowTask(key, isValidationRequired) {
    if (isValidationRequired) {
        if (validExpenseDetails(key)) {
            addRow();
        }
    } else {
        addRow();
    }
}

function validExpenseDetails(i) {
    var lastRow = $('#tbl_Data tr').length;

    for (var key = 1; key < lastRow; key++) {
        var Sel_Sub_Cat = $("#sel_Subcat" + key).val();
        var Sel_Item = $("#sel_Item" + key).val();
        var Sel_Quantity = $("#txtQuantity_" + key).val();
        if (Sel_Sub_Cat == "") {
            $.jAlert({
                'content': 'Please Select Sub Category At Row :' + key + ''
            });
            return false;
        }
        if (Sel_Item == "") {
            $.jAlert({
                'content': 'Please Select Item At Row :' + key + ''
            });
            return false;
        }
        if (Sel_Quantity == "0" || Sel_Quantity == "") {
            $.jAlert({
                'content': 'Please Enter Quantity At Row :' + key + ''
            });
            return false;
        }
    }
    return true;
}

function addRow() {
    var tableBody = "";
    var tbl = $("#tbl_Data tr").length;
    tableBody += "<tr><td><span id='index" + tbl + "'>" + tbl + "</span>";
    tableBody += "<input class='form-control input-sm width-xs' type='text' style='display:none;' value='0'  id='txt_pkDtlId" + tbl + "'></input></td>";
    tableBody += "<td><input value='' class='form-control input-sm mandatory' id='sel_Subcat" + tbl + "' type='text' placeholder='Select Sub-Category' onclick='return BindSubCategory(" + tbl + ")' />";
    tableBody += "<input value='' id='txt_Subcat_ID" + tbl + "'  name='SubHid' style='display:none' type='text'/></td>";
    tableBody += "<td><input value='' class='form-control input-sm mandatory' id='sel_Item" + tbl + "' type='text' placeholder='Select Item' />";
    tableBody += "<input value='' id='txt_Itm_ID" + tbl + "' style='display:none' type='text'/></td>";
    tableBody += "<td><input style='text-align: right' id='txtQuantity_" + tbl + "' name='quantity' class='form-control input-sm mandatory' type='text' onkeypress='return isNumberKey(event)'  disabled='disabled' ></input></td>";
    tableBody += "<td class='text-center' id='add" + tbl + "'><a id='add_row" + tbl + "' onclick='return initiateAddRowTask(" + tbl + ",true)'> <img src='../../assets/images/add.png' title='Add New Row' /></a></td>";
    tableBody += "<td class='text-center' id='rem" + tbl + "'><a id='del_row" + tbl + "' onclick='delete_row(" + tbl + ")'><img src='../../assets/images/delete.png' title='Delete Row' /></a></td></tr>";
    tableBody += "</tr>";

    $("#tbl_Data tbody").append(tableBody);
}

function LoadAuditData() {
    PageMethods.getAuditData($("#txt_ProcessID").val(), $("#txt_InstanceID").val(), $("#txt_Master_Url").val(), $("#txt_P2P_Url").val(), $("#txt_Wfe_Url").val(), function (response) {
        AuditJson = JSON.parse(JSON.parse(response[0]));
        displayAuditData();
    },
        function (error) {
        });
}

function displayAuditData() {
    if (AuditJson) {
        var tableHeader = "";
        var tableBody = "";

        tableHeader += "<th align='center'>SR.No</th>";
        tableHeader += "<th align='center'>Step Name</th>";
        tableHeader += "<th align='center'>Performer</th>";
        tableHeader += "<th align='center'>Date</th>";
        tableHeader += "<th align='center'>Action Name</th>";
        tableHeader += "<th align='center'>Remark</th>";

        $.each(AuditJson, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.STEPNAME + "</td>";
            tableBody += "<td>" + val.ACTIONBYUSER + "</td>";
            tableBody += "<td>" + val.ACTIONDATE + "</td>";
            tableBody += "<td>" + val.ACTION + "</td>";
            tableBody += "<td>" + val.REMARK + "</td>";
            tableBody += "</tr>";
        });

        var table = "<table ID='tblAudit' class='display nowrap dataTable'>" +
            "<thead><tr class='grey'>" + tableHeader + "</tr></thead>" +
            "<tbody>" + tableBody + "</tbody>" +
            "</table>";
        $("#div_Audit").empty();
        $("#div_Audit").html(table);
        $("#loading-div-background").hide();
    }
}

function displayDocumentData() {
    if (DocumentJSON != "[]") {
        var tableHeader = "";
        var tableBody = "";

        //tableHeader += "<th align='center'>SR.No</th>";
        //tableHeader += "<th align='center'>FileName</th>";
        //tableHeader += "<th align='center'>Remark</th>";

        //$.each(DocumentJSON, function (key, val) {
        //    tableBody += "<tr>";
        //    tableBody += "<td>" + (key + 1) + "</td>";
        //    tableBody += "<td><input class='hidden' type='text' name='txt_Region_Add" + (key + 1) + "' id='txt_Document_File" + (key + 1) + "' value=" + val.FILENAME + " readonly ></input><a id='a_downloadfiles" + (key + 1) + "' style='cursor: pointer' onclick=\"return downloadfiles1('" + (key + 1) + "');\" >" + val.FILENAME + "</a><input class='hidden' type='text' name='txt_pono" + (key + 1) + "' id='txt_poreqno" + (key + 1) + "' value=" + val.OBJECT_VALUE + " readonly /></td>";
        //    tableBody += "<td>" + val.DOCUMENT_TYPE + "</td>";
        //    tableBody += "</tr>";
        //});
        tableHeader += "<th align='center'>Description</th>";
        tableHeader += "<th align='center'>File Name</th>";
        tableHeader += "<th align='center'>Delete</th>";

        $.each(DocumentJSON, function (key, val) {
            tableBody += "<tr>";
            //tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.DOCUMENT_TYPE + "</td>";
            tableBody += "<td><input class='hidden' type='text' name='txt_Region_Add" + (key + 1) + "' id='txt_Document_File" + (key + 1) + "' value=" + val.FILENAME + " readonly ></input><a id='a_downloadfiles" + (key + 1) + "' style='cursor: pointer' onclick=\"return downloadfiles1('" + (key + 1) + "');\" >" + val.FILENAME + "</a><input class='hidden' type='text' name='txt_pono" + (key + 1) + "' id='txt_poreqno" + (key + 1) + "' value=" + val.OBJECT_VALUE + " readonly /></td>";
            tableBody += "<td><i id='del" + (key + 1) + "' class='glyphicon glyphicon-trash' align='center' onclick=\"return deletefile(" + (key + 1) + ");\" ></td>";
            tableBody += "</tr>";
        });

        var table = "<table ID='uploadTable' class='display nowrap dataTable'>" +
            "<thead><tr class='grey'>" + tableHeader + "</tr></thead>" +
            "<tbody>" + tableBody + "</tbody>" +
            "</table>";
        $("#div_docs").empty();
        $("#div_docs").html(table);
        var tbllength = $('#uploadTable tr').length;
        if (tbllength > 1) {
            $('#Img1').attr("src", '../../assets/images/attachment_sel.png');
        }
        else {
            $('#Img1').attr("src", '../../assets/images/attachment_non_sel.png');
        }
        $("#loading-div-background").hide();
    }
}

function downloadfiles1(index) {
    window.open('/Common/FileDownload.aspx?indentno=' + $("#txt_poreqno" + index).val() + '&filename=' + $("#tblDoc tr")[index].cells[1].innerText + '&filetag=', 'Download', 'left=150,top=100,width=600,height=300,toolbar=no,menubars=no,status=no,scrollbars=yes,resize=no');
}

function delete_row(RowIndex) {
    try {
        var tbl = document.getElementById("tbl_Data");
        var lastRow = tbl.rows.length;
        if (lastRow <= 2) {
            $.jAlert({
                'content': 'Validation Error:You have to Enter atleast one record..!'
            });
            return false;
        }

        var pkdtlId = $("#txt_pkDtlId" + RowIndex).val();
        detail = { "PK_REQ_DTL_ID": pkdtlId }
        deleteXml.push(detail);

        for (var contolIndex = RowIndex; contolIndex < lastRow - 1; contolIndex++) {

            document.getElementById("index" + (parseInt(contolIndex) + 1)).id = "index" + contolIndex;
            $("#index" + (parseInt(contolIndex))).html(contolIndex);
            document.getElementById("sel_Subcat" + (parseInt(contolIndex) + 1)).onclick = new Function("return BindSubCategory(" + contolIndex + ")");
            document.getElementById("sel_Subcat" + (parseInt(contolIndex) + 1)).id = "sel_Subcat" + contolIndex;
            document.getElementById("txt_Subcat_ID" + (parseInt(contolIndex) + 1)).id = "txt_Subcat_ID" + contolIndex;
            //document.getElementById("txtbaserate_" + (parseInt(contolIndex) + 1)).id = "txtbaserate_" + contolIndex;

            document.getElementById("sel_Item" + (parseInt(contolIndex) + 1)).id = "sel_Item" + contolIndex;
            document.getElementById("txt_Itm_ID" + (parseInt(contolIndex) + 1)).id = "txt_Itm_ID" + contolIndex;
            document.getElementById("txtQuantity_" + (parseInt(contolIndex) + 1)).id = "txtQuantity_" + contolIndex;
            //document.getElementById("Total" + (parseInt(contolIndex) + 1)).id = "Total" + contolIndex;

            document.getElementById("add_row" + (parseInt(contolIndex) + 1)).onclick = new Function("return initiateAddRowTask(" + contolIndex + "," + "true" + ")");
            document.getElementById("add_row" + (parseInt(contolIndex) + 1)).id = "add_row" + contolIndex;
            document.getElementById("del_row" + (parseInt(contolIndex) + 1)).onclick = new Function("delete_row(" + contolIndex + ")");
            document.getElementById("del_row" + (parseInt(contolIndex) + 1)).id = "del_row" + contolIndex;
            document.getElementById("add" + (parseInt(contolIndex) + 1)).id = "add" + contolIndex;
            document.getElementById("rem" + (parseInt(contolIndex) + 1)).id = "rem" + contolIndex;
        }
        tbl.deleteRow(RowIndex);
        //CalTotal();
        $.jAlert({
            'content': 'Record Deleted Successfully..!'
        });
    }
    catch (Exc) { }
}

//function getBudgetDetails(budid, budsubid, finyear) {
//    //$("#loading-div-background").show();
//    //PageMethods.getBudgetDetailData(budid, budsubid, finyear, function (response) {
//    //    BudDtlJson = JSON.parse(response[0]);
//    //    if (response[0] != "[]") {
//    displayBudgetData();
//    //    }
//    //}, function (error) {
//    //    alert(error);
//    //});
//}

//function displayBudgetData() {
//    //if (obj) {
//    var tableHeader = "";
//    var tableBody = "";

//    tableHeader += "<th>#</th>";
//    tableHeader += " <th>Budget Head</th>";
//    tableHeader += " <th>Budget SubHead</th>";
//    tableHeader += " <th>Budget Code</th>";
//    tableHeader += " <th>Fin Year</th>";
//    tableHeader += " <th>Opening Budget</th>";
//    tableHeader += " <th>Consumed Budget</th>";
//    tableHeader += " <th>Inprogress Budget</th>";
//    tableHeader += " <th>Remaining Budget</th>";

//    //$.each(obj, function (key, val) {
//    var key = 0;
//    tableBody += "<tr id='row_" + (key + 1) + "'>";
//    tableBody += "<td name='srno'>" + (key + 1) + "</td>";
//    tableBody += "<td>1</td>";
//    tableBody += "<td>2</td>";
//    tableBody += "<td>3</td>";
//    tableBody += "<td>4</td>";
//    tableBody += "<td class='text-right'>5</td>";
//    tableBody += "<td class='text-right'>6</td>";
//    tableBody += "<td class='text-right'>7</td>";
//    tableBody += "<td class='text-right'>8</td>";

//    //tableBody += "<td>" + val.BUDGET_HEAD + "</td>";
//    //tableBody += "<td>" + val.BUD_SUB_HEAD + "</td>";
//    //tableBody += "<td>" + val.BUD_CODE + "</td>";
//    //tableBody += "<td>" + val.FINANCIAL_YEAR + "</td>";
//    //tableBody += "<td class='text-right'>" + val.BUDGET_AMOUNT.toFixed(2) + "</td>";
//    //tableBody += "<td class='text-right'>" + val.BUDGET_CONS.toFixed(2) + "</td>";
//    //tableBody += "<td class='text-right'>" + val.BUDGET_INPR.toFixed(2) + "</td>";
//    //tableBody += "<td class='text-right'>" + val.BAL_BUDGET.toFixed(2) + "</td>";
//    tableBody += "</tr>";
//    //})
//    var table = "<table class='display nowrap dataTable' id='tbl_BudDetails' runat='server'>" +
//        "<thead style='background-color:gray'><tr>" + tableHeader + "</tr></thead>" +
//        "<tbody>" + tableBody + "</tbody>" +
//        "</table>";
//    $("#div_Budget_details").empty();
//    $("#div_Budget_details").html(table);
//    //}
//}



$("#btn_Save").click(function () {
    try {
        $("#loading-div-background").show();
        var action = "SUBMIT";
        var HdrData = [];
        var DtlData = [];
        var DocData = [];

        var HdrData1 = {
            "CREATED_BY": $("#SessionAdId").val(), "REQUEST_NO": requestNo, "FK_PROCESSID": $("#txt_ProcessID").val(), "FK_INSTANCEID": $("#txt_InstanceID").val(),
            "REMARK": $("#txt_Remark").val(), "JUSTIFICATION": $("#txt_justification").val(), "STATUS": "INPROGRESS"
        };

        HdrData.push(HdrData1);

        var tbllength = $('#tbl_Data tr').length;
        if (tbllength > 1) {
            for (var key = 1; key < tbllength; key++) {

                var PkDtlId = $("#txt_pkDtlId" + key).val();
                if (PkDtlId == "") {
                    PkDtlId = "0";
                }
                var Sel_Cat = $("#txt_category").val();
                var Sel_Sub_Cat = $("#sel_Subcat" + key).val();
                var Sel_Item = $("#sel_Item" + key).val();
                var Sel_Quantity = $("#txtQuantity_" + key).val();
                //var Total = 0;

                if (Sel_Sub_Cat == "") {
                    $.jAlert({
                        'content': 'Please Select Sub Category At Row :' + key + ''
                    });
                    $("#loading-div-background").hide();
                    return false;
                }
                if (Sel_Item == "") {
                    $.jAlert({
                        'content': 'Please Select Item At Row :' + key + ''
                    });
                    $("#loading-div-background").hide();
                    return false;
                }
                if (parseFloat(Sel_Quantity) == 0 || Sel_Quantity == "") {
                    $.jAlert({
                        'content': 'Please Enter Quantity At Row :' + key + ''
                    });
                    $("#loading-div-background").hide();
                    return false;
                }
                var DtlData1 = {
                    "PK_REQ_DTL_ID": PkDtlId, "FK_CAT_ID": catid, "FK_SUB_CAT_ID": $("#txt_Subcat_ID" + key).val(), "FK_ITEM_ID": $("#txt_Itm_ID" + key).val(),
                    "QUANTITY": Sel_Quantity
                };
                DtlData.push(DtlData1);
            }
        }
        else {
            $.jAlert({
                'content': 'Please Add Requisition Details In Table...!'
            });
            $("#loading-div-background").hide();
            return false;
        }
        var lastRow1 = $('#uploadTable tr').length;
        if (lastRow1 > 1) {
            for (var l = 0; l < lastRow1 - 1; l++) {
                var firstCol = $("#uploadTable tr")[l + 1].cells[0].innerText;
                var SecondCol = $("#uploadTable tr")[l + 1].cells[1].innerText;
                var DocData1 = {
                    "OBJECT_TYPE": "ASSET REQUISITION", "OBJECT_VALUE": "0", "DOCUMENT_TYPE": firstCol, "FILENAME": SecondCol
                }
                DocData.push(DocData1);
            }
        }

        var transactionData = { "hdrData": HdrData, "dtlData": DtlData, "dltXml": deleteXml, "docData": DocData }
        $.jAlert({
            'title': 'Confirmation',
            'content': 'Are You Sure To Submit This Request..?', 'type': 'confirm',
            'onConfirm': function () {
                PageMethods.SaveData(JSON.stringify(transactionData), $("#SessionAdId").val(), $("#txt_Email").val(), $('#lbl_category').text(), POtype, action, $("#txt_StepName").val(), $("#txt_Master_Url").val(), $("#txt_P2P_Url").val(), $("#txt_Wfe_Url").val(), function (response) {
                    if (response != "Error occured while saving data" && response != "Approver not found") {
                        var saveMsg = "Request Submitted Successfully. Request Number is " + response;
                        $.jAlert({
                            'content': saveMsg,
                            'onClose': function () {
                                location.href = "../../HomePage.aspx";
                            }
                        });
                    }
                    else {
                        $.jAlert({
                            'content': response,
                        });
                        $("#loading-div-background").hide();
                        return false;
                    }
                }, function (error) {
                });
                $("#loading-div-background").hide();
                return true;
            },
            'onDeny': function () {
                $("#loading-div-background").hide();
                return false;
            }
        });
    }
    catch (exception) {
        return false;
    }
});
