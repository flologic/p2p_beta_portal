﻿
var DtlJson = [];
var UserID, BranchID, requestIni_Email, requestInitiator, requestNo;
var PK_ID = 0;
var Cat_ID = 0;
var Item_ID = 0;
var Branch_ID = 0;
var DocumentJSON = [];
var AuditJson = "";
var requestNo = "";
var requestdate = "";
var supplierJson = "", SuplbranchJson = "", ratecontractitmJson = "";
var supplierlabel = "", supplierpk, Suplbranchid, Suplbranchlabel, contact_name, brnchbillabel, brnchbilid, signlabel, signid;

$(document).ready(function () {
    $("#form1").trigger("reset");
    addmodalpopup();
    $("#loading-div-background").show();
    $("#spn_userTotal").text(0);
    Client_ID = $("#txt_Client_ID").val();
    Master_API = $("#txt_Master_Url").val();
    P2P_API = $("#txt_P2P_Url").val();
    LoadReqDetails();
    getSuplddl();
    LoadAuditData();
    //getBudgetdata();
});

function LoadReqDetails() {
    var ProID = $("#txt_ProcessID").val();
    var InsID = $("#txt_InstanceID").val();
    var StepName = $("#txt_StepName").val();
    PageMethods.GetReqDetails(ProID, InsID, $("#txt_Master_Url").val(), $("#txt_P2P_Url").val(), $("#txt_Wfe_Url").val(), function (response) {
        if (response[0] != "[]") {
            DtlJson = JSON.parse(JSON.parse(response[0]));
            assignHDRValue();
        }
        else {
            $("#loading-div-background").hide();
            $.jAlert({
                'content': 'No Data Found..!'
            });
            return false;
        }
    },
        function (error) {
        });
}

function assignHDRValue() {
    if (DtlJson.length > 0) {
        PK_ID = DtlJson[0].PK_HDR_ID;
        requestIni_Email = DtlJson[0].EMAILID;
        requestInitiator = DtlJson[0].CREATED_BY;
        requestNo = DtlJson[0].REQUEST_NO;
        $("#lbl_ReqNo").text(DtlJson[0].REQUEST_NO);
        $("#lblrequester").text(DtlJson[0].EMP_NAME);
        $("#lblreqdate").text(DtlJson[0].CREATION_DATE);
        $("#lbl_category").text(DtlJson[0].CATNAME);
        $("#lbl_Branch").text(DtlJson[0].BRANCH_NAME);
        $("#lbl_Desi").text(DtlJson[0].DESIGNATION_NAME);
        $("#lbl_Grade").text(DtlJson[0].GRADE_NAME);
        $("#lbl_Department").text(DtlJson[0].Department);

        $("#txt_Remark").text(DtlJson[0].REMARK);
        $("#txt_justification").text(DtlJson[0].JUSTIFICATION);
        Cat_ID = DtlJson[0].FK_CAT_ID;
        branchID = DtlJson[0].FK_BRANCH_ID;

        displayDtlData();

        //PageMethods.getDocumentDetail(requestNo, $("#txt_Master_Url").val(), function (response) {
        //    DocumentJSON = JSON.parse(response[0]);
        //    displayDocumentData();
        //}, function (error) {
        //});
    } else {
        $("#loading-div-background").hide();
        $.jAlert({
            'content': 'No Data Found..!'
        });
        return false;
    }
    //$("#loading-div-background").hide();
}

function getSuplddl() {
    PageMethods.getSuplData(Client_ID, Master_API, function (response) {
        supplierJson = JSON.parse(JSON.parse(response[0]));
        //alert(supplierJson);
        assignSupl();

    }, function (error) {
        alert(error);
    });
}

function assignSupl() {
    $('#sel_Supplier').autocomplete({
        autoFocus: true,
        minLength: 3,
        source: supplierJson,
        select: function (event, ui) {
            event.preventDefault();
            $('#sel_Supplier').val(ui.item.label);
            supplierlabel = ui.item.label;
            supplierpk = ui.item.id;
            displayDtlData();
            // getSuplDtlData(supplierpk);
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $.jAlert({
                    'content': 'Please Select Valid Supplier..!'
                });
                $(this).val("");
                supplierpk = 0;
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function checkforratecontract(index, Cat_ID, Item_ID, Branch_ID, requestdate) {
    PageMethods.CheckRateContractItms(Master_API, P2P_API, Cat_ID, Item_ID, Branch_ID, requestdate, function (response) {
        var rsp = JSON.parse(response[0]);
        if (rsp != "[]") {
            ratecontractitmJson = JSON.parse(JSON.parse(response[0]));
            document.getElementById('Suplbl' + index).innerHTML = ratecontractitmJson[0].Supplier;
            //$("#Suplbl" + index).innerHTML = ratecontractitmJson[0].Supplier;
            $("#supplier_ID" + index).val(ratecontractitmJson[0].FK_SUPPLIER_ID);
            $("#Rate" + index).val(ratecontractitmJson[0].RATE);
        }
        else {
            document.getElementById('Suplbl' + index).innerHTML = supplierlabel;
            $("#supplier_ID" + index).val(supplierpk);
        }

    }, function (error) {
        alert(error);
    });
}

function getSuplDtlData(pkid) {
    PageMethods.getSuplDtlData(Client_ID, Master_API, pkid, function (response) {
        SuplbranchJson = JSON.parse(JSON.parse(response[0]));
        assignSuplDtl();
    }, function (error) {
        alert(error);
    });
}

function displayDtlData() {
    if (DtlJson) {
        var tableHeader = "";
        var tableBody = "";
        tableHeader += "<th style='width: 2%; text-align: center'>#</th>";
        tableHeader += "<th>Sub-Category</th>";
        tableHeader += "<th>Item</th>";
        tableHeader += "<th style='width: 20%; text-align: center'>Supplier</th>";
        tableHeader += "<th style='width: 10%; text-align: center'>Quantity</th>";
        tableHeader += "<th style='width: 10%; text-align: center'>Rate</th>";
        tableHeader += "<th style='width: 10%; text-align: center'>Tax Amount</th>";
        tableHeader += "<th style='width: 10%; text-align: center'>Total Amount</th>";
        $.each(DtlJson, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "";
            tableBody += "<input value=" + val.PK_REQ_DTL_ID + " id='txt_PKDtl_ID" + (key + 1) + "' style='display:none' type='text'/></td>";
            tableBody += "<td>" + val.SUB_CATNAME + "</td>";
            tableBody += "<td>" + val.ITMNAME + "";
            tableBody += "<input value=" + val.FK_ITEM_ID + " id='txt_Itm_ID" + (key + 1) + "' style='display:none' type='text'/>";
            tableBody += "<input type='text' id='supplier_ID" + (key + 1) + "' value='0'  style ='display:none' /></td>";
            tableBody += "<td id='Suplbl" + (key + 1) + "'></td>";
            tableBody += "<td class='text-right'><input style='text-align: right' class='form-control input-sm mandatory' type='text' readonly id='Quantity" + (key + 1) + "' value=" + val.QUANTITY + "></input></td>";
            tableBody += "<td class='text-right'><input style='text-align: right' class='form-control input-sm mandatory' type='text' id='Rate" + (key + 1) + "' value=" + val.RATE.toFixed(2) + " onchange='CalTotal(" + (key + 1) + ")'></input></td>";
            tableBody += "<td class='text-right'><input style='text-align: right' class='form-control input-sm mandatory' type='text' id='TaxAmount" + (key + 1) + "' value=" + val.TAX_AMOUNT.toFixed(2) + " onchange='CalTotal(" + (key + 1) + ")'></input></td>";
            tableBody += "<td class='text-right'><input style='text-align: right' class='form-control input-sm mandatory' type='text' readonly id='TotalAmount" + (key + 1) + "' value=" + (val.TOTAL_AMOUNT).toFixed(2) + "></input></td>";
            tableBody += "</tr>";

            var table = "<table ID='tblDtlData' class='display nowrap dataTable'>" +
                "<thead><tr class='grey'>" + tableHeader + "</tr></thead>" +
                "<tbody>" + tableBody + "</tbody>" +
                "</table>";
            $("#div_displayReqDtl").empty();
            $("#div_displayReqDtl").html(table);

            getspanAmount();
            $("#loading-div-background").hide();

            //});
        });
    }
    var lastRow = $('#tblDtlData tr').length;
    for (var q = 0; q < lastRow - 1; q++) {
        checkforratecontract(q + 1, Cat_ID, $("#txt_Itm_ID" + (q + 1) + "").val(), branchID, $("#lblreqdate").text());
        $("#supplier_ID" + (q + 1)).val(supplierpk);
    }
}

function LoadAuditData() {
    PageMethods.getAuditData($("#txt_ProcessID").val(), $("#txt_InstanceID").val(), $("#txt_Master_Url").val(), $("#txt_P2P_Url").val(), $("#txt_Wfe_Url").val(), function (response) {
        AuditJson = JSON.parse(JSON.parse(response[0]));
        displayAuditData();
    },
        function (error) {
        });
}

function displayAuditData() {
    if (AuditJson) {
        var tableHeader = "";
        var tableBody = "";

        tableHeader += "<th align='center'>SR.No</th>";
        tableHeader += "<th align='center'>Step Name</th>";
        tableHeader += "<th align='center'>Performer</th>";
        tableHeader += "<th align='center'>Date</th>";
        tableHeader += "<th align='center'>Action Name</th>";
        tableHeader += "<th align='center'>Remark</th>";

        $.each(AuditJson, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.STEPNAME + "</td>";
            tableBody += "<td>" + val.ACTIONBYUSER + "</td>";
            tableBody += "<td>" + val.ACTIONDATE + "</td>";
            tableBody += "<td>" + val.ACTION + "</td>";
            tableBody += "<td>" + val.REMARK + "</td>";
            tableBody += "</tr>";
        });

        var table = "<table ID='tblAudit' class='display nowrap dataTable'>" +
            "<thead><tr class='grey'>" + tableHeader + "</tr></thead>" +
            "<tbody>" + tableBody + "</tbody>" +
            "</table>";
        $("#div_Audit").empty();
        $("#div_Audit").html(table);
        $("#loading-div-background").hide();
    }
}

function displayDocumentData() {
    if (DocumentJSON) {
        var tableHeader = "";
        var tableBody = "";

        tableHeader += "<th align='center'>SR.No</th>";
        tableHeader += "<th align='center'>FileName</th>";
        tableHeader += "<th align='center'>Remark</th>";

        $.each(DocumentJSON, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td><input class='hidden' type='text' name='txt_Region_Add" + (key + 1) + "' id='txt_Document_File" + (key + 1) + "' value=" + val.FILENAME + " readonly ></input><a id='a_downloadfiles" + (key + 1) + "' style='cursor: pointer' onclick=\"return downloadfiles1('" + (key + 1) + "');\" >" + val.FILENAME + "</a><input class='hidden' type='text' name='txt_pono" + (key + 1) + "' id='txt_poreqno" + (key + 1) + "' value=" + val.OBJECT_VALUE + " readonly /></td>";
            tableBody += "<td>" + val.DOCUMENT_TYPE + "</td>";
            tableBody += "</tr>";
        });

        var table = "<table ID='tblDoc' class='display nowrap dataTable'>" +
            "<thead><tr class='grey'>" + tableHeader + "</tr></thead>" +
            "<tbody>" + tableBody + "</tbody>" +
            "</table>";
        $("#div_docs").empty();
        $("#div_docs").html(table);
        var tbllength = $('#tblDoc tr').length;
        if (tbllength > 1) {
            $('#Img1').attr("src", '../../assets/images/attachment_sel.png');
        }
        else {
            $('#Img1').attr("src", '../../assets/images/attachment_non_sel.png');
        }
        $("#loading-div-background").hide();
    }
}

function downloadfiles1(index) {
    window.open('/Common/FileDownload.aspx?indentno=' + $("#txt_poreqno" + index).val() + '&filename=' + $("#tblDoc tr")[index].cells[1].innerText + '&filetag=', 'Download', 'left=150,top=100,width=600,height=300,toolbar=no,menubars=no,status=no,scrollbars=yes,resize=no');
}

function getBudgetdata() {
    PageMethods.getBudgetDetails($("#SessionAdId").val(), Finyear, function (response) {
        if (response[0] != "[]") {
            BudgetJson = JSON.parse(response);
            bindBudgetData(DtlJson);
        }
        else {
            $.jAlert({
                'content': 'No Data Found..!',
                'onClose': function () {
                    $("#div_budget_details").empty();
                    $("#loading-div-background").hide();
                }
            });
            return false;
        }
    }, function (error) {
    });
}

function getspanAmount() {
    var len = $("#tblDtlData tr").length;
    $("#spn_userTotal").text(0);
    var spanTotal = 0;
    if (len > 0) {
        for (var i = 1; i < len; i++) {
            var item_total = 0;
            item_total = $("#TotalAmount" + i).val();
            if (spanTotal == 0) {
                spanTotal = item_total;
            }
            else {
                spanTotal = parseFloat(item_total) + parseFloat(spanTotal);
            }
        }
        $("#spn_userTotal").text(spanTotal);
    }
}

function CalTotal(index) {
    var land_tot = 0;
    var tot = 0;
    var totl = 0;
    var lastRow1 = $('#tblDtlData tr').length;
    for (var i = 1; i <= lastRow1 - 1; i++) {
        var Qty = $("#Quantity" + i).val();//$(this).closest('tr').find('[name=valqty]').val();
        var Taxamt = $("#TaxAmount" + i).val();//$(this).closest('tr').find('[name=valqty]').val();
        var Rate = $("#Rate" + i).val();//$(this).closest('tr').find('[name=valRate]').val();//
        if (Rate != "") {
            tot = parseFloat(Qty) * parseFloat(Rate);
            totl = parseFloat(tot) + parseFloat(Taxamt);
            $("#TotalAmount" + i).val(totl.toFixed(2));
            land_tot = parseFloat(land_tot) + parseFloat(totl);
        }
    }
    getspanAmount();
}

function getBudgetDetails(budid, budsubid, finyear) {
    //$("#loading-div-background").show();
    //PageMethods.getBudgetDetailData(budid, budsubid, finyear, function (response) {
    //    BudDtlJson = JSON.parse(response[0]);
    //    if (response[0] != "[]") {
    displayBudgetData();
    //    }
    //}, function (error) {
    //    alert(error);
    //});
}

function displayBudgetData() {
    //if (obj) {
    var tableHeader = "";
    var tableBody = "";

    tableHeader += "<th>#</th>";
    tableHeader += " <th>Budget Head</th>";
    tableHeader += " <th>Budget SubHead</th>";
    tableHeader += " <th>Budget Code</th>";
    tableHeader += " <th>Fin Year</th>";
    tableHeader += " <th>Opening Budget</th>";
    tableHeader += " <th>Consumed Budget</th>";
    tableHeader += " <th>Inprogress Budget</th>";
    tableHeader += " <th>Remaining Budget</th>";

    //$.each(obj, function (key, val) {
    var key = 0;
    tableBody += "<tr id='row_" + (key + 1) + "'>";
    tableBody += "<td name='srno'>" + (key + 1) + "</td>";
    tableBody += "<td>1</td>";
    tableBody += "<td>2</td>";
    tableBody += "<td>3</td>";
    tableBody += "<td>4</td>";
    tableBody += "<td class='text-right'>5</td>";
    tableBody += "<td class='text-right'>6</td>";
    tableBody += "<td class='text-right'>7</td>";
    tableBody += "<td class='text-right'>8</td>";

    //tableBody += "<td>" + val.BUDGET_HEAD + "</td>";
    //tableBody += "<td>" + val.BUD_SUB_HEAD + "</td>";
    //tableBody += "<td>" + val.BUD_CODE + "</td>";
    //tableBody += "<td>" + val.FINANCIAL_YEAR + "</td>";
    //tableBody += "<td class='text-right'>" + val.BUDGET_AMOUNT.toFixed(2) + "</td>";
    //tableBody += "<td class='text-right'>" + val.BUDGET_CONS.toFixed(2) + "</td>";
    //tableBody += "<td class='text-right'>" + val.BUDGET_INPR.toFixed(2) + "</td>";
    //tableBody += "<td class='text-right'>" + val.BAL_BUDGET.toFixed(2) + "</td>";
    tableBody += "</tr>";
    //})
    var table = "<table class='display nowrap dataTable' id='tbl_BudDetails' runat='server'>" +
        "<thead style='background-color:gray'><tr>" + tableHeader + "</tr></thead>" +
        "<tbody>" + tableBody + "</tbody>" +
        "</table>";
    $("#div_Budget_details").empty();
    $("#div_Budget_details").html(table);
    //}
}

function getvendorDetails(budid, budsubid, finyear) {
    //$("#loading-div-background").show();
    //PageMethods.getBudgetDetailData(budid, budsubid, finyear, function (response) {
    //    BudDtlJson = JSON.parse(response[0]);
    //    if (response[0] != "[]") {
    displayBudgetData();
    //    }
    //}, function (error) {
    //    alert(error);
    //});
}

$("#btn_Save").click(function () {
    try {
        $("#loading-div-background").show();
        var action = $("#ddlAction").val();
        var approvalremark = $("#app_Remark").val();
        var DtlData = [];

        if ($("#sel_Supplier").val() == "") {
            $.jAlert({
                'content': 'Please Select Supplier..!'
            });
            $("#loading-div-background").hide();
            return false;
        }

        if (action == "0") {
            $.jAlert({
                'content': 'Please Select Action..!',
                'onClose': function () {
                    $('#ddlAction').focus();
                    $("#loading-div-background").hide();
                }
            });
            return false;
        }

        if (action != "APPROVE") {
            if (approvalremark == "") {
                $.jAlert({
                    'content': 'Please Enter Remark..!',
                    'onClose': function () {
                        $('#app_Remark').focus();
                        $("#loading-div-background").hide();
                    }
                });
                return false;
            }
        }


        var tbllength = $('#tblDtlData tr').length;
        if (tbllength > 1) {
            for (var key = 1; key < tbllength; key++) {
                var PkDtlid = $("#txt_PKDtl_ID" + key).val();
                //var supp_id = $("#supplier_ID" + key).val();
                var supp_id = supplierpk;
                var rate = $("#Rate" + key).val();
                var tax = $("#TaxAmount" + key).val();
                var Total = $("#TotalAmount" + key).val();

                if (supp_id == "0" || supp_id == "") {
                    $.jAlert({
                        'content': 'Supplier is Not Available At Row :' + key + ''
                    });
                    $("#loading-div-background").hide();
                    return false;
                }

                if (rate == "0.00" || rate == "") {
                    $.jAlert({
                        'content': 'Please Enter Rate For Item At Row :' + key + ''
                    });
                    $("#loading-div-background").hide();
                    return false;
                }
                if (Total == "0.00" || Total == "") {
                    $.jAlert({
                        'content': 'Total amount Should not be Zero At Row :' + key + ''
                    });
                    $("#loading-div-background").hide();
                    return false;
                }

                var DtlData1 = {
                    "PK_REQ_DTL_ID": PkDtlid, "FK_SUPPLIER_ID": supp_id, "RATE": rate, "TAX_AMOUNT": tax, "TOTAL_AMOUNT": Total
                };
                DtlData.push(DtlData1);
            }
        }
        else {
            $.jAlert({
                'content': 'Requisition Details Not Available...!'
            });
            $("#loading-div-background").hide();
            return false;
        }

        var transactiondata = { "dtlData": DtlData }

        PageMethods.SaveData($("#txt_ProcessID").val(), $("#txt_InstanceID").val(), $("#txt_StepName").val(), $("#SessionAdId").val(), $("#txt_Email").val(), requestNo, requestInitiator, requestIni_Email, action, approvalremark, $("#lbl_category").text(), Cat_ID, JSON.stringify(transactiondata), $("#txt_Master_Url").val(), $("#txt_P2P_Url").val(), $("#txt_Wfe_Url").val(), function (response) {
            if (response) {
                if (response == "true") {
                    var saveMsg = "Request " + action + " Successfully and Request Number is " + requestNo;
                    $.jAlert({
                        'content': saveMsg,
                        'onClose': function () {
                            location.href = "../../HomePage.aspx";
                            $("#loading-div-background").hide();
                        }
                    });
                }
                else {
                    $.jAlert({
                        'content': 'Error occured while saving data..!',
                    });
                    $("#loading-div-background").hide();
                    return false;
                }
            }
            $("#loading-div-background").hide();
        }, function (error) {
        });
        return true;
    }
    catch (exception) {
        return false;
    }
});


//$("#btn_Save").click(function () {
//    try {
//        $("#loading-div-background").show();
//        var action = "SUBMIT";
//        var HdrData = [];
//        var DtlData = [];
//        var DocData = [];

//        if ($("#txt_category").val() == "") {
//            $.jAlert({
//                'content': 'Please Select Category..!'
//            });
//            $("#loading-div-background").hide();
//            return false;
//        }
//        else if ($("#txt_category").val() == "ADMIN") {
//            if ($("#txt_po_type").val() == "") {
//                $.jAlert({
//                    'content': 'Please Select Po Type..!'
//                });
//                $("#loading-div-background").hide();
//                return false;
//            }
//        }


//        var HdrData1 = {
//            "CREATED_BY": $("#SessionAdId").val(), "FK_BRANCH_ID": Branchid,
//            "FK_CATEGORY_ID": catid, "REMARK": $("#txt_Remark").val(), "JUSTIFICATION": $("#txt_justification").val(), "STATUS": "INPROGRESS"
//        };

//        HdrData.push(HdrData1);

//        var tbllength = $('#tbl_Data tr').length;
//        if (tbllength > 1) {
//            for (var key = 1; key < tbllength; key++) {
//                //var Sel_Type = $("#sel_Type" + key).val();
//                //var Sel_Ref = $("#txtassetrefno_" + key).val();
//                var Sel_Cat = $("#txt_category").val();
//                var Sel_Sub_Cat = $("#sel_Subcat" + key).val();
//                var Sel_Item = $("#sel_Item" + key).val();
//                var Baserate = 0;;
//                var Sel_Quantity = $("#txtQuantity_" + key).val();
//                var Total = 0;

//                //if (Sel_Cat == "") {
//                //    $.jAlert({
//                //        'content': 'Please Select Category At Row :' + key + ''
//                //    });
//                //    return false;
//                //}
//                if (Sel_Sub_Cat == "") {
//                    $.jAlert({
//                        'content': 'Please Select Sub Category At Row :' + key + ''
//                    });
//                    $("#loading-div-background").hide();
//                    return false;
//                }
//                if (Sel_Item == "") {
//                    $.jAlert({
//                        'content': 'Please Select Item At Row :' + key + ''
//                    });
//                    $("#loading-div-background").hide();
//                    return false;
//                }
//                if (parseFloat(Sel_Quantity) == 0 || Sel_Quantity == "") {
//                    $.jAlert({
//                        'content': 'Please Enter Quantity At Row :' + key + ''
//                    });
//                    $("#loading-div-background").hide();
//                    return false;
//                }
//                var DtlData1 = {
//                    "FK_CAT_ID": catid, "FK_SUB_CAT_ID": $("#txt_Subcat_ID" + key).val(), "FK_ITEM_ID": $("#txt_Itm_ID" + key).val(),
//                    "BASE_RATE": Baserate, "QUANTITY": Sel_Quantity, "TOTAL_AMOUNT": Total
//                };
//                DtlData.push(DtlData1);
//            }
//        }
//        else {
//            $.jAlert({
//                'content': 'Please Add Requisition Details In Table...!'
//            });
//            $("#loading-div-background").hide();
//            return false;
//        }
//        var lastRow1 = $('#uploadTable tr').length;
//        if (lastRow1 > 1) {
//            for (var l = 0; l < lastRow1 - 1; l++) {
//                var firstCol = $("#uploadTable tr")[l + 1].cells[0].innerText;
//                var SecondCol = $("#uploadTable tr")[l + 1].cells[1].innerText;
//                var DocData1 = {
//                    "OBJECT_TYPE": "ASSET REQUISITION", "OBJECT_VALUE": "0", "DOCUMENT_TYPE": firstCol, "FILENAME": SecondCol
//                }
//                DocData.push(DocData1);
//            }
//        }

//        var transactionData = { "hdrData": HdrData, "dtlData": DtlData, "docData": DocData }

//        $.jAlert({
//            'title': 'Confirmation',
//            'content': 'Are You Sure To Submit This Request..?', 'type': 'confirm',
//            'onConfirm': function () {
//                $("#loading-div-background").show();
//                PageMethods.SaveData(JSON.stringify(transactionData), $("#SessionAdId").val(), $("#EmailID").val(), $('#txt_category').val(), $("#txt_po_type").val(), action, $("#txt_Master_Url").val(), $("#txt_P2P_Url").val(), $("#txt_Wfe_Url").val(), function (response) {
//                    if (response != "Error occured while saving data" && response != "Approver not found") {
//                        var saveMsg = "Request Submitted Successfully With Request Number is " + response;
//                        $.jAlert({
//                            'content': saveMsg,
//                            'onClose': function () {
//                                location.href = "../../HomePage.aspx";
//                            }
//                        });
//                    }
//                    //else if (response == "false") {
//                    //    var errMsg = "Approver not found";
//                    //    $.jAlert({
//                    //        'content': errMsg,
//                    //    });
//                    //    $("#loading-div-background").hide();
//                    //    return false;
//                    //}
//                    else {
//                        $.jAlert({
//                            'content': response,
//                        });
//                        $("#loading-div-background").hide();
//                        return false;
//                    }
//                }, function (error) {
//                });
//                $("#loading-div-background").hide();
//                return true;
//            },
//            'onDeny': function () {
//                $("#loading-div-background").hide();
//                return false;
//            }
//        });
//    }
//    catch (exception) {
//        return false;
//    }
//});
