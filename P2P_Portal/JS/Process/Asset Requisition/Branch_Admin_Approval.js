﻿
var DtlJson = [], PK_ID = 0;
var UserID, BranchID, requestIni_Email, requestInitiator, requestNo;
var PK_ID = 0;
var DocumentJSON = [];
var AuditJson = "";
var requestNo = "";
var POtype = "";

$(document).ready(function () {
    $("#form1").trigger("reset");
    addmodalpopup();
    $("#loading-div-background").show();
    $("#spn_userTotal").text(0);
    LoadReqDetails();
    LoadAuditData();
    //getBudgetdata();
});

function LoadReqDetails() {
    var ProID = $("#txt_ProcessID").val();
    var InsID = $("#txt_InstanceID").val();
    var StepName = $("#txt_StepName").val();
    PageMethods.GetReqDetails(ProID, InsID, $("#txt_Master_Url").val(), $("#txt_P2P_Url").val(), $("#txt_Wfe_Url").val(), function (response) {
        if (response[0] != "[]") {
            DtlJson = JSON.parse(JSON.parse(response[0]));
            assignHDRValue();
        }
        else {
            $("#loading-div-background").hide();
            $.jAlert({
                'content': 'No Data Found..!'
            });
            return false;
        }
    },
        function (error) {
        });
}

function assignHDRValue() {
    if (DtlJson.length > 0) {
        PK_ID = DtlJson[0].PK_HDR_ID;
        requestIni_Email = DtlJson[0].EMAILID;
        requestInitiator = DtlJson[0].CREATED_BY;
        requestNo = DtlJson[0].REQUEST_NO;
        POtype = DtlJson[0].POtype;

        $("#lbl_ReqNo").text(DtlJson[0].REQUEST_NO);
        $("#lblrequester").text(DtlJson[0].EMP_NAME);
        $("#lblreqdate").text(DtlJson[0].CREATION_DATE);
        $("#lbl_category").text(DtlJson[0].CATNAME);
        $("#lbl_Branch").text(DtlJson[0].BRANCH_NAME);
        $("#lbl_Desi").text(DtlJson[0].DESIGNATION_NAME);
        $("#lbl_Grade").text(DtlJson[0].GRADE_NAME);
        $("#lbl_Department").text(DtlJson[0].Department);

        $("#txt_Remark").text(DtlJson[0].REMARK);
        $("#txt_justification").text(DtlJson[0].JUSTIFICATION);

        branchID = DtlJson[0].FK_BRANCH_ID;

        displayDtlData();

        PageMethods.getDocumentData(requestNo, $("#txt_P2P_Url").val(), function (response) {
            if (response[0] != "Error") {
                DocumentJSON = JSON.parse(response[0]);
                displayDocumentData();
            }
        }, function (error) {
        });
    } else {
        $("#loading-div-background").hide();
        $.jAlert({
            'content': 'No Data Found..!'
        });
        return false;
    }
    $("#loading-div-background").hide();
}

function displayDtlData() {
    if (DtlJson) {
        var tableHeader = "";
        var tableBody = "";
        tableHeader += "<th style='width: 2%; text-align: center'>#</th>";
        tableHeader += "<th>Sub-Category</th>";
        tableHeader += "<th>Item</th>";
        //tableHeader += "<th>Base Rate</th>";
        tableHeader += "<th>Quantity</th>";
        //tableHeader += "<th style='width: 10%; text-align: center'>Total Amount</th>";
        $.each(DtlJson, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.SUB_CATNAME + "</td>";
            tableBody += "<td>" + val.ITMNAME + "</td>";
            //tableBody += "<td class='text-right'>" + val.BASE_RATE.toFixed(2); + "</td>";
            tableBody += "<td class='text-right'>" + val.QUANTITY + "</td>";
            //tableBody += "<td class='text-right'>" + val.TOTAL_AMOUNT.toFixed(2); + "</td>";
            tableBody += "</tr>";
        });
        var table = "<table ID='tblDtlData' class='display nowrap dataTable'>" +
            "<thead><tr class='grey'>" + tableHeader + "</tr></thead>" +
            "<tbody>" + tableBody + "</tbody>" +
            "</table>";
        $("#div_displayReqDtl").empty();
        $("#div_displayReqDtl").html(table);
        //getspanAmount();
        $("#loading-div-background").hide();
    }
}

function LoadAuditData() {
    PageMethods.getAuditData($("#txt_ProcessID").val(), $("#txt_InstanceID").val(), $("#txt_Master_Url").val(), $("#txt_P2P_Url").val(), $("#txt_Wfe_Url").val(), function (response) {
        AuditJson = JSON.parse(JSON.parse(response[0]));
        displayAuditData();
    },
        function (error) {
        });
}

function displayAuditData() {
    if (AuditJson) {
        var tableHeader = "";
        var tableBody = "";

        tableHeader += "<th align='center'>SR.No</th>";
        tableHeader += "<th align='center'>Step Name</th>";
        tableHeader += "<th align='center'>Performer</th>";
        tableHeader += "<th align='center'>Date</th>";
        tableHeader += "<th align='center'>Action Name</th>";
        tableHeader += "<th align='center'>Remark</th>";

        $.each(AuditJson, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.STEPNAME + "</td>";
            tableBody += "<td>" + val.ACTIONBYUSER + "</td>";
            tableBody += "<td>" + val.ACTIONDATE + "</td>";
            tableBody += "<td>" + val.ACTION + "</td>";
            tableBody += "<td>" + val.REMARK + "</td>";
            tableBody += "</tr>";
        });

        var table = "<table ID='tblAudit' class='display nowrap dataTable'>" +
            "<thead><tr class='grey'>" + tableHeader + "</tr></thead>" +
            "<tbody>" + tableBody + "</tbody>" +
            "</table>";
        $("#div_Audit").empty();
        $("#div_Audit").html(table);
        $("#loading-div-background").hide();
    }
}

function displayDocumentData() {
    if (DocumentJSON != "[]") {
        var tableHeader = "";
        var tableBody = "";

        tableHeader += "<th align='center'>SR.No</th>";
        tableHeader += "<th align='center'>FileName</th>";
        tableHeader += "<th align='center'>Remark</th>";

        $.each(DocumentJSON, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td><input class='hidden' type='text' name='txt_Region_Add" + (key + 1) + "' id='txt_Document_File" + (key + 1) + "' value=" + val.FILENAME + " readonly ></input><a id='a_downloadfiles" + (key + 1) + "' style='cursor: pointer' onclick=\"return downloadfiles1('" + (key + 1) + "');\" >" + val.FILENAME + "</a><input class='hidden' type='text' name='txt_pono" + (key + 1) + "' id='txt_poreqno" + (key + 1) + "' value=" + val.OBJECT_VALUE + " readonly /></td>";
            tableBody += "<td>" + val.DOCUMENT_TYPE + "</td>";
            tableBody += "</tr>";
        });

        var table = "<table ID='tblDoc' class='display nowrap dataTable'>" +
            "<thead><tr class='grey'>" + tableHeader + "</tr></thead>" +
            "<tbody>" + tableBody + "</tbody>" +
            "</table>";
        $("#div_docs").empty();
        $("#div_docs").html(table);
        var tbllength = $('#tblDoc tr').length;
        if (tbllength > 1) {
            $('#Img1').attr("src", '../../assets/images/attachment_sel.png');
        }
        else {
            $('#Img1').attr("src", '../../assets/images/attachment_non_sel.png');
        }
        $("#loading-div-background").hide();
    }
}

function downloadfiles1(index) {
    window.open('/Common/FileDownload.aspx?indentno=' + $("#txt_poreqno" + index).val() + '&filename=' + $("#tblDoc tr")[index].cells[1].innerText + '&filetag=', 'Download', 'left=150,top=100,width=600,height=300,toolbar=no,menubars=no,status=no,scrollbars=yes,resize=no');
}

//function getBudgetDetails(budid, budsubid, finyear) {
//    //$("#loading-div-background").show();
//    //PageMethods.getBudgetDetailData(budid, budsubid, finyear, function (response) {
//    //    BudDtlJson = JSON.parse(response[0]);
//    //    if (response[0] != "[]") {
//    displayBudgetData();
//    //    }
//    //}, function (error) {
//    //    alert(error);
//    //});
//}

//function displayBudgetData() {
//    //if (obj) {
//    var tableHeader = "";
//    var tableBody = "";

//    tableHeader += "<th>#</th>";
//    tableHeader += " <th>Budget Head</th>";
//    tableHeader += " <th>Budget SubHead</th>";
//    tableHeader += " <th>Budget Code</th>";
//    tableHeader += " <th>Fin Year</th>";
//    tableHeader += " <th>Opening Budget</th>";
//    tableHeader += " <th>Consumed Budget</th>";
//    tableHeader += " <th>Inprogress Budget</th>";
//    tableHeader += " <th>Remaining Budget</th>";

//    //$.each(obj, function (key, val) {
//    var key = 0;
//    tableBody += "<tr id='row_" + (key + 1) + "'>";
//    tableBody += "<td name='srno'>" + (key + 1) + "</td>";
//    tableBody += "<td>1</td>";
//    tableBody += "<td>2</td>";
//    tableBody += "<td>3</td>";
//    tableBody += "<td>4</td>";
//    tableBody += "<td class='text-right'>5</td>";
//    tableBody += "<td class='text-right'>6</td>";
//    tableBody += "<td class='text-right'>7</td>";
//    tableBody += "<td class='text-right'>8</td>";

//    //tableBody += "<td>" + val.BUDGET_HEAD + "</td>";
//    //tableBody += "<td>" + val.BUD_SUB_HEAD + "</td>";
//    //tableBody += "<td>" + val.BUD_CODE + "</td>";
//    //tableBody += "<td>" + val.FINANCIAL_YEAR + "</td>";
//    //tableBody += "<td class='text-right'>" + val.BUDGET_AMOUNT.toFixed(2) + "</td>";
//    //tableBody += "<td class='text-right'>" + val.BUDGET_CONS.toFixed(2) + "</td>";
//    //tableBody += "<td class='text-right'>" + val.BUDGET_INPR.toFixed(2) + "</td>";
//    //tableBody += "<td class='text-right'>" + val.BAL_BUDGET.toFixed(2) + "</td>";
//    tableBody += "</tr>";
//    //})
//    var table = "<table class='display nowrap dataTable' id='tbl_BudDetails' runat='server'>" +
//        "<thead style='background-color:gray'><tr>" + tableHeader + "</tr></thead>" +
//        "<tbody>" + tableBody + "</tbody>" +
//        "</table>";
//    $("#div_Budget_details").empty();
//    $("#div_Budget_details").html(table);
//    //}
//}


//function getspanAmount() {
//    var len = $("#tblDtlData tr").length;
//    $("#spn_userTotal").text(0);
//    var spanTotal = 0;
//    if (len > 0) {
//        for (var i = 1; i < len; i++) {
//            var item_total = 0;
//            item_total = $("#Total" + i).val();
//            if (spanTotal == 0) {
//                spanTotal = item_total;
//            }
//            else {
//                spanTotal = parseFloat(item_total) + parseFloat(spanTotal);
//            }
//        }
//        $("#spn_userTotal").text(spanTotal);
//    }
//}

$("#btn_Save").click(function () {
    try {
        $("#loading-div-background").show();
        var action = $("#ddlAction").val();
        var approvalremark = $("#app_Remark").val();

        if (action == "0") {
            $.jAlert({
                'content': 'Please Select Action..!',
                'onClose': function () {
                    $('#ddlAction').focus();
                    $("#loading-div-background").hide();
                }
            });
            return false;
        }

        if (action != "APPROVE") {
            if (approvalremark == "") {
                $.jAlert({
                    'content': 'Please Enter Remark..!',
                    'onClose': function () {
                        $('#app_Remark').focus();
                        $("#loading-div-background").hide();
                    }
                });
                return false;
            }
        }

        PageMethods.SaveData($("#txt_ProcessID").val(), $("#txt_InstanceID").val(), $("#txt_StepName").val(), $("#SessionAdId").val(), $("#txt_Email").val(), requestNo, requestInitiator, requestIni_Email, action, POtype, approvalremark, $("#lbl_category").text(), $("#txt_Master_Url").val(), $("#txt_P2P_Url").val(), $("#txt_Wfe_Url").val(), function (response) {
            if (response) {
                if (response == "true") {
                    var saveMsg = "Request " + action + " Successfully and Request Number is " + requestNo;
                    $.jAlert({
                        'content': saveMsg,
                        'onClose': function () {
                            location.href = "../../HomePage.aspx";
                            $("#loading-div-background").hide();
                        }
                    });
                }
                else {
                    $.jAlert({
                        'content': response,
                    });
                    $("#loading-div-background").hide();
                    return false;
                }
            }
            $("#loading-div-background").hide();
        }, function (error) {
        });
        return true;
    }
    catch (exception) {
        return false;
    }
});
