﻿
var CatJson = "", catid = "";
var SubcatJson = "", subcatid = "";
var ItemJson = "", itemid = "";
var UserdataJson = "", UserId = "";
var Branchid = "";

$(document).ready(function () {
    $("#form1").trigger("reset");
    addmodalpopup();
    $("#loading-div-background").show();
    $("#spn_userTotal").text(0);
    LoadCategory();
    getUserData();
});

$("#txt_category").change(function () {
    $("#lblpotype").show();
    $("#txt_po_type").show();

});

function LoadCategory() {
    PageMethods.GetCategory($("#txt_Master_Url").val(), function (response) {
        CatJson = JSON.parse(JSON.parse(response[0]));
        BindCategory();
    },
        function (error) {
        });
}

function BindCategory() {
    $('#txt_category').autocomplete({
        autoFocus: true,
        minLength: 0,
        source: CatJson,
        select: function (event, ui) {
            event.preventDefault();
            $('#txt_category').val(ui.item.label);
            catid = ui.item.id;
            LoadSubCategory();
            displayTblData();
            LoadType();
            if ($('#txt_category').val() == "ADMIN") {
                $("#lblpotype").show();
                $("#divtype").show();
            }
            else {
                $("#lblpotype").hide();
                $("#divtype").hide();
            }
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $.jAlert({
                    'content': 'Please Select Valid Category..!'
                });
                $(this).val("");
                catid = "";
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function LoadType() {
    var availableTags = [
        "Standard PO",
        "Contract PO",
    ];

    BindType(availableTags);
}

function BindType(obj) {
    $("#txt_po_type").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: obj,
        select: function (event, ui) {
            event.preventDefault();
            $('#txt_po_type').val(ui.item.label);
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $.jAlert({
                    'content': 'Please Select Valid Category..!'
                });
                $(this).val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function getUserData() {
    PageMethods.GetUserData($("#SessionAdId").val(), $("#txt_Master_Url").val(), function (response) {
        if (response[0] != "[]") {
            UserdataJson = JSON.parse(JSON.parse(response[0]));
            BindUserData(UserdataJson);
        }
        else {
            $("#loading-div-background").hide();
            $.jAlert({
                'content': 'No Data Found..!',
                'onClose': function () {
                    $("txt_category").attr('disabled', 'disabled');
                }
            });
        }
    },
        function (error) {
        });
}

function BindUserData(obj)
{
    if (obj.length > 0) {
        $("#txt_Branch").val(obj[0].BRANCH_NAME);
        Branchid = obj[0].FK_BRANCH_ID
        $("#txt_Desig").val(obj[0].DESIGNATION_NAME);
        $("#txt_Grade").val(obj[0].GRADE);
        $("#txt_Department").val(obj[0].DEPARTMENT_NAME);
        $("#loading-div-background").hide();
    }
}

function displayTblData() {
    var tableHeader = "";
    var tableBody = "";

    tableHeader += "<th style='width: 2%; text-align: center'>#</th>";
    tableHeader += "<th>Sub-Category</th>";
    tableHeader += "<th>Item</th>";
    tableHeader += "<th>Quantity</th>";
    tableHeader += "<th style='width: 3%; text-align: center'>Add</th>";
    tableHeader += "<th style='width: 3%; text-align: center'>Delete</th>";
    var table = "<table class='display nowrap dataTable' id='tbl_Data'>" +
        "<thead><tr>" + tableHeader + "</tr></thead>" +
        "<tbody>" + tableBody + "</tbody>" +
        "</table>";
    $("#div_displayReqDtl").empty();
    $("#div_displayReqDtl").html(table);
    initiateAddRowTask(null, false);
}

function initiateAddRowTask(key, isValidationRequired) {
    if (isValidationRequired) {
        if (validExpenseDetails(key)) {
            addRow();
        }
    } else {
        addRow();
    }
}

function validExpenseDetails(i) {
    var lastRow = $('#tbl_Data tr').length;

    for (var key = 1; key < lastRow; key++) {
        var Sel_Sub_Cat = $("#sel_Subcat" + key).val();
        var Sel_Item = $("#sel_Item" + key).val();
        var Sel_Quantity = $("#txtQuantity_" + key).val();
        if (Sel_Sub_Cat == "") {
            $.jAlert({
                'content': 'Please Select Sub Category At Row :' + key + ''
            });
            return false;
        }
        if (Sel_Item == "") {
            $.jAlert({
                'content': 'Please Select Item At Row :' + key + ''
            });
            return false;
        }
        if (Sel_Quantity == "0" || Sel_Quantity == "") {
            $.jAlert({
                'content': 'Please Enter Quantity At Row :' + key + ''
            });
            return false;
        }
    }
    return true;
}

function addRow() {
    var tableBody = "";
    var tbl = $("#tbl_Data tr").length;
    tableBody += "<tr><td><span id='index" + tbl + "'>" + tbl + "</span></td>";

    tableBody += "<td><input value='' class='form-control input-sm mandatory' id='sel_Subcat" + tbl + "' type='text' placeholder='Select Sub-Category' onclick='return BindSubCategory(" + tbl + ")' />";
    tableBody += "<input value='' id='txt_Subcat_ID" + tbl + "'  name='SubHid' style='display:none' type='text'/></td>";
    tableBody += "<td><input value='' class='form-control input-sm mandatory' id='sel_Item" + tbl + "' type='text' placeholder='Select Item' />";
    tableBody += "<input value='' id='txt_Itm_ID" + tbl + "' style='display:none' type='text'/></td>";
    //tableBody += "<td><input id='txtbaserate_" + tbl + "' value='0' style='text-align:right' class='form-control input-sm number mandatory' type='text' value='0' name='base_rate' readonly /></td>";
    //tableBody += "<td><input style='text-align: right' id='txtQuantity_" + tbl + "' name='quantity' class='form-control input-sm mandatory' type='text' onkeypress='return isNumberKey(event)' onchange='CalTotal(" + tbl + ")'  disabled='disabled' ></input></td>";
    tableBody += "<td><input style='text-align: right' id='txtQuantity_" + tbl + "' name='quantity' class='form-control input-sm mandatory' type='text' onkeypress='return isNumberKey(event)'  disabled='disabled' ></input></td>";
    //tableBody += "<td><input style='text-align: right' class='form-control input-sm mandatory' type='text' readonly id='Total" + tbl + "'></input></td>";
    tableBody += "<td class='text-center' id='add" + tbl + "'><a id='add_row" + tbl + "' onclick='return initiateAddRowTask(" + tbl + ",true)'> <img src='../../assets/images/add.png' title='Add New Row' /></a></td>";
    tableBody += "<td class='text-center' id='rem" + tbl + "'><a id='del_row" + tbl + "' onclick='delete_row(" + tbl + ")'><img src='../../assets/images/delete.png' title='Delete Row' /></a></td></tr>";
    tableBody += "</tr>";

    $("#tbl_Data tbody").append(tableBody);
}

function LoadSubCategory() {
    PageMethods.GetSubCategory(catid, $("#txt_Master_Url").val(), function (response) {
        SubcatJson = JSON.parse(JSON.parse(response[0]));
        if (SubcatJson.Message == "Data Not Found") {
            $.jAlert({
                'content': 'Sub Category Not Found..!'
            });
            $("#loading-div-background").hide();
            return false;
        }
        else {
            BindSubCategory(1);
        }
    },
        function (error) {
        });
}

function BindSubCategory(index) {
    $('#sel_Subcat' + index).autocomplete({
        autoFocus: true,
        minLength: 3,
        source: SubcatJson,
        select: function (event, ui) {
            event.preventDefault();
            subcatid = ui.item.id;
            $('#sel_Subcat' + index).val(ui.item.label);
            $('#txt_Subcat_ID' + index).val(ui.item.id);
            //$('#txtbaserate_' + index).val(ui.item.BASE_RATE);
            $("#txtQuantity_" + index).prop('disabled', false);
            $("#txtQuantity_" + index).val('');
            //$("#Total" + index).val('');
            LoadItem(index);
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $.jAlert({
                    'content': 'Please Select Valid Sub Category..!'
                });
                $(this).val("");
                $("#txtQuantity_" + index).prop('disabled', true);
                $("#txtQuantity_" + index).val('');
                $("#Total" + index).val('');
                //CalTotal(index);
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function LoadItem(index) {
    PageMethods.GetItem(catid, subcatid, $("#txt_Master_Url").val(), function (response) {
        ItemJson = JSON.parse(JSON.parse(response[0]));
        if (ItemJson.Message == "Data Not Found") {
            $.jAlert({
                'content': 'Item Not Found..!'
            });
            $("#loading-div-background").hide();
            return false;
        }

        else {
            BindItem(index);
        }
    },
        function (error) {
        });
}

function BindItem(index) {
    $('#sel_Item' + index).autocomplete({
        autoFocus: true,
        minLength: 3,
        source: ItemJson,
        select: function (event, ui) {
            event.preventDefault();
            $('#sel_Item' + index).val(ui.item.label);
            $('#txt_Itm_ID' + index).val(ui.item.id);
            //$('#txtbaserate_' + index).val(ui.item.BASE_RATE);
            //$("#txtQuantity_" + index).prop('disabled', false);
            //$("#txtQuantity_" + index).val('');
            //$("#Total" + index).val('');

            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $.jAlert({
                    'content': 'Please Select Valid Item..!'
                });
                $(this).val("");
                $("#txtQuantity_" + index).prop('disabled', true);
                $("#txtQuantity_" + index).val('');
                $("#Total" + index).val('');
                // CalTotal(index);
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

//function getspanAmount() {
//    var len = $("#tbl_Data tr").length;
//    $("#spn_userTotal").text(0);
//    var spanTotal = 0;
//    if (len > 0) {
//        for (var i = 1; i < len; i++) {
//            var item_total = 0;
//            item_total = $("#Total" + i).val();
//            if (spanTotal == 0) {
//                spanTotal = item_total;
//            }
//            else {
//                spanTotal = parseFloat(item_total) + parseFloat(spanTotal);
//            }
//        }
//        $("#spn_userTotal").text(spanTotal);
//    }
//}

//function CalTotal(index) {
//    var land_tot = 0;
//    var tot = 0;
//    var lastRow1 = $('#tbl_Data tr').length;
//    for (var i = 1; i <= lastRow1 - 1; i++) {
//        var Qty = $("#txtQuantity_" + i).val();//$(this).closest('tr').find('[name=valqty]').val();
//        var BaseRate = $("#txtbaserate_" + i).val();//$(this).closest('tr').find('[name=valRate]').val();//
//        if (BaseRate != "") {
//            tot = parseFloat(Qty) * parseFloat(BaseRate);
//            $("#Total" + i).val(tot.toFixed(2));
//            land_tot = parseFloat(land_tot) + parseFloat(tot);
//        }
//    }
//    getspanAmount();
//}

function delete_row(RowIndex) {
    try {
        var tbl = document.getElementById("tbl_Data");
        var lastRow = tbl.rows.length;
        if (lastRow <= 2) {
            $.jAlert({
                'content': 'Validation Error:You have to Enter atleast one record..!'
            });
            return false;
        }
        for (var contolIndex = RowIndex; contolIndex < lastRow - 1; contolIndex++) {

            document.getElementById("index" + (parseInt(contolIndex) + 1)).id = "index" + contolIndex;
            $("#index" + (parseInt(contolIndex))).html(contolIndex);
            document.getElementById("sel_Subcat" + (parseInt(contolIndex) + 1)).onclick = new Function("return BindSubCategory(" + contolIndex + ")");
            document.getElementById("sel_Subcat" + (parseInt(contolIndex) + 1)).id = "sel_Subcat" + contolIndex;
            document.getElementById("txt_Subcat_ID" + (parseInt(contolIndex) + 1)).id = "txt_Subcat_ID" + contolIndex;
            //document.getElementById("txtbaserate_" + (parseInt(contolIndex) + 1)).id = "txtbaserate_" + contolIndex;

            document.getElementById("sel_Item" + (parseInt(contolIndex) + 1)).id = "sel_Item" + contolIndex;
            document.getElementById("txt_Itm_ID" + (parseInt(contolIndex) + 1)).id = "txt_Itm_ID" + contolIndex;
            document.getElementById("txtQuantity_" + (parseInt(contolIndex) + 1)).id = "txtQuantity_" + contolIndex;
            //document.getElementById("Total" + (parseInt(contolIndex) + 1)).id = "Total" + contolIndex;

            document.getElementById("add_row" + (parseInt(contolIndex) + 1)).onclick = new Function("return initiateAddRowTask(" + contolIndex + "," + "true" + ")");
            document.getElementById("add_row" + (parseInt(contolIndex) + 1)).id = "add_row" + contolIndex;
            document.getElementById("del_row" + (parseInt(contolIndex) + 1)).onclick = new Function("delete_row(" + contolIndex + ")");
            document.getElementById("del_row" + (parseInt(contolIndex) + 1)).id = "del_row" + contolIndex;
            document.getElementById("add" + (parseInt(contolIndex) + 1)).id = "add" + contolIndex;
            document.getElementById("rem" + (parseInt(contolIndex) + 1)).id = "rem" + contolIndex;
        }
        tbl.deleteRow(RowIndex);
        //CalTotal();
        $.jAlert({
            'content': 'Record Deleted Successfully..!'
        });
    }
    catch (Exc) { }
}

$("#btn_Save").click(function () {
    try {
        $("#loading-div-background").show();
        var action = "SUBMIT";
        var HdrData = [];
        var DtlData = [];
        var DocData = [];

        if ($("#txt_category").val() == "") {
            $.jAlert({
                'content': 'Please Select Category..!'
            });
            $("#loading-div-background").hide();
            return false;
        }
        else if ($("#txt_category").val() == "ADMIN") {
            if ($("#txt_po_type").val() == "") {
                $.jAlert({
                    'content': 'Please Select Po Type..!'
                });
                $("#loading-div-background").hide();
                return false;
            }
        }


        var HdrData1 = {
            "CREATED_BY": $("#SessionAdId").val(), "FK_BRANCH_ID": Branchid,
            "FK_CATEGORY_ID": catid, "REMARK": $("#txt_Remark").val(), "JUSTIFICATION": $("#txt_justification").val(), "STATUS": "INPROGRESS"
        };

        HdrData.push(HdrData1);

        var tbllength = $('#tbl_Data tr').length;
        if (tbllength > 1) {
            for (var key = 1; key < tbllength; key++) {
                //var Sel_Type = $("#sel_Type" + key).val();
                //var Sel_Ref = $("#txtassetrefno_" + key).val();
                var Sel_Cat = $("#txt_category").val();
                var Sel_Sub_Cat = $("#sel_Subcat" + key).val();
                var Sel_Item = $("#sel_Item" + key).val();
                var Sel_Quantity = $("#txtQuantity_" + key).val();
                var Total = 0;

                //if (Sel_Cat == "") {
                //    $.jAlert({
                //        'content': 'Please Select Category At Row :' + key + ''
                //    });
                //    return false;
                //}
                if (Sel_Sub_Cat == "") {
                    $.jAlert({
                        'content': 'Please Select Sub Category At Row :' + key + ''
                    });
                    $("#loading-div-background").hide();
                    return false;
                }
                if (Sel_Item == "") {
                    $.jAlert({
                        'content': 'Please Select Item At Row :' + key + ''
                    });
                    $("#loading-div-background").hide();
                    return false;
                }
                if (parseFloat(Sel_Quantity) == 0 || Sel_Quantity == "") {
                    $.jAlert({
                        'content': 'Please Enter Quantity At Row :' + key + ''
                    });
                    $("#loading-div-background").hide();
                    return false;
                }
                var DtlData1 = {
                    "FK_CAT_ID": catid, "FK_SUB_CAT_ID": $("#txt_Subcat_ID" + key).val(), "FK_ITEM_ID": $("#txt_Itm_ID" + key).val(),
                    "QUANTITY": Sel_Quantity, "TOTAL_AMOUNT": Total
                };
                DtlData.push(DtlData1);
            }
        }
        else {
            $.jAlert({
                'content': 'Please Add Requisition Details In Table...!'
            });
            $("#loading-div-background").hide();
            return false;
        }
        var lastRow1 = $('#uploadTable tr').length;
        if (lastRow1 > 1) {
            for (var l = 0; l < lastRow1 - 1; l++) {
                var firstCol = $("#uploadTable tr")[l + 1].cells[0].innerText;
                var SecondCol = $("#uploadTable tr")[l + 1].cells[1].innerText;
                var DocData1 = {
                    "OBJECT_TYPE": "ASSET REQUISITION", "OBJECT_VALUE": "0", "DOCUMENT_TYPE": firstCol, "FILENAME": SecondCol
                }
                DocData.push(DocData1);
            }
        }

        var transactionData = { "hdrData": HdrData, "dtlData": DtlData, "docData": DocData }
        //$("#loading-div-background").show();
        $.jAlert({
            'title': 'Confirmation',
            'content': 'Are You Sure To Submit This Request..?', 'type': 'confirm',
            'onConfirm': function () {
                PageMethods.SaveData(JSON.stringify(transactionData), $("#SessionAdId").val(), $("#EmailID").val(), $('#txt_category').val(), $("#txt_po_type").val(), action, $("#txt_Master_Url").val(), $("#txt_P2P_Url").val(), $("#txt_Wfe_Url").val(), function (response) {
                    if (response != "Error occured while saving data" && response != "Approver not found") {
                        var saveMsg = "Request Submitted Successfully. Request Number is " + response;
                        $.jAlert({
                            'content': saveMsg,
                            'onClose': function () {
                                location.href = "../../HomePage.aspx";
                            }
                        });
                    }
                    else {
                        $.jAlert({
                            'content': response,
                        });
                        $("#loading-div-background").hide();
                        return false;
                    }
                }, function (error) {
                });
                $("#loading-div-background").hide();
                return true;
            },
            'onDeny': function () {
                $("#loading-div-background").hide();
                return false;
            }
        });
    }
    catch (exception) {
        return false;
    }
});

$("#btn_SaveAsDraft").click(function () {
    try {
        //$("#loading-div-background").show();
        var action = "SAVE";
        var HdrData = [];
        var DtlData = [];
        var DocData = [];

        if ($("#txt_category").val() == "") {
            $.jAlert({
                'content': 'Please Select Category..!'
            });
            $("#loading-div-background").hide();
            return false;
        }
        var HdrData1 = {
            "CREATED_BY": $("#SessionAdId").val(), "FK_BRANCH_ID": Branchid,
            "FK_CATEGORY_ID": catid, "REMARK": $("#txt_Remark").val(), "JUSTIFICATION": $("#txt_justification").val(), "STATUS": "INPROGRESS"
        };

        HdrData.push(HdrData1);

        var tbllength = $('#tbl_Data tr').length;
        if (tbllength > 1) {
            for (var key = 1; key < tbllength; key++) {
                //var Sel_Type = $("#sel_Type" + key).val();
                //var Sel_Ref = $("#txtassetrefno_" + key).val();
                var Sel_Cat = $("#txt_category").val();
                var Sel_Sub_Cat = $("#sel_Subcat" + key).val();
                var Sel_Item = $("#sel_Item" + key).val();
                var Sel_Quantity = $("#txtQuantity_" + key).val();
                var Total = $("#Total" + key).val();

                //if (Sel_Cat == "") {
                //    $.jAlert({
                //        'content': 'Please Select Category At Row :' + key + ''
                //    });
                //    return false;
                //}
                if (Sel_Sub_Cat == "") {
                    $.jAlert({
                        'content': 'Please Select Sub Category At Row :' + key + ''
                    });
                    $("#loading-div-background").hide();
                    return false;
                }
                if (Sel_Item == "") {
                    $.jAlert({
                        'content': 'Please Select Item At Row :' + key + ''
                    });
                    $("#loading-div-background").hide();
                    return false;
                }
                if (parseFloat(Sel_Quantity) == 0 || Sel_Quantity == "") {
                    $.jAlert({
                        'content': 'Please Enter Quantity At Row :' + key + ''
                    });
                    $("#loading-div-background").hide();
                    return false;
                }
                var DtlData1 = {
                    "FK_CAT_ID": catid, "FK_SUB_CAT_ID": $("#txt_Subcat_ID" + key).val(), "FK_ITEM_ID": $("#txt_Itm_ID" + key).val(),
                    "BASE_RATE": $("#txtbaserate_" + key).val(), "QUANTITY": Sel_Quantity, "TOTAL_AMOUNT": Total
                };
                DtlData.push(DtlData1);
            }
        }
        else {
            $.jAlert({
                'content': 'Please Add Requisition Details In Table...!'
            });
            $("#loading-div-background").hide();
            return false;
        }
        var lastRow1 = $('#uploadTable tr').length;
        if (lastRow1 > 1) {
            for (var l = 0; l < lastRow1 - 1; l++) {
                var firstCol = $("#uploadTable tr")[l + 1].cells[0].innerText;
                var SecondCol = $("#uploadTable tr")[l + 1].cells[1].innerText;
                var DocData1 = {
                    "OBJECT_TYPE": "ASSET REQUISITION", "OBJECT_VALUE": "0", "DOCUMENT_TYPE": firstCol, "FILENAME": SecondCol
                }
                DocData.push(DocData1);
            }
        }

        var transactionData = { "hdrData": HdrData, "dtlData": DtlData, "docData": DocData }

        $.jAlert({
            'title': 'Confirmation',
            'content': 'Are You Sure To Save As Draft This Request..?', 'type': 'confirm',
            'onConfirm': function () {
                $("#loading-div-background").show();
                PageMethods.SaveData(JSON.stringify(transactionData), $("#SessionAdId").val(), $("#EmailID").val(), action, $("#txt_P2P_Url").val(), function (response) {
                    if (response != "Error occured while saving data" && response != "Approver not found") {
                        var saveMsg = "Request Submitted Successfully With Request Number is " + response;
                        $.jAlert({
                            'content': saveMsg,
                            'onClose': function () {
                                location.href = "../../HomePage.aspx";
                            }
                        });
                    }
                    //else if (response == "false") {
                    //    var errMsg = "Approver not found";
                    //    $.jAlert({
                    //        'content': errMsg,
                    //    });
                    //    $("#loading-div-background").hide();
                    //    return false;
                    //}
                    else {
                        $.jAlert({
                            'content': response,
                        });
                        $("#loading-div-background").hide();
                        return false;
                    }
                }, function (error) {
                });
                $("#loading-div-background").hide();
                return true;
            },
            'onDeny': function () {
                $("#loading-div-background").hide();
                return false;
            }
        });
    }
    catch (exception) {
        return false;
    }
});

$("#btn_Cancel").click(function () {
    window.location.replace("../../HomePage.aspx");
});

