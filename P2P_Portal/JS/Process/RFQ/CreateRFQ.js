﻿var deptJson, deptid, regionJson, regionid, branchJson, branchid, rfqDtlJson; var cnnnt = 0; var DocumentData = "";
var paymentTermJson, rfqtermsJson, ItemJSON, rfqDtlPublishJson, lotvJson, rfqsupplierJson, historyJson, queryJSON, venJson, vpriceJson, emailJson, reminJson, unitJson, lotviewJson, checkJson, rfqlotlineJson, generalTermJson, testAucid, catid, buyerJson, adId, TermJson, supplierJson, locid, locationjson, docJson; var cnt = 0;
var pkid = 0; var keyid = 0; var str = ""; var id = 0; var obj = ""; var pkkid = 0; var cntlot = 0; var myvar;
$(document).ready(function () {
    addmodalpopup();
    $("#loading-div-background").hide();
    $(".datepicker").datepicker({
        dateFormat: 'dd-M-yy', autoclose: true, todayBtn: 'linked', changeMonth: true,
        changeYear: true
    });

    $('.clockpicker').clockpicker({
        autoclose: true,
        donetext: 'Done',
        twelvehour: false,
        'default': 'now'

    });
    getRFQDtl();
    //getRFQDtlPublish();
    onLoad();
    $('#txt_checksave').val(0);
});
$('#form1').on('keyup keypress', function (e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
        e.preventDefault();
        return false;
    }
});
function cleardiv() {
    $("#div_Items").html('');
    $("#txt_item_name_type").val('');
}
function getRFQDtl() {
    PageMethods.getRFQDtl($("#txt_Master_Url").val(), $("#txt_Vendor_Url").val(), function (response) {
        rfqDtlJson = JSON.parse(response[0]);
        displayRFQ(rfqDtlJson);
    }, function (error) {
        deptJson = [];
        alert(error);
    });
}
function displayRFQ(obj) {
    if (obj) {
        var tableHeader = "";
        var tableBody = "";
        tableHeader += "<tr>";
        tableHeader += "<th>RFQ Name</th>";
        tableHeader += "<th>Test Project</th>";
        tableHeader += "<th>Department</th>";
        tableHeader += "<th>Category</th>";
        // tableHeader += "<th>Buyer</th>";
        tableHeader += "<th>Currency</th>";
        tableHeader += "<th>Branch</th>";
        tableHeader += "<th>Region</th>";
        tableHeader += "<th>Delivery Location</th>";
        //tableHeader += "<th>Delivery Time</th>";
        tableHeader += "<th>Status</th>";
        tableHeader += "<th></th>";
        tableHeader += "</tr>";

        $.each(obj, function (key, val) {
            tableBody += "<tr id=" + key + ">";
            tableBody += "<td><input class='form-control' id='txt_RFQ_Desc" + (key + 1) + "' value='" + val.RFQ_NAME + "' type='text' style='display:none' /><input class='form-control' id='txt_Pkid" + (key + 1) + "' value='" + val.PK_RFQ_ID + "' type='text' style='display:none' /> " + val.RFQ_NAME + "</td>";
            tableBody += "<td><input class='form-control' id='txt_Test_Rfq" + (key + 1) + "' value='" + val.TEST_PROJECT + "' type='text' style='display:none' /><input class='form-control' id='txt_Status" + (key + 1) + "' value='" + val.STATUS + "' type='text' style='display:none' />" + val.TEST_PROJECT + "</td>";
            tableBody += "<td><input class='form-control' id='txt_dept" + (key + 1) + "' value='" + val.DEPT_NAME + "' type='text' style='display:none' />" + val.DEPT_NAME + "</td>";
            tableBody += "<td><input class='form-control' id='txt_cat" + (key + 1) + "' value='" + val.FK_CATEGORY_ID + "' type='text' style='display:none' /><input class='form-control' id='txt_open" + (key + 1) + "' value='" + val.AUTO_OPEN + "' type='text' style='display:none' /><input class='form-control' id='txt_ad_id" + (key + 1) + "' value='" + val.ad_id + "' type='text' style='display:none' />" + val.FK_CATEGORY_ID + "</td>";
            tableBody += "<td><input class='form-control' id='txt_buyer" + (key + 1) + "' value='" + val.EMP_NAME + "' type='text' style='display:none' /><input class='form-control' id='txt_deptid" + (key + 1) + "' value='" + val.PK_DEPT_ID + "' type='text' style='display:none' /><input class='form-control' id='txt_branchid" + (key + 1) + "' value='" + val.PK_BRANCHID + "' type='text' style='display:none' /><input class='form-control' id='txt_regionid" + (key + 1) + "' value='" + val.PK_REGIONID + "' type='text' style='display:none' /><input class='form-control' id='txt_locid" + (key + 1) + "' value='" + val.PK_LOCATIONID + "' type='text' style='display:none' />" + val.CURRENCY + "</td>";
            tableBody += "<td><input class='form-control' id='txt_Branch" + (key + 1) + "' value='" + val.BRANCH_NAME + "' type='text' style='display:none' /><input class='form-control' id='txt_Start_Date" + (key + 1) + "' value='" + val.START_DATE + "' type='text' style='display:none' />" + val.BRANCH_NAME + "</td>";
            tableBody += "<td><input class='form-control' id='txt_region" + (key + 1) + "' value='" + val.REGION + "' type='text' style='display:none' /><input class='form-control' id='txt_End_Date" + (key + 1) + "' value='" + val.End_Date + "' type='text' style='display:none' /> " + val.REGION + "</td>";
            tableBody += "<td><input class='form-control' id='txt_Delivery_loc" + (key + 1) + "' value='" + val.LOCATION_NAME + "' type='text' style='display:none' /><input class='form-control' id='txt_Start_time" + (key + 1) + "' value='" + val.START_TIME + "' type='text' style='display:none' />" + val.LOCATION_NAME + "</td>";
            //<input class='form-control' id='txt_Delivery_time" + (key + 1) + "' value='" + val.delTime + "' type='text' style='display:none' />
            // tableBody += "<td>" + val.delTime + "</td>";
            tableBody += "<td><input class='form-control' id='txt_End_time" + (key + 1) + "' value='" + val.END_TIME + "' type='text' style='display:none' /><input class='form-control' id='txt_remind_cnt" + (key + 1) + "' value='" + val.remind_cnt + "' type='text' style='display:none' /> <input class='form-control' id='txt_Delivery_Date" + (key + 1) + "' value='" + val.EXP_DELIVERY_DATE + "' type='text' style='display:none' /><input class='form-control' id='txt_Curr" + (key + 1) + "' value='" + val.CURRENCY + "' type='text' style='display:none' /><input class='form-control' id='txt_remind_days" + (key + 1) + "' value='" + val.remind_days + "' type='text' style='display:none' />" + val.STATUS + "</td>";
            if (val.STATUS == "Open") {
                tableBody += "<td class='text-center'><a onclick=\"return edit_Click(" + (key + 1) + ",0);\"><img title='Edit'  src='../../RFQassets/img/edit.png'  style='vertical-align:middle;'/></a></a></td>";
            }
            else {
                tableBody += "<td class='text-center'><button type='button' id='a_view" + (key + 1) + "' class='btn btn-inline btn-info ladda-button' data-toggle='modal' data-target='#div_viewdata' onclick=\'return ViewData(" + (key + 1) + ");\' >View</button>";
            }
            tableBody += "</tr>";
            keyid = (key + 1);
        })

        var table = "<table id='tbl_RFQ' class='table table-bordered table-hover table-sm'>" +
            "<thead><tr>" + tableHeader + "</tr></thead>" +
            "<tbody>" + tableBody + "</tbody>" +
            "</table>";

        //if (status == "Open") {
        $("#dv_RfqData").empty();
        $("#dv_RfqData").html(table);

    }
}
function displayPublishRFQ(obj) {
    if (obj) {
        var tableHeader = "";
        var tableBody = "";
        tableHeader += "<tr>";
        tableHeader += "<th>RFQ Name</th>";
        tableHeader += "<th>Test Project</th>";
        tableHeader += "<th>Department</th>";
        tableHeader += "<th>Category</th>";
        tableHeader += "<th>Buyer</th>";
        tableHeader += "<th>Branch</th>";
        tableHeader += "<th>Region</th>";
        tableHeader += "<th>Delivery Location</th>";
        //tableHeader += "<th>Delivery Time</th>";
        tableHeader += "<th>Status</th>";
        tableHeader += "<th>Publish Date</th>";
        tableHeader += "<th></th>";
        tableHeader += "</tr>";

        $.each(obj, function (key, val) {
            if (val.publish_date == '01-Jan-0001') {
                val.publish_date = "";
            }
            tableBody += "<tr id=" + key + ">";
            tableBody += "<td><input class='form-control' id='txt_RFQ_Desc1" + (key + 1) + "' value='" + val.Rfq_Desc + "' type='text' style='display:none' /><input class='form-control' id='txt_Pkid1" + (key + 1) + "' value='" + val.id + "' type='text' style='display:none' /> " + val.Rfq_Desc + "</td>";
            tableBody += "<td><input class='form-control' id='txt_Test_Rfq1" + (key + 1) + "' value='" + val.test + "' type='text' style='display:none' /><input class='form-control' id='txt_StatusP" + (key + 1) + "' value='" + val.status + "' type='text' style='display:none' />" + val.test + "</td>";
            tableBody += "<td><input class='form-control' id='txt_dept1" + (key + 1) + "' value='" + val.Dept + "' type='text' style='display:none' /><input class='form-control' id='txt_accept1" + (key + 1) + "' value='" + val.accept_term + "' type='text' style='display:none' />" + val.Dept + "</td>";
            tableBody += "<td><input class='form-control' id='txt_cat1" + (key + 1) + "' value='" + val.Cat + "' type='text' style='display:none' /><input class='form-control' id='txt_open" + (key + 1) + "' value='" + val.AUTO_OPEN + "' type='text' style='display:none' /><input class='form-control' id='txt_ad_id" + (key + 1) + "' value='" + val.ad_id + "' type='text' style='display:none' />" + val.Cat + "</td>";
            tableBody += "<td><input class='form-control' id='txt_buyer1" + (key + 1) + "' value='" + val.Buyer + "' type='text' style='display:none' /><input class='form-control' id='txt_deptid" + (key + 1) + "' value='" + val.dept_id + "' type='text' style='display:none' /><input class='form-control' id='txt_branchid" + (key + 1) + "' value='" + val.branch_id + "' type='text' style='display:none' /><input class='form-control' id='txt_regionid" + (key + 1) + "' value='" + val.region_id + "' type='text' style='display:none' /><input class='form-control' id='txt_locid" + (key + 1) + "' value='" + val.loc_id + "' type='text' style='display:none' />" + val.Buyer + "</td>";
            tableBody += "<td><input class='form-control' id='txt_Branch1" + (key + 1) + "' value='" + val.Branch + "' type='text' style='display:none' /><input class='form-control' id='txt_Start_Date1" + (key + 1) + "' value='" + val.StrDate + "' type='text' style='display:none' />" + val.Branch + "</td>";
            tableBody += "<td><input class='form-control' id='txt_region1" + (key + 1) + "' value='" + val.region + "' type='text' style='display:none' /><input class='form-control' id='txt_End_Date1" + (key + 1) + "' value='" + val.End_Date + "' type='text' style='display:none' /> " + val.region + "</td>";
            tableBody += "<td><input class='form-control' id='txt_Delivery_loc1" + (key + 1) + "' value='" + val.locname + "' type='text' style='display:none' /><input class='form-control' id='txt_Start_time1" + (key + 1) + "' value='" + val.StTime + "' type='text' style='display:none' />" + val.locname + "</td>";
            //<input class='form-control' id='txt_Delivery_time1" + (key + 1) + "' value='" + val.delTime + "' type='text' style='display:none' />
            //tableBody += "<td>" + val.delTime + "</td>";
            tableBody += "<td><input class='form-control' id='txt_End_time1" + (key + 1) + "' value='" + val.EndTime + "' type='text' style='display:none' /><input class='form-control' id='txt_remind_cnt" + (key + 1) + "' value='" + val.remind_cnt + "' type='text' style='display:none' /> <input class='form-control' id='txt_Delivery_Date1" + (key + 1) + "' value='" + val.EXP_DELIVERY_DATE + "' type='text' style='display:none' /><input class='form-control' id='txt_Curr1" + (key + 1) + "' value='" + val.Currency + "' type='text' style='display:none' /><input class='form-control' id='txt_remind_days" + (key + 1) + "' value='" + val.remind_days + "' type='text' style='display:none' />" + val.status + "</td>";
            tableBody += "<td>" + val.publish_date + "</td>";
            tableBody += "<td class='text-center'><a data-toggle='modal' id='a_view" + (key + 1) + "' data-target='#div_viewdata' onclick='return ViewData(" + (key + 1) + ");\'><img title='Preview'  src='../../RFQassets/img/preview.png'  style='vertical-align:middle;'/></a></td>";

            tableBody += "</tr>";
            keyid = (key + 1);
        })

        var table = "<table id='tbl_RFQ' class='table table-bordered table-hover table-sm'>" +
            "<thead><tr>" + tableHeader + "</tr></thead>" +
            "<tbody>" + tableBody + "</tbody>" +
            "</table>";

        $("#dv_rfqPublish").empty();
        $("#dv_rfqPublish").html(table);

    }
}
function getRFQDtlPublish() {
    PageMethods.getRFQDtlPublish($("#txt_Vendor_Url").val(), function (response) {
        rfqDtlPublishJson = JSON.parse(response[0]);
        displayPublishRFQ(rfqDtlPublishJson);
    }, function (error) {
        deptJson = [];
        alert(error);
    });
}
function onLoad() {
    getTermsCon();
    assignTestRfq();
    assignCategory();
    reminderTable();
    fillDocumentDetails();
    $("#txt_pk_id").val('');
}
function getdept(deptid) {
    PageMethods.getDeptDetails(deptid, $("#txt_Master_Url").val(), function (response) {
        deptJson = JSON.parse(response);
        assignDept(deptJson);
    }, function (error) {
        deptJson = [];
        alert(error);
    });
}
function getregion(regionid) {
    PageMethods.getRegion(regionid, $("#txt_Master_Url").val(), function (response) {
        regionJson = JSON.parse(response);
        assignRegion(regionJson);
    }, function (error) {
        regionJson = [];
        alert(error);
    });
}
function getbranch(branchid) {
    PageMethods.getBranch(branchid, $("#txt_Master_Url").val(), function (response) {
        branchJson = JSON.parse(response);
        assignBranch(branchJson);
    }, function (error) {
        branchJson = [];
        alert(error);
    });
}
function getUnit() {
    PageMethods.getUnit($("#txt_Master_Url").val(), function (response) {
        unitJson = JSON.parse(response);
        assignLotUnit(unitJson);
        assignLineUnit(unitJson);
    }, function (error) {
        unitJson = [];
        alert(error);
    });
}
function getbuyer() {
    PageMethods.getBuyer($("#txt_Master_Url").val(), function (response) {
        buyerJson = JSON.parse(response);
        assignBuyer(buyerJson);
    }, function (error) {
        buyerJson = [];
        alert(error);
    });
}
function getlocation(locaid) {
    PageMethods.getLocation(locaid, $("#txt_Master_Url").val(), function (response) {
        locationJson = JSON.parse(response);
        assignLocation(locationJson);
    }, function (error) {
        regionJson = [];
        alert(error);
    });
}
function assignDept(deptJson) {
    $('#txt_Dept').autocomplete({
        autoFocus: true,
        minLength: 0,
        source: deptJson,
        select: function (event, ui) {
            event.preventDefault();
            $('#txt_Dept').val(ui.item.label);
            $('#txt_Dept_id').val(ui.item.id);
            return false;
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $('#txt_Dept').val("");
            }

        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}
function assignRegion(regionJson) {
    $('#txt_region').autocomplete({
        autoFocus: true,
        minLength: 0,
        source: regionJson,
        select: function (event, ui) {
            event.preventDefault();
            $('#txt_region').val(ui.item.label);
            $('#txt_region_id').val(ui.item.id);
            return false;
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $('#txt_region').val("");
            }

        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}
function assignBranch(branchJson) {
    $('#txt_branch').autocomplete({
        autoFocus: true,
        minLength: 0,
        source: branchJson,
        select: function (event, ui) {
            event.preventDefault();
            $('#txt_branch').val(ui.item.label);
            $('#txt_branch_id').val(ui.item.id);
            return false;
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $('#txt_branch').val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}
function assignTestRfq() {
    var testJson = [];
    var obj = { "id": "Yes", "label": "Yes" };
    testJson.push(obj);
    obj = { "id": "No", "label": "No" };
    testJson.push(obj);
    $('#txt_Test_Rfq').autocomplete({
        autoFocus: true,
        minLength: 0,
        source: testJson,
        select: function (event, ui) {
            event.preventDefault();
            $('#txt_Test_Rfq').val(ui.item.label);
            testAucid = ui.item.id;
            return false;
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $('#txt_Test_Rfq').val("");
            }

        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}
function assignCategory() {
    var testJson = [];
    var obj = { "id": "Capex", "label": "Capex" };
    testJson.push(obj);
    obj = { "id": "Opex", "label": "Opex" };
    testJson.push(obj);
    $('#txt_Cat').autocomplete({
        autoFocus: true,
        minLength: 0,
        source: testJson,
        select: function (event, ui) {
            event.preventDefault();
            $('#txt_Cat').val(ui.item.label);
            catid = ui.item.id;
            return false;
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $('#txt_Cat').val("");
            }

        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}
function assignLocation(locationJson) {
    $('#txt_Delivery_loc').autocomplete({
        autoFocus: true,
        minLength: 0,
        source: locationJson,
        select: function (event, ui) {
            event.preventDefault();
            $('#txt_Delivery_loc').val(ui.item.label);
            $('#txt_loc_id').val(ui.item.id);
            return false;
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $('#txt_Delivery_loc').val("");
            }

        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}
function assignBuyer(buyerJson) {
    $('#txt_Dept').val('');
    $('#txt_Dept_id').val('');
    $('#txt_branch').val('');
    $('#txt_branch_id').val('');

    $('#txt_buyer').autocomplete({
        autoFocus: true,
        minLength: 3,
        source: buyerJson,
        select: function (event, ui) {
            event.preventDefault();
            $('#txt_buyer').val(ui.item.label);
            $('#txt_buyer_id').val(ui.item.id);
            $('#txt_dept_id').val(ui.item.FK_DEPARTMENT_ID);
            $('#txt_branch_id').val(ui.item.FK_BRANCH_ID);
            $('#txt_region_id').val(ui.item.FK_REGION_ID);
            $('#txt_location_id').val(ui.item.FK_LOCATION_ID);
            getdept(ui.item.FK_DEPARTMENT_ID);
            getbranch(ui.item.FK_BRANCH_ID);
            getregion(ui.item.FK_REGION_ID);
            getlocation(ui.item.FK_LOCATION_ID);
            return false;
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $(this).val("");
            }

        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}
function assignCommodity() {
    var testJson = [];
    var obj = { "id": "Yes", "label": "Yes" };
    testJson.push(obj);
    obj = { "id": "No", "label": "No" };
    testJson.push(obj);
    $('#txt_commodity').autocomplete({
        autoFocus: true,
        minLength: 0,
        source: testJson,
        select: function (event, ui) {
            event.preventDefault();
            $('#txt_commodity').val(ui.item.label);
            catid = ui.item.id;
            return false;
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $('#txt_commodity').val("");
            }

        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}
function assignLotCommodity() {
    var testJson = [];
    var obj = { "id": "Yes", "label": "Yes" };
    testJson.push(obj);
    obj = { "id": "No", "label": "No" };
    testJson.push(obj);
    $('#txt_lot_commodity').autocomplete({
        autoFocus: true,
        minLength: 0,
        source: testJson,
        select: function (event, ui) {
            event.preventDefault();
            $('#txt_lot_commodity').val(ui.item.label);
            catid = ui.item.id;
            return false;
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $('#txt_lot_commodity').val("");
            }

        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}
function assignLotUnit(unitJson) {
    $('#txt_lot_unit').autocomplete({
        autoFocus: true,
        minLength: 0,
        source: unitJson,
        select: function (event, ui) {
            event.preventDefault();
            $('#txt_lot_unit').val(ui.item.label);
            $('#txt_lot_unit_id').val(ui.item.id);
            return false;
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $('#txt_lot_unit').val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}
function assignLineUnit(unitJson) {
    $('#txt_line_unit').autocomplete({
        autoFocus: true,
        minLength: 0,
        source: unitJson,
        select: function (event, ui) {
            event.preventDefault();
            $('#txt_line_unit').val(ui.item.label);
            $('#txt_line_unit_id').val(ui.item.id);
            return false;
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $('#txt_line_unit').val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}
function itemTablecreate() {
    var html = "";
    html = "<table id='tbl_item' class='table table-bordered table-hover table-sm'><thead><tr><th>Name</th><th>Quantity</th><th>Unit</th><th name='addrow'><span class='glyphicon glyphicon-plus' aria-hidden='true'></span></th></tr></thead>";
    html += "<tbody><tr>";
    html += "<td><input class='form-control' id='txt_Name1' type='text' /></td>";
    html += "<td class='text-right'><input class='form-control' id='txt_Qnty1' type='text' /></td>";
    html += "<td><input class='form-control' id='txt_Unit1' type='text' /></td>"
    //html += "<td><span class='glyphicon glyphicon-trash' aria-hidden='true' id='del_row1' onclick=\"return delete_rowi(" + (1) + ");\"></span></td>";
    html += "<td id='deleted1'  class='deleteITEM'><img id='delete_rowi1' src='../../RFQassets/img/ico-delete.gif' style='border:0;'  alt='Click here to delete this Entry.' /></td></tr></tbody>";
    $("#div_item").html(html);
}
$(document).on('click', '[name=addrow]', function () {
    var lastrow1 = $('#tbl_item tr').length;
    if (lastrow1 > 1) {
        for (var i = 0; i < lastrow1 - 1; i++) {
            var name = $("#txt_Name" + (i + 1)).val();
            var qnty = $("#txt_Qnty" + (i + 1)).val();
            var unit = $("#txt_Unit" + (i + 1)).val();

            if (name == "") {
                $.jAlert({
                    'content': 'Please enter name for row no-' + (i + 1)
                });
                return false;

            }
            if (qnty == "") {
                $.jAlert({
                    'content': 'Please enter quantity for row no-' + (i + 1)
                });
                return false;
            }
            if (unit == "") {
                $.jAlert({
                    'content': 'Please enter unit for row no-' + (i + 1)
                });
                return false;
            }
        }

        var html = "";
        html = "<tr>";
        html += "<td><input class='form-control' id='txt_Name" + lastrow1 + "' type='text' /></td>";
        html += "<td class='text-right'><input class='form-control' id='txt_Qnty" + lastrow1 + "' type='text' /></td>";
        html += "<td><input class='form-control' id='txt_Unit" + lastrow1 + "' type='text' /></td>"
        //html += "<td><span class='glyphicon glyphicon-trash' aria-hidden='true' id='del_row" + lastrow1 + "' onclick=\"return delete_rowi(" + (lastrow1) + ");\"></span></td></tr>";
        html += "<td id='deleted" + lastrow1 + "'  class='deleteITEM'><img id='delete_rowi" + lastrow1 + "' src='../../RFQassets/img/ico-delete.gif' style='border:0;'  alt='Click here to delete this Entry.' /></td>";

        $('#tbl_item').append(html);
    }
});
$(document).on('click', '.deleteITEM', function () {
    var RowNumber = $(this).closest("tr")[0].rowIndex;// $(this).closest("tr").index();
    delete_rowi(RowNumber);
});
function delete_rowi(RowIndex) {
    var tbl = document.getElementById("tbl_item");
    var lastRow = tbl.rows.length;
    if (lastRow <= 2) {
        jAlert("Validation Error:You have to Enter atleast one record..!", "Validation");
        return false;
    }
    for (var contolIndex = RowIndex; contolIndex <= lastRow - 1; contolIndex++) {
        if (contolIndex == RowIndex) {
            tbl.deleteRow(RowIndex);
        }
        $("#txt_Name" + (parseInt(contolIndex) + 1)).attr("id", "txt_Name" + contolIndex);
        $("#txt_Qnty" + (parseInt(contolIndex) + 1)).attr("id", "txt_Qnty" + contolIndex);
        $("#txt_Unit" + (parseInt(contolIndex) + 1)).attr("id", "txt_Unit" + contolIndex);
        //$("#del_row" + (parseInt(contolIndex) + 1)).attr("onclick", "delete_rowi(" + contolIndex + ")");
        $("#delete_rowi" + (parseInt(contolIndex) + 1)).onclick = new Function("return delete_rowi(" + contolIndex + ")");
        $("#delete_rowi" + (parseInt(contolIndex) + 1)).attr("id", "delete_rowi" + contolIndex);
    }
}
function supplierTablecreate() {
    var html = "";
    html = "<table id='tbl_supplier' class='table table-bordered table-hover table-sm'><thead><tr><th>Supplier Name</th><th>Main Contact</th><th name='addrowS'><span class='glyphicon glyphicon-plus' aria-hidden='true'></span></th></tr></thead>";
    html += "<tbody>";
    //html += "<td><input class='form-control'  id='txt_Label1' name='suppliername' type='text' /><input class='form-control'  id='txt_LabelId1' name='supplierid' type='text' style='display:none' /></td>";
    //html += "<td><input class='form-control' id='txt_Contact1' name='contactname' type='text' /></td>";
    //html += "<td><span class='glyphicon glyphicon-trash' aria-hidden='true' id='del_rowS1' onclick=\"return delete_rowS(" + (1) + ");\"></span></td></tr></tbody>";
    html += "</tbody>";
    $("#div_Supplier").html(html);
}
function queriesTablecreate() {
    var html = "";
    html = "<table id='tbl_query' class='table table-bordered table-hover table-sm'><thead><tr><th>Query</th><th>Description</th><th name='addquery'><span class='glyphicon glyphicon-plus' aria-hidden='true'></span></th></tr></thead>";
    html += "<tbody>";
    html += "</tbody>";
    $("#div_queries").html(html);

}
function reminderTable() {
    var html1 = "";
    html1 = "<table id='tbl_reminder' class='table table-bordered table-hover table-sm' style='width:500px'><thead><tr><th>Reminder Count</th><th>Reminder Days</th><th name='addreminder'><span class='glyphicon glyphicon-plus' aria-hidden='true'></span></th></tr></thead>";
    html1 += "<tbody>";
    html1 += "</tbody>";
    $("#div_reminder").html(html1);

}
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31
        && (charCode < 48 || charCode > 57))
        return false;

    return true;
}
$(document).on('click', '[name=addreminder]', function () {
    var lastrow1 = $('#tbl_reminder tr').length;
    if (lastrow1 > 0) {
        for (var i = 0; i < lastrow1 - 1; i++) {
            var count = $("#txt_Count" + (i)).val();
            var days = $("#txt_Days" + (i)).val();

            if (count == "") {
                $.jAlert({
                    'content': 'Please enter count for row no -' + (i)
                });
                return false;
            }
            if (days == "") {
                $.jAlert({
                    'content': 'Please enter days for row no -' + (i)
                });
                return false;
            }
        }
        var html1 = "";
        html1 = "<tr>";
        html1 += "<td><input class='form-control input-sm text-right' id='txt_Count" + (lastrow1) + "' name='remincount' type='text'  onkeypress='return isNumberKey(event)'/></td>";
        html1 += "<td class='text-right'><input class='form-control input-sm text-right' id='txt_Days" + (lastrow1) + "' name='remindays' type='text'  onkeypress='return isNumberKey(event)'/></td>";
        html1 += "<td id='dele" + (lastrow1) + "'  class='deleteREMIN'><img id='delete_row_rem" + (lastrow1) + "' src='../../RFQassets/img/ico-delete.gif' style='border:0;'  alt='Click here to delete this Entry.' /></td></tr>";
        $('#tbl_reminder').append(html1);
    }
});
function delete_row_rem(RowIndex) {
    try {
        var tbl = document.getElementById("tbl_reminder");
        var lastRow = tbl.rows.length;
        var IsDelete = confirm("Are you sure to delete this ?");
        if (IsDelete) {
            for (var contolIndex = RowIndex; contolIndex < lastRow - 1; contolIndex++) {
                $("#txt_Count" + (parseInt(contolIndex) + 1)).attr("id", "txt_Count" + contolIndex);
                $("#txt_Days" + (parseInt(contolIndex) + 1)).attr("id", "txt_Days" + contolIndex);

                //  $("#delete_row_rem" + (parseInt(contolIndex) + 1)).onclick = new Function("return delete_row_rem(" + contolIndex + ")");
                $("#delete_row_rem" + (parseInt(contolIndex) + 1)).attr("id", "delete_row_rem" + contolIndex);
            }
            ///////////////////////////////////////////////////////////////////////
            document.getElementById("tbl_reminder").deleteRow(RowIndex);
            // alert("Record Deleted Successfully..!");
        }
    }
    catch (Exc) { }
}
$(document).on('click', '[name=addquery]', function () {
    var lastrow1 = $('#tbl_query tr').length;
    if (lastrow1 > 0) {
        for (var i = 1; i < lastrow1; i++) {
            var count = $("#txt_query" + (i)).val();
            if (count == "") {
                $.jAlert({
                    'content': 'Please enter query for row no-' + (i)
                });
                return false;
            }
        }
        var html1 = "";
        html1 = "<tr>";
        html1 += "<td><input class='form-control input-sm' id='txt_query" + (lastrow1) + "' name='query' type='text' /></td>";
        html1 += "<td><input class='form-control input-sm' id='txt_desc" + (lastrow1) + "' name='desc' type='text'/></td>";
        html1 += "<td id='dele" + (lastrow1) + "'  class='deleteQUERY'><img id='delete_row_query" + (lastrow1) + "' src='../../RFQassets/img/ico-delete.gif' style='border:0;'  alt='Click here to delete this Entry.' /></td></tr>";
        $('#tbl_query').append(html1);
    }
});
function delete_row_query(RowIndex) {
    try {
        var tbl = document.getElementById("tbl_query");
        var lastRow = tbl.rows.length;
        var IsDelete = confirm("Are you sure to delete this ?");
        if (IsDelete) {
            for (var contolIndex = RowIndex; contolIndex < lastRow - 1; contolIndex++) {
                $("#txt_query" + (parseInt(contolIndex) + 1)).attr("id", "txt_query" + contolIndex);
                $("#txt_desc" + (parseInt(contolIndex) + 1)).attr("id", "txt_desc" + contolIndex);

                //$("#delete_row_query" + (parseInt(contolIndex) + 1)).onclick = new Function("return delete_row_query(" + contolIndex + ")");
                $("#delete_row_query" + (parseInt(contolIndex) + 1)).attr("id", "delete_row_query" + contolIndex);
            }
            ///////////////////////////////////////////////////////////////////////
            document.getElementById("tbl_query").deleteRow(RowIndex);
            // alert("Record Deleted Successfully..!");
        }
    }
    catch (Exc) { }
}
$(document).on('click', '[name=addrowS]', function () {
    var lastrow1 = $('#tbl_supplier tr').length;
    if (lastrow1 > 0) {
        for (var i = 1; i < lastrow1; i++) {
            var label = $("#txt_Label" + (i)).val();
            var contact = $("#txt_Contact" + (i)).val();

            if (label == "") {
                $.jAlert({
                    'content': 'Please enter label for row no-' + (i)
                });
                return false;
            }
            if (contact == "") {
                $.jAlert({
                    'content': 'Please enter contact for row no-' + (i)
                });
                return false;
            }
            //else {
            //    validateForm(contact);
            //}
        }
        var html = "";
        html = "<tr>";
        html += "<td><input class='form-control' id='txt_Label" + (lastrow1) + "' name='suppliername' type='text' /> <input class='form-control'  id='txt_LabelId" + (lastrow1) + "' name='supplierid' type='text' style='display:none' /></td>";
        html += "<td><input class='form-control' id='txt_Contact" + (lastrow1) + "' name='contactname' type='text' /></td>";
        //html += "<td></td>";
        //   html += "<td><span class='glyphicon glyphicon-trash' aria-hidden='true' id='del_rowS" + (lastrow1 - 1) + "' onclick=\"return delete_rowS(" + (lastrow1 - 1) + ");\"></span></td></tr>";
        html += "<td id='dele" + (lastrow1) + "'  class='deleteSUPP'><img id='delete_rowS" + (lastrow1) + "' src='../../RFQassets/img/ico-delete.gif' style='border:0;'  alt='Click here to delete this Entry.' /></td></tr>";

        $('#tbl_supplier').append(html);

        ///////Fill Supplier Names////////////////////
        $('#txt_Label' + lastrow1).autocomplete({
            autoFocus: true,
            minLength: 3,
            source: supplierJson,
            select: function (event, ui) {
                event.preventDefault();
                $('#txt_Label' + lastrow1).val(ui.item.label);
                return false;
            },
            change: function (event, ui) {
                if (ui.item == null) {
                    $('#txt_Label' + lastrow1).val("");
                    $('#txt_Contact' + lastrow1).val("");
                }
            }
        })
            .focus(function () {
                $(this).autocomplete("search", "");
            });
    }
});
$(document).on('click', '.deleteSUPP', function () {
    var RowNumber = $(this).closest("tr")[0].rowIndex; //$(this).closest("tr").index();// //
    delete_rowS(RowNumber);
});
$(document).on('click', '.deleteQUERY', function () {
    var RowNumber = $(this).closest("tr")[0].rowIndex; //$(this).closest("tr").index();// //
    delete_row_query(RowNumber);
});
function fillcontact(id) {
    PageMethods.getContactdetails(id, $("#txt_Vendor_Url").val(), function (response) {
        emailJson = JSON.parse(response[0]);
        displayemail(id);
    }, function (error) {
        deptJson = [];
        alert(error);
    });
}
function displayemail(id) {
    if (emailJson) {
        $('#txt_Contact' + id).autocomplete({
            autoFocus: true,
            minLength: 0,
            source: emailJson,
            select: function (event, ui) {
                event.preventDefault();
                $('#txt_Contact' + id).val(ui.item.label);
                return false;
            },
            change: function (event, ui) {
                if (ui.item == null) {
                    $('#txt_Contact' + id).val("");
                }
            }
        })
            .focus(function () {
                $(this).autocomplete("search", "");
            });
    }
}
$(document).on('keydown', '[name=contactname]', function () {
    $("#" + $(this).attr("id")).autocomplete({
        autoFocus: true,
        minLength: 1,
        source: emailJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#" + $(this).closest('tr').find('[name=contactname]').attr("id")).val(ui.item.label);
            return false;
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $("#" + $(this).attr("id")).val("");
            }

        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
});
function validateForm(mail) {
    if (mail != undefined) {
        var x = mail;
        var atpos = x.indexOf("@");
        var dotpos = x.lastIndexOf(".");
        if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
            $.jAlert({
                'content': 'Not a valid e-mail address'
            });
            return false;
        }
    }
}
function delete_rowS(RowIndex) {
    var tbl = document.getElementById("tbl_supplier");
    var lastRow = tbl.rows.length;
    if (lastRow <= 2) {
        $.jAlert({
            'content': 'Validation Error:You have to Enter atleast one record..!'
        });
        return false;
    }
    for (var contolIndex = RowIndex; contolIndex <= lastRow - 1; contolIndex++) {
        $("#txt_LabelId" + (parseInt(contolIndex) + 1)).attr("id", "txt_LabelId" + contolIndex);
        $("#txt_Label" + (parseInt(contolIndex) + 1)).attr("id", "txt_Label" + contolIndex);
        $("#txt_Contact" + (parseInt(contolIndex) + 1)).attr("id", "txt_Contact" + contolIndex);
        $("#delete_rowS" + (parseInt(contolIndex) + 1)).onclick = new Function("return delete_rowS(" + contolIndex + ")");
        $("#delete_rowS" + (parseInt(contolIndex) + 1)).attr("id", "delete_rowS" + contolIndex);
    }
    //if (contolIndex == RowIndex) {
    tbl.deleteRow(RowIndex);
    // }
}
//////select item////////////////////////////
function getItemDetails(itn_name) {
    PageMethods.getItems(itn_name, $("#txt_Master_Url").val(), function (response) {
        ItemJSON = JSON.parse(response);
        displayItem();
    }, function (error) {

    });
}
function displayItem() {
    if (ItemJSON) {
        var tableHeader = "";
        var tableBody = "";
        var count = "";

        tableHeader += "<th>Sr.No</th>";
        tableHeader += "<th>Item Code</th>";
        tableHeader += "<th>Item Name</th>";
        tableHeader += "<th style='display:none'>ItemNameHidden</th>";
        tableHeader += "<th style='display: none;'></th>";
        tableHeader += "<th>Category</th>";
        tableHeader += "<th>Sub-Category</th>";

        $.each(ItemJSON, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td id='code'>" + val.ITMCODE + "</td>";
            tableBody += "<td class='click_item'><a href='#dateWiseItemPopup' data-toggle='modal'>" + val.label + "</a><input id='hid_Item' type='hidden' class='form-control input-sm'/></td>";
            tableBody += "<td style='display: none;'>" + val.label + "</td>";
            tableBody += "<td>" + val.ITMCATNAME + "</td>";
            tableBody += "<td>" + val.ITMSUBCATNAME + "</td>";
            tableBody += "<td style='display: none;'>" + val.PK_ITMID + "</td>";
            tableBody += "</tr>";
        })
        var table = "<table id='tbl_ItemS' class='table table-bordered table-hover table-sm'>" +
            "<thead><tr>" + tableHeader + "</tr></thead>" +
            "<tbody>" + tableBody + "</tbody>" +
            "</table>";

        $("#div_Items").empty();
        $("#div_Items").html(table);
        $("#tbl_ItemS").dataTable({ "bSort": false, "paging": true, "info": false, "oLanguage": { "sZeroRecords": "" } });
    }
}
$('body').on('click', '.click_item', function () {
    var table = $('#tbl_ItemS').DataTable();
    //  var data = table.row(this).data();
    var datas = $(this).parent().children()[3].innerHTML;
    var hid = $(this).parent().children()[6].innerHTML
    if ($('#LotItem').is(':checked')) {
        $("#txt_lot_name").val(datas);
    }
    else if ($('#LineItem').is(':checked')) {
        $("#txt_line_item_name").val(datas);
    }
    $("#hid_Item").val(hid);
    $("#div_Items").html('');
    $("#txt_item_name_type").val('');
});
function rowvalue(ind) {
    getItemDetails($("#txt_item_name_type").val());
    $("#selected_index").val(ind);
}
function getTermsCon() {
    PageMethods.getTermsCon($("#txt_Master_Url").val(), function (response) {
        paymentTermJson = JSON.parse(response[0]);
        TermJson = JSON.parse(response[1]);
        supplierJson = JSON.parse(response[2]);
        displayTeam(paymentTermJson);
    }, function (error) {
        deptJson = [];
        alert(error);
    });
}
function displayTeam(paymentTermJson) {
    var tableBody = "";
    $.each(paymentTermJson, function (key, val) {
        //$.each(paymentTermJson, function (key, val) {
        if ((key + 1) % 2 == 1) {
            tableBody += "<fieldset class='form-group'>";
        }
        tableBody += "<label class='col-md-2'>" + val.label + "</label>";
        tableBody += "<div class='col-md-3'><input class='form-control mandatory' id='txt_" + val.name + "' type='text'/></div>";
        if ((key + 1) == 2) {
            tableBody += "<label  class='col-md-1'><a data-toggle='modal' data-target='#lgpaymentterms_Document'><i class='fa fa-paperclip fa-2x float-left'></i></a></label>";
        }
        if ((key + 1) == 1) {
            tableBody += "<label  class='col-md-1'><a data-toggle='modal' data-target='#genaralterms_Document'><i class='fa fa-paperclip fa-2x float-left'></i></a></label>";
        }
        else {
            tableBody += "<label  class='col-md-1'></label>";
        }
        if ((key + 1) % 2 != 1) {
            tableBody += "</fieldset>";
        }
    });
    $("#dv_teams").html(tableBody);
    var idlen = TermJson.length;
    for (var i = 0; i < paymentTermJson.length; i++) {
        var txt_name = paymentTermJson[i].name;
        var object = {}
        object[String("fkid")] = paymentTermJson[i].id;
        var filteredList = _.where(TermJson, object);
        $('#txt_' + txt_name).autocomplete({
            autoFocus: true,
            minLength: 0,
            source: filteredList,
            select: function (event, ui) {
                event.preventDefault();
                $('#txt_' + ui.item.name).val(ui.item.label);
                return false;
            }
        });
    }
}

////Add record for Lot with Line Item
$("#btn_AddLot").click(function () {
    if (validationLotItem()) {
        var lotname = $('#txt_lot').val();
        var fkitemid = $('#hid_Item').val();
        if (fkitemid == undefined || fkitemid == "" || fkitemid == null) {
            fkitemid = 0;
        }
        var lotdesc = $('#txt_lotdesc').val();
        var commo = $('#txt_lot_commodity').val();
        var histprice = $('#txt_lot_histoprice').val();
        var itname = $('#txt_lot_name').val();
        var qty = $('#txt_lot_qty').val();
        var lunit = $('#txt_lot_unit').val();

        if (isDuplicateLotDataFound() == false) {
            $.jAlert({
                'content': 'Already present data'
            });
            return false;
        }
        var lastrow2 = $('#tbl_LotItem tr').length;
        if (lastrow2 > 0) {
            cntlot = lastrow2 - 1;
        }
        else {
            cntlot = cntlot;
        }
        DocumentData = $("#tbl_DocumentDtlLot").html();
        DocumentData = DocumentData.replace(new RegExp("<tr>", 'g'), '<tableRow>');
        DocumentData = DocumentData.replace(new RegExp("</tr>", 'g'), '</tableRow>');
        DocumentData = DocumentData.replace(new RegExp("<tr", 'g'), '<tableRow');
        DocumentData = DocumentData.replace(new RegExp("</tr", 'g'), '</tableRow');
        DocumentData = DocumentData.replace(new RegExp("<thead", 'g'), '<tableHead');
        DocumentData = DocumentData.replace(new RegExp("</thead", 'g'), '</tableHead');

        DocumentData = DocumentData.replace(new RegExp('<th>', 'g'), '<tableHdr>');
        DocumentData = DocumentData.replace(new RegExp("</th>", 'g'), '</tableHdr>');
        DocumentData = DocumentData.replace(new RegExp("<th", 'g'), '<tableHdr');
        DocumentData = DocumentData.replace(new RegExp("</th", 'g'), '</tableHdr');

        DocumentData = DocumentData.replace(new RegExp("<td>", 'g'), '<tableDtl>');
        DocumentData = DocumentData.replace(new RegExp("</td>", 'g'), '</tableDtl>');
        DocumentData = DocumentData.replace(new RegExp("<td", 'g'), '<tableDtl');
        DocumentData = DocumentData.replace(new RegExp("</td", 'g'), '</tableDtl');
        DocumentData = DocumentData.replace(new RegExp("<tbody", 'g'), '<tableBody');
        DocumentData = DocumentData.replace(new RegExp("</tbody", 'g'), '</tableBody');

        $('#tbl_LotItem tbody').append('<tr><td class="hidden"><input type="text"  value=' + (cntlot) + '  id="number(' + (cntlot) + ');" /></td><td>' + capitalize(lotname) + '</td><td>' + capitalize(lotdesc) + '</td><td align="centre">' + commo + '</td><td align="centre">' + capitalize(itname) + '</td><td align="right">' + qty + '</td><td align="centre">' + lunit + '</td><td align="right">' + histprice + '</td><td class="hidden">' + fkitemid + '</td><td><a data-toggle="modal" data-target="#lgview_Document" id="ddoclot' + (cntlot + 1) + '" onclick="viewdoclot(' + (cntlot + 1) + ');" ><i class="fa fa-paperclip fa-2x float-left"></i></a></td><td id="dtlot' + (cntlot + 1) + '" class="deleteLOTLINE1"><img id="delete_Row_LotLine' + (cntlot + 1) + '" src="../../RFQassets/img/ico-delete.gif" style="border:0;"  alt="Click here to delete this Entry." /></td><td class="hidden"><div id="div_doclot' + (cntlot + 1) + '"><tabledocument class="table color-bordered-table info-bordered-table" id=tbl_docvilot' + (cntlot + 1) + '>' + DocumentData + '</div></tabledocument></td></tr>');
        cntlot++;
        $('#txt_lot_name').val('');
        $('#txt_lot_qty').val('');
        $('#txt_lot_unit').val('');
        $('#txt_lot_histoprice').val('');
        $("#tbl_DocumentDtlLot").html('<thead><tr class="grey"><th>Description</th><th>File Name</th><th>Delete</th></tr></thead>');

    }
});

function viewdoclot(numb) {
    var rown = $('table#tbl_LotItem').find('tr');
    var DocumentD = $("#div_doclot" + numb).html();
    DocumentD = DocumentD.replace(new RegExp("<tabledocument", 'g'), '<table');
    DocumentD = DocumentD.replace(new RegExp("</tabledocument>", 'g'), '</table>');
    DocumentD = DocumentD.replace(new RegExp("<tablerow>", 'g'), '<tr>');
    DocumentD = DocumentD.replace(new RegExp("</tablerow>", 'g'), '</tr>');
    DocumentD = DocumentD.replace(new RegExp("<tablerow", 'g'), '<tr');
    DocumentD = DocumentD.replace(new RegExp("</tablerow", 'g'), '</tr');
    DocumentD = DocumentD.replace(new RegExp("<tablehead", 'g'), '<thead');
    DocumentD = DocumentD.replace(new RegExp("</tablehead", 'g'), '</thead');

    DocumentD = DocumentD.replace(new RegExp('<tablehdr>', 'g'), '<th>');
    DocumentD = DocumentD.replace(new RegExp("</tablehdr>", 'g'), '</th>');
    DocumentD = DocumentD.replace(new RegExp("<tablehdr", 'g'), '<th');
    DocumentD = DocumentD.replace(new RegExp("</tablehdr", 'g'), '</th');

    DocumentD = DocumentD.replace(new RegExp("<tabledtl>", 'g'), '<td>');
    DocumentD = DocumentD.replace(new RegExp("</tabledtl>", 'g'), '</td>');
    DocumentD = DocumentD.replace(new RegExp("<tabledtl", 'g'), '<td');
    DocumentD = DocumentD.replace(new RegExp("</tabledtl", 'g'), '</td');
    DocumentD = DocumentD.replace(new RegExp("<tablebody", 'g'), '<tbody');
    DocumentD = DocumentD.replace(new RegExp("</tablebody", 'g'), '</tbody');
    var rowndc = $("#tbl_docvilot" + numb + " tablerow").length;
    if (rowndc == 1) {
        DocumentD = "<tr>";
        DocumentD += "<td></td>";
        DocumentD += "<td>No Documents Uploaded</td>";
        DocumentD += "<td></td>";
        DocumentD += "</tr>";
    }
    $("#div_viewdoc").html(DocumentD);
}
function validationLotItem() {
    var lotname = $('#txt_lot').val();
    var lotdesc = $('#txt_lotdesc').val();
    var commo = $('#txt_lot_commodity').val();
    var histprice = $('#txt_lot_histoprice').val();
    var itname = $('#txt_lot_name').val();
    var qty = $('#txt_lot_qty').val();
    var lunit = $('#txt_lot_unit').val();

    if (lotname == undefined || lotname == "") {
        $.jAlert({
            'content': 'Please Enter Lot Name'
        });
        return false;
    }
    if (lotdesc == undefined || lotdesc == "") {
        $.jAlert({
            'content': 'Please Enter Lot Description'
        });
        return false;
    }
    if (commo == undefined || commo == "") {
        $.jAlert({
            'content': 'Please Select Commodity'
        });
        return false;
    }
    if (itname == undefined || itname == "") {
        $.jAlert({
            'content': 'Please Enter Item Name'
        });
        return false;
    }
    if (qty == undefined || qty == "" || qty == "0") {
        $.jAlert({
            'content': 'Please Enter Quantity'
        });
        return false;
    }
    if (lunit == undefined || lunit == "") {
        $.jAlert({
            'content': 'Please Select Unit'
        });
        return false;
    }
    if (histprice == undefined || histprice == "" || histprice == "0") {
        $.jAlert({
            'content': 'Please Enter Historic Price'
        });
        return false;
    }
    return true;
}
function isDuplicateLotDataFound() {
    var cnnt = 0;
    var mttab = $('table#tbl_LotItem').find('tr');
    for (var i = 1; i < mttab.length; i++) {
        var lot = $(mttab[i]).find('td:eq(1)').text();
        var lotd = $(mttab[i]).find('td:eq(2)').text();
        var comm = $(mttab[i]).find('td:eq(3)').text();
        var itnam = $(mttab[i]).find('td:eq(4)').text();
        var qtyy = $(mttab[i]).find('td:eq(5)').text();
        var unit = $(mttab[i]).find('td:eq(6)').text();
        var hprice = $(mttab[i]).find('td:eq(7)').text();

        var lotname = $('#txt_lot').val();
        var lotdesc = $('#txt_lotdesc').val();
        var commo = $('#txt_lot_commodity').val();
        var histprice = $('#txt_lot_histoprice').val();
        var itname = $('#txt_lot_name').val();
        var qty = $('#txt_lot_qty').val();
        var lunit = $('#txt_lot_unit').val();

        if (capitalize(itnam) == capitalize(itname) && capitalize(lot) == capitalize(lotname)) {
            cnnt++;
        }
    }
    if (cnnt > 0) {
        return false;
    }
    else {
        return true;
    }
}
////Add record for Line Item
$("#btn_Add_LItem").click(function () {
    if (validationLItem()) {
        var itname = $('#txt_line_item_name').val();
        var fkitemid = $('#hid_Item').val();
        if (fkitemid == undefined || fkitemid == "" || fkitemid == null) {
            fkitemid = 0;
        }
        var qty = $('#txt_line_qty').val();
        var lunit = $('#txt_line_unit').val();
        var lhistprice = $('#txt_line_histoprice').val();

        if (isDuplicateLineDataFound() == false) {
            $.jAlert({
                'content': 'Already present data'
            });
            return false;
        }
        var lastrow2 = $('#tbl_LItem tr').length;
        if (lastrow2 > 0) {
            cnt = lastrow2 - 1;
        }
        else {
            cnt = cnt;
        }
        DocumentData = $("#tbl_DocumentDtlLine").html();
        DocumentData = DocumentData.replace(new RegExp("<tr>", 'g'), '<tableRow>');
        DocumentData = DocumentData.replace(new RegExp("</tr>", 'g'), '</tableRow>');
        DocumentData = DocumentData.replace(new RegExp("<tr", 'g'), '<tableRow');
        DocumentData = DocumentData.replace(new RegExp("</tr", 'g'), '</tableRow');
        DocumentData = DocumentData.replace(new RegExp("<thead", 'g'), '<tableHead');
        DocumentData = DocumentData.replace(new RegExp("</thead", 'g'), '</tableHead');

        DocumentData = DocumentData.replace(new RegExp('<th>', 'g'), '<tableHdr>');
        DocumentData = DocumentData.replace(new RegExp("</th>", 'g'), '</tableHdr>');
        DocumentData = DocumentData.replace(new RegExp("<th", 'g'), '<tableHdr');
        DocumentData = DocumentData.replace(new RegExp("</th", 'g'), '</tableHdr');

        DocumentData = DocumentData.replace(new RegExp("<td>", 'g'), '<tableDtl>');
        DocumentData = DocumentData.replace(new RegExp("</td>", 'g'), '</tableDtl>');
        DocumentData = DocumentData.replace(new RegExp("<td", 'g'), '<tableDtl');
        DocumentData = DocumentData.replace(new RegExp("</td", 'g'), '</tableDtl');
        DocumentData = DocumentData.replace(new RegExp("<tbody", 'g'), '<tableBody');
        DocumentData = DocumentData.replace(new RegExp("</tbody", 'g'), '</tableBody');

        $('#tbl_LItem tbody').append('<tr><td  class="hidden"><input type="text"  value=' + (cnt) + '  id="number(' + (cnt) + ');" /></td><td>' + capitalize(itname) + '</td><td align="right">' + qty + '</td><td align="centre">' + lunit + '</td><td align="right">' + lhistprice + '</td><td class="hidden">' + fkitemid + '</td><td><a data-toggle="modal" data-target="#lgview_Document" id="ddoc' + (cnt + 1) + '" onclick="viewdocum(' + (cnt + 1) + ')"><i class="fa fa-paperclip fa-2x float-left"></i></a></td><td id="dele' + (cnt + 1) + '"  class="deleteLINE"><img id="delete_Row_Line' + (cnt + 1) + '" src="../../RFQassets/img/ico-delete.gif" style="border:0;"  alt="Click here to delete this Entry." /></td><td class="hidden"><div id="div_doc' + (cnt + 1) + '"><tabledocument class="table color-bordered-table info-bordered-table" id=tbl_docvi' + (cnt + 1) + '>' + DocumentData + '</div></tabledocument></td></tr>');

        cnt++;
        var rown = $('table#tbl_LItem').find('tr');
        $('#txt_line_item_name').val('');
        $('#txt_line_qty').val('');
        $('#txt_line_unit').val('');
        $('#txt_line_histoprice').val('');
        $("#tbl_DocumentDtlLine").html('<thead><tr class="grey"><th>Description</th><th>File Name</th><th>Delete</th></tr></thead>');
    }
});
function viewdocum(num) {
    var rown = $('table#tbl_LItem').find('tr');
    var DocumentD = $("#div_doc" + num).html();
    DocumentD = DocumentD.replace(new RegExp("<tabledocument", 'g'), '<table');
    DocumentD = DocumentD.replace(new RegExp("</tabledocument>", 'g'), '</table>');
    DocumentD = DocumentD.replace(new RegExp("<tablerow>", 'g'), '<tr>');
    DocumentD = DocumentD.replace(new RegExp("</tablerow>", 'g'), '</tr>');
    DocumentD = DocumentD.replace(new RegExp("<tablerow", 'g'), '<tr');
    DocumentD = DocumentD.replace(new RegExp("</tablerow", 'g'), '</tr');
    DocumentD = DocumentD.replace(new RegExp("<tablehead", 'g'), '<thead');
    DocumentD = DocumentD.replace(new RegExp("</tablehead", 'g'), '</thead');

    DocumentD = DocumentD.replace(new RegExp('<tablehdr>', 'g'), '<th>');
    DocumentD = DocumentD.replace(new RegExp("</tablehdr>", 'g'), '</th>');
    DocumentD = DocumentD.replace(new RegExp("<tablehdr", 'g'), '<th');
    DocumentD = DocumentD.replace(new RegExp("</tablehdr", 'g'), '</th');

    DocumentD = DocumentD.replace(new RegExp("<tabledtl>", 'g'), '<td>');
    DocumentD = DocumentD.replace(new RegExp("</tabledtl>", 'g'), '</td>');
    DocumentD = DocumentD.replace(new RegExp("<tabledtl", 'g'), '<td');
    DocumentD = DocumentD.replace(new RegExp("</tabledtl", 'g'), '</td');
    DocumentD = DocumentD.replace(new RegExp("<tablebody", 'g'), '<tbody');
    DocumentD = DocumentD.replace(new RegExp("</tablebody", 'g'), '</tbody');
    var rowndc = $("#tbl_docvi" + num + " tablerow").length;
    if (rowndc == 1) {
        DocumentD = "<tr>";
        DocumentD += "<td></td>";
        DocumentD += "<td>No Documents Uploaded</td>";
        DocumentD += "<td></td>";
        DocumentD += "</tr>";
    }
    $("#div_viewdoc").html(DocumentD);
}
function validationLItem() {
    var itname = $('#txt_line_item_name').val();
    var qty = $('#txt_line_qty').val();
    var lunit = $('#txt_line_unit').val();
    var lhistprice = $('#txt_line_histoprice').val();

    if (itname == undefined || itname == "") {
        $.jAlert({
            'content': 'Please Enter Item Name'
        });
        return false;
    }
    if (qty == undefined || qty == "" || qty == "0") {
        $.jAlert({
            'content': 'Please Enter Quantity'
        });
        return false;
    }
    if (lunit == undefined || lunit == "") {
        $.jAlert({
            'content': 'Please Select Unit'
        });
        return false;
    }
    if (lhistprice == undefined || lhistprice == "" || lhistprice == "0") {
        $.jAlert({
            'content': 'Please Enter Historic Price'
        });
        return false;
    }
    return true;
}
function isDuplicateLineDataFound() {
    var cnnt = 0;
    var mttab = $('table#tbl_LItem').find('tr');
    for (var i = 1; i < mttab.length; i++) {
        var itnam = $(mttab[i]).find('td:eq(1)').text();
        var itname = $('#txt_line_item_name').val();
        if (capitalize(itnam) == capitalize(itname)) {
            cnnt++;
        }

    }
    if (cnnt > 0) {
        return false;
    }
    else {
        return true;
    }
}
////Add record for Lot without Line Item
$("#btn_Add").click(function () {
    if (validationLineItem()) {
        var lotname = $('#txt_LotName').val();
        var lotdesc = $('#txt_LotDesc').val();
        var commo = $('#txt_commodity').val();
        var histprice = $('#txt_HistoPrice').val();

        if (isDuplicateDataFound() == false) {
            $.jAlert({
                'content': 'Already present data'
            });
            return false;
        }
        $('#tbl_LineItem tbody').append('<tr><td class="hidden">' + cnt + '</td><td>' + capitalize(lotname) + '</td><td>' + capitalize(lotdesc) + '</td><td align="centre">' + commo + '</td><td align="right">' + histprice + '</td><td id="delete' + (cnt + 1) + '"  class="deleteLOT"><img id="delete_Row_Lot' + (cnt + 1) + '" src="../../RFQassets/img/ico-delete.gif" style="border:0;"  alt="Click here to delete this Entry." /></td></tr>');
        cnt++;
        $('#txt_LotName').val('');
        $('#txt_LotDesc').val('');
        $('#ddl_Commodity').val('');
        $('#txt_HistoPrice').val('');

    }
});
function validationLineItem() {
    var lotname = $('#txt_LotName').val();
    var lotdesc = $('#txt_LotDesc').val();
    var commo = $('#txt_commodity').val();
    var histprice = $('#txt_HistoPrice').val();

    if (lotname == undefined || lotname == "") {
        $.jAlert({
            'content': 'Please Enter Lot Name'
        });
        return false;
    }
    if (lotdesc == undefined || lotdesc == "") {
        $.jAlert({
            'content': 'Please Enter Lot Description'
        });
        return false;
    }
    if (commo == undefined || commo == "") {
        $.jAlert({
            'content': 'Please Select Commodity'
        });
        return false;
    }
    if (histprice == undefined || histprice == "" || histprice == "0") {
        $.jAlert({
            'content': 'Please Enter Historic Price'
        });
        return false;
    }
    return true;
}
function isDuplicateDataFound() {
    var cnnt = 0;
    var mttab = $('table#tbl_LineItem').find('tr');
    for (var i = 1; i < mttab.length; i++) {
        var lotn = $(mttab[i]).find('td:eq(1)').text();
        var lotna = $('#txt_LotName').val();

        if (capitalize(lotna) == capitalize(lotn)) {
            cnnt++;
        }

    }
    if (cnnt > 0) {
        return false;
    }
    else {
        return true;
    }
}

function delete_Row_Line(RowIndex) {
    try {
        var tbl = $('table#tbl_LItem').find('tr');
        var lastRow = tbl.length;
        var IsDelete = confirm("Are you sure to delete this ?");
        if (IsDelete) {
            for (var contolIndex = RowIndex; contolIndex < lastRow; contolIndex++) {
                $("#number" + (parseInt(contolIndex) + 1)).attr("id", "number" + contolIndex);
                $("#div_doc" + (parseInt(contolIndex) + 1)).attr("id", "div_doc" + contolIndex);
                $("#tbl_docvi" + (parseInt(contolIndex) + 1)).attr("id", "tbl_docvi" + contolIndex);
                $("#ddoc" + (parseInt(contolIndex) + 1)).attr("onclick", "viewdocum(" + contolIndex + ")");
                $("#ddoc" + (parseInt(contolIndex) + 1)).attr("id", "ddoc" + contolIndex);
                // $("#delete_Row_Line" + (parseInt(contolIndex) + 1)).attr("onclick", "delete_Row_Line(" + contolIndex + ")");
                // $("#delete_Row_Line" + (parseInt(contolIndex) + 1)).onclick = new Function("return delete_Row_Line(" + contolIndex + ")");
                $("#delete_Row_Line" + (parseInt(contolIndex) + 1)).attr("id", "delete_Row_Line" + contolIndex);
            }
            ///////////////////////////////////////////////////////////////////////
            document.getElementById("tbl_LItem").deleteRow(RowIndex);
            $.jAlert({
                'content': 'Record Deleted Successfully..!'
            });
        }
    }
    catch (Exc) { }
}
function delete_Row_LotLine(RowIndex) {
    try {
        var tbl = $('table#tbl_LotItem').find('tr');
        var lastRow = tbl.length;
        var IsDelete = confirm("Are you sure to delete this ?");
        if (IsDelete) {
            for (var contolIndex = RowIndex; contolIndex < lastRow; contolIndex++) {
                $("#number" + (parseInt(contolIndex) + 1)).attr("id", "number" + contolIndex);
                $("#div_doclot" + (parseInt(contolIndex) + 1)).attr("id", "div_doclot" + contolIndex);
                $("#tbl_docvilot" + (parseInt(contolIndex) + 1)).attr("id", "tbl_docvilot" + contolIndex);
                $("#ddoclot" + (parseInt(contolIndex) + 1)).attr("onclick", "viewdoclot(" + contolIndex + ")");
                $("#ddoclot" + (parseInt(contolIndex) + 1)).attr("id", "ddoclot" + contolIndex);
                // $("#delete_Row_LotLine" + (parseInt(contolIndex) + 1)).attr("onclick", "delete_Row_LotLine(" + contolIndex + ")");
                $("#delete_Row_LotLine" + (parseInt(contolIndex) + 1)).attr("id", "delete_Row_LotLine" + contolIndex);
            }
            ///////////////////////////////////////////////////////////////////////
            document.getElementById("tbl_LotItem").deleteRow(RowIndex);
            $.jAlert({
                'content': 'Record Deleted Successfully..!'
            });
        }
    }
    catch (Exc) { }
}
function delete_Row_Lot(RowIndex) {
    try {
        var tbl = document.getElementById("tbl_LineItem");
        var lastRow = tbl.rows.length;
        var IsDelete = confirm("Are you sure to delete this ?");
        if (IsDelete) {
            for (var contolIndex = RowIndex; contolIndex < lastRow - 1; contolIndex++) {
                // $("#delete_Row_Lot" + (parseInt(contolIndex) + 1)).attr("onclick", "delete_Row_Lot(" + contolIndex + ")");
                $("#delete_Row_Lot" + (parseInt(contolIndex) + 1)).attr("id", "delete_Row_Lot" + contolIndex);
            }
            ///////////////////////////////////////////////////////////////////////
            document.getElementById("tbl_LineItem").deleteRow(RowIndex);
            $.jAlert({
                'content': 'Record Deleted Successfully..!'
            });
        }
    }
    catch (Exc) { }
}
$('body').on('click', '.delete_row', function () {
    var IsDelete = confirm("Are you sure to delete this ?");
    if (IsDelete) {
        $(this).closest('tr').remove();
    }

});
$('body').on('click', '.selectallp', function () {
    if (this.checked) {
        $('.ptermcheck').prop('checked', true);
    } else {
        $('.ptermcheck').prop('checked', false);
    }

});
$('body').on('click', '.selectallg', function () {
    if (this.checked) {
        $('.gtermcheck').prop('checked', true);
    } else {
        $('.gtermcheck').prop('checked', false);
    }
});

function getMonthFromString(mon) {
    var month = new Date(Date.parse(mon + " 1, 2012")).getMonth() + 1;
    return month;
}
function downloadfilesLine(index) {
    var tbl = document.getElementById("tbl_DocumentDtlLine");
    var lastRow = tbl.rows.length;
    window.open('../../Common/FileDownload.aspx?DocType=RFQ&RequestNo=' + pkid + '&filename=' + tbl.rows[index].cells[1].innerText + '&filetag=', 'Download', 'left=150,top=100,width=600,height=300,toolbar=no,menubars=no,status=no,scrollbars=yes,resize=no');
}
function downloadfiles(index) {
    var tbl = document.getElementById("tbl_DocumentDtl");
    var lastRow = tbl.rows.length;
    window.open('../../Common/FileDownload.aspx?DocType=RFQ&RequestNo=' + pkid + '&filename=' + tbl.rows[index].cells[1].innerText + '&filetag=', 'Download', 'left=150,top=100,width=600,height=300,toolbar=no,menubars=no,status=no,scrollbars=yes,resize=no');
}
function downloadfilesh(index) {
    var tbl = document.getElementById("tbl_DocumentDtlh");
    var lastRow = tbl.rows.length;
    window.open('../../Common/FileDownload.aspx?DocType=RFQ&RequestNo=' + pkid + '&filename=' + tbl.rows[index].cells[1].innerText + '&filetag=', 'Download', 'left=150,top=100,width=600,height=300,toolbar=no,menubars=no,status=no,scrollbars=yes,resize=no');
}
function downloadfilesLH(index) {
    var tbl = document.getElementById("tbl_DocumentDtlLH");
    var lastRow = tbl.rows.length;
    window.open('../../Common/FileDownload.aspx?DocType=RFQ&RequestNo=' + pkid + '&filename=' + tbl.rows[index].cells[1].innerText + '&filetag=', 'Download', 'left=150,top=100,width=600,height=300,toolbar=no,menubars=no,status=no,scrollbars=yes,resize=no');
}
function downloadfilesLot(index) {
    var tbl = document.getElementById("tbl_DocumentDtlLot");
    var lastRow = tbl.rows.length;
    window.open('../../Common/FileDownload.aspx?DocType=RFQ&RequestNo=' + pkid + '&filename=' + tbl.rows[index].cells[1].innerText + '&filetag=', 'Download', 'left=150,top=100,width=600,height=300,toolbar=no,menubars=no,status=no,scrollbars=yes,resize=no');
}
function downloadfilesTerms(index) {
    var tbl = document.getElementById("tbl_DocumentDtlTerms");
    var lastRow = tbl.rows.length;
    window.open('../../Common/FileDownload.aspx?DocType=RFQ&RequestNo=' + pkid + '&filename=' + tbl.rows[index].cells[1].innerText + '&filetag=', 'Download', 'left=150,top=100,width=600,height=300,toolbar=no,menubars=no,status=no,scrollbars=yes,resize=no');
}
function downloadfilesGenTerms(index) {
    var tbl = document.getElementById("tbl_DocumentDtlGenTerms");
    var lastRow = tbl.rows.length;
    window.open('../../Common/FileDownload.aspx?DocType=RFQ&RequestNo=' + pkid + '&filename=' + tbl.rows[index].cells[1].innerText + '&filetag=', 'Download', 'left=150,top=100,width=600,height=300,toolbar=no,menubars=no,status=no,scrollbars=yes,resize=no');
}
function downloadfilesPayTerms(index) {
    var tbl = document.getElementById("tbl_DocumentDtlPayTerms");
    var lastRow = tbl.rows.length;
    window.open('../../Common/FileDownload.aspx?DocType=RFQ&RequestNo=' + pkid + '&filename=' + tbl.rows[index].cells[1].innerText + '&filetag=', 'Download', 'left=150,top=100,width=600,height=300,toolbar=no,menubars=no,status=no,scrollbars=yes,resize=no');
}

$("#btn_save").click(function () {
    try {
        saveBtn('RFQ', '');
        PageMethods.Save_click($("#txt_SupplierXML").val(), $("#txt_pk_id").val(), $('#txt_RFQ_Desc').val(), $('#txt_Test_Rfq').val(), $('#txt_Dept_id').val(), $('#txt_loc_id').val(), $('#txt_Cat').val(), $('#txt_Start_Date').val(), $('#txt_Start_Time').val(), $('#txt_End_Date').val(), $('#txt_End_Time').val(), $('#txt_buyer_id').val(), $('#txt_branch_id').val(), $('#txt_region_id').val(), $('#txt_Currency').val(), $("#SessionAdId").val(), "0", $("#processid").val(), $("#stepid").val(), $("#txt_Vendor_Url").val(), $("#txt_Wfe_Url").val(), function (response) {
            if (response != "Error occured while saving data" && response != "false") {
                var saveMsg = "RFQ Data Saved Successfully...!";
                $.jAlert({
                    'content': saveMsg,
                    'onClose': function () {
                        location.href = "../../HomePage.aspx";
                    }
                });
            }
            else if (response == "false") {
                var errMsg = "Error occured while saving data";
                $.jAlert({
                    'content': errMsg,
                });
                return false;
            }
            else {
                $.jAlert({
                    'content': response,
                });
                return false;
            }
        }, function (error) {
        });
        $("#loading-div-background").hide();
        return true;
    }
    catch (exception) {
        return false;
    }
});
function saveBtn(btn_name, action) {
    pkid = $("#txt_pk_id").val();
    var rfqn = $('#txt_RFQ_Desc').val();
    var testpro = $('#txt_Test_Rfq').val();
    var dept = $('#txt_Dept').val();
    var catid = $('#txt_Cat').val();
    var devltime = $('#txt_Delivery_loc').val();
    var delidate = ""; $('#txt_Delivery_Date').val();
    var branch = $('#txt_branch').val();
    var sdate = $('#txt_Start_Date').val();
    var stime = $('#txt_Start_Time').val();
    var edate = $('#txt_End_Date').val();
    var etime = $('#txt_End_Time').val();
    var reg = $('#txt_region').val();
    var buyer = $('#txt_buyer').val();
    var curr = $('#txt_Currency').val();
    $('#txt_btn_name').val(btn_name);
    pkid = $("#txt_pk_id").val();
    /////////////////time validation/////////////////////////
    $("#txt_publish").val("0");
    var start = sdate;
    var stard = start.substring(0, 2);
    var starm = start.substring(3, 6);
    var stary = start.substring(7, 11);

    var end = edate;
    var endd = end.substring(0, 2);
    var endm = end.substring(3, 6);
    var endy = end.substring(7, 11);
    var d1 = new Date(stary, getMonthFromString(starm) - 1, stard);
    var d2 = new Date(endy, getMonthFromString(endm) - 1, endd);
    if (d1 > d2) {
        $.jAlert({
            'content': 'Start Date should be less than End Date'
        });
        return false;
    }
    ////////////////////////////////////////////////////////////////
    //var dateStart = '20-05-2017'; // 2013-12-11
    //var timeStart = '18:05';
    //var dateEnd = end;
    //var timeEnd = etime;
    ////var dateStart = '2013-12-11';
    ////var timeStart = '11:00';
    //var arr = dateStart.split('-');
    //var timeArr = timeStart.split(':');

    //var arr1 = dateEnd.split('-');
    //var timeArr1 = timeEnd.split(':');
    //var ssdate = new Date(arr[1], arr[2] - 1, arr[0], timeArr[0] - 1, timeArr[1] - 1);//
    //var eedate = console.log(new Date(arr1[0], arr1[1] - 1, arr1[2], timeArr1[0] - 1, timeArr1[1] - 1));
    //if (Date.parse(ssdate) >= Date.parse(eedate)) {
    //    alert("Start Time should be less than End Time");
    //    return false;
    //}
    //if (stime > etime) {
    //    alert("Start Time should be less than End Time");
    //    return false;
    //}
    /////////////////////////////////////////////////////////////
    if (action == '') {
        $("#loading-div-background").show();
    }
    if (rfqn == "") {
        $.jAlert({
            'content': 'Please Enter RFQ Name'
        });
        $("#loading-div-background").hide();
        return false;
    }
    if (testpro == "") {
        $.jAlert({
            'content': 'Please Enter Test Project'
        });
        $("#loading-div-background").hide();
        return false;
    }
    if (dept == "") {
        $.jAlert({
            'content': 'Please Select Department'
        });
        $("#loading-div-background").hide();
        return false;
    }
    if (catid == "") {
        $.jAlert({
            'content': 'Please Select Category'
        });
        $("#loading-div-background").hide();
        return false;
    }
    if (devltime == "") {
        $.jAlert({
            'content': 'Please Select Delivery Location'
        });
        $("#loading-div-background").hide();
        return false;
    }
    if (delidate == "") {
        delidate = 0;
    }
    if (branch == "") {
        $.jAlert({
            'content': 'Please Select Branch'
        });
        $("#loading-div-background").hide();
        return false;
    }
    if (sdate == "") {
        $.jAlert({
            'content': 'Please Select Start Date'
        });
        $("#loading-div-background").hide();
        return false;
    }
    if (stime == "") {
        $.jAlert({
            'content': 'Please Select Start Time'
        });
        $("#loading-div-background").hide();
        return false;
    }
    if (edate == "") {
        $.jAlert({
            'content': 'Please Select End Date'
        });
        $("#loading-div-background").hide();
        return false;
    }
    if (etime == "") {
        $.jAlert({
            'content': 'Please Select End Time'
        });
        $("#loading-div-background").hide();
        return false;
    }
    if (reg == "") {
        $.jAlert({
            'content': 'Please Select Region'
        });
        $("#loading-div-background").hide();
        return false;
    }
    if (buyer == "") {
        $.jAlert({
            'content': 'Please Select Buyer'
        });
        $("#loading-div-background").hide();
        return false;
    }
    if (curr == "") {
        $.jAlert({
            'content': 'Please Enter Currency'
        });
        $("#loading-div-background").hide();
        return false;
    }
    var rowrem = $('#tbl_reminder tr').length;
    if (pkid != "") {
        for (var i = 1; i < rowrem; i++) {
            var count = $("#txt_Count" + (i)).val();
            var days = $("#txt_Days" + (i)).val();
            if (count == "") {
                $.jAlert({
                    'content': 'Please enter count for row no - ' + (i + 1)
                });
                $("#loading-div-background").hide();
                return false;
            }
            if (days == "") {
                $.jAlert({
                    'content': 'Please enter days for row no - ' + (i + 1)
                });
                $("#loading-div-background").hide();
                return false;
            }
        }
    }
    if (pkid != "") {
        var rowsupp = $('#tbl_supplier tr').length;
        if (rowsupp < 2) {
            $.jAlert({
                'content': 'Please Add at least one Supplier...'
            });
            $("#loading-div-background").hide();
            return false;
        }
        var lastrow1 = $('#tbl_supplier tr').length;
        for (var i = 1; i < lastrow1; i++) {
            var label = $("#txt_Label" + (i)).val();
            var contact = $("#txt_Contact" + (i)).val();

            if (label == "") {
                $.jAlert({
                    'content': 'Please enter label for row no -' + (i + 1)
                });
                $("#loading-div-background").hide();
                return false;
            }
            if (contact == "") {
                $.jAlert({
                    'content': 'Please enter contact for row no -' + (i + 1)
                });
                $("#loading-div-background").hide();
                return false;
            }
        }
    }

    var xmlsave = [];
    var xmlTerms = [];
    var xmlItem = [];
    var xmlSupplier = [];
    var xmlReminder = [];
    var xmlDoc = [];
    var xmlQuery = [];
    var Fdate = "";
    var Tdate = "";

    //for xml Reminder
    var rowrem = $('#tbl_reminder tr').length;
    if (rowrem > 1) {
        for (var i = 1; i < rowrem; i++) {
            if ($('#txt_Count' + (i)).val() != undefined) {
                var cnt = $('#txt_Count' + (i)).val();
                var days = $('#txt_Days' + (i)).val();
                var remObj = { "FkRFQId": pkid, "rem_count": cnt, "rem_days": days };
                xmlReminder.push(remObj);
            }
        }
    }
    var rowpay = 0;//paymentTermJson.length;
    if (rowpay > 0) {
        for (var i = 0; i < paymentTermJson.length; i++) {
            var txt_name = paymentTermJson[i].Name;
            var terms = $('#txt_' + txt_name).val();
            if (terms == "Other") {
                if (i == 0) {
                    var lastr = $('#tbl_DocumentDtlGenTerms tr').length;
                    if (lastr <= 1) {
                        $.jAlert({
                            'content': 'Please Attach Document for General Terms'
                        });
                        $("#loading-div-background").hide();
                        return false;
                    }
                }
                if (i == 1) {
                    var lastrow1 = $('#tbl_DocumentDtlPayTerms tr').length;
                    if (lastrow1 <= 1) {
                        $.jAlert({
                            'content': 'Please Attach Document for Payment Terms'
                        });
                        $("#loading-div-background").hide();
                        return false;
                    }
                }
            }
            var termsId = paymentTermJson[i].id;
            if (terms != "") {
                var termsObj = { "FkRFQId": pkid, "FktermId": termsId, "terms": terms };
                xmlTerms.push(termsObj);
            }
        }
    }
    //for xml M_Documents for Terms
    var lastrow1 = $('#tbl_DocumentDtlTerms tr').length;
    if (lastrow1 > 1) {
        for (var i = 0; i < lastrow1 - 1; i++) {
            var desc = $('#tbl_DocumentDtlTerms tr')[i + 1].cells[0].innerText;
            var fileName = $('#tbl_DocumentDtlTerms tr')[i + 1].cells[1].innerText;
            var docObj = { "ObjectType": desc, "ObjectValue": 'RFQTerm', "FileName": fileName, "Id": pkid };
            xmlDoc.push(docObj);
        }
    }
    //for xml M_Documents for General Terms
    var lastrow1 = $('#tbl_DocumentDtlGenTerms tr').length;
    if (lastrow1 > 1) {
        for (var i = 0; i < lastrow1 - 1; i++) {
            var desc = $('#tbl_DocumentDtlGenTerms tr')[i + 1].cells[0].innerText;
            var fileName = $('#tbl_DocumentDtlGenTerms tr')[i + 1].cells[1].innerText;
            var docObj = { "ObjectType": desc, "ObjectValue": 'RFQGeneralTerm', "FileName": fileName, "Id": pkid };
            xmlDoc.push(docObj);
        }
    }
    //for xml M_Documents for Payment Terms
    var lastrow1 = $('#tbl_DocumentDtlPayTerms tr').length;
    if (lastrow1 > 1) {
        for (var i = 0; i < lastrow1 - 1; i++) {
            var desc = $('#tbl_DocumentDtlPayTerms tr')[i + 1].cells[0].innerText;
            var fileName = $('#tbl_DocumentDtlPayTerms tr')[i + 1].cells[1].innerText;
            var docObj = { "ObjectType": desc, "ObjectValue": 'RFQPaymentTerm', "FileName": fileName, "Id": pkid };
            xmlDoc.push(docObj);
        }
    }
    if ($('#Lot').is(':checked')) {
        var lastrow3 = $('#tbl_LineItem tr').length;
        var mttab = $('table#tbl_LineItem').find('tr');
        if (lastrow3 > 1) {
            for (var i = 1; i < mttab.length; i++) {
                var lotn = $(mttab[i]).find('td:eq(1)').text();
                var lotde = $(mttab[i]).find('td:eq(2)').text();
                var comm = $(mttab[i]).find('td:eq(3)').text();
                var hprice = $(mttab[i]).find('td:eq(4)').text();
                var itname = "";
                var unit = "";
                var flag = "Item";
                var itemObj = { "FkRFQId": pkid, "LotName": lotn, "LotDesc": lotde, "Commodity": comm, "Item_name": itname, "Unit": unit, "Historic_Price": hprice, "Flag": flag };
                xmlItem.push(itemObj);
            }
        }
    }
    else if ($('#LotItem').is(':checked')) {
        var lastrow1 = $('#tbl_LotItem tr').length;
        var mttab = $('table#tbl_LotItem').find('tr');
        if (lastrow1 > 1) {
            for (var i = 1; i < mttab.length; i++) {
                var lot = $(mttab[i]).find('td:eq(1)').text();
                var lotd = $(mttab[i]).find('td:eq(2)').text();
                var comm = $(mttab[i]).find('td:eq(3)').text();
                var itnam = $(mttab[i]).find('td:eq(4)').text();
                var qtyy = $(mttab[i]).find('td:eq(5)').text();
                var unit = $(mttab[i]).find('td:eq(6)').text();
                var hprice = $(mttab[i]).find('td:eq(7)').text();
                var fkitemid = $(mttab[i]).find('td:eq(8)').text();
                if (fkitemid == "") {
                    fkitemid = 0;
                }
                var flag = "Lot";
                var itemObj = { "FkRFQId": pkid, "LotName": lot, "LotDesc": lotd, "Commodity": comm, "Item_name": itnam, "Qnty": qtyy, "Unit": unit, "Historic_Price": hprice, "Flag": flag, "FkItemId": fkitemid };
                xmlItem.push(itemObj);

                ////for document line//////////////////////////////
                var mttasb = $('tabledocument#tbl_docvilot' + i).find('tablerow');
                var lastro = $('#tbl_docvilot' + i + ' tablerow').length;
                if (mttasb.length > 1) {
                    for (var d = 1; d < mttasb.length; d++) {
                        var desc = $(mttasb[d]).find('tabledtl:eq(0)').text();
                        var fileName = $(mttasb[d]).find('tabledtl:eq(1)').text();
                        var fklineid = "0";
                        var docObj = { "ObjectType": desc, "ObjectValue": 'RFQ' + lot + '' + itnam + '' + pkid + '', "FileName": fileName, "Id": pkid, "Fklineid": itnam };
                        xmlDoc.push(docObj);
                    }
                }
                if (docJson.length > 0) {
                    for (var h = 0; h < docJson.length; h++) {
                        if (docJson[h].obj_val == 'RFQ' + lot + itnam + '' + pkid + '') {
                            var desc1 = docJson[h].obj_type;
                            var fileName1 = docJson[h].filename;
                            var fklineid1 = "0";
                            var docObj = { "ObjectType": desc1, "ObjectValue": docJson[h].obj_val, "FileName": fileName1, "Id": pkid, "Fklineid": itnam };
                            xmlDoc.push(docObj);
                        }

                    }
                }
            }
        }
        //for xml M_Documents for Terms
        var lastrow1 = $('#tbl_DocumentDtlLine tr').length;
        if (lastrow1 > 1) {
            for (var i = 0; i < lastrow1 - 1; i++) {
                var desc = $('#tbl_DocumentDtlLine tr')[i + 1].cells[0].innerText;
                var fileName = $('#tbl_DocumentDtlLine tr')[i + 1].cells[1].innerText;
                var docObj = { "ObjectType": desc, "ObjectValue": 'RFQLotLine', "FileName": fileName, "Id": pkid };
                xmlDoc.push(docObj);
            }
        }
    }

    else if ($('#LineItem').is(':checked')) {
        var lastrow2 = $('#tbl_LItem tr').length;
        var mttab = $('table#tbl_LItem').find('tr');
        if (lastrow2 > 1) {
            for (var i = 1; i < mttab.length; i++) {
                var lot = "";
                var lotd = "";
                var comm = "";
                var itnam = $(mttab[i]).find('td:eq(1)').text();
                var qtyy = $(mttab[i]).find('td:eq(2)').text();
                var unit = $(mttab[i]).find('td:eq(3)').text();
                var hprice = $(mttab[i]).find('td:eq(4)').text();
                var fkitemid = $(mttab[i]).find('td:eq(5)').text();
                if (fkitemid == "") {
                    fkitemid = 0;
                }
                var flag = "Line";
                var itemObj = { "FkRFQId": pkid, "LotName": lot, "LotDesc": lotd, "Commodity": comm, "Item_name": itnam, "Qnty": qtyy, "Unit": unit, "Historic_Price": hprice, "Flag": flag, "FkItemId": fkitemid };
                xmlItem.push(itemObj);
                ////for document line//////////////////////////////
                var mttasb = $('tabledocument#tbl_docvi' + i).find('tablerow');
                var lastro = $('#tbl_docvi' + i + ' tablerow').length;
                if (mttasb.length > 1) {
                    for (var d = 1; d < mttasb.length; d++) {
                        var desc = $(mttasb[d]).find('tabledtl:eq(0)').text();
                        var fileName = $(mttasb[d]).find('tabledtl:eq(1)').text();
                        var fklineid = "0";
                        var docObj = { "ObjectType": desc, "ObjectValue": 'RFQ' + itnam + '' + pkid + '', "FileName": fileName, "Id": pkid, "Fklineid": itnam };
                        xmlDoc.push(docObj);
                    }
                }
                if (docJson.length > 0) {
                    for (var h = 0; h < docJson.length; h++) {
                        if (docJson[h].obj_val == 'RFQ' + itnam + '' + pkid + '') {
                            var desc1 = docJson[h].obj_type;
                            var fileName1 = docJson[h].filename;
                            var fklineid1 = "0";
                            var docObj = { "ObjectType": desc1, "ObjectValue": docJson[h].obj_val, "FileName": fileName1, "Id": pkid, "Fklineid": itnam };
                            xmlDoc.push(docObj);
                        }

                    }
                }
            }

        }
        //for xml M_Documents for Terms
        var lastrow1 = $('#tbl_DocumentDtlLot tr').length;
        if (lastrow1 > 1) {
            for (var i = 0; i < lastrow1 - 1; i++) {
                var desc = $('#tbl_DocumentDtlLot tr')[i + 1].cells[0].innerText;
                var fileName = $('#tbl_DocumentDtlLot tr')[i + 1].cells[1].innerText;
                var docObj = { "ObjectType": desc, "ObjectValue": 'RFQLine', "FileName": fileName, "Id": pkid };
                xmlDoc.push(docObj);
            }
        }
    }
    //for xml Supplier
    var rowsupp = $('#tbl_supplier tr').length;
    if (rowsupp > 1) {
        for (var i = 1; i < rowsupp; i++) {
            var fksuppId = $('#txt_LabelId' + (i)).val();
            var contact = $('#txt_Contact' + (i)).val();
            if (fksuppId != 0 && contact != "" && contact != null) {
                var itemObj = { "FkRFQId": pkid, "FkSupplierId": fksuppId, "SuppCon": contact };
                xmlSupplier.push(itemObj);
            }
        }
    }
    //for xml Query
    var rowsup = 0;// $('#tbl_query tr').length;
    if (rowsup > 1) {
        for (var t = 0; t < rowsup; t++) {
            if ($('#txt_query' + (t)).val() != undefined) {
                var query = $('#txt_query' + (t)).val();
                var desc = $('#txt_desc' + (t)).val();
                var itemObj = { "FkRFQId": pkid, "Query": query, "Desc": desc, "Desc_Dtl": "" };
                xmlQuery.push(itemObj);
            }
        }
    }
    //for xml M_Documents
    var lastrow1 = $('#tbl_DocumentDtl tr').length;
    if (lastrow1 > 1) {
        for (var i = 0; i < lastrow1 - 1; i++) {
            var desc = $('#tbl_DocumentDtl tr')[i + 1].cells[0].innerText;
            var fileName = $('#tbl_DocumentDtl tr')[i + 1].cells[1].innerText;
            var docObj = { "ObjectType": desc, "ObjectValue": 'RFQ', "FileName": fileName, "Id": pkid };
            xmlDoc.push(docObj);
        }
    }
    var xmlData = { "xmlReminder": xmlReminder, "xmlSupplier": xmlSupplier, "xmlTerms": xmlTerms, "xmlItem": xmlItem, "xmlDoc": xmlDoc, "xmlQuery": xmlQuery };
    $("#txt_SupplierXML").val(JSON.stringify(xmlData));
    return true;
}

function getDocumentxml() {
    var lastrow1 = $('#tbl_DocumentDtl tr').length;
    if (lastrow1 > 1) {
        for (var i = 0; i < lastrow1 - 1; i++) {
            var desc = $('#tbl_DocumentDtl tr')[i + 1].cells[0].innerText;
            var fileName = $('#tbl_DocumentDtl tr')[i + 1].cells[1].innerText;
            var docObj = { "ObjectType": desc, "ObjectValue": 'RFQ', "FileName": fileName, "Id": '##' };
            xmlDoc.push(docObj);
        }
    }

}
function edit_Click(id) {
    itemTablecreate();
    supplierTablecreate();
    queriesTablecreate();
    fillDocumentDetailsLot();
    fillDocumentDetailsLine();
    fillDocumentDetailsTerm();
    fillDocumentDetailsGenTerm();
    fillDocumentDetailsPayTerm();
    getUnit();
    assignLotUnit();
    assignLotCommodity();
    assignCommodity();
    $("#dv_RfqDtl").hide();
    $("#dv_Create").show();
    $("#btn_AucPublish").show();
    $("#dvRule").show();
    $("#dv_team").show();
    $("#preview").show();
    $("#btn_publish").show();
    $("#txt_RFQ_Desc").val($("#txt_RFQ_Desc" + id).val());
    $("#txt_Test_Rfq").val($("#txt_Test_Rfq" + id).val());
    $("#txt_Dept").val($("#txt_dept" + id).val());
    $("#txt_Cat").val($("#txt_cat" + id).val());
    $("#txt_buyer").val($("#txt_buyer" + id).val());
    $("#txt_buyer_id").val($("#txt_ad_id" + id).val());
    $("#txt_branch").val($("#txt_Branch" + id).val());
    $("#txt_region").val($("#txt_region" + id).val());
    $("#txt_Delivery_loc").val($("#txt_Delivery_loc" + id).val());
    $("#txt_Delivery_time").val($("#txt_Delivery_time" + id).val());
    $("#txt_Delivery_Date").val($("#txt_Delivery_Date" + id).val());
    $("#txt_Start_Date").val($("#txt_Start_Date" + id).val());
    $("#txt_Start_Time").val($("#txt_Start_time" + id).val());
    $("#txt_End_Date").val($("#txt_End_Date" + id).val());
    $("#txt_End_Time").val($("#txt_End_time" + id).val());
    $("#txt_Currency").val($("#txt_Curr" + id).val());
    $("#txt_Dept_id").val($("#txt_deptid" + id).val());
    $("#txt_region_id").val($("#txt_regionid" + id).val());
    $("#txt_branch_id").val($("#txt_branchid" + id).val());
    $("#txt_loc_id").val($("#txt_locid" + id).val());
    pkid = $("#txt_Pkid" + id).val();
    $("#txt_pk_id").val(pkid);
    $("#txt_id").val(id);
    // getRFQTermsDetail();
}
function getRFQTermsDetail() {
    PageMethods.getRFQTermsDetails(pkid, $("#txt_Vendor_Url").val(), function (response) {
        rfqtermsJson = JSON.parse(response[0]);
        rfqsupplierJson = JSON.parse(response[1]);
        rfqlotlineJson = JSON.parse(response[2]);
        docJson = JSON.parse(response[3]);
        reminJson = JSON.parse(response[4]);
        queryJSON = JSON.parse(response[5]);
        displayTermsRFQ();
    }, function (error) {
        rfqtermsJson = [];
        alert(error);
    });
}
function displayTermsRFQ() {
    //////////Display Terms & Conditions/////////////////
    if (rfqtermsJson) {
        for (var i = 0; i < rfqtermsJson.length; i++) {
            if (rfqtermsJson[i].term_id = rfqtermsJson[i].termstype_id) {
                $("#txt_" + rfqtermsJson[i].termstype).val(rfqtermsJson[i].terms);
            }
        }
    }
    ////////////////Display Reminder/////////////  
    if (reminJson) {
        if (reminJson.length > 0) {
            var lastrow1 = reminJson.length;
            for (var j = 0; j < reminJson.length; j++) {
                var html = "";
                html = "<tr>";
                html += "<td><input class='form-control input-sm text-right number' id='txt_Count" + (j + 1) + "' name='remincount' type='text'  onkeypress='return isNumberKey(event)' value='" + reminJson[j].count + "' /></td>";
                html += "<td class='text-right'><input class='form-control input-sm text-right number' id='txt_Days" + (j + 1) + "' name='remindays' type='text'  onkeypress='return isNumberKey(event)' value='" + reminJson[j].days + "'/></td>";
                //html += "<td class='delete_row' ><i class='fa fa-fw m-r-10 pull-left f-s-18 fa-trash'></i></td>";
                html += "<td id='dele" + (j + 1) + "'  class='deleteREMIN'><img id='delete_row_rem" + (j + 1) + "' src='../../RFQassets/img/ico-delete.gif' style='border:0;'  alt='Click here to delete this Entry.' /></td></tr>";

                $('#tbl_reminder').append(html);
            }
        }
    }
    else {
    }
    ////////////////Display Supplier/////////////  
    if (rfqsupplierJson) {
        if (rfqsupplierJson.length > 0) {
            var lastrow1 = rfqsupplierJson.length;
            for (var j = 0; j < rfqsupplierJson.length; j++) {
                var html = "";
                html = "<tr>";
                html += "<td><input class='form-control' id='txt_Label" + (j + 1) + "' name='suppliername' type='text' value='" + rfqsupplierJson[j].sname + "' /> <input class='form-control'  id='txt_LabelId" + (j + 1) + "' name='supplierid' value='" + rfqsupplierJson[j].sid + "' type='text' style='display:none' /></td>";
                html += "<td class='text-right'><input class='form-control' id='txt_Contact" + (j + 1) + "' name='contactname' type='text' value='" + rfqsupplierJson[j].contact + "'/></td>";
                // html += "<td class='delete_row' ><i class='fa fa-fw m-r-10 pull-left f-s-18 fa-trash'></i></td>";
                html += "<td id='dele" + (j + 1) + "'  class='deleteSUPP'><img id='delete_rowS" + (j + 1) + "' src='../../RFQassets/img/ico-delete.gif' style='border:0;'  alt='Click here to delete this Entry.' /></td></tr>";
                $('#tbl_supplier').append(html);
            }
        }
    }
    else {
    }
    ///////////Display Query///////////////////// 
    if (queryJSON) {
        if (queryJSON.length > 0) {
            var lastrow1 = queryJSON.length;
            for (var j = 0; j < queryJSON.length; j++) {
                var html = "";
                html = "<tr>";
                html += "<td><input class='form-control' id='txt_query" + (j + 1) + "' name='query' type='text' value='" + queryJSON[j].query + "' /></td>";
                html += "<td><input class='form-control' id='txt_desc" + (j + 1) + "' name='desc' type='text' value='" + queryJSON[j].desc + "'/></td>";
                html += "<td id='dele" + (j + 1) + "'  class='deleteQUERY'><img id='delete_row_query" + (j + 1) + "' src='../../RFQassets/img/ico-delete.gif' style='border:0;'  alt='Click here to delete this Entry.' /></td></tr>";

                $('#tbl_query').append(html);
            }
        }
    }
    else {
    }
    ////////////////Display Lot & Line/////////////  
    if (rfqlotlineJson) {
        if (rfqlotlineJson.length > 0) {
            var lastro = rfqlotlineJson.length;
            for (var k = 0; k < lastro; k++) {
                getItem(rfqlotlineJson[k].flag);
                if (rfqlotlineJson[k].flag == 'Item') {
                    $('#tbl_LineItem tbody').append('<tr><td class="hidden">' + cnt + '</td><td>' + capitalize(rfqlotlineJson[k].lname) + '</td><td>' + capitalize(rfqlotlineJson[k].ldesc) + '</td><td align="centre">' + rfqlotlineJson[k].commo + '</td><td align="right">' + rfqlotlineJson[k].histoprice + '</td><td id="delete' + (cnt + 1) + '"  class="deleteLOT"><img id="delete_Row_Lot' + (cnt + 1) + '" src="../../RFQassets/img/ico-delete.gif" style="border:0;"  alt="Click here to delete this Entry." /></td></tr>');
                }
                if (rfqlotlineJson[k].flag == 'Lot') {
                    $('#tbl_LotItem tbody').append('<tr><td class="hidden">' + cnt + '</td><td>' + capitalize(rfqlotlineJson[k].lname) + '</td><td>' + capitalize(rfqlotlineJson[k].ldesc) + '</td><td align="centre">' + rfqlotlineJson[k].commo + '</td><td>' + capitalize(rfqlotlineJson[k].itname) + '</td><td align="right">' + rfqlotlineJson[k].qty + '</td><td align="centre">' + rfqlotlineJson[k].unit + '</td><td align="right">' + rfqlotlineJson[k].histoprice + '</td><td><a data-toggle="modal" data-target="#lgview_Document"  onclick="viewdoclots(\'' + rfqlotlineJson[k].lname + '\',\'' + rfqlotlineJson[k].itname + '\' ,\'' + +rfqlotlineJson[k].rfqid + '\')" ><i class="fa fa-paperclip fa-2x float-left"></i></a></td><td id="delet' + (cnt + 1) + '"  class="deleteLOTLINE1"><img id="delete_Row_LotLine' + (cnt + 1) + '" src="../../RFQassets/img/ico-delete.gif" style="border:0;"  alt="Click here to delete this Entry." /></td></tr>');
                }
                if (rfqlotlineJson[k].flag == 'Line') {
                    $('#tbl_LItem tbody').append('<tr><td class="hidden">' + cnt + '</td><td>' + capitalize(rfqlotlineJson[k].itname) + '</td><td align="right">' + rfqlotlineJson[k].qty + '</td><td align="centre">' + rfqlotlineJson[k].unit + '</td><td align="right">' + rfqlotlineJson[k].histoprice + '</td><td><a data-toggle="modal" data-target="#lgview_Document"  onclick="viewdoclines(\'' + rfqlotlineJson[k].lname + '\',\'' + rfqlotlineJson[k].itname + '\',\'' + rfqlotlineJson[k].rfqid + '\')" ><i class="fa fa-paperclip fa-2x float-left"></i></a></td><td id="dele' + (cnt + 1) + '"  class="deleteLINE"><img id="delete_Row_Line' + (cnt + 1) + '" src="../../RFQassets/img/ico-delete.gif" style="border:0;"  alt="Click here to delete this Entry." /></td></tr>');
                }
            }
        }
    }
    ///for display document/////////////
    if (docJson) {
        var tableHeader = "";
        var cntsrno = 0;
        var tableBody = "";
        tableHeader += "<th>Description</th>";
        tableHeader += "<th>File Name</th>";
        tableHeader += "<th>Delete</th>";
        /////////////for Header RFQ//////////////////////////
        for (var i = 0; i < docJson.length; i++) {
            if (docJson[i].obj_val == 'RFQ') {
                tableBody += "<tr>";
                tableBody += "<td>" + docJson[i].obj_type + " </td>";
                tableBody += "<td><input class='hidden' type='text' name='txt_Region_Add" + (cntsrno + 1) + "' id='txt_Document_File" + (cntsrno + 1) + "' value=" + docJson[i].filename + " readonly ></input><a  id='a_downloadfiles" + (cntsrno + 1) + "' style='color:blue;'  onclick=\"return downloadfiles('" + (cntsrno + 1) + "','" + (docJson[i].obj_val) + "');\" ><u>" + docJson[i].filename + "</u></a> </td>";
                tableBody += "<td style='border: 1px solid #ADBBCA;' align='left' width='5%' valign='middle'><img id='del" + (cntsrno + 1) + "' alt='Click Here To Delete The Record.'  onclick=\"return deletefile(" + (cntsrno + 1) + ");\" src='/RFQassets/img/ico-delete.gif'  style='vertical-align:middle;'/></td></td>";
                tableBody += "</tr>";
                cntsrno++;
            }
        }
        var table = "<table class='table table-bordered' id='tbl_DocumentDtl'>" +
            "<thead><tr class='bg-blue-chambray bg-font-blue-chambray'>" + tableHeader + "</tr></thead>" +
            "<tbody>" + tableBody + "</tbody>" +
            "</table>";
        $("#div_Doc").empty();
        $("#div_Doc").html(table);
        /////////////for Terms RFQ//////////////////////////
        var tableBody1 = "";
        var cntgrno = 0;
        for (var i = 0; i < docJson.length; i++) {
            if (docJson[i].obj_val == 'RFQTerm') {
                tableBody1 += "<tr>";
                tableBody1 += "<td>" + docJson[i].obj_type + " </td>";
                tableBody1 += "<td><input class='hidden' type='text' name='txt_Region_Add" + (cntgrno + 1) + "' id='txt_Document_File" + (cntgrno + 1) + "' value=" + docJson[i].filename + " readonly ></input><a id='a_downloadfiles" + (cntgrno + 1) + "'  style='color:blue;' onclick=\"return downloadfilesTerms('" + (cntgrno + 1) + "');\" ><u>" + docJson[i].filename + "</u></a> </td>";//,'" + (docJson[i].obj_val) + "'
                tableBody1 += "<td style='border: 1px solid #ADBBCA;' align='left' width='5%' valign='middle'><img id='del" + (cntgrno + 1) + "' alt='Click Here To Delete The Record.'  onclick=\"return deletefileterms(" + (cntgrno + 1) + ");\" src='/RFQassets/img/ico-delete.gif'  style='vertical-align:middle;'/></td></td>";
                tableBody1 += "</tr>";
                cntgrno++;
            }

        }
        var table = "<table class='table table-bordered' id='tbl_DocumentDtlTerms'>" +
            "<thead><tr class='bg-blue-chambray bg-font-blue-chambray'>" + tableHeader + "</tr></thead>" +
            "<tbody>" + tableBody1 + "</tbody>" +
            "</table>";
        $("#div_docterm").empty();
        $("#div_docterm").html(table);
        /////////////for General Terms RFQ//////////////////////////
        var tableBody1 = "";
        var cntgtno = 0;
        for (var i = 0; i < docJson.length; i++) {
            if (docJson[i].obj_val == 'RFQGeneralTerm') {
                tableBody1 += "<tr>";
                tableBody1 += "<td>" + docJson[i].obj_type + " </td>";
                tableBody1 += "<td><input class='hidden' type='text' name='txt_Region_Add" + (cntgtno + 1) + "' id='txt_Document_File" + (cntgtno + 1) + "' value=" + docJson[i].filename + " readonly ></input><a id='a_downloadfiles" + (cntgtno + 1) + "' style='color:blue;' onclick=\"return downloadfilesGenTerms('" + (cntgtno + 1) + "');\" ><u>" + docJson[i].filename + "</u></a> </td>";//,'" + (docJson[i].obj_val) + "'
                tableBody1 += "<td style='border: 1px solid #ADBBCA;' align='left' width='5%' valign='middle'><img id='del" + (cntgtno + 1) + "' alt='Click Here To Delete The Record.'  onclick=\"return deletefilegenterms(" + (cntgtno + 1) + ");\" src='/RFQassets/img/ico-delete.gif'  style='vertical-align:middle;'/></td></td>";
                tableBody1 += "</tr>";
                cntgtno++;
            }
        }
        var table = "<table class='table table-bordered' id='tbl_DocumentDtlGenTerms'>" +
            "<thead><tr class='bg-blue-chambray bg-font-blue-chambray'>" + tableHeader + "</tr></thead>" +
            "<tbody>" + tableBody1 + "</tbody>" +
            "</table>";
        $("#div_generalterms").empty();
        $("#div_generalterms").html(table);
        /////////////for Payment Terms RFQ//////////////////////////
        var tableBody1 = "";
        var cntptno = 0;
        for (var i = 0; i < docJson.length; i++) {
            if (docJson[i].obj_val == 'RFQPaymentTerm') {
                tableBody1 += "<tr>";
                tableBody1 += "<td>" + docJson[i].obj_type + " </td>";
                tableBody1 += "<td><input class='hidden' type='text' name='txt_Region_Add" + (cntptno + 1) + "' id='txt_Document_File" + (cntptno + 1) + "' value=" + docJson[i].filename + " readonly ></input><a id='a_downloadfiles" + (cntptno + 1) + "' style='color:blue;' onclick=\"return downloadfilesPayTerms('" + (cntptno + 1) + "');\" ><u>" + docJson[i].filename + "</u></a> </td>";//,'" + (docJson[i].obj_val) + "'
                tableBody1 += "<td style='border: 1px solid #ADBBCA;' align='left' width='5%' valign='middle'><img id='del" + (cntptno + 1) + "' alt='Click Here To Delete The Record.'  onclick=\"return deletefilepayterms(" + (cntptno + 1) + ");\" src='/RFQassets/img/ico-delete.gif'  style='vertical-align:middle;'/></td></td>";
                tableBody1 += "</tr>";
                cntptno++;
            }
        }
        var table = "<table class='table table-bordered' id='tbl_DocumentDtlPayTerms'>" +
            "<thead><tr class='bg-blue-chambray bg-font-blue-chambray'>" + tableHeader + "</tr></thead>" +
            "<tbody>" + tableBody1 + "</tbody>" +
            "</table>";
        $("#div_paymentterms").empty();
        $("#div_paymentterms").html(table);
    }
}
$(document).on('click', '.deleteLOTLINE1', function () {
    var RowNumber = $(this).closest("tr")[0].rowIndex;// $(this).closest("tr").index();
    delete_Row_LotLine(RowNumber);
});
$(document).on('click', '.deleteLOT', function () {
    var RowNumber = $(this).closest("tr")[0].rowIndex;// $(this).closest("tr").index();
    delete_Row_Lot(RowNumber);
});
$(document).on('click', '.deleteLINE', function () {
    var RowNumber = $(this).closest("tr")[0].rowIndex;// $(this).closest("tr").index();
    delete_Row_Line(RowNumber);
});
function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}
$(document).on('click', '.deleteREMIN', function () {
    var RowNumber = $(this).closest("tr")[0].rowIndex;// $(this).closest("tr").index();////
    delete_row_rem(RowNumber);
});
function viewdoclines(lotname, itname, pkrfqid) {
    if (docJson.length > 0) {
        var tableHeader7 = "";
        var tableBody7 = "";
        var cnsrno = 0;
        tableHeader7 += "<th>Description</th>";
        tableHeader7 += "<th>File Name</th>";
        tableHeader7 += "<th>Delete</th>";
        /////////////for Header RFQ//////////////////////////
        for (var i = 0; i < docJson.length; i++) {
            if (docJson[i].obj_val == 'RFQ' + lotname + '' + itname + '' + pkrfqid + '') {
                tableBody7 += "<tr>";
                tableBody7 += "<td>" + docJson[i].obj_type + " </td>";
                tableBody7 += "<td><input class='hidden' type='text' name='txt_Region_Add" + (cnsrno + 1) + "' id='txt_Document_File" + (cnsrno + 1) + "' value='" + docJson[i].filename + "'  readonly ></input><a id='a_downloadfiles" + (cnsrno + 1) + "' style='color:blue;' onclick=\"return downloadfilesLH(" + (cnsrno + 1) + ");\" ><u>" + docJson[i].filename + "</u></a> </td>";
                tableBody7 += "<td style='border: 1px solid #ADBBCA;' align='left' width='5%' valign='middle'><img id='del" + (cnsrno + 1) + "' alt='Click Here To Delete The Record.'  onclick=\"return deletefileline(" + (cnsrno + 1) + ");\" src='../../RFQassets/img/ico-delete.gif'  style='vertical-align:middle;'/></td></td>";//,'" + (docJson[i].obj_val) + "'
                tableBody7 += "</tr>";
                cnsrno++;
            }
        }
        if (tableBody7 != "") {
            var table7 = "<table class='table table-bordered' id='tbl_DocumentDtlLH'>" +
                "<thead><tr class='bg-blue-chambray bg-font-blue-chambray'>" + tableHeader7 + "</tr></thead>" +
                "<tbody>" + tableBody7 + "</tbody>" +
                "</table>";
            $("#div_viewdoc").empty();
            $("#div_viewdoc").html(table7);
        }
        else {
            $("#div_viewdoc").html('No Documents Uploaded');
        }
    }
    else {
        $("#div_viewdoc").html('No Documents Uploaded');
    }
}
function viewdoclots(lotname, itname, pkrfqid) {
    if (docJson.length > 0) {
        var tableHeader7 = "";
        var tableBody7 = "";
        var cnsno = 0;
        tableHeader7 += "<th>Description</th>";
        tableHeader7 += "<th>File Name</th>";
        tableHeader7 += "<th>Delete</th>";
        /////////////for Header RFQ//////////////////////////
        for (var i = 0; i < docJson.length; i++) {
            if (docJson[i].obj_val == 'RFQ' + lotname + '' + itname + '' + pkrfqid + '') {
                tableBody7 += "<tr>";
                tableBody7 += "<td>" + docJson[i].obj_type + " </td>";
                tableBody7 += "<td><input class='hidden' type='text' name='txt_Region_Add" + (cnsno + 1) + "' id='txt_Document_File" + (cnsno + 1) + "' value=" + docJson[i].filename + " readonly ></input><a id='a_downloadfiles" + (cnsno + 1) + "' style='color:blue;' onclick=\"return downloadfilesh(" + (cnsno + 1) + ");\" ><u>" + docJson[i].filename + "</u></a></td>";
                tableBody7 += "<td style='border: 1px solid #ADBBCA;' align='left' width='5%' valign='middle'><img id='del" + (cnsno + 1) + "' alt='Click Here To Delete The Record.'  onclick=\"return deletefilelot(" + (cnsno + 1) + ");\" src='../../RFQassets/img/ico-delete.gif'  style='vertical-align:middle;'/></td></td>";//,'" + (docJson[i].obj_val) + "'
                tableBody7 += "</tr>";
                cnsno++;
            }
        }
        if (tableBody7 != "") {
            var table7 = "<table class='table table-bordered' id='tbl_DocumentDtlh'>" +
                "<thead><tr class='bg-blue-chambray bg-font-blue-chambray'>" + tableHeader7 + "</tr></thead>" +
                "<tbody>" + tableBody7 + "</tbody>" +
                "</table>";
            $("#div_viewdoc").empty();
            $("#div_viewdoc").html(table7);
        }
        else {
            $("#div_viewdoc").html('No Documents Uploaded');
        }
    }
    else {
        $("#div_viewdoc").html('No Documents Uploaded');
    }
}
$("#btn_Back").click(function () {
    $("#dv_RfqDtl").show();
    $("#dv_Create").hide();
    $("#dvRule").hide();
    $("#dv_team").hide();
    $("#btn_publish").hide();
    $("#preview").hide();
    //getRFQDtl();
    clear();
});

$('body').on('click', '.new-rfq', function () {
    $("#dv_RfqDtl").hide();
    $("#dv_Create").show();
    $("#dvRule").hide();
    $("#dv_team").hide();
    $("#btn_publish").hide();
    $("#preview").hide();
    clear();
    $("#txt_Currency").val('Rs');
    $("#txt_pk_id").val('');
});

function clear() {
    $("#txt_RFQ_Desc").val('');
    $("#txt_pk_id").val('');
    $("#txt_Test_Rfq").val('');
    $("#txt_Dept").val('');
    $("#txt_Cat").val('');
    $("#txt_buyer").val('');
    $("#txt_branch").val('');
    $("#txt_region").val('');
    $("#txt_Delivery_loc").val('');
    $("#txt_Delivery_time").val('');
    $("#txt_Delivery_Date").val('');
    $("#txt_Start_Date").val('');
    $("#txt_Start_Time").val('');
    $("#txt_End_Date").val('');
    $("#txt_End_Time").val('');
    $("#txt_Currency").val('');
    $("#div_Supplier").val('');
    $("#dv_LineItem").val('');
    $("#dv_LotItem").val('');
    $("#dv_LItem").val('');
    $("#div_reminder").val('');
    $("#div_queries").val('');
    $('#tbl_reminder tbody > tr').remove();
    $('#tbl_query tbody > tr').remove();
    $('#tbl_supplier tbody > tr').remove();
    $('#tbl_LineItem tbody > tr').remove();
    $('#tbl_LotItem tbody > tr').remove();
    $('#tbl_LItem tbody > tr').remove(); str = "";
}
$(document).on('keydown', '[name=suppliername]', function () {
    $("#" + $(this).closest('tr').find('[name=contactname]').attr("id")).val("");
    $("#" + $(this).attr("id")).autocomplete({
        autoFocus: true,
        minLength: 3,
        source: supplierJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#" + $(this).attr("id")).val(ui.item.label);
            // $("#" + $(this).closest('tr').find('[name=contactname]').attr("id")).val(ui.item.EmailId);
            $("#" + $(this).closest('tr').find('[name=supplierid]').attr("id")).val(ui.item.id);
            fillcontact(ui.item.id);
            return false;
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $("#" + $(this).attr("id")).val("");
                $("#" + $(this).closest('tr').find('[name=contactname]').attr("id")).val("");
            }

        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
});

function getItem(name) {
    if (name == "Lot") {
        $("#dv_LineItem").hide();
        $("#dv_LotItem").show();
        $("#dv_LItem").hide();
        $("#Lot").removeAttr('checked');
        $('#Lot').attr('checked', false);
        $("#LineItem").removeAttr('checked');
        $('#LineItem').attr('checked', false);
        $('#LotItem').attr('checked', true);
    }
    else if (name == "Item") {
        $("#dv_LineItem").show();
        $("#dv_LotItem").hide();
        $("#dv_LItem").hide();
        $("#LotItem").removeAttr('checked');
        $('#LotItem').attr('checked', false);
        $("#LineItem").removeAttr('checked');
        $('#LineItem').attr('checked', false);
        $('#Lot').attr('checked', true);
    }
    else if (name == "Line") {
        $("#dv_LineItem").hide();
        $("#dv_LotItem").hide();
        $("#dv_LItem").show();
        $("#Lot").removeAttr('checked');
        $('#Lot').attr('checked', false);
        $("#LotItem").removeAttr('checked');
        $('#LotItem').attr('checked', false);
        $('#LineItem').attr('checked', true);
    }
}
function fillTermsDocument() {
    var DisplayData = "";
    DisplayData = "<table class='table color-bordered-table info-bordered-table' id='tbl_TermsDocumentDtl'><thead><tr class='grey'><th>Description</th><th>File Name</th><th>Delete</th></tr></thead>";
    DisplayData += "</table>";
    $("#div_docterm").html(DisplayData)

}
function fillLotDocument() {
    var DisplayData = "";
    DisplayData = "<table class='table color-bordered-table info-bordered-table' id='tbl_LotDocumentDtl'><thead><tr class='grey'><th>Description</th><th>File Name</th><th>Delete</th></tr></thead>";
    DisplayData += "</table>";
    $("#div_doclot").html(DisplayData)

}
function fillLineDocument() {
    var DisplayData = "";
    DisplayData = "<table class='table color-bordered-table info-bordered-table' id='tbl_LineDocumentDtl'><thead><tr class='grey'><th>Description</th><th>File Name</th><th>Delete</th></tr></thead>";
    DisplayData += "</table>";
    $("#div_docline").html(DisplayData)

}
function getRFQTermsDet(pkkid) {
    PageMethods.getRFQTermsDetails(pkkid, function (response) {
        rfqtermsJson = JSON.parse(response[0]);
        rfqsupplierJson = JSON.parse(response[1]);
        rfqlotlineJson = JSON.parse(response[2]);
        docJson = JSON.parse(response[3]);
        queryJSON = JSON.parse(response[5]);
        displayTermsRFQPre();
    }, function (error) {
        rfqtermsJson = [];
        alert(error);
    });
}
function displayTermsRFQPre() {
    //////////Display Terms & Conditions/////////////////
    var tableHeader = "";
    var tableBody = "";
    tableHeader += "<th style='display:none'></th>";
    tableHeader += "<th style='display:none'></th>";
    if (rfqtermsJson) {

        for (var i = 0; i < rfqtermsJson.length; i++) {
            var txt_name = rfqtermsJson[i].terms;
            if (txt_name == "") {
            } else {
                if ((i + 1) % 2 == 1) {
                    tableBody += "<fieldset class='form-group'>";
                }
                tableBody += "<label class='col-md-3'>" + rfqtermsJson[i].terms_type + "</label>";
                tableBody += "<label class='col-md-3'>" + txt_name + "</label>";
                if ((i + 1) % 2 != 1) {
                    tableBody += "</fieldset>";
                }
            }
        }
    }
    var table1 = "<table class='table table-bordered table-hover table-sm' id='tbl_PreviewTer'>" +
        "<thead><tr>" + tableHeader + "</tr></thead>" +
        "<tbody>" + tableBody + "</tbody>" +
        "</table>";
    $("#div_previewterms").empty();
    $("#div_previewterms").html(table1);
    /////////////////////////////////////////////
    var tableHeaderDoc = "";
    var tableBodyDoc = "";
    tableHeaderDoc += "<th>Description</th>";
    tableHeaderDoc += "<th>File Name</th>";
    tableHeaderDoc += "<th>Delete</th>";
    var tableBodyDoc = "";
    for (var i = 0; i < docJson.length; i++) {
        if (docJson[i].obj_val == 'RFQTerm') {
            tableBodyDoc += "<tr>";
            tableBodyDoc += "<td>" + docJson[i].obj_type + " </td>";
            tableBodyDoc += "<td><input class='hidden' type='text' name='txt_Region_Add" + (i + 1) + "' id='txt_Document_File" + (i + 1) + "' value=" + docJson[i].filename + " readonly ></input><a id='a_downloadfiles" + (i + 1) + "' style='color:blue;' onclick=\"return downloadfiles('" + (i + 1) + "','" + (docJson[i].obj_val) + "');\" ><u>" + docJson[i].filename + "</u></a> </td>";
            tableBodyDoc += "<td style='border: 1px solid #ADBBCA;' align='left' width='5%' valign='middle'><img id='del" + (i + 1) + "' alt='Click Here To Delete The Record.'  onclick=\"return deletefile(" + (i + 1) + ");\" src='../../RFQassets/img/ico-delete.gif'  style='vertical-align:middle;'/></td></td>";
            tableBodyDoc += "</tr>";
        }
    }
    var table = "<table class='table table-bordered' id='tbl_DocumentDtlTerms'>" +
        "<thead><tr>" + tableHeaderDoc + "</tr></thead>" +
        "<tbody>" + tableBodyDoc + "</tbody>" +
        "</table>";
    $("#div_docterm").empty();
    $("#div_docterm").html(table);
    ///////////////////////////////////////////k
    /////////////////////////////////////////////
    var tableHeaderDoc = "";
    var tableBodyDoc = "";
    tableHeaderDoc += "<th>Description</th>";
    tableHeaderDoc += "<th>File Name</th>";
    tableHeaderDoc += "<th>Delete</th>";
    var tableBodyDoc = "";
    for (var i = 0; i < docJson.length; i++) {
        if (docJson[i].obj_val == 'RFQ') {
            tableBodyDoc += "<tr>";
            tableBodyDoc += "<td>" + docJson[i].obj_type + " </td>";
            tableBodyDoc += "<td><input class='hidden' type='text' name='txt_Region_Add" + (i + 1) + "' id='txt_Document_File" + (i + 1) + "' value=" + docJson[i].filename + " readonly ></input><a id='a_downloadfiles" + (i + 1) + "' style='color:blue;' onclick=\"return downloadfiles('" + (i + 1) + "','" + (docJson[i].obj_val) + "');\" ><u>" + docJson[i].filename + "</u></a> </td>";
            tableBodyDoc += "<td style='border: 1px solid #ADBBCA;' align='left' width='5%' valign='middle'><img id='del" + (i + 1) + "' alt='Click Here To Delete The Record.'  onclick=\"return deletefile(" + (i + 1) + ");\" src='../../RFQassets/img/ico-delete.gif'  style='vertical-align:middle;'/></td></td>";
            tableBodyDoc += "</tr>";
        }
    }
    var table = "<table class='table table-bordered' id='tbl_DocumentDtlTerms'>" +
        "<thead><tr>" + tableHeaderDoc + "</tr></thead>" +
        "<tbody>" + tableBodyDoc + "</tbody>" +
        "</table>";
    $("#div_Doc").empty();
    $("#div_Doc").html(table);
    ///////////////////////////////////////////k
    ////////////////Display Supplier/////////////  
    var tableHeader2 = "";
    var tableBody2 = "";
    tableHeader2 += "<th nowrap>Supplier Name</th>";
    tableHeader2 += "<th nowrap>Main Contact</th>";
    if (rfqsupplierJson) {
        if (rfqsupplierJson.length > 0) {
            var lastrow1 = rfqsupplierJson.length;
            for (var j = 0; j < rfqsupplierJson.length; j++) {
                var suppname = rfqsupplierJson[j].sname;
                var contact = rfqsupplierJson[j].contact;

                tableBody2 += "<tr>";
                tableBody2 += "<td>" + capitalize(suppname) + "</td>";
                tableBody2 += "<td>" + contact + "</td>";
                tableBody2 += "</tr>";
                //$('#tbl_supplier').append(html);
            }
        }
    }
    var table2 = "<table class='table table-bordered table-hover table-sm' id='tbl_Preview'>" +
        "<thead><tr >" + tableHeader2 + "</tr></thead>" +
        "<tbody>" + tableBody2 + "</tbody>" +
        "</table>";
    $("#div_previewpodetails").empty();
    $("#div_previewpodetails").html(table2);
    ///////////////////display query info//////////////////
    var tableHeader2 = "";
    var tableBody2 = "";
    tableHeader2 += "<th nowrap>Query</th>";
    tableHeader2 += "<th nowrap>Description</th>";
    if (queryJSON) {
        if (queryJSON.length > 0) {
            var lastrow1 = queryJSON.length;
            for (var j = 0; j < queryJSON.length; j++) {
                var quer = queryJSON[j].query;
                var desc = queryJSON[j].desc;

                tableBody2 += "<tr>";
                tableBody2 += "<td>" + capitalize(quer) + "</td>";
                tableBody2 += "<td>" + capitalize(desc) + "</td>";
                tableBody2 += "</tr>";
            }
        }
    }
    var table2 = "<table class='table table-bordered table-hover table-sm' id='tbl_PreviewQu'>" +
        "<thead><tr >" + tableHeader2 + "</tr></thead>" +
        "<tbody>" + tableBody2 + "</tbody>" +
        "</table>";
    $("#div_prequeries").empty();
    $("#div_prequeries").html(table2);
    //////////////display lot  line data ///////////////
    var tableHeader1 = "";
    var tableBody1 = "";

    if (rfqlotlineJson) {
        var lastro = rfqlotlineJson.length;
        for (var k = 0; k < lastro; k++) {
            getItem(rfqlotlineJson[k].flag);
            if (rfqlotlineJson[k].flag == "Item") {
                if (k == 0) {
                    tableHeader1 += "<th nowrap>Lot Name</th>";
                    tableHeader1 += "<th nowrap>Lot Description</th>";
                    tableHeader1 += "<th nowrap>Commodity</th>";
                    tableHeader1 += "<th nowrap>Historic Price</th>";
                }
                tableBody1 += "<tr>";
                tableBody1 += "<td>" + capitalize(rfqlotlineJson[k].lname) + "</td>";
                tableBody1 += "<td>" + capitalize(rfqlotlineJson[k].ldesc) + "</td>";
                tableBody1 += "<td>" + rfqlotlineJson[k].commo + "</td>";
                tableBody1 += "<td  align='right'>" + rfqlotlineJson[k].histoprice + "</td>";
                tableBody1 += "</tr>";

            }
            else if (rfqlotlineJson[k].flag == "Lot") {
                if (k == 0) {
                    tableHeader1 += "<th nowrap>Lot Name</th>";
                    tableHeader1 += "<th nowrap>Lot Description</th>";
                    tableHeader1 += "<th nowrap>Commodity</th>";
                    tableHeader1 += "<th nowrap>Historic Price</th>";
                    tableHeader1 += "<th nowrap>Item Name</th>";
                    tableHeader1 += "<th nowrap>Quantity</th>";
                    tableHeader1 += "<th nowrap>Unit</th>";
                }
                tableBody1 += "<tr>";
                tableBody1 += "<td>" + capitalize(rfqlotlineJson[k].lname) + "</td>";
                tableBody1 += "<td>" + capitalize(rfqlotlineJson[k].ldesc) + "</td>";
                tableBody1 += "<td>" + rfqlotlineJson[k].commo + "</td>";
                tableBody1 += "<td  align='right'>" + rfqlotlineJson[k].histoprice + "</td>";
                tableBody1 += "<td>" + capitalize(rfqlotlineJson[k].itname) + "</td>";
                tableBody1 += "<td  align='right'>" + rfqlotlineJson[k].qty + "</td>";
                tableBody1 += "<td>" + rfqlotlineJson[k].unit + "</td>";
                tableBody1 += "</tr>";
            }
            else if (rfqlotlineJson[k].flag == "Line") {
                if (k == 0) {
                    tableHeader1 += "<th nowrap>Historic Price</th>";
                    tableHeader1 += "<th nowrap>Item Name</th>";
                    tableHeader1 += "<th nowrap>Quantity</th>";
                    tableHeader1 += "<th nowrap>Unit</th>";
                }
                tableBody1 += "<tr>";
                tableBody1 += "<td  align='right'>" + rfqlotlineJson[k].histoprice + "</td>";
                tableBody1 += "<td>" + capitalize(rfqlotlineJson[k].itname) + "</td>";
                tableBody1 += "<td  align='right'>" + rfqlotlineJson[k].qty + "</td>";
                tableBody1 += "<td>" + rfqlotlineJson[k].unit + "</td>";
                tableBody1 += "</tr>";
            }

        }
    }
    var table = "<table class='table table-bordered table-hover table-sm' id='tbl_PreviewLot'>" +
        "<thead><tr>" + tableHeader1 + "</tr></thead>" +
        "<tbody>" + tableBody1 + "</tbody>" +
        "</table>";
    $("#div_lotline").empty();
    $("#div_lotline").html(table);
}
function PreviewData(pid, statu) {
    if (pid == 0) {
        $("#li_vendor").hide();
        $("#li_item").hide();
        $("#btn_aucsave").hide();
        $("#li_preview").hide();
        $("#tabPreview").show();
        id = $("#txt_id").val();
    }
    else {
        $("#li_vendor").show();
        $("#li_item").show();
        $("#li_preview").show();
        id = pid;
    }
    str = $("#txt_StatusP" + id).val();
    var accept = $("#txt_accept1" + id).val();
    if (statu == "Publish" || statu == "AucPublish" || statu == "Closed") {
        if (statu == "AucPublish") {
            $("#btn_aucsave").hide();
        }
        else {
            $("#btn_aucsave").show();
        }
        $("#btn_publish").hide();
        pkid = $("#txt_Pkid1" + id).val();
        $("#txt_pk_id").val(pkid);
        getRFQTermsDet(pkid);

        $("#lbl_rfq").html($("#txt_RFQ_Desc1" + id).val());
        //$("#lbl_test").html($("#txt_test1" + id).val());
        $("#lbl_dept").html($("#txt_dept1" + id).val());
        $("#lbl_cat").text($("#txt_cat1" + id).val());
        $("#lbl_deliloc").text($("#txt_Delivery_loc1" + id).val());
        $("#lbl_delit").text($("#txt_Delivery_time1" + id).val());
        $("#lbl_expectd").text($("#txt_Delivery_Date1" + id).val());
        $("#lbl_branch").text($("#txt_Branch1" + id).val());
        $("#lbl_region").text($("#txt_region1" + id).val());
        $("#lbl_buyer").text($("#txt_buyer1" + id).val());
        $("#lbl_currency").text($("#txt_Curr1" + id).val());
        $("#lbl_status").text($("#txt_StatusP" + id).val());
        var start = $("#txt_Start_Date1" + id).val() + " " + $("#txt_Start_time1" + id).val();
        $("#lbl_start").text(start);
        var end = $("#txt_End_Date1" + id).val() + " " + $("#txt_End_time1" + id).val();
        $("#lbl_end").text(end);
    }
    else {
        ////Display RFQ basic info//////////////////
        $("#btn_publish").show();
        $("#lbl_rfq").text($("#txt_RFQ_Desc" + id).val());
        $("#lbl_dept").html($("#txt_dept" + id).val());
        $("#lbl_cat").text($("#txt_cat" + id).val());
        $("#lbl_deliloc").text($("#txt_Delivery_loc" + id).val());
        $("#lbl_delit").text($("#txt_Delivery_time" + id).val());
        $("#lbl_expectd").text($("#txt_Delivery_Date" + id).val());
        $("#lbl_branch").text($("#txt_Branch" + id).val());
        $("#lbl_region").text($("#txt_region" + id).val());
        $("#lbl_buyer").text($("#txt_buyer" + id).val());
        $("#lbl_currency").text($("#txt_Curr" + id).val());
        $("#lbl_status").text($("#txt_Status" + id).val());
        var start = $("#txt_Start_Date" + id).val() + " " + $("#txt_Start_time" + id).val();
        $("#lbl_start").text(start);
        var end = $("#txt_End_Date" + id).val() + " " + $("#txt_End_time" + id).val();
        $("#lbl_end").text(end);
    }
    ///////////////////display supplier info//////////////////
    var tableHeader2 = "";
    var tableBody2 = "";
    tableHeader2 += "<th nowrap>Supplier Name</th>";
    tableHeader2 += "<th nowrap>Main Contact</th>";
    if (statu == "Publish") {
    }
    else {
        var supp = $('table#tbl_supplier').find('tr').length;
        $("#txt_suppcnt").val(supp);
        if (supp != null) {
            for (var i = 1; i < supp; i++) {
                var suppname = $('#txt_Label' + (i)).val();;
                var contact = $('#txt_Contact' + (i)).val();

                tableBody2 += "<tr>";
                tableBody2 += "<td>" + suppname + "</td>";
                tableBody2 += "<td>" + contact + "</td>";
                tableBody2 += "</tr>";
            }
        }
    }
    var table2 = "<table class='table table-bordered table-hover table-sm' id='tbl_Preview'>" +
        "<thead><tr >" + tableHeader2 + "</tr></thead>" +
        "<tbody>" + tableBody2 + "</tbody>" +
        "</table>";
    $("#div_previewpodetails").empty();
    $("#div_previewpodetails").html(table2);
    ///////////////////display query info//////////////////
    var tableHeader2 = "";
    var tableBody2 = "";
    tableHeader2 += "<th nowrap>Query</th>";
    tableHeader2 += "<th nowrap>Description</th>";
    if (statu == "Publish") {
    }
    else {
        var supp = $('table#tbl_query').find('tr').length;
        if (supp != null) {
            for (var i = 1; i < supp; i++) {
                var quer = $('#txt_query' + (i)).val();;
                var desc = $('#txt_desc' + (i)).val();

                tableBody2 += "<tr>";
                tableBody2 += "<td>" + quer + "</td>";
                tableBody2 += "<td>" + desc + "</td>";
                tableBody2 += "</tr>";
            }
        }
    }
    var table2 = "<table class='table table-bordered table-hover table-sm' id='tbl_PreviewQ'>" +
        "<thead><tr >" + tableHeader2 + "</tr></thead>" +
        "<tbody>" + tableBody2 + "</tbody>" +
        "</table>";
    $("#div_prequeries").empty();
    $("#div_prequeries").html(table2);
    /////////////////display terms//////////////////
    var cntrems = 0;
    var tableBody = "";
    var tableHeader = "";
    tableHeader += "<th nowrap style='display:none'></th>";
    tableHeader += "<th nowrap style='display:none'></th>";
    if (statu == "Publish") {
    }
    else {
        var rowpay = paymentTermJson.length;
        for (var i = 0; i < paymentTermJson.length; i++) {
            var txt_name = paymentTermJson[i].Name;
            var terms = $('#txt_' + txt_name).val();
            if (terms == "") {

            } else {
                cntrems++;
                $("#txt_termscnt").val(1);
                if ((i + 1) % 2 == 1) {
                    tableBody += "<fieldset class='form-group'>";
                }
                tableBody += "<label class='col-md-3'>" + txt_name + "</label>";
                tableBody += "<label class='col-md-3'>" + terms + "</label>";
                if ((i + 1) % 2 != 1) {
                    tableBody += "</fieldset>";
                }
            }
            if (cntrems == 0) {
                $("#txt_termscnt").val(0);
            }
        }
    }
    var table1 = "<table class='table table-bordered table-hover table-sm' id='tbl_PreviewTer'>" +
        "<thead><tr>" + tableHeader + "</tr></thead>" +
        "<tbody>" + tableBody + "</tbody>" +
        "</table>";
    $("#div_previewterms").empty();
    $("#div_previewterms").html(table1);
    ///////////////////display lotline info//////////////////
    var tableHeader1 = "";
    var tableBody1 = "";

    if (statu == "Publish") {
    }
    else {
        if ($('#Lot').is(':checked')) {
            tableHeader1 += "<th nowrap>Lot Name</th>";
            tableHeader1 += "<th nowrap>Lot Description</th>";
            tableHeader1 += "<th nowrap>Commodity</th>";
            tableHeader1 += "<th nowrap>Historic Price</th>";
            var lastrow2 = $('#tbl_LineItem tr').length;
            $("#txt_itemcnt").val(lastrow2);
            var mttab = $('table#tbl_LineItem').find('tr');
            for (var i = 1; i < mttab.length; i++) {
                var lotn = $(mttab[i]).find('td:eq(1)').text();
                var lotde = $(mttab[i]).find('td:eq(2)').text();
                var comm = $(mttab[i]).find('td:eq(3)').text();
                var hprice = $(mttab[i]).find('td:eq(4)').text();
                var flag = "Item";

                tableBody1 += "<tr>";
                tableBody1 += "<td>" + capitalize(lotn) + "</td>";
                tableBody1 += "<td>" + capitalize(lotde) + "</td>";
                tableBody1 += "<td>" + comm + "</td>";
                tableBody1 += "<td  align='right'>" + hprice + "</td>";
                tableBody1 += "</tr>";
            }
        }
        else if ($('#LotItem').is(':checked')) {
            tableHeader1 += "<th nowrap>Lot Name</th>";
            tableHeader1 += "<th nowrap>Lot Description</th>";
            tableHeader1 += "<th nowrap>Commodity</th>";
            tableHeader1 += "<th nowrap>Historic Price</th>";
            tableHeader1 += "<th nowrap>Item Name</th>";
            tableHeader1 += "<th nowrap>Quantity</th>";
            tableHeader1 += "<th nowrap>Unit</th>";
            var lastrow2 = $('#tbl_LotItem tr').length;
            $("#txt_itemcnt").val(lastrow2);
            var mttab = $('table#tbl_LotItem').find('tr');
            for (var i = 1; i < mttab.length; i++) {
                var lot = $(mttab[i]).find('td:eq(1)').text();
                var lotd = $(mttab[i]).find('td:eq(2)').text();
                var comm = $(mttab[i]).find('td:eq(3)').text();
                var itnam = $(mttab[i]).find('td:eq(4)').text();
                var qtyy = $(mttab[i]).find('td:eq(5)').text();
                var unit = $(mttab[i]).find('td:eq(6)').text();
                var hprice = $(mttab[i]).find('td:eq(7)').text();
                var flag = "Lot";
                tableBody1 += "<tr>";
                tableBody1 += "<td>" + capitalize(lot) + "</td>";
                tableBody1 += "<td>" + capitalize(lotd) + "</td>";
                tableBody1 += "<td>" + comm + "</td>";
                tableBody1 += "<td  align='right'>" + hprice + "</td>";
                tableBody1 += "<td>" + capitalize(itnam) + "</td>";
                tableBody1 += "<td  align='right'>" + qtyy + "</td>";
                tableBody1 += "<td>" + unit + "</td>";
                tableBody1 += "</tr>";
            }
        }
        else if ($('#LineItem').is(':checked')) {
            tableHeader1 += "<th nowrap>Historic Price</th>";
            tableHeader1 += "<th nowrap>Item Name</th>";
            tableHeader1 += "<th nowrap>Quantity</th>";
            tableHeader1 += "<th nowrap>Unit</th>";
            var lastrow2 = $('#tbl_LItem tr').length;
            $("#txt_itemcnt").val(lastrow2);
            var mttab = $('table#tbl_LItem').find('tr');
            for (var i = 1; i < mttab.length; i++) {
                var itnam = $(mttab[i]).find('td:eq(1)').text();
                var qtyy = $(mttab[i]).find('td:eq(2)').text();
                var unit = $(mttab[i]).find('td:eq(3)').text();
                var hprice = $(mttab[i]).find('td:eq(4)').text();
                var flag = "Line";
                tableBody1 += "<tr>";
                tableBody1 += "<td  align='right'>" + hprice + "</td>";
                tableBody1 += "<td>" + capitalize(itnam) + "</td>";
                tableBody1 += "<td  align='right'>" + qtyy + "</td>";
                tableBody1 += "<td>" + unit + "</td>";
                tableBody1 += "</tr>";
            }
        }
        else {
            $("#txt_itemcnt").val(0);
        }
    }
    var table = "<table class='table table-bordered table-hover table-sm' id='tbl_PreviewLot'>" +
        "<thead><tr>" + tableHeader1 + "</tr></thead>" +
        "<tbody>" + tableBody1 + "</tbody>" +
        "</table>";
    $("#div_lotline").empty();
    $("#div_lotline").html(table);
}
function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}
////////////////////vendor tracking//////////////////////////
function ViewData(pid) {
    var str = $("#txt_StatusP" + pid).val();
    PreviewData(pid, str);
    pkid = $("#txt_Pkid1" + pid).val();
    $("#txt_pk_id").val(pkid);
    viewdata1(pkid);
    clearInterval(myvar);
    myvar = setInterval(function () { ViewData(id) }, 10000);
}
function viewdata1(pkid) {
    PageMethods.getRFQLotDetails(pkid, function (response) {
        lotviewJson = JSON.parse(response[0]);
        lotvJson = JSON.parse(response[1]);
        venJson = JSON.parse(response[2]);
        historyJson = JSON.parse(response[3]);
        vpriceJson = JSON.parse(response[4]);
        displayLotRFQView();
    }, function (error) {
        lotviewJson = [];
        alert(error);
    });
}
function displayLotRFQView() {
    var tableHeader1 = "";
    var tableBody1 = "";
    var totptax = 0;
    var totvp = 0;

    $("#div_vendorlotdetail").html('');
    $("#div_vendordetails").html('');
    $("#div_pricehist").html('');
    if (lotviewJson) {
        var lastro = lotviewJson.length;
        var lrow = lotvJson.length;
        for (var k = 0; k < lastro; k++) {
            totptax = 0;
            totvp = 0;
            if (k == 0) {
                tableHeader1 += "<th nowrap>Lot / Line Item</th>";
                if (lotviewJson[k].commo != "") {
                    tableHeader1 += "<th nowrap>Commodity</th>";
                }
                tableHeader1 += "<th nowrap>Historic Price</th>";
                tableHeader1 += "<th nowrap>Quantity</th>";
                tableHeader1 += "<th nowrap>Unit</th>";
                tableHeader1 += "<th nowrap>Tax</th>";
                tableHeader1 += "<th nowrap>Vendor Price</th>";
                tableHeader1 += "<th nowrap>Price With Tax</th>";
                tableHeader1 += "<th nowrap>Vendor</th>";
            }
            //for (var h = 0; h < lrow; h++) {
            //    if (lotviewJson[k].lname == lotvJson[h].lname) {
            //        if (lotvJson[h].tax == null) {
            //            lotvJson[h].tax = 0;
            //        }
            //        if (lotvJson[h].price_tax == null) {
            //            lotvJson[h].price_tax = 0;
            //        }
            //        totptax = parseFloat(totptax) + parseFloat(lotvJson[h].price_tax);
            //        totvp = parseFloat(totvp) + parseFloat(lotvJson[h].vprice);
            //        totptax = checkRound(roundVal(totptax));
            //        totvp = checkRound(roundVal(totvp));
            //    }
            //}
            tableBody1 += "<tr style='background-color:#F39C12'>";
            tableBody1 += "<td>" + lotviewJson[k].lname + "</td>";
            if (lotviewJson[k].commo != "") {
                tableBody1 += "<td>" + lotviewJson[k].commo + "</td>";
            }
            tableBody1 += "<td  align='right'></td>";
            tableBody1 += "<td></td>";
            tableBody1 += "<td></td>";
            tableBody1 += "<td></td>";
            tableBody1 += "<td></td>";
            tableBody1 += "<td></td>";
            tableBody1 += "<td></td>";
            tableBody1 += "</tr>";
            if (lotvJson.length > 0) {
                for (var j = 0; j < lrow; j++) {
                    if (lotviewJson[k].lname == lotvJson[j].lname) {
                        if (lotvJson[j].tax == null) {
                            lotvJson[j].tax = 0;
                        }
                        if (lotvJson[j].price_tax == null) {
                            lotvJson[j].price_tax = 0;
                        }
                        tableBody1 += "<tr>";
                        tableBody1 += "<td>" + lotvJson[j].itname + "</td>";
                        if (lotviewJson[k].commo != "") {
                            tableBody1 += "<td></td>";
                        }
                        tableBody1 += "<td  align='right'>" + lotvJson[j].histoprice + "</td>";
                        tableBody1 += "<td  align='right'>" + lotvJson[j].qty + "</td>";
                        tableBody1 += "<td>" + lotvJson[j].unit + "</td>";
                        tableBody1 += "<td align='right'>" + lotvJson[j].tax + "</td>";
                        tableBody1 += "<td align='right'>" + lotvJson[j].vprice + "</td>";
                        tableBody1 += "<td align='right'>" + lotvJson[j].price_tax + "</td>";
                        tableBody1 += "<td>" + lotvJson[j].suppname + "</td>";
                        tableBody1 += "</tr>";
                    }
                }
            }
        }
    }
    var table = "<table class='table table-bordered table-hover table-sm' id='tbl_viewLot'>" +
        "<thead><tr>" + tableHeader1 + "</tr></thead>" +
        "<tbody>" + tableBody1 + "</tbody>" +
        "</table>";
    $("#div_vendorlotdetail").empty();
    $("#div_vendorlotdetail").html(table);
    ////////////////////////////vendor tracking//////////////////////////////
    var tableHeader2 = "";
    var tableBody2 = "";
    var status = "";
    var statusgui = "";
    tableHeader2 += "<th nowrap>Vendor</th>";
    tableHeader2 += "<th style='width:100px'>Status</th>";
    tableHeader2 += "<th nowrap>Contact Person</th>";
    tableHeader2 += "<th nowrap>User Id/Mail Id</th>";
    tableHeader2 += "<th nowrap>T&C</th>";
    tableHeader2 += "<th nowrap>RFQ Publish Date</th>";

    if (venJson) {
        var lastro = venJson.length;

        for (var j = 0; j < lastro; j++) {
            tableBody2 += "<tr >";
            tableBody2 += "<td>" + venJson[j].sname + "</td>";
            statusgui = venJson[j].acceptgui;
            status = venJson[j].accept;
            if (statusgui == null) {
                statusgui = "0";
            }
            if (status == null) {
                status = "0";
            }
            if (vpriceJson) {
                var la = vpriceJson.length;
                var ss = 0;
                for (var l = 0; l < la; l++) {
                    if (venJson[j].supp_contact == vpriceJson[l].scontact) {
                        ss++;
                    }
                }
            }
            if (status == "1" && statusgui == "1" && ss != 0)
                tableBody2 += "<td nowarp><img src='../../RFQassets/img/green.png'/><img src='../../RFQassets/img/green.png' /><img src='../../RFQassets/img/green.png'  /></td>";
            else if (status == "0" && statusgui == "1")
                tableBody2 += "<td nowarp><img src='../../RFQassets/img/green.png' /><img src='../../RFQassets/img/yellow.png'  /><img src='../../RFQassets/img/grey.png'/></td>";
            else if ((statusgui == "0" || statusgui == null) && (status == "0" || status == null))
                tableBody2 += "<td nowarp><img src='../../RFQassets/img/grey.png'  /><img src='../../RFQassets/img/grey.png' /><img src='../../RFQassets/img/grey.png' /></td>";
            else if (statusgui == "2")
                tableBody2 += "<td nowarp><img src='../../RFQassets/img/red.png'  /><img src='../../RFQassets/img/grey.png' /><img src='../../RFQassets/img/grey.png' /></td>";

            else if (status == "1" && statusgui == "1" && ss == 0)
                tableBody2 += "<td nowarp><img src='../../RFQassets/img/green.png'/><img src='../../RFQassets/img/green.png' /><img src='../../RFQassets/img/yellow.png'  /></td>";

            else {
                tableBody2 += "<td></td>";
            }
            tableBody2 += "<td>" + venJson[j].contact + "</td>";
            tableBody2 += "<td>" + venJson[j].semail + "</td>";
            if (venJson[j].acceptgui == "1") {
                tableBody2 += "<td>Accepted</td>";
            }
            else if (venJson[j].acceptgui == "2") {
                tableBody2 += "<td>Declined</td>";
            }
            else {
                tableBody2 += "<td></td>";
            }
            tableBody2 += "<td>" + venJson[j].pdate + "</td>";
            tableBody2 += "</tr>";
        }
    }
    var table1 = "<table class='table table-bordered table-hover table-sm' id='tbl_vendortrck'>" +
        "<thead><tr>" + tableHeader2 + "</tr></thead>" +
        "<tbody>" + tableBody2 + "</tbody>" +
        "</table>";
    $("#div_vendordetails").empty();
    $("#div_vendordetails").html(table1);
    ////////////////////////////vendor history//////////////////////////////
    var tableHeader3 = "";
    var tableBody3 = "";

    if (historyJson) {
        var last = historyJson.length;
        for (var k = 0; k < last; k++) {
            if (k == 0) {
                tableHeader3 += "<th nowrap>Vendor Connection Status</th>";
                tableHeader3 += "<th nowrap>Participant</th>";
                if (historyJson[k].flag != 'Line') {
                    tableHeader3 += "<th nowrap>Lot Name</th>";
                    tableHeader3 += "<th nowrap>Description</th>";
                }
                tableHeader3 += "<th nowrap>Submitted Time</th>";
                tableHeader3 += "<th nowrap>Quantity</th>";
                tableHeader3 += "<th nowrap>Tax</th>";
                tableHeader3 += "<th nowrap>Vendor Price</th>";
                tableHeader3 += "<th nowrap>Price With Tax</th>";
            }
            if (historyJson[k].tax == null) {
                historyJson[k].tax = 0;
            }
            if (historyJson[k].price_tax == null) {
                historyJson[k].price_tax = 0;
            }
            tableBody3 += "<tr>";
            tableBody3 += "<td><img src='../../RFQassets/img/online.gif'></td>";
            tableBody3 += "<td align='center'>" + historyJson[k].suppname + "</td>";
            if (historyJson[k].flag != 'Line') {
                tableBody3 += "<td>" + historyJson[k].lname + "</td>";
                tableBody3 += "<td>" + historyJson[k].ldesc + "</td>";
            }
            tableBody3 += "<td>" + historyJson[k].subdate + "</td>";
            tableBody3 += "<td  align='right'>" + historyJson[k].qty + "</td>";
            tableBody3 += "<td  align='right'>" + historyJson[k].tax + "</td>";
            tableBody3 += "<td  align='right'>" + historyJson[k].vprice + "</td>";
            tableBody3 += "<td  align='right'>" + historyJson[k].price_tax + "</td>";
            tableBody3 += "</tr>";
        }
    }
    var table2 = "<table class='table table-bordered table-hover table-sm' id='tbl_history'>" +
        "<thead><tr>" + tableHeader3 + "</tr></thead>" +
        "<tbody>" + tableBody3 + "</tbody>" +
        "</table>";
    $("#div_pricehist").empty();
    $("#div_pricehist").html(table2);
}
function checkRound(intvalue) {
    var flg = 0;
    var ch;
    var v;
    var ReturnVal;
    var st = intvalue.toString();
    for (var index = 0; index < st.length; index++) {

        ch = st.charAt(index);
        if (ch == ".") {
            var ch1 = st.substring(st.indexOf(".") + 1, st.length);
            if (ch1.length > 2) {
                flg = 4;
            }
            if (ch1.length == 2) {
                flg = 3;
            }
            if (ch1.length == 1) {
                flg = 2;
            }
        }
    }

    if (flg == 0) {
        ReturnVal = st + ".00";
    }
    if (flg == 2) {
        ReturnVal = st + "0";
    }
    if (flg == 3) {
        ReturnVal = st;
    }
    if (flg == 4) {
        ReturnVal = roundVal(parseFloat(st)).toString();
    }
    if (ReturnVal == ".00") {
        ReturnVal = "";
    }
    return ReturnVal;
}
function roundVal(val) {
    var dec = 2;
    var result = Math.round(val * Math.pow(10, dec)) / Math.pow(10, dec);
    return result;
}
function publishBtn() {
    saveBtn('RFQ', 'Publish');
    var suppcnt = $("#txt_suppcnt").val();
    var itemcnt = $("#txt_itemcnt").val();
    var termcnt = $("#txt_termscnt").val();
    var sdate = $('#txt_Start_Date').val();
    var edate = $('#txt_End_Date').val();
    var start = sdate;
    var stard = start.substring(0, 2);
    var starm = start.substring(3, 6);
    var stary = start.substring(7, 11);

    var end = edate;
    var endd = end.substring(0, 2);
    var endm = end.substring(3, 6);
    var endy = end.substring(7, 11);
    var d1 = new Date(stary, getMonthFromString(starm) - 1, stard);
    var d2 = new Date(endy, getMonthFromString(endm) - 1, endd);
    if (d1 > d2) {
        return false;
    }
    pkid = $("#txt_pk_id").val();
    var rfqn = $('#txt_RFQ_Desc').val();
    var testpro = $('#txt_Test_Rfq').val();
    var dept = $('#txt_Dept').val();
    var catid = $('#txt_Cat').val();
    var devltime = $('#txt_Delivery_loc').val();
    var delidate = ""; $('#txt_Delivery_Date').val();
    var branch = $('#txt_branch').val();
    var sdate = $('#txt_Start_Date').val();
    var stime = $('#txt_Start_Time').val();
    var edate = $('#txt_End_Date').val();
    var etime = $('#txt_End_Time').val();
    var reg = $('#txt_region').val();
    var buyer = $('#txt_buyer').val();
    var curr = $('#txt_Currency').val();
    pkid = $("#txt_pk_id").val();
    if (rfqn == "") {
        return false;
    }
    if (testpro == "") {
        return false;
    }
    if (dept == "") {
        return false;
    }
    if (catid == "") {
        return false;
    }
    if (devltime == "") {
        return false;
    }
    if (branch == "") {
        return false;
    }
    if (sdate == "") {
        return false;
    }
    if (stime == "") {
        return false;
    }
    if (edate == "") {
        return false;
    }
    if (etime == "") {
        return false;
    }
    if (reg == "") {
        return false;
    }
    if (buyer == "") {
        return false;
    }
    if (curr == "") {
        return false;
    }
    var rowrem = $('#tbl_reminder tr').length;
    if (pkid != "") {
        for (var i = 1; i < rowrem; i++) {
            var count = $("#txt_Count" + (i)).val();
            var days = $("#txt_Days" + (i)).val();
            if (count == "") {
                return false;
            }
            if (days == "") {
                return false;
            }
        }
    }
    if (suppcnt < 2) {
        return false;
    }
    else if (itemcnt <= 1) {
        $.jAlert({
            'content': 'Please add Lot/Line item'
        });
        return false;
    }
    else if (termcnt == 0) {
        $.jAlert({
            'content': 'Please select one of the terms & conditions'
        });
        return false;
    }
    var rowpay = paymentTermJson.length;
    for (var i = 0; i < paymentTermJson.length; i++) {
        var txt_name = paymentTermJson[i].Name;
        var terms = $('#txt_' + txt_name).val();
        if (terms == "Other") {
            if (i == 0) {
                var lastr = $('#tbl_DocumentDtlGenTerms tr').length;
                if (lastr <= 1) {
                    return false;
                }
            }
            if (i == 1) {
                var lastrow1 = $('#tbl_DocumentDtlPayTerms tr').length;
                if (lastrow1 <= 1) {
                    return false;
                }
            }
        }
    }
    $("#loading-div-background").show();
    return true;
}
function aucpublishBtn() {
    $("#loading-div-background").show();
    return true;
}
