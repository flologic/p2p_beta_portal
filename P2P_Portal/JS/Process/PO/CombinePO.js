﻿
var catJson = "", subcatJson = "", itmJson = "", BrnchbilJson = "", signatoryJson;
var catdata = "", catid = "", catid1 = "", subcat = "", subcat1 = "", subcatdata = "", subcatid1 = "";
var supplierJson = "", SuplbranchJson = "", DtlJson = "", templateJson, tmpldtlJson, ItemJson, itemlabel, itemid;
var supplierlabel, supplierpk, Suplbranchid, Suplbranchlabel, contact_name, brnchbillabel, brnchbilid, signlabel, signid;
var taxJson = '', termsJson = '';
$(document).ready(function () {
    //loadDatePicker();
    $("#form1").trigger("reset");
    addmodalpopup();
    $("#loading-div-background").show();
    Client_ID = $("#txt_Client_ID").val();
    Master_API = $("#txt_Master_URL").val();
    P2P_API = $("#txt_P2P_Url").val();
    WFE_API = $("#txt_Wfe_Url").val();
    // getCategoryddl();
    getSuplddl();
    LoadSignatory();
    LoadTax();
    LoadPOTerms();
    LoadTemplates();
    //   LoadTemplateDetails();
});

$(document).on('focus', '.datepicker-rtl', function () {
    $(this).datepicker({
        dateFormat: 'dd-M-yy', autoclose: true, todayBtn: 'linked', maxDate: 0, //maybe you want something like this
        //showButtonPanel: true,
        onClose: function () {
            $('.datepicker-rtl').removeClass('hasDatepicker');
            // or 'destroy' or $('.datepicker').remove(); or $(this).datepick('remove');
        }
    });
});

function getCategoryddl() {
    PageMethods.getCatData(Client_ID, Master_API, function (response) {
        catJson = JSON.parse(JSON.parse(response[0]));
        assignCat(catJson);
    }, function (error) {
        alert(error);
    });
}


function assignCat(obj) {
    if (obj) {
        var tableHeader = "";
        var tableBody = "";

        tableHeader += "<tr>";
        tableHeader += "<th><input type='checkbox' id='chk_main_cat'></th>";
        tableHeader += "<th>Category</th>";
        tableHeader += "</tr>";

        $.each(obj, function (key, val) {
            tableBody += "<tr id=" + key + ">";
            tableBody += "<td> <input type='checkbox' id='chk_" + (key + 1) + "'></td>";
            tableBody += "<td>" + val.label + "</td>";
            tableBody += "</tr>";
        })
        var table = "<table class='display nowrap dataTable' id='tbl_catdtl' runat='server'>" +
            "<thead><tr>" + tableHeader + "</tr></thead>" +
            "<tbody>" + tableBody + "</tbody>" +
            "</table>";
        $("#div_cat").empty();
        $("#div_cat").html(table);

        var oTable = $('#tbl_catdtl').DataTable({ paging: false, bFilter: true, ordering: false, searching: true, dom: 't' });
        $('#txt_category_srch').keyup(function () {
            oTable.search($(this).val()).draw();
        })


    }

}

function getSuplddl() {
    PageMethods.getSuplData(Client_ID, Master_API, function (response) {
        supplierJson = JSON.parse(JSON.parse(response[0]));

        assignSupl();

    }, function (error) {
        alert(error);
    });
}

function assignSupl() {
    $('#sel_Supplier').autocomplete({
        autoFocus: true,
        minLength: 3,
        source: supplierJson,
        select: function (event, ui) {
            event.preventDefault();
            $('#sel_Supplier').val(ui.item.label);
            supplierlabel = ui.item.label;
            supplierpk = ui.item.id;
            getSuplDtlData(supplierpk);
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $.jAlert({
                    'content': 'Please Select Valid Supplier..!'
                });
                $(this).val("");
                supplierpk = 0;
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function getSuplDtlData(pkid) {
    PageMethods.getSuplDtlData(Client_ID, Master_API, pkid, function (response) {
        SuplbranchJson = JSON.parse(JSON.parse(response[0]));
        assignSuplDtl();
    }, function (error) {
        alert(error);
    });
}


function assignSuplDtl() {
    $('#sel_Branch').autocomplete({
        autoFocus: true,
        minLength: 0,
        source: SuplbranchJson,
        select: function (event, ui) {
            event.preventDefault();
            $('#sel_Branch').val(ui.item.label);
            Suplbranchlabel = ui.item.label;
            Suplbranchid = ui.item.id;
            $('#txt_contact_person').val(ui.item.CONTACT_PERSON);
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $.jAlert({
                    'content': 'Please Select Valid Supplier Branch..!'
                });
                $(this).val("");
                Suplbranchid = 0;
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}


function LoadSignatory() {
    PageMethods.GetEmployeeData(Client_ID, Master_API, function (response) {
        signatoryJson = JSON.parse(JSON.parse(response[0]));
        assignSignatory();

    }, function (error) {
    });
}

function assignSignatory() {
    $('#sel_authsign').autocomplete({
        autoFocus: true,
        minLength: 3,
        source: signatoryJson,
        select: function (event, ui) {
            event.preventDefault();
            $('#sel_authsign').val(ui.item.label);
            signlabel = ui.item.label;
            signid = ui.item.id;
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $.jAlert({
                    'content': 'Please Select Valid Auth Signitory..!'
                });
                $(this).val("");
                signid = 0;
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function LoadTax() {
    PageMethods.GetTaxData(Client_ID, Master_API, function (response) {
        taxJson = JSON.parse(JSON.parse(response[0]));
        displayTax();
    }, function (error) {
        //jAlert(error,"VALIDATION");
    });
}

function displayTax() {
    if (taxJson) {
        var tableHeader = "";
        var tableBody = "";

        tableHeader += "<th>#</th>";
        tableHeader += " <th>TAXES</th>";

        $.each(taxJson, function (key, val) {
            tableBody += "<tr id='tax" + (key + 1) + "'>";
            tableBody += "<td><input type='checkbox' id='chkTax" + (key + 1) + "'/><input type='text' id='pkTax" + (key + 1) + "' value='" + val.id + "' style='display:none'/></td>";
            tableBody += "<td>" + val.label + "</td>";

            tableBody += "</tr>";
        })
        var table = "<table class='display nowrap dataTable' id='tbl_Tax' runat='server'>" +
            "<thead style='background-color:gray'><tr>" + tableHeader + "</tr></thead>" +
            "<tbody>" + tableBody + "</tbody>" +
            "</table>";
        $("#div_tax").empty();
        $("#div_tax").html(table);
    }
}

function LoadPOTerms() {
    PageMethods.GetPOTermsData(Client_ID, Master_API, function (response) {
        termsJson = JSON.parse(JSON.parse(response[0]));
        displayPOTerms();
    }, function (error) {
        //jAlert(error,"VALIDATION");
    });
}

function displayPOTerms() {
    if (termsJson) {
        var tableHeader = "";
        var tableBody = "";

        tableHeader += "<th>#</th>";
        tableHeader += " <th>PO TERMS</th>";

        $.each(termsJson, function (key, val) {
            tableBody += "<tr id='terms" + (key + 1) + "'>";
            tableBody += "<td><input type='checkbox' id='chkTerms" + (key + 1) + "'/><input type='text' id='pkTerms" + (key + 1) + "' value='" + val.id + "' style='display:none'/></td>";
            tableBody += "<td>" + val.label + "</td>";

            tableBody += "</tr>";
        })
        var table = "<table class='display nowrap dataTable' id='tbl_Term' runat='server'>" +
            "<thead style='background-color:gray'><tr>" + tableHeader + "</tr></thead>" +
            "<tbody>" + tableBody + "</tbody>" +
            "</table>";
        $("#div_terms").empty();
        $("#div_terms").html(table);
    }
}

$(document).on('change', '#chk_main_cat', function () {
    clear_filter();
    var catid = "";
    if (this.checked) {
        $('#tbl_catdtl :checkbox').not(this).each(function () {
            this.checked = true;
            var index = $(this).closest("tr").attr("id");
            var fkcatid = catJson[index].id;
            if (catid == "")
                catid = fkcatid
            else
                catid = catid + "," + fkcatid;

            catdata = catid;
        });
    }
    else {

        $('#tbl_catdtl :checkbox').each(function () {
            this.checked = false;
            catdata = "";
        });
    }
    if (catid != "") {
        PageMethods.getSubCatData(Client_ID, Master_API, catid, function (response) {
            subcatJson = JSON.parse(JSON.parse(response[0]));
            displaySubCatDetail(subcatJson);
        }, function (error) {
            alert(error);
        });
    }
    else {
        displaySubCatDetail([]);
    }
});

$(document).on('change', '#tbl_catdtl >tbody :checkbox', function () {
    clear_filter();
    var catid1 = "";
    $('#tbl_catdtl >tbody :checkbox').each(function () {
        if (this.checked) {
            var index = $(this).closest("tr").attr("id");
            var fkcatid1 = catJson[index].id;
            if (catid1 == "")
                catid1 = fkcatid1
            else
                catid1 = catid1 + "," + fkcatid1;

            catdata = catid1;
        }

    });
    if (catid1 != "") {
        PageMethods.getSubCatData(Client_ID, Master_API, catid1, function (response) {
            subcatJson = JSON.parse(JSON.parse(response[0]));
            displaySubCatDetail(subcatJson);
        }, function (error) {
            alert(error);
        });
    }
    else {
        displaySubCatDetail([]);
    }
});

function displaySubCatDetail(obj) {
    if (obj) {
        var tableHeader = "";
        var tableBody = "";

        tableHeader += "<tr>";
        tableHeader += "<th><input type='checkbox' id='chk_main_subcat'></th>";
        tableHeader += "<th>Sub Category</th>";
        tableHeader += "</tr>";

        $.each(obj, function (key, val) {
            tableBody += "<tr id=" + key + ">";
            tableBody += "<td> <input type='checkbox' id='chk_" + (key + 1) + "'></td>";
            tableBody += "<td>" + val.label + "</td>";
            tableBody += "</tr>";
        })
        var table = "<table class='display nowrap dataTable' id='tbl_subcatdtl' runat='server'>" +
            "<thead><tr>" + tableHeader + "</tr></thead>" +
            "<tbody>" + tableBody + "</tbody>" +
            "</table>";
        $("#div_subcat").empty();
        $("#div_subcat").html(table);

        var oTable = $('#tbl_subcatdtl').DataTable({ paging: false, bFilter: true, ordering: false, searching: true, dom: 't' });
        $('#txt_subcat_srch').keyup(function () {
            oTable.search($(this).val()).draw();
        })

    }

}

$(document).on('change', '#chk_main_subcat', function () {
    clear_filter();
    var catid = "";
    if (this.checked) {
        $('#tbl_subcatdtl :checkbox').not(this).each(function () {
            this.checked = true;
            var index = $(this).closest("tr").attr("id");
            var fksubcatid1 = subcatJson[index].id;
            if (subcatid1 == "")
                subcatid1 = fksubcatid1
            else
                subcatid1 = subcatid1 + "," + fksubcatid1;

            subcatdata = subcatid1;
        });
    }
    else {

        $('#tbl_subcatdtl :checkbox').each(function () {
            this.checked = false;
            subcatdata = "";
        });
    }

});

$(document).on('change', '#tbl_subcatdtl >tbody :checkbox', function () {
    clear_filter();
    var catid1 = "";
    $('#tbl_subcatdtl >tbody :checkbox').each(function () {
        if (this.checked) {
            var index = $(this).closest("tr").attr("id");
            var fksubcatid1 = subcatJson[index].id;
            if (subcatid1 == "")
                subcatid1 = fksubcatid1
            else
                subcatid1 = subcatid1 + "," + fksubcatid1;

            subcatdata = subcatid1;
        }
    });

});

function clear_filter() {
    $('.dataTables_filter input').val('');
    $('#txt_category_srch,#txt_subcat_srch').val('');
    $('#txt_category_srch,#txt_subcat_srch').trigger('keyup');
}

$("#btn_Show").click(function () {
    $("#loading-div-background").show();

    if (supplierpk == "" || supplierpk == undefined || supplierpk == 0) {
        $.jAlert({
            'content': 'Please Select Supplier..!',
            'onClose': function () {
                $("#sel_Supplier").focus();
                $("#loading-div-background").hide();
            }
        });
        return false;
    }
    if (Suplbranchid == "" || Suplbranchid == undefined || Suplbranchid == 0) {
        $.jAlert({
            'content': 'Please Select Supplier Branch..!',
            'onClose': function () {
                $("#sel_Branch").focus();
                $("#loading-div-background").hide();
            }
        });
        return false;
    }


    PageMethods.getReqDetailData(Client_ID, Master_API, P2P_API, catdata, subcatdata, supplierpk, function (response) {
        DtlJson = JSON.parse(JSON.parse(response[0]));
        if (response[0] != "[]") {
            displayTblData(DtlJson);
            $("#div_Req_Details").show();
        }
        else {
            $("#loading-div-background").hide();
            $.jAlert({
                'content': 'Data Not Available',
                'onClose': function () {
                    $("#div_displayReqDtl").empty();
                }
            });
        }

    }, function (error) {
        alert(error);
    });

});

function displayTblData(obj) {
    if (obj) {
        var tableHeader = "";
        var tableBody = "";

        tableHeader += "<th><input class='selectall' type='checkbox' id='chk_main'></th>";
        tableHeader += "<th text-align: center'>#</th>";
        tableHeader += "<th>Request No.</th>";
        tableHeader += "<th>Branch</th>";
        tableHeader += "<th >Category</th>";
        tableHeader += "<th >Sub-Category</th>";
        tableHeader += "<th >Item</th>";
        tableHeader += "<th >Template</th>";
        tableHeader += "<th >Req.Qty</th>";
        tableHeader += "<th >Status</th>";
        // tableHeader += "<th >Qty</th>";
        tableHeader += "<th >Rate</th>";
        tableHeader += "<th >Amount</th>";
        //  tableHeader += "<th >Amount</th>";
        //  tableHeader += "<th >Item Type</th>";
        //  tableHeader += "<th >Multiple Asset</th>";
        $.each(DtlJson, function (key, val) {

            tableBody += "<tr id=" + (key + 1) + ">";
            tableBody += "<td><input class='reqcheck' type='checkbox' id='chkbox" + (key + 1) + "' /></td>"; //onclick='check()'
            tableBody += "<td name='srno'>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.Req_NO + "<input value='" + val.Req_NO + "' id='Req_no" + (key + 1) + "' class='form-control input-sm' style='display:none;' type='text' />";
            tableBody += "<td>" + val.BRANCH + "<input value='" + val.FK_BRANCH_ID + "' id='brnchid" + (key + 1) + "' class='form-control input-sm' style='display:none;' type='text' />";
            tableBody += "<input value='" + val.PK_REQ_DTL_ID + "' id='Req_Dtl_id" + (key + 1) + "' class='form-control input-sm' style='display:none;' type='text' />";
            tableBody += "<input value='" + val.FK_BRANCH_ID + "' id='Req_Branch_id" + (key + 1) + "' class='form-control input-sm' style='display:none;' type='text' />";
            tableBody += "<input value='" + val.BRANCH + "' id='Req_Branch" + (key + 1) + "' class='form-control input-sm' style='display:none;' type='text' /></td>";

            tableBody += "<td>" + val.ITMCATNAME + "<input value='" + val.FK_ITEM_CAT + "' id='Catid" + (key + 1) + "' class='form-control input-sm' style='display:none;' type='text' /></td>";
            tableBody += "<td>" + val.ITMSUBCATNAME + "<input value='" + val.FK_ITEM_SUB_CAT + "' id='Sub_Catid" + (key + 1) + "' class='form-control input-sm' style='display:none;' type='text'/></td>";
            tableBody += "<td>" + val.ITMNAME + "<input value='" + val.FK_ITEM_ID + "' id='Itemid" + (key + 1) + "' class='form-control input-sm' style='display:none;' type='text'/></td>";

            //tableBody += "<td><input value='' id='sel_Item" + (key + 1) + "' class='form-control input-sm' type='text' onclick='getItemOptions(" + (key + 1) + ", " + val.FK_ITEM_CAT + "," + val.FK_ITEM_SUB_CAT + ")' />";
            //tableBody += "<input value='' id='sel_Item_ID" + (key + 1) + "' class='form-control input-sm' style='display:none;' type='text' /></td>";
            //tableBody += "<td><select id='sel_Item" + (key + 1) + "' class='form-control input-sm' onchange='bindTemplate(" + (key + 1) + ",this.value)'>" + getItemOptions((key + 1), val.FK_ITEM_CAT, val.FK_ITEM_SUB_CAT) + "</select></td>";
            tableBody += "<td><div class='col-md-8'><select id='sel_Template" + (key + 1) + "' class='form-control input-sm'  onchange=bindTemplateDtl(" + (key + 1) + ",this.value)/><input id='hid_Template" + (key + 1) + "' type='hidden' class='form-control input-sm'/><input id='TMPL_DESC" + (key + 1) + "' type='hidden' class='form-control input-sm'/><input id='txt_warranty" + (key + 1) + "' type='hidden' value='0'/><input id='hid_Make" + (key + 1) + "' type='hidden'/><input id='hid_Model" + (key + 1) + "' type='hidden'/></div><div class='col-md-2'><button class='btn btn-facebook waves-effect btn-circle waves-light' type='button' id='edit_template" + (key + 1) + "' onclick='edit_template(" + (key + 1) + ")'><i class='fa fa-edit'></i></button></div></td>";

            tableBody += "<td>" + val.QUANTITY + "<input name='reqvalqty' value='" + val.QUANTITY + "' id='Quantity" + (key + 1) + "' class='form-control input-sm number' style='display:none' type='text'/></td>";
            tableBody += "<td><select id='sel_status" + (key + 1) + "' class='form-control input-sm')><option value=''>---Select One---</option><option value='Release From Stock'>Release From Stock</option><option value='Release For Procurment'>Release For Procurment</option><option value='Non-PO'>Non-PO</option></select></td>";
            // tableBody += "<td><input style='text-align: right' id='Qty" + (key + 1) + "' name='valQty' class='form-control input-sm' type='text' onkeypress='return isNumberKey(event)' onchange='CalTotal(" + (key + 1) + ")'  disabled='disabled' ></input></td>";
            tableBody += "<td><input style='text-align: right' id='Rate" + (key + 1) + "' name='valRate' class='form-control input-sm' type='text' onkeypress='return isNumberKey(event)' onchange='CalTotal(" + (key + 1) + ")'  disabled='disabled' ></input></td>";
            tableBody += "<input value='" + val.TOTATL_AMOUNT + "' id='Po_Line_Total" + (key + 1) + "' class='form-control input-sm number' style='display:none;' type='text'/></td>";
            tableBody += "<td><input style='text-align: right' class='form-control input-sm type='text' readonly id='Total" + (key + 1) + "'></input></td>";
            //   tableBody += "<td><select id='sel_Type" + (key + 1) + "' class='form-control input-sm' style='text-align:left; padding:2px' disabled ><option>C</option><option>R</option></select></td>";
            //if (val.MULTIPLE_ASSET == "1") {
            //    tableBody += "<td><input type='checkbox' id='chk_multiple_" + (key + 1) + "' /></td>";
            //}
            //else {
            //    tableBody += "<td><input type='checkbox' id='chk_multiple_" + (key + 1) + "' disabled/></td>";
            //}

            var table = "<table class='display nowrap dataTable' id='tbl_Data'>" +
                "<thead><tr>" + tableHeader + "</tr></thead>" +
                "<tbody>" + tableBody + "</tbody>" +
                "</table>";
            $("#div_displayReqDtl").empty();
            $("#div_displayReqDtl").html(table);
            //$.each(DtlJson, function (key, val) {
            //    //fillsubddl("#sel_Template" + (key + 1), templateJson, "FK_ITMID", val.FK_ITEM_ID, "");
            //    fillsubddl1("#sel_Template" + (key + 1), templateJson, "FK_ITMID", val.FK_ITEM_ID, "");
            //});
            //   $("#tbl_Data").dataTable();
            bindDropDownData();

        });
    }
    bind_Distribution();
    $("#loading-div-background").hide();
}

$(document).on('change', '#tbl_Data >tbody :checkbox', function () {
    //var index = $(this).closest("tr").attr("id");
    if (this.checked) {
        var index = $(this).closest("tr").attr("id");
        this.checked = true;
        $("#Rate" + index).prop('disabled', false);
        //$("#Qty" + index).prop('disabled', false);
        $("#Rate" + index).val('');
        // $("#Qty" + index).val('');
        $("#Total" + index).val('');

    }
    else {
        var index = $(this).closest("tr").attr("id");
        this.checked = false;
        $("#Rate" + index).prop('disabled', true);
        //  $("#Qty" + index).prop('disabled', true);

        $("#Rate" + index).val('');
        //  $("#Qty" + index).val('');

        $("#Total" + index).val('');

        CalTotal(index);
    }
    bind_Distribution();
});

$('body').on('click', '.selectall', function () {
    var table = $("#tbl_Data").dataTable();
    if ($(".selectall").is(':checked')) {
        $(".reqcheck", table.fnGetNodes()).each(function () {
            $(this).prop("checked", true);
            var index = $(this).closest("tr").attr("id");
            // $("#Rate" + index).removeAttr("readonly");
            $("#Rate" + index).prop('disabled', false);
            //   $("#Qty" + index).prop('disabled', false);

        });
    }
    else {
        $(".reqcheck", table.fnGetNodes()).each(function () {
            $(this).prop("checked", false);
            var index = $(this).closest("tr").attr("id");
            //$("#Rate" + index).attr("readonly", "readonly");
            $("#Rate" + index).prop('disabled', true);
            // $("#Qty" + index).prop('disabled', true);

        })
    }
});

$("#btn_Save").click(function () {
    $("#loading-div-background").show();

    var action = $("#ddlAction").val();
    if (action == "0") {
        $.jAlert({
            'content': 'Please Select Action..!',
            'onClose': function () {
                $('#ddlAction').focus();
                $("#loading-div-background").hide();
            }
        });
        return false;
    }
    else if (action == "Submit" || action == "Save As Draft") {
        var tableHeader = "";
        var tableBody = "";
        var HdrData = [];
        var DtlData = [];
        var DocData = [];
        var flag = false;
        var xmlTax = [];
        var xmlTerm = [];
        var xmlSCHDL = [];

        if (supplierpk == "" || supplierpk == undefined || supplierpk == 0) {
            $.jAlert({
                'content': 'Please Select Supplier..!',
                'onClose': function () {
                    $("#sel_Supplier").focus();
                    $("#loading-div-background").hide();
                }
            });
            return false;
        }
        if (Suplbranchid == "" || Suplbranchid == undefined || Suplbranchid == 0) {
            $.jAlert({
                'content': 'Please Select Supplier Branch..!',
                'onClose': function () {
                    $("#sel_Branch").focus();
                    $("#loading-div-background").hide();
                }
            });
            return false;
        }
        if (signid == "" || signid == undefined || signid == 0) {
            $.jAlert({
                'content': 'Please Select Auth.Signatory..!',
                'onClose': function () {
                    $("#sel_authsign").focus();
                    $("#loading-div-background").hide();
                }
            });
            return false;
        }

        var initID = '';
        initID = $("#SessionAdId").val();
        var HdrData1 = {

            "CREATED_BY": initID, "PO_DATE": $("#txt_Po_Date").val(), "FK_SUPPLIER_HDR_ID": supplierpk, "FK_BILL_LOC_ID": Suplbranchid, "PO_SUBJECT": $("#txt_Subject").val(),
            "CONTACT_PERSON": $("#txt_contact_person").val(), "FK_ATHOSIGN_ID": signid, "QUOTATION_NO": $("#txt_Q_No").val(), "QUATATION_DATE": $("#txt_Q_date").val(),
            "INST_CHRG": $("#inst_charges").val(), "DISC_CHRG": $("#discount").val(), "TRANS_CHRG": $("#trans_charges").val(), "TOTAL_AMOUNT": $("#total_amount").val(),
            "PO_LAND_COST": $("#landed_cost").val(), "OTHTRM": $("#txt_other_terms").val(), "PO_STATUS": "Pending For PO Approval"
        };
        HdrData.push(HdrData1);

        //var table = $('#tbl_Data').dataTable();
        var lastRow1 = $('#tbl_Data tr').length;
        var land_tot = 0;
        // $(".reqcheck", table.fnGetNodes()).each(function () {
        //   if (this.checked) {
        for (var i = 1; i <= lastRow1 - 1; i++) {

            var chk = ($("#chkbox" + (i) + "").is(':checked'));
            if (chk == true) {
                flag = true;
                itmflag = true;
                tmplflag = true;
                rateflag = true;

                if ($("#sel_Template" + i).val() == "0") {
                    $.jAlert({
                        'content': 'Please Select Template At Row ' + i + '..!',
                        'onClose': function () {
                            $("#loading-div-background").hide();
                        }
                    });
                    tmplflag = false;
                    return false;

                }
                else if ($("#sel_status" + (i)).val() == "") {
                    $.jAlert({
                        'content': 'Please Select Status At Row ' + (i) + '..!',
                        'onClose': function () {
                            $("#loading-div-background").hide();
                        }
                    });
                    rateflag = false;
                    return false;

                }
                else if ($("#Rate" + (i)).val() == "") {
                    $.jAlert({
                        'content': 'Please Enter Rate At Row ' + (i) + '..!',
                        'onClose': function () {
                            $("#loading-div-background").hide();
                        }
                    });
                    rateflag = false;
                    return false;

                }
                else {
                    var reqno = DtlJson[(i - 1)].Req_NO;
                    var dtlid = DtlJson[(i - 1)].PK_REQ_DTL_ID;
                    var dtlbranchid = DtlJson[(i - 1)].FK_BRANCH_ID;
                    var catid = DtlJson[(i - 1)].FK_ITEM_CAT;
                    var subcatid = DtlJson[(i - 1)].FK_ITEM_SUB_CAT;
                    //var itemid = $("#sel_Item_ID" + (i)).val();
                    var itemid = DtlJson[(i - 1)].FK_ITEM_ID;
                    var quantity = DtlJson[(i - 1)].QUANTITY;
                    var brate = DtlJson[(i - 1)].BASE_RATE;
                    var potot = DtlJson[(i - 1)].TOTATL_AMOUNT;
                    var rate = $("#Rate" + (i)).val();
                    var tot = $("#Total" + (i)).val();
                    var templid = $("#sel_Template" + (i)).val();
                    var tmpl_desc = $("#TMPL_DESC" + (i)).val();
                    var status = $("#sel_status" + (i)).val();
                    // var itmtype = $("#sel_Type" + (i)).val();
                    //var multiple_asset = "0";

                    //if ($("#chk_multiple_" + index).prop("checked")) {
                    //    multiple_asset = "1";
                    //}

                    detail = {
                        "IND_REQ_NO": reqno, "FK_CAT_ID": catid, "FK_SUBCAT_ID": subcatid, "FK_ITEM_ID": itemid, "PO_ITEM_QTY": quantity, "PO_ITEM_RATE": rate, "PO_TOT_AMT": tot,
                        "FK_BRANCH_ID": dtlbranchid, "FK_REQ_DTL_ID": dtlid, "FK_TMPL_ID": 1, "TMPL_PO_DESC": tmpl_desc, "STATUS": status
                    }
                    DtlData.push(detail);
                }
            }
        }
        if (!flag) {
            $.jAlert({
                'content': 'Please Select Atleast One Record..!',
                'onClose': function () {
                    $("#loading-div-background").hide();
                }
            });
            return false;
        }
        else if (tmplflag != false && rateflag != false && itmflag != false) {

            var taxrow = $('#tbl_Tax tr').length;
            for (var q = 0; q < taxrow - 1; q++) {
                //chkTax
                if ($("#chkTax" + (q + 1)).prop("checked")) {
                    var pk_tax = $("#pkTax" + (q + 1)).val();
                    var taxObj = {
                        "TAXTYPEID": pk_tax
                    };
                    xmlTax.push(taxObj);
                }
            }

            var termrow = $('#tbl_Term tr').length;
            for (var q = 0; q < termrow - 1; q++) {
                //chkTax
                if ($("#chkTerms" + (q + 1)).prop("checked")) {
                    var pk_term = $("#pkTerms" + (q + 1)).val();
                    var termObj = {
                        "PK_TERM_ID": pk_term
                    };
                    xmlTerm.push(termObj);
                }
            }


            var rows = $('#tblDist tr').length;

            for (var index = 1; index <= rows - 1; index++) {

                var hid_branch = $("#hid_Branch_" + index).val();
                var hid_Qty = $("#dist_qty_" + index).val();
                var hid_itmid = $("#hid_Item_" + index).val();

                var cont_person = $("#contact_person_" + index).val();
                var cont_details = $("#contact_detail_" + index).val();

                if (hid_itmid == "0" || hid_itmid == undefined || hid_itmid == "") {
                    hid_itmid = 0;
                }
                if (hid_branch == "0" || hid_branch == undefined || hid_branch == "") {
                    hid_branch = 0;
                }

                if (hid_Qty == "0" || hid_Qty == undefined || hid_Qty == "") {
                    hid_Qty = 0;
                }

                var dtlObj = {
                    "FK_PWR_DTL_ID": '0', "FK_ITEM_ID": hid_itmid, "LOC_ID": hid_branch, "PO_ITEM_QTY": hid_Qty, "POSRNO": '$REQUEST_NO$', "CONTACT_PERSON": cont_person, "CONTACT_DETAILS": cont_details
                };

                xmlSCHDL.push(dtlObj);
            }
            var lastRow1 = $('#uploadTable tr').length;
            if (lastRow1 > 1) {
                for (var l = 0; l < lastRow1 - 1; l++) {
                    var firstCol = $("#uploadTable tr")[l + 1].cells[0].innerText;
                    var SecondCol = $("#uploadTable tr")[l + 1].cells[1].innerText;
                    var DocData1 = {
                        "OBJECT_TYPE": "PO Creation", "OBJECT_VALUE": "0", "DOCUMENT_TYPE": firstCol, "FILENAME": SecondCol
                    }
                    DocData.push(DocData1);
                }
            }

            var transactionData = { "hdrData": HdrData, "dtlData": DtlData, "docData": DocData, "TaxData": xmlTax, "TermData": xmlTerm, "SchdlData": xmlSCHDL } //, "SchdlData": xmlSCHDL
            PageMethods.SaveData(JSON.stringify(transactionData), initID, $('#EmailID').val(), action, Master_API, P2P_API, WFE_API, function (response) {
                if (response != "Error occured while saving data") {
                    if (response != "Approver Not Found") {
                        var saveMsg = "Request " + action + " Successfully.Request Number is " + response;
                        $.jAlert({
                            'content': saveMsg,
                            'onClose': function () {

                                $("#loading-div-background").hide();

                                location.href = "../../HomePage.aspx";
                            }
                        });
                    }
                    else {
                        $.jAlert({
                            'content': response,
                            'onClose': function () {
                                $("#loading-div-background").hide();
                            }
                        });
                        return false;
                    }
                }
                else {
                    $.jAlert({
                        'content': response,
                        'onClose': function () {
                            $("#loading-div-background").hide();
                        }
                    });
                    return false;
                }
            });

        }
    }
});

function bind_Distribution() {
    var tableHeader = "";
    var tableBody = "";
    var distflg = false;
    tableHeader += "<th align='center'>SR.No</th>";
    tableHeader += "<th align='center'>Branch Name</th>";
    tableHeader += "<th align='center'>Address</th>";
    tableHeader += "<th align='center'>Item Name</th>";
    tableHeader += "<th align='center'>Quantity</th>";
    tableHeader += "<th align='center'>Contact Person</th>";
    tableHeader += "<th align='center'>Contact Details</th>";

    // var table = $('#tbl_Data').dataTable();
    var lastRow1 = $('#tbl_Data tr').length;
    var cnt = 0;
    //$(".reqcheck", table.fnGetNodes()).each(function () {
    //    if (this.checked) {
    for (var i = 1; i <= lastRow1 - 1; i++) {

        var chk = ($("#chkbox" + (i) + "").is(':checked'));
        if (chk == true) {
            distflg = true;
            cnt++;

            var quantity = DtlJson[(i - 1)].QUANTITY;
            var branch = DtlJson[(i - 1)].BRANCH;
            var itm = DtlJson[(i - 1)].ITMNAME;
            var itmid = DtlJson[(i - 1)].FK_ITEM_ID;

            tableBody += "<tr>";
            tableBody += "<td>" + cnt + "</td>";
            tableBody += "<td>" + branch + "<input id='hid_Branch_" + cnt + "' type='hidden'  value='" + DtlJson[(i - 1)].FK_BRANCH_ID + "'/></td>";
            tableBody += "<td>" + DtlJson[(i - 1)].ADDRESS + "</td>";
            tableBody += "<td>" + itm + "<input id='hid_Item_" + cnt + "' type='hidden'  value='" + itmid + "'/></td>";
            tableBody += "<td>" + quantity + "<input id='dist_qty_" + cnt + "' type='hidden'  value='" + DtlJson[(i - 1)].QUANTITY + "'/></td>";
            tableBody += "<td><input id='contact_person_" + cnt + "' class='form-control input-sm' type='text'  value=''/></td>";
            tableBody += "<td><input id='contact_detail_" + cnt + "' class='form-control input-sm' type='text'  value=''/></td>";
            tableBody += "</tr>";
        }
        //});
    }

    if (distflg == true) {
        var table = "<table ID='tblDist' class='display nowrap dataTable'>" +
            "<thead><tr class='grey'>" + tableHeader + "</tr></thead>" +
            "<tbody>" + tableBody + "</tbody>" +
            "</table>";
        $("#Div_Distr").empty();
        $("#Div_Distr").html(table);
    }
    else {
        $("#Div_Distribution").modal('hide');
        $("#Div_Distribution").hide();
        $("#Div_Distr").empty();
        //$.jAlert({
        //    'content': 'Please select Any One Item',
        //    'onClose': function () {

        //    }
        //});
    }
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function CalTotal(index) {
    var land_tot = 0;
    var tot = 0;
    var lastRow1 = $('#tbl_Data tr').length;
    //var table = $("#tbl_Data").dataTable();
    //  $(".reqcheck", table.fnGetNodes()).each(function () {
    for (var i = 1; i <= lastRow1 - 1; i++) {
        //if (this.checked) {
        // var index = $(this).closest("tr").attr("id");
        var chk = ($("#chkbox" + (i) + "").is(':checked'));
        if (chk == true) {
            var Qty = $("#Quantity" + i).val();//$(this).closest('tr').find('[name=valqty]').val();
            var Rate = $("#Rate" + i).val();//$(this).closest('tr').find('[name=valRate]').val();//
            if (Rate != "") {
                tot = parseFloat(Qty) * parseFloat(Rate);
                $("#Total" + i).val(tot.toFixed(2));
                land_tot = parseFloat(land_tot) + parseFloat(tot);
                $("#total_amount").val(land_tot.toFixed(2));
                $("#landed_cost").val(land_tot.toFixed(2));
                calculateAmount(i);
            }
        }
        //}
        //}
        //});
    }
}


function calculateAmount(index) {
    //var table = $('#tbl_Data').dataTable();
    var land_tot = 0;
    //$(".reqcheck", table.fnGetNodes()).each(function () {

    //    if (this.checked) {
    //        flag = true;
    //        var index = $(this).closest("tr").attr("id");
    //        var rate = $("#Rate" + index).val();
    //        land_tot = parseInt(land_tot) + parseInt(rate);
    //        $("#total_amount").val(land_tot).toFixed(2);

    //    }
    //});
    if ($("#total_amount").val() == "" || $("#total_amount").val() == undefined || isNaN($("#total_amount").val())) {
        $("#total_amount").val("0.00");
    }
    else {
        total_amount = $("#total_amount").val();
    }
    if ($("#inst_charges").val() == "" || $("#inst_charges").val() == undefined || isNaN($("#inst_charges").val())) {
        $("#inst_charges").val("0.00");
        inst_charges = 0;
    }
    else {
        inst_charges = $("#inst_charges").val();
    }
    if ($("#trans_charges").val() == "" || $("#trans_charges").val() == undefined || isNaN($("#trans_charges").val())) {
        $("#trans_charges").val("0.00");
        trans_charges = 0;
    }
    else {
        trans_charges = $("#trans_charges").val();
    }
    if ($("#discount").val() == "" || $("#discount").val() == undefined || isNaN($("#discount").val())) {
        $("#discount").val("0.00");
        discount = 0;
    }
    else {
        discount = $("#discount").val();
    }

    landed_cost = parseFloat(total_amount) + parseFloat(inst_charges) + parseFloat(trans_charges) - parseFloat(discount);
    $("#landed_cost").val(landed_cost.toFixed(2));
}

function LoadTemplates() {
    PageMethods.gettemplateData(Client_ID, Master_API, function (response) {
        templateJson = JSON.parse(JSON.parse(response[0]));
        $("#loading-div-background").hide();
    }, function (error) {
        //jAlert(error, "VALIDATION");
    });
}

function LoadTemplateDetails() {
    PageMethods.gettemplatedtlData(function (response) {
        tmpldtlJson = JSON.parse(response);
        $("#loading-div-background").hide();
    }, function (error) {
        // jAlert(error, "VALIDATION");
    });

}

function bindDropDownData() {
    var rows = $("#tbl_Data tr").length;
    for (var index = 1; index <= rows - 1; index++) {
        var itid = $("#Itemid" + index).val(5);
        fillsubddl("#sel_Template" + index, templateJson, "FK_ITMID", 5, "0");
    }
}

function bindTemplateDtl(index, line_tmpl) {

    var rowIndex = $('#tbl_Data tr').length;
    for (var q = 1; q <= rowIndex - 1; q++) {
        var checkID = $("#chkbox" + q).is(':checked');
        if (line_tmpl == $("#hid_Template" + q).val() && q != index) {
            $("#hid_Template" + index).val(0);
            $("#sel_Template" + index).val("");
            $.jAlert({
                'content': 'Please Select Other Template..!',
                'onClose': function () {

                }
            });
            // jAlert("Please Select Other Template", "Validation");
            //return false;
        }
        if (checkID == true) {
            for (var i = 0; i < templateJson.length; i++) {
                $('#TMPL_DESC' + q).val(templateJson[(i)].TMPLPODESC);
            }
        }

    }

    var object1 = {}
    object1[String("FK_TMPLHDRID")] = parseInt(line_tmpl);
    var filteredChildList = _.where(tmpldtlJson, object1);

    if (filteredChildList.length > 0) {
        for (var i = 0; i < filteredChildList.length; i++) {
            if (filteredChildList[i].PRTNAME == "WARRANTY(IN MONTHS)") {
                $('#txt_warranty' + index).val(filteredChildList[i].TMPLVAL);
            }
            if (filteredChildList[i].PRTNAME == "MAKE") {
                $('#hid_Make' + index).val(filteredChildList[i].MAKENAME);
            }
            if (filteredChildList[i].PRTNAME == "MODEL") {
                $('#hid_Model' + index).val(filteredChildList[i].MODELNAME);
            }
        }
    }
}

function edit_template(index) {
    $("#modal_Template").modal('show');
    $("#edit_template_id").val(index);
    $("#edit_template_data").val($('#TMPL_DESC' + index).val());
}

function updateTemplate() {
    if ($("#edit_template_id").val() != "" && $("#edit_template_id").val() != null && $("#edit_template_id").val() != undefined) {
        var row_index = $('#edit_template_id').val();
        $("#TMPL_DESC" + row_index).val($('#edit_template_data').val());
    }
    $("#modal_Template").modal('hide');
}

function getItemOptions(index, catID, subcatID) {
    PageMethods.GetItem(parseInt(catID), parseInt(subcatID), function (response) {
        ItemJson = JSON.parse(response[0]);
        BindItem(index);

    },
        function (error) {
        });
}

function BindItem(index) {
    //fillsubddl1("#sel_Item" + index, ItemJson, "", "", "");
    $('#sel_Item' + index).autocomplete({
        autoFocus: true,
        minLength: 0,
        source: ItemJson,
        select: function (event, ui) {
            event.preventDefault();
            $('#sel_Item' + index).val(ui.item.label);
            $('#sel_Item_ID' + index).val(ui.item.id);
            itemlabel = ui.item.label;
            itemid = ui.item.id;
            bindTemplate(index, itemid);
            bind_Distribution();
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $.jAlert({
                    'content': 'Please Select Valid Item..!'
                });
                $(this).val("");
                itemid = 0;
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function bindTemplate(index, item) {
    $.each(DtlJson, function (key, val) {
        //fillsubddl("#sel_Template" + (key + 1), templateJson, "FK_ITMID", val.FK_ITEM_ID, "");
        fillsubddl1("#sel_Template" + index, templateJson, "FK_ITMID", item, "");
    });
}




