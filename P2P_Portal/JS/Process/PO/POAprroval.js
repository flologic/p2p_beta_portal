﻿var DtlJson = [], AuditJson = [], DocumentJSON = [], pono = "", POTAXJson = [], POTERMJson = [], requestIni_Email, requestInitiator;

$(document).ready(function () {
    //loadDatePicker();
    $("#form1").trigger("reset");
    addmodalpopup();
    $("#loading-div-background").show();
    Client_ID = $("#txt_Client_ID").val();
    Master_API = $("#txt_Master_Url").val();
    P2P_API = $("#txt_P2P_Url").val();
    WFE_API = $("#txt_Wfe_Url").val();
    getPODetails();
    LoadAuditData();

});

function getPODetails() {

    PageMethods.GetPODetails($('#txt_ProcessID').val(), $('#txt_InstanceID').val(), Master_API, P2P_API, WFE_API, function (response) {
        DtlJson = JSON.parse(JSON.parse(response[0]));
        if (response[0] != "[]") {
            displayTblData();
            $("#div_displayPODtl").show();
        }
        else {
            $("#loading-div-background").hide();
            $.jAlert({
                'content': 'Data Not Available',
                'onClose': function () {
                    $("#div_displayPODtl").empty();
                }
            });
        }

    }, function (error) {
        alert(error);
    });

}

function displayTblData() {
    if (DtlJson) {

        $("#lbl_PoNo").text(DtlJson[0].PO_NO);
        pono = DtlJson[0].PO_NO;
        $("#lblpodate").text(DtlJson[0].PO_DATE);
        $("#lblsuplname").text(DtlJson[0].NAME);
        $("#lbl_Branch").text(DtlJson[0].CITY);
        $("#lbl_contperson").text(DtlJson[0].CONTACT_PERSON);
        $("#lbl_AuthSign").text(DtlJson[0].AUTHSIGNNAME);
        $("#lbl_quatno").text(DtlJson[0].QUOTATION_NO);
        $("#lbl_quatDate").text(DtlJson[0].QUATATION_DATE);
        $("#lbl_subject").text(DtlJson[0].PO_SUBJECT);
        $("#txt_other_terms").val(DtlJson[0].OTHTRM);

        $("#inst_charges").val(DtlJson[0].INST_CHRG);
        $("#trans_charges").val(DtlJson[0].DISC_CHRG);
        $("#discount").val(DtlJson[0].TRANS_CHRG);
        $("#total_amount").val(DtlJson[0].TOTAT_AMOUNT);
        $("#landed_cost").val(DtlJson[0].PO_LAND_COST);

        requestIni_Email = DtlJson[0].CREATEDEMAIL;
        requestInitiator = DtlJson[0].CREATED_BY;


        var tableHeader = "";
        var tableBody = "";
        tableHeader += "<th text-align: center'>#</th>";
        tableHeader += "<th>Request No.</th>";
        tableHeader += "<th>Branch</th>";
        tableHeader += "<th >Category</th>";
        tableHeader += "<th >Sub-Category</th>";
        tableHeader += "<th >Item</th>";
        tableHeader += "<th >Template</th>";
        tableHeader += "<th >Req.Qty</th>";
        tableHeader += "<th >Status</th>";
        tableHeader += "<th >Rate</th>";
        tableHeader += "<th >Amount</th>";
        $.each(DtlJson, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.Req_NO + "</td>";
            tableBody += "<td>" + val.BRANCH + "</td>";
            tableBody += "<td>" + val.ITMCATNAME + "</td>";
            tableBody += "<td>" + val.ITMSUBCATNAME + "</td>";
            tableBody += "<td>" + val.ITMNAME + "</td>";
            tableBody += "<td>" + val.TMPL_NAME + "</td>";
            tableBody += "<td>" + val.QUANTITY + "</td>";
            tableBody += "<td>" + val.STATUS + "</td>";
            tableBody += "<td>" + val.PO_ITEM_BASE_RATE + "</td>";
            tableBody += "<td>" + val.PO_TOT_AMT + "</td>";
            tableBody += "</tr>";
        });
        var table = "<table ID='tblDtlData' class='display nowrap dataTable'>" +
            "<thead><tr class='grey'>" + tableHeader + "</tr></thead>" +
            "<tbody>" + tableBody + "</tbody>" +
            "</table>";
        $("#div_displayPODtl").empty();
        $("#div_displayPODtl").html(table);
        getPOTAXData();
        getPOTERMData();

    }
}

function LoadAuditData() {
    PageMethods.getAuditData($("#txt_ProcessID").val(), $("#txt_InstanceID").val(), Master_API, P2P_API, WFE_API, function (response) {
        AuditJson = JSON.parse(JSON.parse(response[0]));
        displayAuditData();
    },
        function (error) {
        });
}

function displayAuditData() {
    if (AuditJson) {
        var tableHeader = "";
        var tableBody = "";

        tableHeader += "<th align='center'>SR.No</th>";
        tableHeader += "<th align='center'>Step Name</th>";
        tableHeader += "<th align='center'>Performer</th>";
        tableHeader += "<th align='center'>Date</th>";
        tableHeader += "<th align='center'>Action Name</th>";
        tableHeader += "<th align='center'>Remark</th>";

        $.each(AuditJson, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.STEPNAME + "</td>";
            tableBody += "<td>" + val.ACTIONBYUSER + "</td>";
            tableBody += "<td>" + val.ACTIONDATE + "</td>";
            tableBody += "<td>" + val.ACTION + "</td>";
            tableBody += "<td>" + val.REMARK + "</td>";
            tableBody += "</tr>";
        });

        var table = "<table ID='tblAudit' class='display nowrap dataTable'>" +
            "<thead><tr class='grey'>" + tableHeader + "</tr></thead>" +
            "<tbody>" + tableBody + "</tbody>" +
            "</table>";
        $("#div_Audit").empty();
        $("#div_Audit").html(table);

    }
    getDocument();
}

function getDocument() {

    PageMethods.getDocumentData(pono, P2P_API, function (response) {
        DocumentJSON = JSON.parse(JSON.parse(response[0]));
        if (response[0] != "[]") {
            displayDocumentData();

        }
        else {
            $("#loading-div-background").hide();
            $.jAlert({
                'content': 'Data Not Available',
                'onClose': function () {

                }
            });
        }

    }, function (error) {
        alert(error);
    });

}

function displayDocumentData() {
    if (DocumentJSON) {
        var tableHeader = "";
        var tableBody = "";

        tableHeader += "<th align='center'>SR.No</th>";
        tableHeader += "<th align='center'>FileName</th>";
        tableHeader += "<th align='center'>Remark</th>";

        $.each(DocumentJSON, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td><input class='hidden' type='text' name='txt_Region_Add" + (key + 1) + "' id='txt_Document_File" + (key + 1) + "' value=" + val.FILENAME + " readonly ></input><a id='a_downloadfiles" + (key + 1) + "' style='cursor: pointer' onclick=\"return downloadfiles1('" + (key + 1) + "');\" >" + val.FILENAME + "</a><input class='hidden' type='text' name='txt_pono" + (key + 1) + "' id='txt_poreqno" + (key + 1) + "' value=" + val.OBJECT_VALUE + " readonly /></td>";
            tableBody += "<td>" + val.DOCUMENT_TYPE + "</td>";
            tableBody += "</tr>";
        });

        var table = "<table ID='tblDoc' class='display nowrap dataTable'>" +
            "<thead><tr class='grey'>" + tableHeader + "</tr></thead>" +
            "<tbody>" + tableBody + "</tbody>" +
            "</table>";
        $("#div_docs").empty();
        $("#div_docs").html(table);
        var tbllength = $('#tblDoc tr').length;
        if (tbllength > 1) {
            $('#Img1').attr("src", '../../assets/images/attachment_sel.png');
        }
        else {
            $('#Img1').attr("src", '../../assets/images/attachment_non_sel.png');
        }
        $("#loading-div-background").hide();
    }
}

function downloadfiles1(index) {
    window.open('/Common/FileDownload.aspx?indentno=' + $("#txt_poreqno" + index).val() + '&filename=' + $("#tblDoc tr")[index].cells[1].innerText + '&filetag=', 'Download', 'left=150,top=100,width=600,height=300,toolbar=no,menubars=no,status=no,scrollbars=yes,resize=no');
}

function getBudgetDetails(budid, budsubid, finyear) {
    //$("#loading-div-background").show();
    //PageMethods.getBudgetDetailData(budid, budsubid, finyear, function (response) {
    //    BudDtlJson = JSON.parse(response[0]);
    //    if (response[0] != "[]") {
    displayBudgetData();
    //    }
    //}, function (error) {
    //    alert(error);
    //});
}

function displayBudgetData() {
    //if (obj) {
    var tableHeader = "";
    var tableBody = "";

    tableHeader += "<th>#</th>";
    tableHeader += " <th>Budget Head</th>";
    tableHeader += " <th>Budget SubHead</th>";
    tableHeader += " <th>Budget Code</th>";
    tableHeader += " <th>Fin Year</th>";
    tableHeader += " <th>Opening Budget</th>";
    tableHeader += " <th>Consumed Budget</th>";
    tableHeader += " <th>Inprogress Budget</th>";
    tableHeader += " <th>Remaining Budget</th>";

    //$.each(obj, function (key, val) {
    var key = 0;
    tableBody += "<tr id='row_" + (key + 1) + "'>";
    tableBody += "<td name='srno'>" + (key + 1) + "</td>";
    tableBody += "<td>1</td>";
    tableBody += "<td>2</td>";
    tableBody += "<td>3</td>";
    tableBody += "<td>4</td>";
    tableBody += "<td class='text-right'>5</td>";
    tableBody += "<td class='text-right'>6</td>";
    tableBody += "<td class='text-right'>7</td>";
    tableBody += "<td class='text-right'>8</td>";

    //tableBody += "<td>" + val.BUDGET_HEAD + "</td>";
    //tableBody += "<td>" + val.BUD_SUB_HEAD + "</td>";
    //tableBody += "<td>" + val.BUD_CODE + "</td>";
    //tableBody += "<td>" + val.FINANCIAL_YEAR + "</td>";
    //tableBody += "<td class='text-right'>" + val.BUDGET_AMOUNT.toFixed(2) + "</td>";
    //tableBody += "<td class='text-right'>" + val.BUDGET_CONS.toFixed(2) + "</td>";
    //tableBody += "<td class='text-right'>" + val.BUDGET_INPR.toFixed(2) + "</td>";
    //tableBody += "<td class='text-right'>" + val.BAL_BUDGET.toFixed(2) + "</td>";
    tableBody += "</tr>";
    //})
    var table = "<table class='display nowrap dataTable' id='tbl_BudDetails' runat='server'>" +
        "<thead style='background-color:gray'><tr>" + tableHeader + "</tr></thead>" +
        "<tbody>" + tableBody + "</tbody>" +
        "</table>";
    $("#div_Budget_details").empty();
    $("#div_Budget_details").html(table);
    //}
}

function getPOTAXData() {
    PageMethods.getPOTAXData($("#txt_ProcessID").val(), $("#txt_InstanceID").val(), Master_API, P2P_API, WFE_API, function (response) {
        POTAXJson = JSON.parse(JSON.parse(response[0]));
        displayTaxData();
    },
        function (error) {
        });
}

function displayTaxData() {
    if (POTAXJson) {
        var tableHeader = "";
        var tableBody = "";

        tableHeader += "<th align='center'>SR.No</th>";
        tableHeader += "<th align='center'>Tax</th>";

        $.each(POTAXJson, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.label + "<input class='hidden' type='text' name='txt_termid" + (key + 1) + "' value='" + val.id + "'/></td>";
            tableBody += "</tr>";
        });

        var table = "<table ID='tbltax' class='display nowrap dataTable'>" +
            "<thead><tr class='grey'>" + tableHeader + "</tr></thead>" +
            "<tbody>" + tableBody + "</tbody>" +
            "</table>";
        $("#div_tax").empty();
        $("#div_tax").html(table);

    }
}

function getPOTERMData() {
    PageMethods.getPOTERMData($("#txt_ProcessID").val(), $("#txt_InstanceID").val(), Master_API, P2P_API, WFE_API, function (response) {
        POTERMJson = JSON.parse(JSON.parse(response[0]));
        displayTermData();
    },
        function (error) {
        });
}

function displayTermData() {
    if (POTERMJson) {
        var tableHeader = "";
        var tableBody = "";

        tableHeader += "<th align='center'>SR.No</th>";
        tableHeader += "<th align='center'>Term & Condition</th>";

        $.each(POTERMJson, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.label + "<input class='hidden' type='text' name='txt_termid" + (key + 1) + "' value='" + val.id + "'/></td>";
            tableBody += "</tr>";
        });

        var table = "<table ID='tblterms' class='display nowrap dataTable'>" +
            "<thead><tr class='grey'>" + tableHeader + "</tr></thead>" +
            "<tbody>" + tableBody + "</tbody>" +
            "</table>";
        $("#div_terms").empty();
        $("#div_terms").html(table);

    }
}

$("#btn_Save").click(function () {
    try {
        $("#loading-div-background").show();
        var action = $("#ddlAction").val();
        var approvalremark = $("#app_Remark").val();

        if (action == "0") {
            $.jAlert({
                'content': 'Please Select Action..!',
                'onClose': function () {
                    $('#ddlAction').focus();
                    $("#loading-div-background").hide();
                }
            });
            return false;
        }

        if (action != "APPROVE") {
            if (approvalremark == "") {
                $.jAlert({
                    'content': 'Please Enter Remark..!',
                    'onClose': function () {
                        $('#app_Remark').focus();
                        $("#loading-div-background").hide();
                    }
                });
                return false;
            }
        }

        PageMethods.SaveData($("#txt_ProcessID").val(), $("#txt_InstanceID").val(), $("#txt_StepName").val(), $("#SessionAdId").val(), $("#txt_Email").val(), pono, requestInitiator, requestIni_Email, action, approvalremark, Master_API, P2P_API, WFE_API, function (response) {
            if (response) {
                if (response == "true") {
                    var saveMsg = "PO " + action + " Successfully and PO Number is " + pono;
                    $.jAlert({
                        'content': saveMsg,
                        'onClose': function () {
                            location.href = "../../HomePage.aspx";
                            $("#loading-div-background").hide();
                        }
                    });
                }
                else {
                    $.jAlert({
                        'content': response,
                    });
                    $("#loading-div-background").hide();
                    return false;
                }
            }
            $("#loading-div-background").hide();
        }, function (error) {
        });
        return true;
    }
    catch (exception) {
        $("#loading-div-background").hide();
        return false;

    }
});