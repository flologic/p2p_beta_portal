﻿
var Branchid = "";

$(document).ready(function () {
    $("#form1").trigger("reset");
    addmodalpopup();
    $("#loading-div-background").show();
    $("#spn_userTotal").text(0);
    displayData();
});


function LoadBranch() {
    PageMethods.GetBranch(function (response) {
        if (response[0] != "[]") {
            BranchJson = JSON.parse(response[0]);
            BindBranch();
        }
        else {
            $.jAlert({
                'content': 'No Data Found..!'
            });
            return false;
        }
    },
        function (error) {
        });
}

function BindBranch() {
    $("#txt_Branch").autocomplete({
        autoFocus: true,
        minLength: 3,
        source: BranchJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#txt_Branch").val(ui.item.label);
            branchID = ui.item.id;
            $("#txt_StockArea").val(ui.item.areaname);
            $("#txt_StockID").val(ui.item.areaid);
            $("#txt_PO_No").val('');
            POID = 0;
            PONO = "";
            supplierID = 0;
            $("#lbl_supp_name").text('');
            $("#lbl_PO_Date").text('');
            $("#tbl_RPO").text('');

            if ($("#txt_Branch").val() != "") {
                $("#txt_PO_No").prop('disabled', false);
            }
            else {
                $("#txt_PO_No").prop('disabled', true);
            }
            bindPO_NO();
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $.jAlert({
                    'content': 'Please Select Valid Branch..!'
                });
                $(this).val("");
                catid = "";
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function LoadPo() {
    PageMethods.GetPONO(branchID, function (response) {
        if (response[0] != "[]") {
            POJson = JSON.parse(response[0]);
            BindPOData();
        }
        else {
            $.jAlert({
                'content': 'No Data Found..!'
            });
            return false;
        }
    },
        function (error) {
        });
}

function BindPOData() {
    $("#txt_PO_No").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: POJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#txt_PO_No").val(ui.item.label);
            POID = ui.item.id;
            PONO = ui.item.label;
            supplierID = ui.item.FK_Supplier_ID;
            $("#lbl_PO_Date").text(ui.item.PO_Date);
            $("#lbl_supp_name").text(ui.item.Supplier);
            if ($("#txt_PO_No").val() != "") {
                getGRNDetails();
            }
            return false;
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $("#" + $(this).attr("id")).val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function getGRNDetails() {
    PageMethods.getPOJsonData(PONO, POID, branchID, function (response) {
        if (response != null) {
            PODataJson = JSON.parse(response[0]);
            displayData();
        }
    }, function (error) {
        $.jAlert({
            'content': error
        });
    });
}

function displayData() {
    $("#loading-div-background").show();
    var tableHeader = "";
    var tableBody = "";
    var div_asset = "";
    var object = {}

    tableHeader += "<th><input name='chkall' id='chkall' type='checkbox' onclick='checkall()'/></th>";
    tableHeader += "<th>Item Category</th>";
    tableHeader += "<th>Item Sub-Category</th>";
    tableHeader += "<th>Item Name</th>";
    tableHeader += "<th>PO Quantity</th>";
    tableHeader += "<th>Cumulative Received Quantity</th>";
    //tableHeader += "<th>Challan Quantity</th>";
    tableHeader += "<th>Received Quantity</th>";
    tableHeader += "<th>Balance Quantity</th>";
    tableHeader += "<th>Details</th>";
    tableHeader += "<th class='text-center' style='width: 5%' colspan='2'>C / R</th>";
    var key = 0;
    // tableHeader += "<th>Multiple Asset</th>";
    //$.each(PODataJson, function (key, val) {
    //if (val.PO_ITEM_QTY > val.Supplied_QTY) {
    //tableBody += "<tr><td><input name='chkno' id='chk_" + (key + 1) + "' type='checkbox'/></td>";
    //tableBody += "<td><input name='tempid' id='txt_tempid" + (key + 1) + "' value=" + (val.FK_TMPL_ID) + " class='hidden' type='text'/>";
    //tableBody += "<input id = 'txt_CatHid" + (key + 1) + "' value = " + (val.FK_CAT_ID) + " class='hidden' type = 'text' /> " + val.CATEGORY + "</td > ";
    //tableBody += "<td><input name='subcatHid' id='txt_subcatid" + (key + 1) + "' value=" + (val.FK_SUBCAT_ID) + " class='hidden' type='text'/>" + val.SUBCATEGORY + "</td>";
    //tableBody += "<td><input name='itmid' id='txt_ITMid" + (key + 1) + "' value=" + (val.FK_ITEM_ID) + " class='hidden' type='text'/>" + val.ITEM + "</td>";
    //tableBody += "<td style='text-align:center'><input name='POQty' id='txt_POQty" + (key + 1) + "' value=" + (val.PO_ITEM_QTY) + " class='hidden' type='text'/>" + val.PO_ITEM_QTY + "</td>";
    //tableBody += "<td style='text-align:center'><input name='SuppliedQty' id='txt_Supplied_Qty" + (key + 1) + "' value=" + (val.Supplied_QTY) + "  class='hidden' type='text'/>" + val.Supplied_QTY + "</td>";
    ////tableBody += "<td><input name='ChlQty' placeholder='Enter Challan Quantity' id='txt_ChaQty" + (key + 1) + "' value='' onkeyup='check_Cal(" + (key + 1) + ")'  type='text' class='form-control input-sm number' /></td>";
    //tableBody += "<td><input name='RecQty' style='text-align:center' placeholder='Enter Recieved Quantity' id='txt_RecQty" + (key + 1) + "' value='' onkeyup='check_Cal(" + (key + 1) + ")'  type='text' class='form-control input-sm number'/></td>";
    //tableBody += "<td><input name='BalQty' style='text-align:center' id='txt_BalQty" + (key + 1) + "' value='' class='form-control input-sm'  type='text' disabled='true'/></td>";
    //tableBody += "<td><a alt='details' id='ank_detail_" + (key + 1) + "' value='' data-toggle='modal' href='#div_Eligible' onclick='fill_Value(" + (key + 1) + ")' value='0'><span class='text-info' onclick='displayAsset(" + (key + 1) + ")'>Details</span></a></td>"
    //if (val.PO_ITEM_TYPE == "C") {
    //    tableBody += "<td colspan='2'><input name='POTYPE' id='txt_POTYPE" + (key + 1) + "' value=" + ("C") + " class='hidden' type='text'/>" + val.PO_ITEM_TYPE + "</td>";//val.POITEMTYP
    //}
    //else {
    //    tableBody += "<td colspan='2'><input name='POTYPE' id='txt_POTYPE" + (key + 1) + "' value='' class='hidden' type='text'/></td>";
    //}
    //tableBody += "<td class='hidden'><input name='Warranty' id='txt_Warranty" + (key + 1) + "' value=" + (val.WARRENTY) + "  type='text' class='hidden'/></td>";
    //tableBody += "<td class='hidden'><input name='Make' id='txt_Make" + (key + 1) + "' value=" + (val.MAKEID) + "  type='text' class='hidden'/></td>";
    //tableBody += "<td class='hidden'><input name='Model' id='txt_Model" + (key + 1) + "' value=" + (val.MODELID) + "  type='text' class='hidden'/></td>";
    //tableBody += "<td class='hidden'><input name='Rate_Amount' id='txt_Rate" + (key + 1) + "' value=" + (val.ITEM_RATE) + "  type='text' class='hidden'/></td>";
    //tableBody += "<td class='hidden'><input name='Amount' id='txt_Amount" + (key + 1) + "' value='' type='text' class='hidden'/></td>";
    //tableBody += "</tr>";
    //div_asset += "<div id='asset_Row_" + (key + 1) + "'></div>";

    tableBody += "<tr><td><input name='chkno' id='chk_" + (key + 1) + "' type='checkbox'/></td>";
    tableBody += "<td><input name='tempid' id='txt_tempid" + (key + 1) + "' value='' class='hidden' type='text'/>";
    tableBody += "<input id = 'txt_CatHid" + (key + 1) + "' value ='' class='hidden' type = 'text' />A</td > ";
    tableBody += "<td><input name='subcatHid' id='txt_subcatid" + (key + 1) + "' value='' class='hidden' type='text'/>B</td>";
    tableBody += "<td><input name='itmid' id='txt_ITMid" + (key + 1) + "' value='' class='hidden' type='text'/>C</td>";
    tableBody += "<td style='text-align:center'><input name='POQty' id='txt_POQty" + (key + 1) + "' value='' class='hidden' type='text'/>5</td>";
    tableBody += "<td style='text-align:center'><input name='SuppliedQty' id='txt_Supplied_Qty" + (key + 1) + "' value='' class='hidden' type='text'/>0</td>";
    //tableBody += "<td><input name='ChlQty' placeholder='Enter Challan Quantity' id='txt_ChaQty" + (key + 1) + "' value='' onkeyup='check_Cal(" + (key + 1) + ")'  type='text' class='form-control input-sm number' /></td>";
    tableBody += "<td><input name='RecQty' style='text-align:center' placeholder='Enter Recieved Quantity' id='txt_RecQty" + (key + 1) + "' value='' onkeyup='check_Cal(" + (key + 1) + ")'  type='text' class='form-control input-sm number'/></td>";
    tableBody += "<td><input name='BalQty' style='text-align:center' id='txt_BalQty" + (key + 1) + "' value='' class='form-control input-sm'  type='text' disabled='true'/></td>";
    tableBody += "<td><a alt='details' id='ank_detail_" + (key + 1) + "' value='' data-toggle='modal' href='#div_Eligible' onclick='fill_Value(" + (key + 1) + ")' value='0'><span class='text-info' onclick='displayAssetDetails(" + (key + 1) + ")'>Details</span></a></td>"
    //if (val.PO_ITEM_TYPE == "C") {
    //    tableBody += "<td colspan='2'><input name='POTYPE' id='txt_POTYPE" + (key + 1) + "' value=" + ("C") + " class='hidden' type='text'/>" + val.PO_ITEM_TYPE + "</td>";//val.POITEMTYP
    //}
    //else {
    tableBody += "<td colspan='2'><input name='POTYPE' id='txt_POTYPE" + (key + 1) + "' value='' class='hidden' type='text'/></td>";
    //}
    tableBody += "<td class='hidden'><input name='Warranty' id='txt_Warranty" + (key + 1) + "' value=''  type='text' class='hidden'/></td>";
    tableBody += "<td class='hidden'><input name='Make' id='txt_Make" + (key + 1) + "' value=''  type='text' class='hidden'/></td>";
    tableBody += "<td class='hidden'><input name='Model' id='txt_Model" + (key + 1) + "' value=" + "  type='text' class='hidden'/></td>";
    tableBody += "<td class='hidden'><input name='Rate_Amount' id='txt_Rate" + (key + 1) + "' value=''  type='text' class='hidden'/></td>";
    tableBody += "<td class='hidden'><input name='Amount' id='txt_Amount" + (key + 1) + "' value='' type='text' class='hidden'/></td>";
    tableBody += "</tr>";
    div_asset += "<div id='asset_Row_" + (key + 1) + "'></div>";
    //}
    //});
    //}

    key = 1;
    tableBody += "<tr><td><input name='chkno' id='chk_" + (key + 1) + "' type='checkbox'/></td>";
    tableBody += "<td><input name='tempid' id='txt_tempid" + (key + 1) + "' value='' class='hidden' type='text'/>";
    tableBody += "<input id = 'txt_CatHid" + (key + 1) + "' value ='' class='hidden' type = 'text' />A</td > ";
    tableBody += "<td><input name='subcatHid' id='txt_subcatid" + (key + 1) + "' value='' class='hidden' type='text'/>B</td>";
    tableBody += "<td><input name='itmid' id='txt_ITMid" + (key + 1) + "' value='' class='hidden' type='text'/>C</td>";
    tableBody += "<td style='text-align:center'><input name='POQty' id='txt_POQty" + (key + 1) + "' value='' class='hidden' type='text'/>5</td>";
    tableBody += "<td style='text-align:center'><input name='SuppliedQty' id='txt_Supplied_Qty" + (key + 1) + "' value='' class='hidden' type='text'/>0</td>";
    //tableBody += "<td><input name='ChlQty' placeholder='Enter Challan Quantity' id='txt_ChaQty" + (key + 1) + "' value='' onkeyup='check_Cal(" + (key + 1) + ")'  type='text' class='form-control input-sm number' /></td>";
    tableBody += "<td><input name='RecQty' style='text-align:center' placeholder='Enter Recieved Quantity' id='txt_RecQty" + (key + 1) + "' value='' onkeyup='check_Cal(" + (key + 1) + ")'  type='text' class='form-control input-sm number'/></td>";
    tableBody += "<td><input name='BalQty' style='text-align:center' id='txt_BalQty" + (key + 1) + "' value='' class='form-control input-sm'  type='text' disabled='true'/></td>";
    tableBody += "<td><a alt='details' id='ank_detail_" + (key + 1) + "' value='' data-toggle='modal' href='#div_Eligible' onclick='fill_Value(" + (key + 1) + ")' value='0'><span class='text-info' onclick='displayAssetDetails(" + (key + 1) + ")'>Details</span></a></td>"
    //if (val.PO_ITEM_TYPE == "C") {
    //    tableBody += "<td colspan='2'><input name='POTYPE' id='txt_POTYPE" + (key + 1) + "' value=" + ("C") + " class='hidden' type='text'/>" + val.PO_ITEM_TYPE + "</td>";//val.POITEMTYP
    //}
    //else {
    tableBody += "<td colspan='2'><input name='POTYPE' id='txt_POTYPE" + (key + 1) + "' value='' class='hidden' type='text'/></td>";
    //}
    tableBody += "<td class='hidden'><input name='Warranty' id='txt_Warranty" + (key + 1) + "' value=''  type='text' class='hidden'/></td>";
    tableBody += "<td class='hidden'><input name='Make' id='txt_Make" + (key + 1) + "' value=''  type='text' class='hidden'/></td>";
    tableBody += "<td class='hidden'><input name='Model' id='txt_Model" + (key + 1) + "' value=" + "  type='text' class='hidden'/></td>";
    tableBody += "<td class='hidden'><input name='Rate_Amount' id='txt_Rate" + (key + 1) + "' value=''  type='text' class='hidden'/></td>";
    tableBody += "<td class='hidden'><input name='Amount' id='txt_Amount" + (key + 1) + "' value='' type='text' class='hidden'/></td>";
    tableBody += "</tr>";
    div_asset += "<div id='asset_Row_" + (key + 1) + "'></div>";

    var table = "<table class='display nowrap dataTable' id='tbl_RPO' runat='server'>" +
        "<thead><tr>" + tableHeader + "</tr></thead>" +
        "<tbody>" + tableBody + "</tbody>" +
        "</table>";
    $("#div_displayGrnDtl").html(table);
    $("#div_Asset").html(div_asset);
    $("#loading-div-background").hide();
}

function fill_Value(id) {
    $("#ank_detail_" + id).val(1);
}

function displayAssetDetails(index) {
    var tableHeader = "";
    var tableBody = "";

    var table1 = $("#tbl_Asset_" + index).find("tr").length - 1;
    if (table1 == $('#txt_RecQty' + index).val()) {
    }
    else {
        $("#tbl_Asset_" + index).empty();
    }

    tableHeader += "<th>Hardware Serial No</th>";
    tableHeader += "<th>Confirm Serial No</th>";
    tableHeader += "<th>Hardware Asset No</th>";

    var tbl = $("#tbl_RPO tr").length - 1;
    for (var m = 0; m < tbl; m++) {
        //if (index != 1)
        //{
        //    if (SubCategory == $("#txt_subCatHid" + (rows + 1)).val()) {
        //        var pre_qty = $("#txt_POQty" + (rows + 1)).val();
        //        if (pre_qty == undefined || pre_qty == "") {
        //            pre_qty = 0;
        //        }
        //        pre_count = parseInt(pre_count) + parseInt(pre_qty);
        //    }
        //}

        if (parseInt(m) + 1 == index) {
            $("#asset_Row_" + (m + 1)).show();
        }
        else {
            $("#asset_Row_" + (m + 1)).hide();
        }
    }

    var i = 0;
    tableBody += "<tr><td><input name='hardSerNo' id='txt_HardWareNo_" + (index) + "_" + (i + 1) + "' class='form-control input-sm' placeholder='Enter Hardware Serial Number' value='' type='text' onchange='checkHardSerial(" + index + "," + (i + 1) + ")'/></td>";
    tableBody += "<td><input name='ConfirmNo' id='txt_Confirno_" + (index) + "_" + (i + 1) + "' class='form-control input-sm' placeholder='Enter Confirm Serial Number' value='' type='text' onchange='checkCommon(" + index + "," + (i + 1) + ")'/></td>";
    tableBody += "<td><input name='HardAssetno' id='txt_AssetNo_" + (index) + "_" + (i + 1) + "' class='form-control input-sm' value=ABC type='text' disabled='disabled'/></td>";
    //tableBody += "<td class='hidden'><input name='HardAssetnoHidden' id='txt_AssetNoHidden_" + (index) + "_" + (i + 1) + "' class='form-control input-sm' value='" + val_asset + "' type='text' class='hidden'/></td>";
    $("#ank_detail_" + (index) + "").val('1');
    tableBody += "</tr>";
    //asset_count = (parseInt(asset_count) + (1));
    //arr_cnt = parseInt(arr_cnt) + 1;
    var table = "<table class='display nowrap dataTable' id='tbl_Asset_" + (index) + "' runat='server'>" +
        "<thead><tr>" + tableHeader + "</tr></thead>" +
        "<tbody>" + tableBody + "</tbody>" +
        "</table>";
    $("#asset_Row_" + index).html(table);
}

