﻿var SubCatPartJson, PartJson, SubCatJson;
var Client_ID = '', Master_API = '';
var role_PKID, Subcatid, Subcat, Partid, Part;
var saveType;

$(document).ready(function () {
    onLoad();
});

function onLoad() {
    $('#form1').trigger("reset");
    saveType = "SAVE";
    $("#btn_Save").html('Save');
    $("#sel_sub_cat").val("");
    $('#sel_sub_cat').attr('readonly', false);
    $("#sel_part_name").val("");
    $('#sel_part_name').attr('readonly', false);
    Client_ID = $("#txt_Client_ID").val();
    Master_API = $("#txt_Master_URL").val();
    GetSubCat();
    GetPartdata();
    GetSubCatPartData();

}

function GetSubCat() {
    PageMethods.getSubCatData(Client_ID, Master_API, function (response) {
        SubCatJson = JSON.parse(JSON.parse(response[0]));
        assignsubcat();
    },
        function (error) {
        });
}

function assignsubcat() {
    $("#sel_sub_cat").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: SubCatJson,
        select: function (event, ui) {
            $("#sel_sub_cat").val(ui.item.label);
            Subcatid = ui.item.id;
            Subcat = ui.item.label;
          
            return false;
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $("#sel_sub_cat").val("");
            }
            else {

            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function GetPartdata() {
    PageMethods.getPartAllData(Client_ID, Master_API, function (response) {
        PartJson = JSON.parse(JSON.parse(response[0]));
        assignpart();
    },
        function (error) {
        });
}

function assignpart() {
    $("#sel_part_name").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: PartJson,
        select: function (event, ui) {
            $("#sel_part_name").val(ui.item.label);
            Partid = ui.item.id;
            Part = ui.item.label;          
            return false;
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $("#sel_part_name").val("");
            }
            else {

            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function GetSubCatPartData() {
    PageMethods.getSubCatWisePartAllData(Client_ID, Master_API, function (response) {
        SubCatPartJson = JSON.parse(JSON.parse(response[0]));
        if (SubCatPartJson.Message == "Data Not Found") {
            $.jAlert({
                'content': 'Data Not Available.',
                'onClose': function () {

                }
            });
        }
        else {
            displayData();
        }
    },
        function (error) {
        });
}

function displayData() {
    if (SubCatPartJson) {
        var tableHeader = "";
        var tableBody = "";

        tableHeader += "<th style='width: 3%'>#</th>";
        tableHeader += "<th>Sub-Cat Name</th>";
        tableHeader += "<th>Part Name</th>";
        tableHeader += "<th style='width: 5%'>Edit</th>";
        tableHeader += "<th style='width: 5%'>Delete</th>";

        $.each(SubCatPartJson, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.ITMTYPE + "</td>";
            tableBody += "<td>" + val.PART + "</td>";
            tableBody += "<td class='click_Edit'><img src='../../assets/images/edit.png' title='Edit' alt='Edit'/></td>";
            tableBody += "<td class='click_Delete'><img src='../../assets/images/delete.png' title='Delete' alt='Delete'/></td>";
            tableBody += "</tr>";
        })
        var table = "<table class='display nowrap' id='tbl_part'>" +
              "<thead><tr>" + tableHeader + "</tr></thead>" +
              "<tbody>" + tableBody + "</tbody>" +
              "</table>";
        $("#div_Part").empty();
        $("#div_Part").html(table);

        $("#tbl_part").dataTable({
            dom: 'Bfrtip', "bSort": true,
            buttons: [
                'excel'
            ]
        });
    }
}

$("#btn_Save").click(function () {
    SaveData();
});

function SaveData() {
    var Part = [];
    var subcat = $("#sel_sub_cat").val();
    var PartName = $("#sel_part_name").val();
    if (subcat == "") {
        $.jAlert({
            'content': 'Please Select Sub Category.',
            'onClose': function () {
                $('#sel_sub_cat').focus();
            }
        });
        return false;
    }
    else if (PartName == "") {
        $.jAlert({
            'content': 'Please Select Part Name.',
            'onClose': function () {
                $('#sel_part_name').focus();
            }
        });
        return false;
    }



    var objLocation;
    if (saveType == "SAVE") {
        objLocation = {
            "FK_ITMTYPEID": Subcatid, "FK_PARTID": Partid, "IS_ACTIVE": "1", "CREATED_BY": $('#txt_UserID').val(), "FK_CLIENTID": Client_ID
        };
    }
    else {
        objLocation = {
            "id": role_PKID, "FK_ITMTYPEID": Subcatid, "FK_PARTID": Partid, "IS_ACTIVE": "1", "CREATED_BY": $('#txt_UserID').val()
        };
    }
    Part.push(objLocation);

    var transactionData = { "partData": Part }

    PageMethods.saveData(JSON.stringify(transactionData), Master_API, saveType, function (response) {
        $.jAlert({
            'content': JSON.parse(JSON.parse(response)),
        });
        onLoad();
    }, function (error) {
        $.jAlert({
            'content': error,
        });
    });
}

$("#btn_Cancel").click(function () {
    onLoad();
});


$('body').on('click', '.click_Edit', function () {
    var $row = $(this).closest('tr');
    var table = $('#tbl_part').DataTable();
    var data = table.row($row).data();
    $("#sel_sub_cat").val(SubCatPartJson[parseInt(data[0]) - 1].ITMTYPE);
    $("#sel_part_name").val(SubCatPartJson[parseInt(data[0]) - 1].PART);
    role_PKID = SubCatPartJson[parseInt(data[0]) - 1].id;
    saveType = "EDIT";
    $("#btn_Save").html('Update');
});


$('body').on('click', '.click_Delete', function () {
    var $row = $(this).closest('tr');
    var table = $('#tbl_part').DataTable();
    var data = table.row($row).data();
    $("#sel_sub_cat").val(SubCatPartJson[parseInt(data[0]) - 1].ITMTYPE);
    $("#sel_part_name").val(SubCatPartJson[parseInt(data[0]) - 1].PART);
    $('#sel_sub_cat').attr('readonly', true);
    $('#sel_part_name').attr('readonly', true);
    role_PKID = SubCatPartJson[parseInt(data[0]) - 1].id;
    saveType = "DELETE";
    $("#btn_Save").html('Delete');
    $.jAlert({ 'type': 'confirm', 'onConfirm': function () { SaveData() } });
});
