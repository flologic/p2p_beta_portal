﻿var Client_ID = '', Master_API = '';
var UserJson = [], UserID;
var RoleDtl = '';

$(document).ready (function(){
    Client_ID=$("#txt_Client_ID").val();
    Master_API=$("#txt_Master_URL").val();
    GetRoleData();
    });

function GetRoleData() {
    PageMethods.GetRoleData(Client_ID, Master_API, function (response) {
        RoleDtl = JSON.parse(JSON.parse(response[0]));
        if (RoleDtl.Message == "Data Not Found") {
            $.jalert({
                'content': 'Role Not Found..!'
            });
        }
        else {
            RoleOnSuccess();
        }
    },
    function (error) {
    });
}

function RoleOnSuccess() {
    if (RoleDtl) {
        var tableHeader = "";
        var tableBody = "";
        tableHeader += "<th style='width: 2%;'>#</th>";
        tableHeader += "<th>Role Name</th>";
        tableHeader += "<th>Edit</th>";
        tableHeader += "<th>Delete</th>";
        $.each(RoleDtl, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.label + "<input class='form-control input-sm' type='text' id='txt_role_name" + (key + 1) + "' value='" + val.label + "' style='display:none'></input></td>";            
            tableBody += "<td><a><img src='../../assets/images/edit.png' onclick='return bindData(" + (key + 1) + ")' title='Add New Row' /></a><input class='form-control input-sm' type='text' id='txt_Pk" + (key + 1) + "' value='" + val.id + "' style='display:none'></input></td>";
            tableBody += "<td><a><img src='../../assets/images/delete.png' onclick='return DeleteData(" + (key + 1) + ")' title='delete Row' /></a></td>";
            tableBody += "</tr>";
        });
        var table = "<table ID='RoleTable' class='display nowrap dataTable'>" +
              "<thead><tr>" + tableHeader + "</tr></thead>" +
              "<tbody>" + tableBody + "</tbody>" +
              "</table>";
        $("#div_displayDtl").empty();
        $("#div_displayDtl").html(table);
        $("#RoleTable").dataTable({
            dom: 'Bfrtip', "bSort": true,
            buttons: [
                'excel'
            ]
        });
    }
}
   
function bindData(index) {
    $("#btn_Save").hide();
    $("#btn_Upd").show();
    $("#Sel_Role_Name").val($("#txt_role_name" + index).val());
    $("#txt_PK_ROLEID").val($("#txt_Pk" + index).val());
}

$("#btn_Save").click(function () {
    if ($("#Sel_Role_Name").val() == "") {
        $.jAlert({
            'content': 'Please Enter Role Name..!'
        });
        return false;
    }
  
    PageMethods.SaveData(Client_ID, Master_API, $("#Sel_Role_Name").val(), $("#txt_Username").val(), function (response) {
        if (JSON.parse(response) == "Save") {
            $.jAlert({
                'content': 'Data Saved Successfully..!'
            });
            GetRoleData();
            $("#Sel_Role_Name").val("");            
        }
        else if (JSON.parse(response) == "Duplicate") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(response) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Saved Due To Internal Server Error..!'
            });
        }
    }, function (error) {
    });
});

$("#btn_Upd").click(function () {
    PageMethods.UpdateData(Client_ID, Master_API, $("#txt_PK_ROLEID").val(), $("#Sel_Role_Name").val(),  $("#txt_Username").val(), function (response) {
        if (JSON.parse(response) == "Update") {
            $.jAlert({
                'content': 'Data Updated Successfully..!'
            });

            GetRoleData();
            $("#Sel_Role_Name").val("");
            $("#btn_Save").show();
            $("#btn_Upd").hide();
        }
        else if (JSON.parse(response) == "Duplicate") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(response) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Update Due To Internal Server Error..!'
            });
        }

    }, function (error) {
    });
});

function DeleteData(index) {
    $.jAlert({
        'title': 'Confirmation',
        'content': 'Are you sure to delete this record..?', 'type': 'confirm',
        'onConfirm': function () {
            $("#txt_PK_ROLEID").val($("#txt_Pk" + index).val());
            PageMethods.Deletedata(Client_ID, Master_API, $("#txt_PK_ROLEID").val(), $("#txt_Username").val(), function (response) {
                if (JSON.parse(response) == "Delete") {
                    $.jAlert({
                        'content': 'Data Deleted Successfully..!'
                    });

                    GetRoleData();
                }
                else if (JSON.parse(response) == "NotFound") {
                    $.jAlert({
                        'content': 'Data Not Delete..!'
                    });
                }
                else if (JSON.parse(response) == "InternalServerError") {
                    $.jAlert({
                        'content': 'Data Not Delete Due To Internal Server Error..!'
                    });
                }
            }, function (error) {
            });
        }
    });
}

$("#btn_Cancel").click(function () {
    window.location.replace("../../HomePage.aspx");
});


