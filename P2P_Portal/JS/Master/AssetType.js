﻿var Client_ID = '', Master_API = '';
var AssetTypeJson = '';

$(document).ready(function () {
    Client_ID = $("#txt_Client_ID").val();
    Master_API = $("#txt_Master_URL").val();
    getAssetTypeData();

});

function getAssetTypeData() {
    PageMethods.GetAssetTypeData(Client_ID, Master_API, function (response) {
        AssetTypeJson = JSON.parse(JSON.parse(response[0]));
        if (AssetTypeJson.Message == "Data Not Found") {
            $.jAlert({
                'content': 'Asset Type Data Not Found..!'
            });
        }
        else {
            bindAssetType();
        }

    },
     function (error) {
     });
}

function bindAssetType() {
    if (AssetTypeJson) {
        var tableheader = "";
        var tableBody = "";
        tableheader += "<th style='width: 2%;'>#</th>";
        tableheader += "<th>Asset Type</th>";
        tableheader += "<th>Edit</th>";
        tableheader += "<th>Delete</th>";

        $.each(AssetTypeJson, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.label + "<input class='form-control input-sm' type='text' id='txt_Asset_Typename" + (key + 1) + "' value='" + val.label + "' style='display:none'></input>";
            //tableBody += "<td>" + val.COST_CENTER_DESC + "<input class='form-control input-sm' type='text' id='txt_costcntr_desc" + (key + 1) + "' value='" + val.COST_CENTER_DESC + "' style='display:none'></input>";
            tableBody += "<td><a><img src='../../assets/images/edit.png' onclick='return bindData(" + (key + 1) + ")' title='Add New Row' /></a><input class='form-control input-sm' type='text' id='txt_Pk" + (key + 1) + "' value='" + val.id + "' style='display:none'></input></td>";
            tableBody += "<td><a><img src='../../assets/images/delete.png' onclick='return DeleteData(" + (key + 1) + ")' title='delete Row' /></a></td>";
            tableBody += "</tr>";
        });
        var table = "<table ID='AssetTypetable' class='display nowrap dataTable'>" +
           "<thead><tr>" + tableheader + "</tr></thead>" +
           "<tbody>" + tableBody + "<tbody>" +
        "</table>";
        $("#div_displayDtl").empty();
        $("#div_displayDtl").html(table);
        $("#AssetTypetable").dataTable({
            dom: 'Bfrtip', "bSort": true,
            buttons: [
                'excel'
            ]
        });
    }
}


function bindData(index) {
    $("#btn_Save").hide();
    $("#btn_Upd").show();
    $("#txt_Asset_type").val($("#txt_Asset_Typename" + index).val());
    $("#txt_PK_AssetType_ID").val($("#txt_Pk" + index).val());
}

$("#btn_Save").click(function () {
    if ($("#txt_Asset_type").val() == "") {
        $.jAlert({
            'content': 'Please Enter Asset Type ..!'
        });
        return false;
    }

    PageMethods.SaveData(Client_ID, Master_API, $("#txt_Asset_type").val(), $("#txt_Username").val(), function (response) {
        if (JSON.parse(response) == "Save") {
            $.jAlert({
                'content': 'Data Saved Successfully..!'
            });
            getAssetTypeData();
            $("#txt_Asset_type").val("");
        }
        else if (JSON.parse(response) == "Duplicate") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(response) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Saved Due To Internal Server Error..!'
            });
        }
    }, function (error) {
    });
});

$("#btn_Upd").click(function () {
    PageMethods.UpdateData(Client_ID, Master_API, $("#txt_PK_AssetType_ID").val(), $("#txt_Asset_type").val(), $("#txt_Username").val(), function (response) {
        if (JSON.parse(response) == "Update") {
            $.jAlert({
                'content': 'Data Updated Successfully..!'
            });

            getAssetTypeData();
            $("#txt_Asset_type").val("");
            $("#btn_Save").show();
            $("#btn_Upd").hide();
        }
        else if (JSON.parse(response) == "Duplicate") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(response) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Update Due To Internal Server Error..!'
            });
        }

    }, function (error) {
    });
});

function DeleteData(index) {
    $.jAlert({
        'title': 'Confirmation',
        'content': 'Are you sure to delete this record..?', 'type': 'confirm',
        'onConfirm': function () {
            $("#txt_PK_AssetType_ID").val($("#txt_Pk" + index).val());
            PageMethods.Deletedata(Client_ID, Master_API, $("#txt_PK_AssetType_ID").val(), $("#txt_Username").val(), function (response) {
                if (JSON.parse(response) == "Delete") {
                    $.jAlert({
                        'content': 'Data Deleted Successfully..!'
                    });

                    getAssetTypeData();
                }
                else if (JSON.parse(response) == "NotFound") {
                    $.jAlert({
                        'content': 'Data Not Delete..!'
                    });
                }
                else if (JSON.parse(response) == "InternalServerError") {
                    $.jAlert({
                        'content': 'Data Not Delete Due To Internal Server Error..!'
                    });
                }
            }, function (error) {
            });
        }
    });
}

$("#btn_Cancel").click(function () {
    window.location.replace("../../HomePage.aspx");
});