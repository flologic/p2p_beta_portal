﻿var Client_ID = '', Master_API = '';
var ItemJson = "", ItemCatId;
var BindAllDetail = '';

$(document).ready(function () {
    Client_ID = $("#txt_Client_ID").val();
    Master_API = $("#txt_Master_URL").val();
    // $('#form1').trigger("reset");
    addmodalpopup();
    $("#loading-div-background").show();
    GetItemSubCatData();
    getItemCat();
});

function GetItemSubCatData() {
    PageMethods.GetItemSubCatData(Client_ID, Master_API, function (response) {
        BindAllDetail = JSON.parse(JSON.parse(response[0]));
        if (BindAllDetail.Message == "Data Not Found") {
            alert("Item Sub Category Data Not Found");
            $("#loading-div-background").hide();
        }
        else {
            SubCatOnSuccess();
        }
    });
}

function getItemCat() {
    PageMethods.GetItemCatData(Client_ID, Master_API, function (response) {
        ItemJson = JSON.parse(JSON.parse(response[0]));
        BindItemCat();
    },
        function (error) {
        });
}

function BindItemCat() {
    $("#txt_cat_Id").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: ItemJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#txt_cat_Id").val(ui.item.label);
            ItemCatId = ui.item.id;
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function SubCatOnSuccess() {
    if (BindAllDetail) {
        var tableHeader = "";
        var tableBody = "";
        tableHeader += "<th style='width: 2%;'>#</th>";
        tableHeader += "<th>Item Category</th>";
        tableHeader += "<th>Item Sub-Category</th>";
        tableHeader += "<th>Item Sub-Category Code</th>";
        tableHeader += "<th>Base Rate</th>";
        tableHeader += "<th>Allowed Quantity</th>";
        tableHeader += "<th>Edit</th>";
        tableHeader += "<th>Delete</th>";
        $.each(BindAllDetail, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.ITMCATNAME + "<input class='form-control input-sm' type='text' id='txt_itmname" + (key + 1) + "' value='" + val.ITMCATNAME + "' style='display:none'/><input class='form-control input-sm' type='text' id='txt_Itemctid" + (key + 1) + "' value='" + val.FK_ITMCATID + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.label + "<input class='form-control input-sm' type='text' id='txt_itsbcatnm" + (key + 1) + "' value='" + val.label + "' style='display:none'/><input class='form-control input-sm' type='text' id='txt_Itemctid" + (key + 1) + "' value='" + val.ITMSUBCATNAME + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.ITMSUBCATCODE + "<input class='form-control input-sm' type='text' id='txt_itsbcatcode" + (key + 1) + "' value='" + val.ITMSUBCATCODE + "' style='display:none'/><input class='form-control input-sm' type='text' id='txt_regionhd1" + (key + 1) + "' value='" + val.ITMSUBCATCODE + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.BASE_RATE + "<input class='form-control input-sm'  type='text' id='txt_baserate" + (key + 1) + "' value='" + val.BASE_RATE + "' style='display:none'/></td>";
            tableBody += "<td>" + val.ALLOWED_QTY + "<input class='form-control input-sm'  type='text' id='txt_all_qty" + (key + 1) + "' value='" + val.ALLOWED_QTY + "' style='display:none'/></td>";
            tableBody += "<td><a><img src='../../assets/images/edit.png' onclick='return bindData(" + (key + 1) + ")' title='Add New Row' /></a><input class='form-control input-sm' type='text' id='txt_Pk_" + (key + 1) + "' value='" + val.id + "' style='display:none'></input></td>";
            tableBody += "<td><a><img src='../../assets/images/delete.png' onclick='return DeleteData(" + (key + 1) + ")' title='delete Row' /></a></td>";
            tableBody += "</tr>";
        });

        var table = "<table ID='itemsbcatTable' class='display nowrap dataTable'>" +
              "<thead><tr>" + tableHeader + "</tr></thead>" +
              "<tbody>" + tableBody + "</tbody>" +
              "</table>";
        $("#div_displayDtl").empty();
        $("#div_displayDtl").html(table);
        $("#itemsbcatTable").dataTable({
            dom: 'Bfrtip', "bSort": true,
            buttons: [
                'excel'
            ]
        });
    }
    $("#loading-div-background").hide();
}

$("#btn_Save").click(function () {
    if ($("#txt_cat_Id").val() == "") {
        $.jAlert({
            'content': 'Please Select Item Category..!'
        });
        return false;
    }
    if ($("#txt_itmsbcat_name").val() == "") {
        $.jAlert({
            'content': 'Please Enter Item Subcategory..!'
        });
        return false;
    }
    if ($("#txt_itmsbcat_code").val() == "") {
        $.jAlert({
            'content': 'Please Enter Item Subcategory code..!'
        });
        return false;
    }
    if ($("#txt_Base_Rate").val() == "") {
        $.jAlert({
            'content': 'Please Enter Base Rate..!'
        });
        return false;
    }
    if ($("#txt_Allowed_Qty").val() == "") {
        $.jAlert({
            'content': 'Please Enter Allowed Quantity..!'
        });
        return false;
    }
    PageMethods.SaveData(ItemCatId, $("#txt_itmsbcat_name").val(), $("#txt_itmsbcat_code").val(), $("#txt_Base_Rate").val(), $("#txt_Allowed_Qty").val(), $("#txt_UserName").val(), Client_ID, Master_API, function (response) {
        if (JSON.parse(response) == "OK") {
            $.jAlert({
                'content': 'Data Saved Successfully..!'
            });

            GetItemSubCatData();
            $("#txt_cat_Id").val("");
            $("#txt_itmsbcat_name").val("");
            $("#txt_itmsbcat_code").val("");
            $("#txt_Base_Rate").val("");
            $("#txt_Allowed_Qty").val("");
        }
        else if (JSON.parse(response) == "NotFound") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(response) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Saved Due To Internal Server Error..!'
            });
        }
    }, function (error) {
    });
})

function bindData(index) {
    $("#btn_Save").hide();
    $("#btn_Upd").show();
    ItemCatId = $("#txt_Itemctid" + index).val();
    $("#txt_cat_Id").val($("#txt_itmname" + index).val());
    $("#txt_itmsbcat_name").val($("#txt_itsbcatnm" + index).val());
    $("#txt_itmsbcat_code").val($("#txt_itsbcatcode" + index).val());
    $("#txt_Base_Rate").val($("#txt_baserate" + index).val());
    $("#txt_Allowed_Qty").val($("#txt_all_qty" + index).val());
    $("#txt_PK_ITMSUBCATID").val($("#txt_Pk_" + index).val());
}


$("#btn_Upd").click(function () {
    if ($("#txt_cat_Id").val() == "") {
        $.jAlert({
            'content': 'Please Select Item Category..!'
        });
        return false;
    }
    if ($("#txt_itmsbcat_name").val() == "") {
        $.jAlert({
            'content': 'Please Enter Item Subcategory..!'
        });
        return false;
    }
    if ($("#txt_itmsbcat_code").val() == "") {
        $.jAlert({
            'content': 'Please Enter Item Subcategory code..!'
        });
        return false;
    }
    if ($("#txt_Base_Rate").val() == "") {
        $.jAlert({
            'content': 'Please Enter Base Rate..!'
        });
        return false;
    }
    if ($("#txt_Allowed_Qty").val() == "") {
        $.jAlert({
            'content': 'Please Enter Allowed Quantity..!'
        });
        return false;
    }

    PageMethods.UpdateData($("#txt_PK_ITMSUBCATID").val(), ItemCatId, $("#txt_itmsbcat_name").val(), $("#txt_itmsbcat_code").val(), $("#txt_Base_Rate").val(), $("#txt_Allowed_Qty").val(), Client_ID, Master_API, function (response) {
        if (JSON.parse(response) == "OK") {
            $.jAlert({
                'content': 'Data Edited Successfully..!'
            });

            GetItemSubCatData();
            $("#txt_cat_Id").val("");
            $("#txt_itmsbcat_name").val("");
            $("#txt_itmsbcat_code").val("");
            $("#txt_Base_Rate").val("");
            $("#txt_Allowed_Qty").val("");
            $("#btn_Save").show();
            $("#btn_Upd").hide();
        }
        else if (JSON.parse(response) == "NotFound") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(response) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Saved Due To Internal Server Error..!'
            });
        }
    }, function (error) {
    });
});

function DeleteData(index) {
    $("#txt_PK_ITMSUBCATID").val($("#txt_Pk_" + index).val());
    PageMethods.Deletedata($("#txt_PK_ITMSUBCATID").val(), Client_ID, Master_API, function (response) {
        if (JSON.parse(response) == "OK") {
            $.jAlert({
                'content': 'Data Deleted Successfully..!'
            });
            GetItemSubCatData();
        }
        else {
            $.jAlert({
                'content': 'Data Not Deleted..!'
            });
        }
    }, function (error) {
    });
}

$("#btn_Cancel").click(function () {
    window.location.replace("../../HomePage.aspx");
});