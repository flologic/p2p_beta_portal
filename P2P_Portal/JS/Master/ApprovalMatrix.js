﻿var Client_ID = '', Master_API = '';
//var BUdgetSubHeadJson = '';
var UserJson = [], ApprovarID;
var CatJson = '';
var ApprovarJson = '';

$(document).ready(function () {
    Client_ID = $("#txt_Client_ID").val();
    Master_API = $("#txt_Master_URL").val();
    getUser();
    getCattypedata();
    getApprovarData();
});


function getUser() {
    PageMethods.GetUser(Client_ID, Master_API, function (response) {
        UserJson = JSON.parse(JSON.parse(response[0]));
        if (UserJson.Message == "Data Not Found") {
            $.jAlert({
                'content': 'City Data Not Found..!'
            });
        }
        else {
            BindUser();
        }
    },
        function (error) {
        });
}

function BindUser() {//response
    // var UserJson = JSON.parse(JSON.parse(response.d));
    $("#sel_Approvar").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: UserJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#sel_Approvar").val(ui.item.label);
            ApprovarID = ui.item.id;
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}



function getCattypedata() {
    PageMethods.GetCatType(Client_ID, Master_API, function (response) {
        CatJson = JSON.parse(JSON.parse(response[0]));
        if (CatJson.Message == "Data Not Found") {
            $.jAlert({
                'content': 'Category Data Not Found..!'
            });
        }
        else {
            BindCategory();
        }
    },
        function (error) {
        });
}

function BindCategory() {//response
    // var UserJson = JSON.parse(JSON.parse(response.d));
    $("#Sel_Cat_type").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: CatJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#Sel_Cat_type").val(ui.item.label);
            CattypeID = ui.item.id;
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}



function getApprovarData() {
    PageMethods.GetApproverData(Client_ID, Master_API, function (response) {
        ApprovarJson = JSON.parse(JSON.parse(response[0]));
        if (ApprovarJson.Message == "Data Not Found") {
            $.jAlert({
                'content': 'Approvar Data Not Found..!'
            });
        }
        else {
            bindApprovarData();
        }

    },
     function (error) {
     });
}


function bindApprovarData() {
    if (ApprovarJson) {
        var tableheader = "";
        var tableBody = "";
        tableheader += "<th style='width: 2%;'>#</th>";
        tableheader += "<th>Category Type</th>";
        tableheader += "<th>Approvar</th>";
        tableheader += "<th>Sequence Number</th>";
        tableheader += "<th>Edit</th>";
        tableheader += "<th>Delete</th>";

        $.each(ApprovarJson, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            
            //tableBody += "<td>" + val.CAT_TYPE + "<input class='form-control input-sm' type='text' id='txt_Cat_Type" + (key + 1) + "' value='" + val.CAT_TYPE + "' style='display:none'></input>";
            tableBody += "<td>" + val.CAT_NAME + "<input class='form-control input-sm' type='text' id='txt_Cat_Type" + (key + 1) + "' value='" + val.CAT_NAME + "' style='display:none'></input>";
            tableBody += "<input class='form-control input-sm' type='text' id='txt_cat_typeID" + (key + 1) + "' value='" + val.FK_CAT_ID + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.APPROVAR_NAME + "<input class='form-control input-sm' type='text' id='txt_Approvar" + (key + 1) + "' value='" + val.APPROVAR_NAME + "' style='display:none'></input>";
            tableBody += "<input class='form-control input-sm' type='text' id='txt_ApprovarID" + (key + 1) + "' value='" + val.APPROVARID + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.LINENUMBER + "<input class='form-control input-sm' type='text' id='txt_LinNumber" + (key + 1) + "' value='" + val.LINENUMBER + "' style='display:none'></input>";
            tableBody += "<td><a><img src='../../assets/images/edit.png' onclick='return bindData(" + (key + 1) + ")' title='Add New Row' /></a><input class='form-control input-sm' type='text' id='txt_Pk" + (key + 1) + "' value='" + val.PK_ID + "' style='display:none'></input></td>";
            tableBody += "<td><a><img src='../../assets/images/delete.png' onclick='return DeleteData(" + (key + 1) + ")' title='delete Row' /></a></td>";
            tableBody += "</tr>";
        });
        var table = "<table ID='Approvartable' class='display nowrap dataTable'>" +
           "<thead><tr>" + tableheader + "</tr></thead>" +
           "<tbody>" + tableBody + "<tbody>" +
        "</table>";
        $("#div_displayDtl").empty();
        $("#div_displayDtl").html(table);
        $("#Approvartable").dataTable({
            dom: 'Bfrtip', "bSort": true,
            buttons: [
                'excel'
            ]
        });
    }
}

function bindData(index) {
    $("#btn_Save").hide();
    $("#btn_Upd").show();
    CattypeID = $("#txt_cat_typeID" + index).val();
    $("#Sel_Cat_type").val($("#txt_Cat_Type" + index).val());
    ApprovarID = $("#txt_ApprovarID" + index).val()
    $("#sel_Approvar").val($("#txt_Approvar" + index).val());
    $("#ddlAction").val($("#txt_LinNumber" + index).val());
    $("#txt_PK_APprovar_ID").val($("#txt_Pk" + index).val());
}

$("#btn_Save").click(function () {
    if ($("#Sel_Cat_type").val() == "") {
        $.jAlert({
            'content': 'Please select Category Type..!'
        });
        return false;
    }
    if ($("#sel_Approvar").val() == "") {
        $.jAlert({
            'content': 'Please select Approvar..!'
        });
        return false;
    }
    if ($("#ddlAction").val() == "") {
        $.jAlert({
            'content': 'Please select Sequence Number..!'
        });
        return false;
    }

    PageMethods.SaveData(Client_ID, Master_API, CattypeID, ApprovarID, $("#ddlAction").val(), $("#txt_Username").val(), function (response) {
        if (JSON.parse(response) == "Save") {
            $.jAlert({
                'content': 'Data Saved Successfully..!'
            });
            getApprovarData();
            $("#Sel_Cat_type").val("");
            $("#sel_Approvar").val("");
            $("#ddlAction").val("");
          
        }
        else if (JSON.parse(response) == "Duplicate") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(response) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Saved Due To Internal Server Error..!'
            });
        }
    }, function (error) {
    });
});

$("#btn_Upd").click(function () {
    PageMethods.UpdateData(Client_ID, Master_API, $("#txt_PK_APprovar_ID").val(), CattypeID, ApprovarID, $("#ddlAction").val(), $("#txt_Username").val(), function (response) {
        if (JSON.parse(response) == "Update") {
            $.jAlert({
                'content': 'Data Updated Successfully..!'
            });

            getApprovarData();
            $("#Sel_Cat_type").val("");
            $("#sel_Approvar").val("");
            $("#ddlAction").val("");
            $("#btn_Save").show();
            $("#btn_Upd").hide();
        }
        else if (JSON.parse(response) == "Duplicate") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(response) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Update Due To Internal Server Error..!'
            });
        }

    }, function (error) {
    });
});

function DeleteData(index) {
    $.jAlert({
        'title': 'Confirmation',
        'content': 'Are you sure to delete this record..?', 'type': 'confirm',
        'onConfirm': function () {
            $("#txt_PK_APprovar_ID").val($("#txt_Pk" + index).val());
            PageMethods.Deletedata(Client_ID, Master_API, $("#txt_PK_APprovar_ID").val(), $("#txt_Username").val(), function (response) {
                if (JSON.parse(response) == "Delete") {
                    $.jAlert({
                        'content': 'Data Deleted Successfully..!'
                    });

                    getApprovarData();
                }
                else if (JSON.parse(response) == "NotFound") {
                    $.jAlert({
                        'content': 'Data Not Delete..!'
                    });
                }
                else if (JSON.parse(response) == "InternalServerError") {
                    $.jAlert({
                        'content': 'Data Not Delete Due To Internal Server Error..!'
                    });
                }
            }, function (error) {
            });
        }
    });
}

$("#btn_Cancel").click(function () {
    window.location.replace("../../HomePage.aspx");
});
