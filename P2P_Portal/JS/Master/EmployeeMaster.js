﻿var Client_ID = '', Master_API = '';
var DesigJson = "", DesigID = 0;
var BranchJson = "", BranchID = 0;
var DepartmentJson = "", DepartmentID = 0;
var CostCenterJson = "", CostCenterID = 0;
var GradeJson = "", GradeID = 0;
var UserJson = [], UserID;
var HRJson = [], HRID;
var CompanyJson = [], CompanyID;
var EmpJson = "";

$(document).ready(function () {
    Client_ID = $("#txt_Client_ID").val();
    Master_API = $("#txt_Master_URL").val();
    bindCostCenter();
    //GetEmpData();
});

$(document).on('focus', '.datepicker-rtl', function () {
    $(this).datepicker({
        dateFormat: 'dd-M-yy', autoclose: true, todayBtn: 'linked', maxDate: 0, //maybe you want something like this
        onClose: function () {
            $('.datepicker-rtl').removeClass('hasDatepicker');
        }
    });
});

function checkValid() {
    var email = $("#txt_emailid").val();
    if (isEmail(email))
    { }
    else
    {
        $.jAlert({
            'content': 'Please Enter Valid EmailID',
            'onClose': function () {
                $('#txt_emailid').focus();
            }
        });
        return false;
    }
}

function bindDesignation() {
    var Desig = $("#txt_designation").val();
    CapLetters("txt_designation");
    if (Desig.length >= 3) {
        PageMethods.GetDesignation(Client_ID, Master_API, Desig, function (response) {
            DesigJson = JSON.parse(JSON.parse(response[0]));
            if (DesigJson.Message == "Data Not Found") {
                $.jAlert({
                    'content': 'Designation Data Not Found..!'
                });
            }
            else {
                BindDesig();
            }
        },
            function (error) {
            });
    }
}

function BindDesig() {
    $("#txt_designation").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: DesigJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#txt_designation").val(ui.item.label);
            DesigID = ui.item.id;
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function bindBranch() {
    var Branch = $("#txt_branchnm").val();
    CapLetters("txt_branchnm");
    if (Branch.length >= 3) {
        PageMethods.GetBranch(Client_ID, Master_API, Branch, function (response) {
            BranchJson = JSON.parse(JSON.parse(response[0]));
            if (BranchJson.Message == "Data Not Found") {
                $.jAlert({
                    'content': 'Branch Data Not Found..!'
                });
            }
            else {
                BindBranch();
            }
        },
        function (error) {
        });
    }
}

function BindBranch() {
    $("#txt_branchnm").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: BranchJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#txt_branchnm").val(ui.item.label);
            BranchID = ui.item.id;
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function bindDepartment() {
    var Depart = $("#txt_Dept_name").val();
    CapLetters("txt_Dept_name");
    if (Depart.length >= 3) {
        PageMethods.GetDepartment(Client_ID, Master_API, Depart, function (response) {
            DepartmentJson = JSON.parse(JSON.parse(response[0]));
            if (DepartmentJson.Message == "Data Not Found") {
                $.jAlert({
                    'content': 'Department Data Not Found..!'
                });
            }
            else {
                BindDepartment();

            }
        },
            function (error) {
            });
    }
}

function BindDepartment() {
    $("#txt_Dept_name").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: DepartmentJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#txt_Dept_name").val(ui.item.label);
            DepartmentID = ui.item.id;
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function bindCostCenter() {
    PageMethods.GetCostCenter(Client_ID, Master_API, function (response) {
        CostCenterJson = JSON.parse(JSON.parse(response[0]));
        if (CostCenterJson.Message == "Data Not Found") {
            $.jAlert({
                'content': 'CostCenter Data Not Found..!'
            });
        }
        else {
            BindCostCenter();
            // bindGrade();

        }
    },
        function (error) {
        });
}

function BindCostCenter() {
    $("#txt_Costcenter").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: CostCenterJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#txt_Costcenter").val(ui.item.label);
            CostCenterID = ui.item.id;
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
            }
        }
    }).focus(function () {
        // $(this).autocomplete("search", "");
    });
}

function bindGrade() {
    var Grade = $("#txt_grade").val();
    CapLetters("txt_grade");
    if (Grade.length >= 3) {
        PageMethods.GetGrade(Client_ID, Master_API, Grade, function (response) {
            GradeJson = JSON.parse(JSON.parse(response[0]));
            if (GradeJson.Message == "Data Not Found") {
                $.jAlert({
                    'content': 'Grade Data Not Found..!'
                });
            }
            else {
                BindGrade();
            }
        },
        function (error) {
        });
    }
}

function BindGrade() {
    $("#txt_grade").autocomplete({
        autoFocus: true,
        minLength: 3,
        source: GradeJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#txt_grade").val(ui.item.label);
            GradeID = ui.item.id;
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                //  $(this).val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function bindUser() {
    var Manager = $("#txt_Manager").val();
    CapLetters("txt_Manager");
    if (Manager.length >= 3) {
        PageMethods.GetUser(Client_ID, Master_API, Manager, function (response) {
            UserJson = JSON.parse(JSON.parse(response[0]));
            if (UserJson.Message == "Data Not Found") {
                $.jAlert({
                    'content': 'Manager Data Not Found..!'
                });
            }
            else {
                BindUser();
            }
        },
        function (error) {
        });
    }
}

function BindUser() {
    $("#txt_Manager").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: UserJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#txt_Manager").val(ui.item.label);
            UserID = ui.item.id;
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function bindHR() {
    var HR = $("#txt_HR").val();
    CapLetters("txt_HR");
    if (HR.length >= 3) {
        PageMethods.GetUser(Client_ID, Master_API, HR, function (response) {
            HRJson = JSON.parse(JSON.parse(response[0]));
            if (HRJson.Message == "Data Not Found") {
                $.jAlert({
                    'content': 'HR Data Not Found..!'
                });
            }
            else {
                BindHR();
            }
        },
        function (error) {
        });
    }
}

function BindHR() {
    $("#txt_HR").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: HRJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#txt_HR").val(ui.item.label);
            HRID = ui.item.id;
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function bindCompany() {
    var Company = $("#txt_Company").val();
    CapLetters("txt_Company");
    if (Company.length >= 3) {
        PageMethods.GetCompany(Client_ID, Master_API, Company, function (response) {
            CompanyJson = JSON.parse(JSON.parse(response[0]));
            if (CompanyJson.Message == "Data Not Found") {
                $.jAlert({
                    'content': 'Company Data Not Found..!'
                });
            }
            else {
                BindCompany();
            }
        },
        function (error) {
        });
    }
}

function BindCompany() {
    $("#txt_Company").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: CompanyJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#txt_Company").val(ui.item.label);
            CompanyID = ui.item.id;
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function GetEmpData() {
    var temp = $("#txt_Subject").val();
    CapLetters("txt_Subject");
    if (temp.length >= 3) {

        PageMethods.GetEmployeeData(Client_ID, Master_API, temp, function (response) {
            EmpJson = JSON.parse(JSON.parse(response[0]));
            if (EmpJson.Message == "Data Not Found") {
                $.jAlert({
                    'content': 'Employee Data Not Found..!'
                });
            }
            else {
                EmployeeOnSuccess();
            }
        },
            function (error) {
            });
    }
}

function EmployeeOnSuccess() {
    if (EmpJson) {
        var tableHeader = "";
        var tableBody = "";
        tableHeader += "<th style='width: 3%'>#</th>";
        tableHeader += "<th>Employee Name</th>";
        tableHeader += "<th>Employee Code</th>";
        tableHeader += "<th>Mobile No</th>";
        tableHeader += "<th>Address</th>";
        tableHeader += "<th>Email Id</th>";
        tableHeader += "<th>Designation</th>";
        tableHeader += "<th>Branch</th>";
        tableHeader += "<th>Department</th>";
        tableHeader += "<th>Date of Joining</th>";
        tableHeader += "<th>Date of Birth</th>";
        tableHeader += "<th>Date of last working Day</th>";
        //tableHeader += "<th>Vertical</th>";
        tableHeader += "<th>Cost Center</th>";
        tableHeader += "<th>IFSC Code</th>";
        tableHeader += "<th>Grade</th>";
        tableHeader += "<th>Admin Reporting</th>";
        tableHeader += "<th>HR Admin</th>";
        tableHeader += "<th>Company</th>";
        tableHeader += "<th style='width: 5%'>Edit</th>";
        tableHeader += "<th style='width: 5%'>Delete</th>";

        $.each(EmpJson, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.label + "<input class='form-control input-sm' type='text' id='txt_emp_Name" + (key + 1) + "' value='" + val.label + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.EMP_CODE + "<input class='form-control input-sm' type='text' id='txt_emp_Code" + (key + 1) + "' value='" + val.EMP_CODE + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.MOBILE_NO + "<input class='form-control input-sm' type='text' id='txt_mobno" + (key + 1) + "' value='" + val.MOBILE_NO + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.ADDRESS + "<input class='form-control input-sm' type='text' id='txt_add" + (key + 1) + "' value='" + val.ADDRESS + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.EMAILID + "<input class='form-control input-sm' type='text' id='txt_emailid" + (key + 1) + "' value='" + val.EMAILID + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.DESIGNATION_NAME + "<input class='form-control input-sm' type='text' id='txt_degnname" + (key + 1) + "' value='" + val.DESIGNATION_NAME + "' style='display:none'><input class='form-control input-sm' type='text' id='txt_designtn" + (key + 1) + "' value='" + val.FK_DESIGNATION_ID + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.BRANCH_NAME + "<input class='form-control input-sm' type='text' id='txt_brnchname" + (key + 1) + "' value='" + val.BRANCH_NAME + "' style='display:none'><input class='form-control input-sm' type='text' id='txt_brnch" + (key + 1) + "' value='" + val.FK_BRANCH_ID + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.DEPARTMENT_NAME + "<input class='form-control input-sm' type='text' id='txt_deptname" + (key + 1) + "' value='" + val.DEPARTMENT_NAME + "' style='display:none'><input class='form-control input-sm' type='text' id='txt_deprt" + (key + 1) + "' value='" + val.FK_DEPARTMENT_ID + "' style='display:none'></input></td>";

            tableBody += "<td>" + val.DATE_JOINING + "<input class='form-control input-sm' type='text' id='txt_doj" + (key + 1) + "' value='" + val.DATE_JOINING + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.DATE_BIRTH + "<input class='form-control input-sm' type='text' id='txt_dob" + (key + 1) + "' value='" + val.DATE_BIRTH + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.LAST_WORKING_DATE + "<input class='form-control input-sm' type='text' id='txt_Lwd" + (key + 1) + "' value='" + val.LAST_WORKING_DATE + "' style='display:none'></input></td>";
            // tableBody += "<td>" + val.VERTICAL_NAME + "<input class='form-control input-sm' type='text' id='txt_vertname" + (key + 1) + "' value='" + val.VERTICAL_NAME + "' style='display:none'><input class='form-control input-sm' type='text' id='txt_vert" + (key + 1) + "' value='" + val.FK_VERTICAL_ID + "' style='display:none'></input></td>";
            if (val.COST_CENTER_NAME == null) {
                tableBody += "<td><input class='form-control input-sm' type='text' id='txt_costname" + (key + 1) + "' value='' style='display:none'><input class='form-control input-sm' type='text' id='txt_cost" + (key + 1) + "' value='0' style='display:none'></input></td>";
            }
            else {
                tableBody += "<td>" + val.COST_CENTER_NAME + "<input class='form-control input-sm' type='text' id='txt_costname" + (key + 1) + "' value='" + val.COST_CENTER_NAME + "' style='display:none'><input class='form-control input-sm' type='text' id='txt_cost" + (key + 1) + "' value='" + val.FK_COST_CENTRE_ID + "' style='display:none'></input></td>";
            }
            tableBody += "<td>" + val.IFSCODE + "<input class='form-control input-sm' type='text' id='txt_Ifsc" + (key + 1) + "' value='" + val.IFSCODE + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.GRADE + "<input class='form-control input-sm' type='text' id='grade" + (key + 1) + "' value='" + val.GRADE + "' style='display:none'><input class='form-control input-sm' type='text' id='txt_grade" + (key + 1) + "' value='" + val.FK_GRADE_ID + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.FUNCTIONALREPORTING_NAME + "<input class='form-control input-sm' type='text' id='txt_Admin" + (key + 1) + "' value='" + val.FUNCTIONALREPORTING_NAME + "' style='display:none'><input class='form-control input-sm' type='text' id='txt_AdminID" + (key + 1) + "' value='" + val.FUNCTIONALREPORTING_TO + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.HR_NAME + "<input class='form-control input-sm' type='text' id='HR" + (key + 1) + "' value='" + val.HR_NAME + "' style='display:none'><input class='form-control input-sm' type='text' id='txt_HRID" + (key + 1) + "' value='" + val.REGIONAL_HR + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.COMPANY_NAME + "<input class='form-control input-sm' type='text' id='txt_CompanyName" + (key + 1) + "' value='" + val.COMPANY_NAME + "' style='display:none'><input class='form-control input-sm' type='text' id='txt_CompanyID" + (key + 1) + "' value='" + val.FK_COMPANY_ID + "' style='display:none'></input></td>";

            tableBody += "<td class='click_Edit'><img src='../../assets/images/edit.png' title='Edit' alt='Edit'/></td>";
            tableBody += "<td class='click_Delete'><img src='../../assets/images/delete.png' onclick='return DeleteData(" + (key + 1) + ")' title='Delete' alt='Delete'/></td>";
            tableBody += "</tr>";
        })
        var table = "<table class='display nowrap' id='tbl_Employee'>" +
              "<thead><tr>" + tableHeader + "</tr></thead>" +
              "<tbody>" + tableBody + "</tbody>" +
              "</table>";
        $("#div_Employee").empty();
        $("#div_Employee").html(table);

        $("#tbl_Employee").DataTable({
            dom: 'Bfrtip', "bSort": true,
            buttons: [
                'excel'
            ]
        });
        $("#loading-div-background").hide();
    }
}

$("#btn_Save").click(function () {
    SubmitData("Save");
});

$("#btn_Upd").click(function () {
    SubmitData("Update");
});

function SubmitData(Value) {
    var EmpName = $("#txt_Empname").val();
    var Empcode = $("#txt_empcode").val();
    var MobNo = $("#txt_Mobno").val();
    var Addrss = $("#txt_address").val();
    var emailId = $("#txt_emailid").val();
    var Desig = $("#txt_designation").val();
    var Branch = $("#txt_branchnm").val();
    var Depart = $("#txt_Dept_name").val();
    var dtofbirth = $("#txt_dob").val();
    var dtofJoin = $("#txt_dojin").val();
    var dtofLWD = $("#txt_lwd").val();
    var CostCenter = $("#txt_Costcenter").val();
    var IfscCode = $("#txt_ifsccode").val();
    var Grade = $("#txt_grade").val();
    var Manager = $("#txt_Manager").val();
    var HR = $("#txt_HR").val();
    var Company = $("#txt_Company").val();

    if (EmpName == "") {
        $.jAlert({
            'content': 'Please Enter Employee Name.',
            'onClose': function () {
                $('#txt_Empname').focus();
            }
        });
        return false;
    }
    else if (Empcode == "") {
        $.jAlert({
            'content': 'Please Enter Employee Code.',
            'onClose': function () {
                $('#txt_empcode').focus();
            }
        });
        return false;
    }
    else if (MobNo == "") {
        $.jAlert({
            'content': 'Please Enter Mobile Number.',
            'onClose': function () {
                $('#txt_Mobno').focus();
            }
        });
        return false;
    }
    else if (Addrss == "") {
        $.jAlert({
            'content': 'Please Enter Employee Address.',
            'onClose': function () {
                $('#txt_address').focus();
            }
        });
        return false;
    }
    else if (emailId == "") {
        $.jAlert({
            'content': 'Please Enter Email Id.',
            'onClose': function () {
                $('#txt_emailid').focus();
            }
        });
        return false;
    }
    else if (Desig == "") {
        $.jAlert({
            'content': 'Please Select Designation',
            'onClose': function () {
                $('#txt_designation').focus();
            }
        });
        return false;
    }
    else if (Branch == "") {
        $.jAlert({
            'content': 'Please Select Branch.',
            'onClose': function () {
                $('#txt_branchnm').focus();
            }
        });
        return false;
    }
    else if (Depart == "") {
        $.jAlert({
            'content': 'Please Select Department.',
            'onClose': function () {
                $('#txt_Dept_name').focus();
            }
        });
        return false;
    }
    else if (dtofbirth == "") {
        $.jAlert({
            'content': 'Please Select Date of Birth.',
            'onClose': function () {
                $('#txt_dob').focus();
            }
        });
        return false;
    }
    else if (dtofJoin == "") {
        $.jAlert({
            'content': 'Please Select Date of Joining.',
            'onClose': function () {
                $('#txt_dojin').focus();
            }
        });
        return false;
    }
    else if (dtofLWD == "") {
        $.jAlert({
            'content': 'Please Select Last working Day.',
            'onClose': function () {
                $('#txt_lwd').focus();
            }
        });
        return false;
    }
    else if (CostCenter == "") {
        $.jAlert({
            'content': 'Please Select Cost Center',
            'onClose': function () {
                $('#txt_Costcenter').focus();
            }
        });
        return false;
    }
    else if (IfscCode == "") {
        $.jAlert({
            'content': 'Please Enter IFSC code.',
            'onClose': function () {
                $('#txt_ifsccode').focus();
            }
        });
        return false;
    }
    else if (Grade == "") {
        $.jAlert({
            'content': 'Please Select Grade.',
            'onClose': function () {
                $('#txt_grade').focus();
            }
        });
        return false;
    }
    if (Manager == "") {
        $.jAlert({
            'content': 'Please Select Admin Reporting',
            'onClose': function () {
                $('#txt_Manager').focus();
            }
        });
        return false;
    }
    if (HR == "") {
        $.jAlert({
            'content': 'Please Select HR Admin',
            'onClose': function () {
                $('#txt_HR').focus();
            }
        });
        return false;
    }
    if (Company == "") {
        $.jAlert({
            'content': 'Please Select Company',
            'onClose': function () {
                $('#txt_Company').focus();
            }
        });
        return false;
    }

    PageMethods.submitData(Client_ID, Master_API, EmpName, Empcode, MobNo, Addrss, emailId, DesigID, BranchID, DepartmentID, dtofbirth, dtofJoin, dtofLWD, CostCenterID, IfscCode, GradeID, UserID, HRID, CompanyID, $('#txt_Username').val(), Value, function (response) {
        if (JSON.parse(response) == "OK") {
            $.jAlert({
                'content': 'Data Saved Successfully..!'
            });
            $("#btn_Save").show();
            $("#btn_Upd").hide();
            $("#txt_Empname").val("");
            $("#txt_empcode").val("");
            $("#txt_Mobno").val("");
            $("#txt_address").val("");
            $("#txt_emailid").val("");
            $("#txt_designation").val("");
            $("#txt_branchnm").val("");
            $("#txt_Dept_name").val("");
            $("#txt_dob").val("");
            $("#txt_dojin").val("");
            $("#txt_lwd").val("");
            $("#txt_Costcenter").val("");
            $("#txt_ifsccode").val("");
            $("#txt_grade").val("");
            $("#txt_Manager").val("");
            $("#txt_HR").val("");
            $("#txt_Company").val("");
            $("#div_Employee").empty();
        }
        else if (JSON.parse(response) == "NotFound") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(response) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Saved Due To Internal Server Error..!'
            });
        }
        
    }, function (error) {
        $.jAlert({
            'content': error,
        });
    });
}

$('body').on('click', '.click_Edit', function () {
    var $row = $(this).closest('tr');
    var table = $('#tbl_Employee').DataTable();
    var data = table.row($row).data();
    $("#txt_Empname").prop('disabled', true);
    $("#txt_empcode").prop('disabled', true);
    $("#txt_Empname").val(EmpJson[parseInt(data[0]) - 1].EMP_NAME);
    $("#txt_empcode").val(EmpJson[parseInt(data[0]) - 1].EMP_CODE);
    $("#txt_Mobno").val(EmpJson[parseInt(data[0]) - 1].MOBILE_NO);
    $("#txt_address").val(EmpJson[parseInt(data[0]) - 1].ADDRESS);
    $("#txt_emailid").val(EmpJson[parseInt(data[0]) - 1].EMAILID);
    DesigID = EmpJson[parseInt(data[0]) - 1].FK_DESIGNATION_ID;
    $("#txt_designation").val(EmpJson[parseInt(data[0]) - 1].DESIGNATION_NAME);
    BranchID = EmpJson[parseInt(data[0]) - 1].FK_BRANCH_ID;
    $("#txt_branchnm").val(EmpJson[parseInt(data[0]) - 1].BRANCH_NAME);
    DepartmentID = EmpJson[parseInt(data[0]) - 1].FK_DEPARTMENT_ID;
    $("#txt_Dept_name").val(EmpJson[parseInt(data[0]) - 1].DEPARTMENT_NAME);
    $("#txt_dojin").val(EmpJson[parseInt(data[0]) - 1].DATE_JOINING);
    $("#txt_dob").val(EmpJson[parseInt(data[0]) - 1].DATE_BIRTH);
    $("#txt_lwd").val(EmpJson[parseInt(data[0]) - 1].LAST_WORKING_DATE);
    //Verticalid = EmpJson[parseInt(data[0]) - 1].FK_VERTICAL_ID;
    //$("#txt_vertical").val(EmpJson[parseInt(data[0]) - 1].VERTICAL_NAME);
    CostCenterID = EmpJson[parseInt(data[0]) - 1].FK_COST_CENTRE_ID;
    $("#txt_Costcenter").val(EmpJson[parseInt(data[0]) - 1].COST_CENTER_NAME);
    $("#txt_ifsccode").val(EmpJson[parseInt(data[0]) - 1].IFSCODE);
    GradeID = EmpJson[parseInt(data[0]) - 1].FK_GRADE_ID;
    $("#txt_grade").val(EmpJson[parseInt(data[0]) - 1].GRADE);
    UserID = EmpJson[parseInt(data[0]) - 1].FUNCTIONALREPORTING_TO;
    $("#txt_Manager").val(EmpJson[parseInt(data[0]) - 1].FUNCTIONALREPORTING_NAME);
    HRID = EmpJson[parseInt(data[0]) - 1].REGIONAL_HR;
    $("#txt_HR").val(EmpJson[parseInt(data[0]) - 1].HR_NAME);
    CompanyID = EmpJson[parseInt(data[0]) - 1].FK_COMPANY_ID;
    $("#txt_Company").val(EmpJson[parseInt(data[0]) - 1].COMPANY_NAME);
    EmpPKid = EmpJson[parseInt(data[0]) - 1].PK_EMP_ID;
    $("#btn_Save").hide();
    $("#btn_Upd").show();
});

function DeleteData(index) {
    $.jAlert({
        'title': 'Confirmation',
        'content': 'Are you sure to delete this record..?', 'type': 'confirm',
        'onConfirm': function () {
            PageMethods.Deletedata(Client_ID, Master_API, $("#txt_emp_Code" + index).val(), $("#txt_Username").val(), function (response) {
                if (JSON.parse(response) == "OK") {
                    $.jAlert({
                        'content': 'Data Deleted Successfully..!'
                    });
                    $("#btn_Save").show();
                    $("#btn_Upd").hide();
                    $("#txt_Empname").val("");
                    $("#txt_empcode").val("");
                    $("#txt_Mobno").val("");
                    $("#txt_address").val("");
                    $("#txt_emailid").val("");
                    $("#txt_designation").val("");
                    $("#txt_branchnm").val("");
                    $("#txt_Dept_name").val("");
                    $("#txt_dob").val("");
                    $("#txt_dojin").val("");
                    $("#txt_lwd").val("");
                    $("#txt_Costcenter").val("");
                    $("#txt_ifsccode").val("");
                    $("#txt_grade").val("");
                    $("#txt_Manager").val("");
                    $("#txt_HR").val("");
                    $("#txt_Company").val("");
                    $("#div_Employee").empty();
                }
                else if (JSON.parse(response) == "NotFound") {
                    $.jAlert({
                        'content': 'Data Not Delete..!'
                    });
                }
                else if (JSON.parse(response) == "InternalServerError") {
                    $.jAlert({
                        'content': 'Data Not Delete Due To Internal Server Error..!'
                    });
                }
            }, function (error) {
            });
        }
    });
}

$("#btn_Cancel").click(function () {
    window.location.replace("../../HomePage.aspx");
});