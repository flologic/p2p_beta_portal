﻿var Client_ID = '', Master_API = '';
var UserJson = [], UserID, CityId;
var CatDtl = '';

$(document).ready(function () {
    Client_ID = $("#txt_Client_ID").val();
    Master_API = $("#txt_Master_URL").val();
    addmodalpopup();
    $("#loading-div-background").show();
    getLocationData();
    getCityData();
});


function getCityData() {
    PageMethods.GetCityData(Client_ID, Master_API, function (response) {
        ItemJson = JSON.parse(JSON.parse(response[0]));
        BindCity();
    },
        function (error) {
        });
}

function BindCity() {
    $("#Sel_City_Name").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: ItemJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#Sel_City_Name").val(ui.item.label);
            CityId = ui.item.id;
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}


function getLocationData() {
    PageMethods.GetLocationData(Client_ID, Master_API, function (response) {
        CatDtl = JSON.parse(JSON.parse(response[0]));
        if (CatDtl.Message == "Data Not Found") {
            alert("Location Data Not Found");
            $("#loading-div-background").hide();
        }
        else {
            LocationOnSuccess();
        }
    },
        function (error) {
        });
}

function LocationOnSuccess() {
    if (CatDtl) {
        var tableHeader = "";
        var tableBody = "";
        tableHeader += "<th style='width: 2%;'>#</th>";
        tableHeader += "<th>City Name</th>";
        tableHeader += "<th>Location Name</th>";
        tableHeader += "<th>Location Code</th>";
        tableHeader += "<th>Edit</th>";
        tableHeader += "<th>Delete</th>";
        $.each(CatDtl, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.CityName + "<input class='form-control input-sm' type='text' id='txt_CityName" + (key + 1) + "' value='" + val.CityName + "' style='display:none'><input class='form-control input-sm' type='text' id='txt_CityId" + (key + 1) + "' value='" + val.FK_CITYID + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.label + "<input class='form-control input-sm' type='text' id='txt_Location" + (key + 1) + "' value='" + val.label + "' style='display:none'></input>";
            tableBody += "<td>" + val.LocationCode + "<input class='form-control input-sm' type='text' id='txt_LocationCode" + (key + 1) + "' value='" + val.LocationCode + "' style='display:none'></input>";
            tableBody += "<td><a><img src='../../assets/images/edit.png' onclick='return bindData(" + (key + 1) + ")' title='Add New Row' /></a><input class='form-control input-sm' type='text' id='txt_Pk" + (key + 1) + "' value='" + val.id + "' style='display:none'></input></td>";
            tableBody += "<td><a><img src='../../assets/images/delete.png' onclick='return DeleteData(" + (key + 1) + ")' title='delete Row' /></a></td>";
            tableBody += "</tr>";
        });
        var table = "<table ID='CatTable' class='display nowrap dataTable'>" +
              "<thead><tr>" + tableHeader + "</tr></thead>" +
              "<tbody>" + tableBody + "</tbody>" +
              "</table>";
        $("#div_displayDtl").empty();
        $("#div_displayDtl").html(table);
        $("#CatTable").dataTable({
            dom: 'Bfrtip', "bSort": true,
            buttons: [
                'excel'
            ]
        });
    }
    $("#loading-div-background").hide();
}

$("#btn_Save").click(function () {
    if ($("#Sel_City_Name").val() == "") {
        $.jAlert({
            'content': 'Please Select City Name..!'
        });
        return false;
    }
    if ($("#txt_Location_Name").val() == "") {
        $.jAlert({
            'content': 'Please Enter Location Name..!'
        });
        return false;
    }
    if ($("#txt_Location_Code").val() == "") {
        $.jAlert({
            'content': 'Please Enter Location Code..!'
        });
        return false;
    }
    PageMethods.SaveData(CityId, $("#txt_Location_Name").val(), $("#txt_Location_Code").val(), $("#txt_UserName").val(), Client_ID, Master_API, function (response) {
        if (JSON.parse(response) == "OK") {
            $.jAlert({
                'content': 'Data Saved Successfully..!'
            });
            getLocationData();
            $("#Sel_City_Name").val("");
            $("#txt_Location_Name").val("");
            $("#txt_Location_Code").val("");
        }
        else if (JSON.parse(response) == "NotFound") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(response) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Saved Due To Internal Server Error..!'
            });
        }
    }, function (error) {
    });
})

function bindData(index) {
    $("#btn_Save").hide();
    $("#btn_Upd").show();
    CityId = $("#txt_CityId" + index).val();
    $("#Sel_City_Name").val($("#txt_CityName" + index).val());
    $("#txt_Location_Name").val($("#txt_Location" + index).val());
    $("#txt_Location_Code").val($("#txt_LocationCode" + index).val());
    $("#txt_PK_LocationID").val($("#txt_Pk" + index).val());
}

$("#btn_Upd").click(function () {
    if ($("#Sel_City_Name").val() == "") {
        $.jAlert({
            'content': 'Please Select City Name..!'
        });
        return false;
    }
    if ($("#txt_Location_Name").val() == "") {
        $.jAlert({
            'content': 'Please Enter Location Name..!'
        });
        return false;
    }
    if ($("#txt_Location_Code").val() == "") {
        $.jAlert({
            'content': 'Please Enter Location Code..!'
        });
        return false;
    }
    PageMethods.UpdateData($("#txt_PK_LocationID").val(), CityId, $("#txt_Location_Name").val(), $("#txt_Location_Code").val(), Client_ID, Master_API, function (response) {
        if (JSON.parse(response) == "OK") {
            $.jAlert({
                'content': 'Data Edited Successfully..!'
            });
            getLocationData();
            CityId = "";
            $("#Sel_City_Name").val("");
            $("#txt_Location_Name").val("");
            $("#txt_Location_Code").val("");
            $("#btn_Save").show();
            $("#btn_Upd").hide();
        }
        else if (JSON.parse(response) == "NotFound") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(response) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Saved Due To Internal Server Error..!'
            });
        }
    }, function (error) {
    });
});

function DeleteData(index) {
    $("#txt_PK_LocationID").val($("#txt_Pk" + index).val());
    PageMethods.Deletedata($("#txt_PK_LocationID").val(), Client_ID, Master_API, function (response) {
        if (JSON.parse(response) == "OK") {
            $.jAlert({
                'content': 'Data Deleted Successfully..!'
            });
            getLocationData();
        }
        else {
            $.jAlert({
                'content': 'Data Not Deleted..!'
            });
        }
    }, function (error) {
    });
}

$("#btn_Cancel").click(function () {
    window.location.replace("../../HomePage.aspx");
});