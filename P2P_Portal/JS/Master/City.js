﻿var Client_ID = '', Master_API = '';
var UserJson = [], UserID;
var ZoneDtl = '';

$(document).ready(function () {
    Client_ID = $("#txt_Client_ID").val();
    Master_API = $("#txt_Master_URL").val();
    addmodalpopup();
    $("#loading-div-background").show();
    GetCityData();
});

function GetCityData() {
    PageMethods.GetCityData(Client_ID, Master_API, function (response) {
        ZoneDtl = JSON.parse(JSON.parse(response[0]));
        if (ZoneDtl.Message == "Data Not Found") {
            alert("City Data Not Found");
            $("#loading-div-background").hide();
        }
        else {
            CityOnSuccess();
        }
    },
        function (error) {
        });
}

function CityOnSuccess() {
    if (ZoneDtl) {
        var tableHeader = "";
        var tableBody = "";
        tableHeader += "<th style='width: 2%;'>#</th>";
        tableHeader += "<th>City Name</th>";
        tableHeader += "<th>City Code</th>";
        tableHeader += "<th>Edit</th>";
        tableHeader += "<th>Delete</th>";
        $.each(ZoneDtl, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.label + "<input class='form-control input-sm' type='text' id='txt_City_name" + (key + 1) + "' value='" + val.label + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.CITY_CODE + "<input class='form-control input-sm' type='text' id='txt_City_Code" + (key + 1) + "' value='" + val.CITY_CODE + "' style='display:none'></input>";
            tableBody += "<td><a><img src='../../assets/images/edit.png' onclick='return bindData(" + (key + 1) + ")' title='Add New Row' /></a><input class='form-control input-sm' type='text' id='txt_Pk" + (key + 1) + "' value='" + val.id + "' style='display:none'></input></td>";
            tableBody += "<td><a><img src='../../assets/images/delete.png' onclick='return DeleteData(" + (key + 1) + ")' title='delete Row' /></a></td>";
            tableBody += "</tr>";
        });
        var table = "<table ID='zoneTable' class='display nowrap dataTable'>" +
              "<thead><tr>" + tableHeader + "</tr></thead>" +
              "<tbody>" + tableBody + "</tbody>" +
              "</table>";
        $("#div_displayDtl").empty();
        $("#div_displayDtl").html(table);
        $("#zoneTable").dataTable({
            dom: 'Bfrtip', "bSort": true,
            buttons: [
                'excel'
            ]
        });
    }
    $("#loading-div-background").hide();
}

$("#btn_Save").click(function () {
    if ($("#Sel_City_Name").val() == "") {
        $.jAlert({
            'content': 'Please Enter City Name..!'
        });
        return false;
    }
    if ($("#txt_City_Code").val() == "") {
        $.jAlert({
            'content': 'Please Enter City Code..!'
        });
        return false;
    }
    PageMethods.SaveData($("#Sel_City_Name").val(), $("#txt_City_Code").val(), $("#txt_UserName").val(), Client_ID, Master_API, function (response) {
        if (JSON.parse(response) == "OK") {
            $.jAlert({
                'content': 'Data Saved Successfully..!'
            });

            GetCityData();
            $("#Sel_City_Name").val("");
            $("#txt_City_Code").val("");
        }
        else if (JSON.parse(response) == "NotFound") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(response) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Saved Due To Internal Server Error..!'
            });
        }
    }, function (error) {
    });
});

function bindData(index) {
    $("#btn_Save").hide();
    $("#btn_Upd").show();
    $("#Sel_City_Name").val($("#txt_City_name" + index).val());
    $("#txt_City_Code").val($("#txt_City_Code" + index).val());
    $("#txt_PK_CityID").val($("#txt_Pk" + index).val());
}

$("#btn_Upd").click(function () {
    PageMethods.UpdateData($("#txt_PK_CityID").val(), $("#Sel_City_Name").val(), $("#txt_City_Code").val(), Client_ID, Master_API, function (response) {
        if (JSON.parse(response) == "OK") {
            $.jAlert({
                'content': 'Data Edited Successfully..!'
            });

            GetCityData();
            $("#Sel_City_Name").val("");
            $("#txt_City_Code").val("");
            $("#btn_Save").show();
            $("#btn_Upd").hide();
        }
        else if (JSON.parse(response) == "NotFound") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(response) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Saved Due To Internal Server Error..!'
            });
        }
    }, function (error) {
    });
});

function DeleteData(index) {
    $("#txt_PK_CityID").val($("#txt_Pk" + index).val());
    PageMethods.Deletedata($("#txt_PK_CityID").val(), Client_ID, Master_API, function (response) {
        if (JSON.parse(response) == "OK") {
            $.jAlert({
                'content': 'Data Deleted Successfully..!'
            });
            GetCityData();
        }
        else {
            $.jAlert({
                'content': 'Data Not Deleted..!'
            });
        }
    }, function (error) {
    });
}

$("#btn_Cancel").click(function () {
    window.location.replace("../../HomePage.aspx");
});