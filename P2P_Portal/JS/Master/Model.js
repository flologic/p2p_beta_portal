﻿var Client_ID = '', Master_API = '';
var ItemJson = "", ItemCatId, ItemSubCatId;
var BindAllDetail = '';

$(document).ready(function () {
    Client_ID = $("#txt_Client_ID").val();
    Master_API = $("#txt_Master_URL").val();
    // $('#form1').trigger("reset");
    addmodalpopup();
    $("#loading-div-background").show();
    GetModelData();
    getItemCat();
    getItemSubCat();
});

function GetModelData() {
    PageMethods.GetModelData(Client_ID, Master_API, function (response) {
        BindAllDetail = JSON.parse(JSON.parse(response[0]));
        if (BindAllDetail.Message == "Data Not Found") {
            alert("Model Data Not Found");
            $("#loading-div-background").hide();
        }
        else {
            ModelOnSuccess();
        }
    });
}

function getItemCat() {
    PageMethods.GetItemCatData(Client_ID, Master_API, function (response) {
        ItemJson = JSON.parse(JSON.parse(response[0]));
        BindItemCat();
    },
        function (error) {
        });
}

function BindItemCat() {
    $("#txt_cat_Id").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: ItemJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#txt_cat_Id").val(ui.item.label);
            ItemCatId = ui.item.id;
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function getItemSubCat() {
    PageMethods.GetItemSubCatData(Client_ID, Master_API, function (response) {
        ItemJson = JSON.parse(JSON.parse(response[0]));
        BindItemSubCat();
    },
        function (error) {
        });
}

function BindItemSubCat() {
    $("#txt_itmsbcat_name").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: ItemJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#txt_itmsbcat_name").val(ui.item.label);
            ItemSubCatId = ui.item.id;
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function ModelOnSuccess() {
    if (BindAllDetail) {
        var tableHeader = "";
        var tableBody = "";
        tableHeader += "<th style='width: 2%;'>#</th>";
        tableHeader += "<th>Item Category</th>";
        tableHeader += "<th>Item Sub-Category</th>";
        tableHeader += "<th>Model Name</th>";
        tableHeader += "<th>Description</th>";
        tableHeader += "<th>Edit</th>";
        tableHeader += "<th>Delete</th>";
        $.each(BindAllDetail, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.ItmCat + "<input class='form-control input-sm' type='text' id='txt_itmCatname" + (key + 1) + "' value='" + val.ItmCat + "' style='display:none'/><input class='form-control input-sm' type='text' id='txt_Itemctid" + (key + 1) + "' value='" + val.FK_ITMCATID + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.ItmSubCat + "<input class='form-control input-sm' type='text' id='txt_itmSubCatname" + (key + 1) + "' value='" + val.ItmSubCat + "' style='display:none'/><input class='form-control input-sm' type='text' id='txt_ItemSubctid" + (key + 1) + "' value='" + val.FK_ITMSUBCATID + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.label + "<input class='form-control input-sm' type='text' id='txt_itmnm" + (key + 1) + "' value='" + val.label + "' style='display:none'/><input class='form-control input-sm' type='text' id='txt_Itemid" + (key + 1) + "' value='" + val.id + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.DESCRIPTION + "<input class='form-control input-sm'  type='text' id='txt_Desc" + (key + 1) + "' value='" + val.DESCRIPTION + "' style='display:none'/></td>";
            tableBody += "<td><a><img src='../../assets/images/edit.png' onclick='return bindData(" + (key + 1) + ")' title='Add New Row' /></a><input class='form-control input-sm' type='text' id='txt_Pk_" + (key + 1) + "' value='" + val.id + "' style='display:none'></input></td>";
            tableBody += "<td><a><img src='../../assets/images/delete.png' onclick='return DeleteData(" + (key + 1) + ")' title='delete Row' /></a></td>";
            tableBody += "</tr>";
        });

        var table = "<table ID='itemsbcatTable' class='display nowrap dataTable'>" +
              "<thead><tr>" + tableHeader + "</tr></thead>" +
              "<tbody>" + tableBody + "</tbody>" +
              "</table>";
        $("#div_displayDtl").empty();
        $("#div_displayDtl").html(table);
        $("#itemsbcatTable").dataTable({
            dom: 'Bfrtip', "bSort": true,
            buttons: [
                'excel'
            ]
        });
    }
    $("#loading-div-background").hide();
}


$("#btn_Save").click(function () {
    if ($("#txt_cat_Id").val() == "") {
        $.jAlert({
            'content': 'Please Select Item Category..!'
        });
        return false;
    }
    if ($("#txt_itmsbcat_name").val() == "") {
        $.jAlert({
            'content': 'Please Enter Item Subcategory..!'
        });
        return false;
    }
    if ($("#txt_Model_Name").val() == "") {
        $.jAlert({
            'content': 'Please Enter Model Name..!'
        });
        return false;
    }
    if ($("#txt_Model_Desc").val() == "") {
        $.jAlert({
            'content': 'Please Enter Model Description..!'
        });
        return false;
    }
    PageMethods.SaveData(ItemCatId, ItemSubCatId, $("#txt_Model_Name").val(), $("#txt_Model_Desc").val(), $("#txt_UserName").val(), Client_ID, Master_API, function (response) {
        if (JSON.parse(response) == "OK") {
            $.jAlert({
                'content': 'Data Saved Successfully..!'
            });
            GetModelData();
            $("#txt_cat_Id").val("");
            $("#txt_itmsbcat_name").val("");
            $("#txt_Model_Name").val("");
            $("#txt_Model_Desc").val("");
        }
        else if (JSON.parse(response) == "NotFound") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(response) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Saved Due To Internal Server Error..!'
            });
        }
    }, function (error) {
    });
})

function bindData(index) {
    $("#btn_Save").hide();
    $("#btn_Upd").show();
    ItemCatId = $("#txt_Itemctid" + index).val();
    ItemSubCatId = $("#txt_ItemSubctid" + index).val();
    $("#txt_cat_Id").val($("#txt_itmCatname" + index).val());
    $("#txt_itmsbcat_name").val($("#txt_itmSubCatname" + index).val());
    $("#txt_Model_Name").val($("#txt_itmnm" + index).val());
    $("#txt_Model_Desc").val($("#txt_Desc" + index).val());
    $("#txt_PK_ModelID").val($("#txt_Pk_" + index).val());
}

$("#btn_Upd").click(function () {
    if ($("#txt_cat_Id").val() == "") {
        $.jAlert({
            'content': 'Please Select Item Category..!'
        });
        return false;
    }
    if ($("#txt_itmsbcat_name").val() == "") {
        $.jAlert({
            'content': 'Please Enter Item Subcategory..!'
        });
        return false;
    }
    if ($("#txt_Model_Name").val() == "") {
        $.jAlert({
            'content': 'Please Enter Model Name..!'
        });
        return false;
    }
    if ($("#txt_Model_Desc").val() == "") {
        $.jAlert({
            'content': 'Please Enter Model Description..!'
        });
        return false;
    }
    PageMethods.UpdateData($("#txt_PK_ModelID").val(), ItemCatId, ItemSubCatId, $("#txt_Model_Name").val(), $("#txt_Model_Desc").val(), Client_ID, Master_API, function (response) {
        if (JSON.parse(response) == "OK") {
            $.jAlert({
                'content': 'Data Edited Successfully..!'
            });
            GetModelData();
            ItemCatId = "";
            ItemSubCatId = "";
            $("#txt_cat_Id").val("");
            $("#txt_itmsbcat_name").val("");
            $("#txt_Model_Name").val("");
            $("#txt_Model_Desc").val("");
            $("#btn_Save").show();
            $("#btn_Upd").hide();
        }
        else if (JSON.parse(response) == "NotFound") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(response) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Saved Due To Internal Server Error..!'
            });
        }
    }, function (error) {
    });
})

function DeleteData(index) {
    $("#txt_PK_ModelID").val($("#txt_Pk_" + index).val());
    PageMethods.Deletedata($("#txt_PK_ModelID").val(), Client_ID, Master_API, function (response) {
        if (JSON.parse(response) == "OK") {
            $.jAlert({
                'content': 'Data Deleted Successfully..!'
            });
            GetModelData();
        }
        else {
            $.jAlert({
                'content': 'Data Not Deleted..!'
            });
        }
    }, function (error) {
    });
}

$("#btn_Cancel").click(function () {
    window.location.replace("../../HomePage.aspx");
});