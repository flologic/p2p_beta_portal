﻿var PageAccessRoleJson, roleJson, panelJson, accessJson, accessJsonbck, roleid1, panelid1, pageaccessJson;
var role_PKID;
var saveType;

$(document).ready(function () {
    onLoad();
});

function onLoad() {
    $('#form1').trigger("reset");
    saveType = "SAVE";
    $("#btn_Save").html('Save');

    getRoleData();
    getAccessData();
}

function getRoleData() {
    PageMethods.getRoleAllData(function (response) {
        roleJson = JSON.parse(response);
        setRoleData(roleJson);
    }, function (error) {
        alert(error);
    });
}

function setRoleData(obj) {
    if (obj) {
        var tableHeader = "";
        var tableBody = "";

        tableHeader += "<tr>";
        tableHeader += "<th><input type='radio' style='display:none' id='chk_main_role'></th>";
        tableHeader += "<th>Role</th>";
        tableHeader += "</tr>";

        $.each(obj, function (key, val) {
            tableBody += "<tr id=" + key + ">";
            tableBody += "<td> <input type='radio' name='RadioBtn' id='chk_" + (key + 1) + "'></td>";
            tableBody += "<td>" + val.ROLE_NAME + "</td>";
            tableBody += "</tr>";
        })
        var table = "<table class='display nowrap'  id='tbl_roledtl' runat='server'>" +
              "<thead><tr>" + tableHeader + "</tr></thead>" +
              "<tbody>" + tableBody + "</tbody>" +
              "</table>";
        $("#div_role").empty();
        $("#div_role").html(table);

        var oTable = $('#tbl_roledtl').DataTable({ paging: false, bFilter: true, ordering: false, searching: true, dom: 't' });
        $('#role_srch').keyup(function () {
            oTable.search($(this).val()).draw();
        })

    }
}

$(document).on('change', '#tbl_roledtl >tbody :radio', function () {
    roleid1 = "";
    $('#tbl_roledtl >tbody :radio').each(function () {
        if (this.checked) {
            var index = $(this).closest("tr").attr("id");
            var fkroleid1 = roleJson[index].PK_ROLE_ID;
            if (roleid1 == "")
                roleid1 = fkroleid1
            else
                roleid1 = roleid1 + "," + fkroleid1;
            getPanelData();
        }
    });

});

function getPanelData() {
    PageMethods.getpaneldata(function (response) {
        panelJson = JSON.parse(response);
        setPanelData(panelJson);
    }, function (error) {
        alert(error);
    });
}

function setPanelData(obj) {
    if (obj) {
        var tableHeader = "";
        var tableBody = "";

        tableHeader += "<tr>";
        tableHeader += "<th><input type='radio'  style='display:none' id='chk_main_panel'></th>";
        tableHeader += "<th>Panel</th>";
        tableHeader += "</tr>";

        $.each(obj, function (key, val) {
            tableBody += "<tr id=" + key + ">";
            tableBody += "<td> <input type='radio' name='RadioPanel' id='chk_" + (key + 1) + "'></td>";
            tableBody += "<td>" + val.PANE_DESC + "</td>";
            tableBody += "</tr>";
        })
        var table = "<table class='display nowrap'  id='tbl_paneldtl' runat='server'>" +
              "<thead><tr>" + tableHeader + "</tr></thead>" +
              "<tbody>" + tableBody + "</tbody>" +
              "</table>";
        $("#div_panel").empty();
        $("#div_panel").html(table);

        var oTable = $('#tbl_paneldtl').DataTable({ paging: false, bFilter: true, ordering: false, searching: true, dom: 't' });
        $('#txt_panel_srch').keyup(function () {
            oTable.search($(this).val()).draw();
        })

    }
}

function getAccessData() {
    PageMethods.getPageAccessDetails(function (response) {
        accessJson = JSON.parse(response);
        accessJsonbck = accessJson;
        displayData(accessJson);
    }, function (error) {
        alert(error);
    });
}

function displayData() {
    if (accessJson) {
        var tableHeader = "";
        var tableBody = "";

        tableHeader += "<th style='width: 6%'>Sr.No</th>";
        tableHeader += "<th>Role Name</th>";
        tableHeader += "<th>Page Name</th>";

        $.each(accessJson, function (key, val) {
            tableBody += "<tr id='" + (key) + "'>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.ROLLNAME + "</td>";
            tableBody += "<td>" + val.PAGENAME + "</td>";
            tableBody += "</tr>";
        })
        var table = "<table class='display nowrap'  id='tbl_SubCatagory' runat='server'>" +
              "<thead><tr>" + tableHeader + "</tr></thead>" +
              "<tbody>" + tableBody + "</tbody>" +
              "</table>";
        $("#div_pageaccesstorole").empty();
        $("#div_pageaccesstorole").html(table);
        
        $("#tbl_SubCatagory").dataTable({
            dom: 'Bfrtip', "bSort": true,
            buttons: [
                'excel'
            ]
        });

        // $("#loading-div-background").hide();
    }
}

$(document).on('change', '#tbl_paneldtl >tbody :radio', function () {
    panelid1 = "";

    if (roleid1 != "" && roleid1 != undefined) {

        $('#tbl_paneldtl >tbody :radio').each(function () {
            if (this.checked) {
                var index = $(this).closest("tr").attr("id");
                var fkpanelid1 = panelJson[index].PANE_ID;
                if (panelid1 == "")
                    panelid1 = fkpanelid1
                else
                    panelid1 = panelid1 + "," + fkpanelid1;

            }
        });
        if (panelid1 != "") {
            PageMethods.getpageaccess(panelid1, roleid1, function (response) {
                pageaccessJson = JSON.parse(response);
                displayPageAccessDetail(pageaccessJson);
            }, function (error) {
                alert(error);
            });
        }
        else {
            displayPageAccessDetail([]);
        }
    }
    else {
        $.jAlert({
            'content': 'Please Select First Role.',
            'onClose': function () {

            }
        });
        return false;
    }
});

function displayPageAccessDetail(obj) {
    if (obj) {
        var tableHeader = "";
        var tableBody = "";

        tableHeader += "<tr>";
        tableHeader += "<th></th>";//<input type='checkbox' id='chk_main_pageaccess'>
        tableHeader += "<th>Page Access</th>";
        tableHeader += "</tr>";

        $.each(obj, function (key, val) {
            tableBody += "<tr id=" + key + ">";
            if (val.HASACCESS == 1) {
                tableBody += "<td> <input type='checkbox' class='assetcheck' checked='true' id='chk_" + (key + 1) + "'></td>";
            }
            else {
                tableBody += "<td> <input type='checkbox' class='assetcheck' id='chk_" + (key + 1) + "'></td>";
            }
            tableBody += "<td>" + val.LABEL + "</td>";
            tableBody += "</tr>";
        })
        var table = "<table class='display nowrap'  id='tbl_pagedtl' runat='server'>" +
              "<thead><tr>" + tableHeader + "</tr></thead>" +
              "<tbody>" + tableBody + "</tbody>" +
              "</table>";
        $("#div_pageaccess").empty();
        $("#div_pageaccess").html(table);

        var oTable = $('#tbl_pagedtl').DataTable({ paging: false, bFilter: true, ordering: false, searching: true, dom: 't' });
        $('#txt_page_srch').keyup(function () {
            oTable.search($(this).val()).draw();
        })

    }
}

$("#btn_Save").click(function () {
    SaveData();
});

function SaveData() {

    var roleid2 = "";
    $('#tbl_roledtl >tbody :radio').each(function () {
        if (this.checked) {
            var index = $(this).closest("tr").attr("id");
            var fkroleid2 = roleJson[index].PK_ROLE_ID;
            if (roleid2 == "")
                roleid2 = fkroleid2
            else
                roleid2 = roleid2 + "," + fkroleid2;

        }
    });

    var panelid2 = "";
    $('#tbl_paneldtl >tbody :radio').each(function () {
        if (this.checked) {
            var index = $(this).closest("tr").attr("id");
            var fkpanelid2 = panelJson[index].PANE_ID;
            if (panelid2 == "")
                panelid2 = fkpanelid2
            else
                panelid2 = panelid2 + "," + fkpanelid2;

        }
    });

    var pageacces2 = "";
    $('#tbl_pagedtl >tbody :checkbox').each(function () {
        if (this.checked) {
            var index = $(this).closest("tr").attr("id");
            var fkaccessid2 = pageaccessJson[index].OBJECT_ID;
            if (pageacces2 == "")
                pageacces2 = fkaccessid2
            else
                pageacces2 = pageacces2 + "," + fkaccessid2;

        }
    });


    if (roleid2 == undefined || roleid2 == "") {
        $.jAlert({
            'content': 'Please Select AnyOne Role.',
            'onClose': function () {
            }
        });

        return false;
    }
    else if (panelid2 == undefined || panelid2 == "") {
        $.jAlert({
            'content': 'Please Select AnyOne Panel.',
            'onClose': function () {
            }
        });
        return false;
    }
    else if (pageacces2 == undefined || pageacces2 == "") {
        $.jAlert({
            'content': 'Please Select Anyone Page Access.',
            'onClose': function () {
            }
        });

        return false;
    }

    var PageRole = [];


    if (saveType == "SAVE") {
        var xmlsave;
        xmlsave = "<ROWSET>";
        var table = $('#tbl_pagedtl').dataTable();
        $(".assetcheck", table.fnGetNodes()).each(function () {

            if (this.checked) {
                var index = $(this).closest("tr").attr("id");
                var fkaccessid3 = pageaccessJson[index].OBJECT_ID;

                xmlsave += "<ROW>";
                xmlsave += "<OBJECT_ID>" + fkaccessid3 + "</OBJECT_ID>";
                xmlsave += "<ACCESS_ROLE_ID>" + roleid2 + "</ACCESS_ROLE_ID>";
                xmlsave += "<PANEL_ID>" + panelid2 + "</PANEL_ID>";
                xmlsave += "<IS_ACTIVE>1</IS_ACTIVE>";
                xmlsave += "<AMD_BY>" + $("#txt_UserID").val() + "</AMD_BY>"
                xmlsave += "<AMD_DATE>$date$</AMD_DATE>";
                xmlsave += "</ROW>";
            }

        });
    }
    xmlsave += "</ROWSET>";
    // xmlsave = replaceSpecialChar(xmlsave);
    $('#role_srch,#txt_panel_srch,#txt_page_srch').val('');
    $('#role_srch,#txt_panel_srch,#txt_page_srch').trigger('keyup');

    PageMethods.saveData(xmlsave, roleid2, panelid2, function (response) {
        $.jAlert({
            'content': response,

        });
        setPanelData([]);
        displayPageAccessDetail([]);
        getRoleData();
        getAccessData();
    }, function (error) {
        $.jAlert({
            'content': error,
        });
    });
}

$("#btn_Cancel").click(function () {
    setPanelData([]);
    displayPageAccessDetail([]);
    getRoleData();
    getAccessData();
});