﻿var PartJson;
var Client_ID = '', Master_API = '';
var role_PKID;
var saveType;

$(document).ready(function () {
    onLoad();
});

function onLoad() {
    $('#form1').trigger("reset");
    saveType = "SAVE";
    $("#btn_Save").html('Save');
    $("#txt_part_Name").val("");
    $('#txt_part_Name').attr('readonly', false);
    $("#txt_part_value").val("");
    $('#txt_part_value').attr('readonly', false);
    Client_ID = $("#txt_Client_ID").val();
    Master_API = $("#txt_Master_URL").val();
    GetPartData();

}

function GetPartData() {
    PageMethods.getPartAllData(Client_ID, Master_API, function (response) {
        PartJson = JSON.parse(JSON.parse(response[0]));
        if (PartJson.Message == "Data Not Found") {
            $.jAlert({
                'content': 'Data Not Available.',
                'onClose': function () {

                }
            });
        }
        else {
            displayData();
        }
    },
        function (error) {
        });
}

function displayData() {
    if (PartJson) {
        var tableHeader = "";
        var tableBody = "";

        tableHeader += "<th style='width: 3%'>#</th>";
        tableHeader += "<th>Part Name</th>";
        tableHeader += "<th>Part Value</th>";
        tableHeader += "<th style='width: 5%'>Edit</th>";
        tableHeader += "<th style='width: 5%'>Delete</th>";

        $.each(PartJson, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.PRTNAME + "</td>";
            tableBody += "<td>" + val.PRTVALUE + "</td>";
            tableBody += "<td class='click_Edit'><img src='../../assets/images/edit.png' title='Edit' alt='Edit'/></td>";
            tableBody += "<td class='click_Delete'><img src='../../assets/images/delete.png' title='Delete' alt='Delete'/></td>";
            tableBody += "</tr>";
        })
        var table = "<table class='display nowrap' id='tbl_part'>" +
              "<thead><tr>" + tableHeader + "</tr></thead>" +
              "<tbody>" + tableBody + "</tbody>" +
              "</table>";
        $("#div_Part").empty();
        $("#div_Part").html(table);

        $("#tbl_part").dataTable({
            dom: 'Bfrtip', "bSort": true,
            buttons: [
                'excel'
            ]
        });
    }
}

$("#btn_Save").click(function () {
    SaveData();
});

function SaveData() {
    var Part = [];
    var PartName = $("#txt_part_Name").val();
    var PartValue = $("#txt_part_value").val();
    if (PartName == "") {
        $.jAlert({
            'content': 'Please Enter Part Name.',
            'onClose': function () {
                $('#txt_part_Name').focus();
            }
        });
        return false;
    }
    else if (PartValue == "") {
        $.jAlert({
            'content': 'Please Enter Part Value.',
            'onClose': function () {
                $('#txt_part_value').focus();
            }
        });
        return false;
    }

    var ispc = "P";
    var iscompl = "Y";
    var objLocation;
    if (saveType == "SAVE") {
        objLocation = {
            "PRTNAME": PartName, "PRTVALUE": PartValue, "ISPC": ispc, "ISCOMPULSORY": iscompl, "ISNUMBERS": "1", "IS_ACTIVE": "1", "CREATED_BY": $('#txt_UserID').val(), "FK_CLIENTID": Client_ID
        };
    }
    else {
        objLocation = {
            "PK_PRTID": role_PKID, "PRTNAME": PartName, "PRTVALUE": PartValue, "ISPC": ispc, "ISCOMPULSORY": iscompl, "ISNUMBERS": "1", "CREATED_BY": $('#txt_UserID').val()
        };
    }
    Part.push(objLocation);

    var transactionData = { "partData": Part }

    PageMethods.saveData(JSON.stringify(transactionData), Master_API, saveType, function (response) {
        $.jAlert({
            'content': JSON.parse(JSON.parse(response)),
        });
        onLoad();
    }, function (error) {
        $.jAlert({
            'content': error,
        });
    });
}

$("#btn_Cancel").click(function () {
    onLoad();
});


$('body').on('click', '.click_Edit', function () {
    var $row = $(this).closest('tr');
    var table = $('#tbl_part').DataTable();
    var data = table.row($row).data();
    $("#txt_part_Name").val(PartJson[parseInt(data[0]) - 1].PRTNAME);
    $("#txt_part_value").val(PartJson[parseInt(data[0]) - 1].PRTVALUE);
    role_PKID = PartJson[parseInt(data[0]) - 1].PK_PRTID;
    saveType = "EDIT";
    $("#btn_Save").html('Update');
});


$('body').on('click', '.click_Delete', function () {
    var $row = $(this).closest('tr');
    var table = $('#tbl_part').DataTable();
    var data = table.row($row).data();
    $("#txt_part_Name").val(PartJson[parseInt(data[0]) - 1].PRTNAME);
    $("#txt_part_value").val(PartJson[parseInt(data[0]) - 1].PRTVALUE);
    $('#txt_part_Name').attr('readonly', true);
    $('#txt_part_value').attr('readonly', true);
    role_PKID = PartJson[parseInt(data[0]) - 1].PK_PRTID;
    saveType = "DELETE";
    $("#btn_Save").html('Delete');
    $.jAlert({ 'type': 'confirm', 'onConfirm': function () { SaveData() } });
});
