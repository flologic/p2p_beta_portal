﻿var roleJson, empJson, roleJsonbck, empJsonbck, accessJson, accessJsonbck;
var roleName, empName, roleid, empid, role_PKID;
var filterdaccessJson = [];
var filterdempJson = [], EmployeeDetail;
var fkroleid = 0, saveType;
$(document).ready(function () {
    onLoad();
});

function onLoad() {
    $('#form1').trigger("reset");
    addmodalpopup();
    $("#loading-div-background").show();
    $("#txt_user_srch").attr("readonly", true);
    saveType = "SAVE";
    getRoleData();
    getEmpData();
    getAccessData();

}

function getRoleData() {
    PageMethods.getRoleAllData(function (response) {
        roleJson = JSON.parse(response);
        setRoles(roleJson);
    }, function (error) {
        alert(error);
    });
}

function setRoles(sourceJson) {
    if (sourceJson) {
        var tableHeader = "";
        var tableBody = "";

        tableHeader += "<tr>";
        tableHeader += "<th></th>";
        tableHeader += "<th>Role Name</th>";
        tableHeader += "</tr>";

        $.each(sourceJson, function (key, val) {
            tableBody += "<tr id=" + key + ">";
            tableBody += "<td> <input type='radio' name='roleRadio' chkval='" + val.PK_ROLE_ID + "' id='chk_" + (key + 1) + "'></td>";
            tableBody += "<td>" + val.ROLE_NAME + "</td>";
            tableBody += "</tr>";
        })
        var table = "<table class='display nowrap' id='tbl_role' runat='server'>" +
            "<thead><tr>" + tableHeader + "</tr></thead>" +
            "<tbody>" + tableBody + "</tbody>" +
            "</table>";
        $("#div_role").empty();
        $("#div_role").html(table);
        var oTable = $('#tbl_role').DataTable({ paging: false, bFilter: true, ordering: false, searching: true, dom: 't' });
        $('#txt_role_srch').keyup(function () {
            oTable.search($(this).val()).draw();
        })
       
    }
}

function getEmpData() {
    PageMethods.getEmpAllData(function (response) {
        empJson = JSON.parse(response);
        $("#loading-div-background").hide();
    }, function (error) {
        alert(error);
    });
}

function setEmployees() {

    var ser = capitalizeFirstLetter($("#txt_user_srch").val());
    var EmployeeDetail1 = getResult("USER_NAME", ser);
    var EmployeeDetail2 = getResult("EMP_CODE", ser);

    EmployeeDetail = EmployeeDetail1.concat(EmployeeDetail2);

    if (EmployeeDetail) {
        var tableHeader = "";
        var tableBody = "";

        tableHeader += "<tr>";
        tableHeader += "<th><input name='chkall' type='checkbox' id='chk_main_loc'></th>";
        tableHeader += "<th>User Name</th>";
        tableHeader += "</tr>";

        $.each(EmployeeDetail, function (key, val) {
            tableBody += "<tr id=" + key + ">";
            tableBody += "<td> <input type='checkbox' class='assetcheck' chkval='" + val.PK_USER_ID + "' id='chk_" + (key + 1) + "'></td>";
            tableBody += "<td>" + val.label + "</td>";
            tableBody += "</tr>";
        })
        var table = "<table class='display nowrap dataTable' id='tbl_user' runat='server'>" +
            "<thead><tr>" + tableHeader + "</tr></thead>" +
            "<tbody>" + tableBody + "</tbody>" +
            "</table>";
        $("#div_user").empty();
        $("#div_user").html(table);

        $("#loading-div-background").hide();

        //var oTable = $('#tbl_user').DataTable({ paging: false, bFilter: true, ordering: false, searching: true, dom: 't' });
        //$('#txt_user_srch').onChange(function () {
        //    oTable.search($(this).val()).draw();
        //})

    }
}

function getResult(keyToFilter, valueStartsWith) {
    return _.filter(empJson, function (d) { return d[keyToFilter].startsWith(valueStartsWith); })
}

$(document).on('change', '[name=chkall]', function () {
    var table = $(this).closest('table').DataTable();
    var cells = table.cells().nodes();
    $(cells).find(':checkbox').prop('checked', $(this).is(':checked'));
});

$(document).on('change', '[name=roleRadio]', function () {
    addmodalpopup();
    $("#loading-div-background").show();
    $("#txt_user_srch").attr("readonly", false);
    $(this).val()
    if ($(this).is(':checked')) {
        var roleid = $(this).attr('chkval');
        // roleid = isempty(roleid) ? 0 : parseInt(roleid);
        filterdaccessJson = accessJson.filter(function (item) {
            return item.PK_ROLE_ID === parseInt(roleid);
        });

    }
    //setEmployees(empJson);
    displayData(filterdaccessJson);
    $("#loading-div-background").hide();


});

function getAccessData() {
    PageMethods.getAccessDetails(function (response) {
        accessJson = JSON.parse(response);
        $("#loading-div-background").hide();
        // displayData(accessJson);
        //accessJsonbck = accessJson;
    }, function (error) {
        alert(error);
    });
}

function displayData(accessJson) {
    var RolestoCheck = [];
    var UserstoCheck = [];
    if (accessJson) {

        var tableHeader = "";
        var tableBody = "";

        tableHeader += "<th style='width: 5%'>Sr.No</th>";
        tableHeader += "<th>Role Name</th>";
        tableHeader += "<th>User Name</th>";
        tableHeader += "<th style='width: 5%'>Delete</th>";
        $.each(accessJson, function (key, val) {
            tableBody += "<tr id='" + (key) + "'>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.ROLLNAME + "</td>";
            tableBody += "<td>" + val.USERNAME + "-" + val.USER_ADID + "</td>";
           // tableBody += "<td class='click_Delete'><img src='../../assets/images/delete.png' title='Delete' alt='Delete'/></td>";
            tableBody += "<td class='click_Delete'><input id='txt_PKID_" + (key + 1) + "' runat='server' style='display:none' value=" + val.PK_USER_ROLE_ACCESS_ID + "><img src='../../assets/images/delete.png' title='Delete' alt='Delete' onclick='deletePK(" + (key + 1) + ")'/></td>";
            tableBody += "</tr>";
        })
        var table = "<table class='display nowrap' id='tbl_SubCatagory' runat='server'>" +
            "<thead><tr>" + tableHeader + "</tr></thead>" +
            "<tbody>" + tableBody + "</tbody>" +
            "</table>";
        $("#div_useraccesstorole").empty();
        $("#div_useraccesstorole").html(table);

        $("#tbl_SubCatagory").dataTable({
            dom: 'Bfrtip', "bSort": true,
            buttons: [
                'excel'
            ]
        });
        $("#loading-div-background").hide();
    }
}
      
function checkUsers(UserstoCheck) {
    var temp = "";
    $.each($("#tbl_user tbody input:checkbox"), function () {
        var currval = $(this).attr('chkval');
        //currval = isempty(currval) ? "NA" : parseInt(currval);
        if (_.some(UserstoCheck, function (c) {
            return c == currval;
        }))
            this.checked = true;
        else
            this.checked = false;
    });
    $("#loading-div-background").hide();

}

$("#btn_Save").click(function () {
    SaveData();
});

function SaveData() {
    $("#loading-div-background").show();
    var roleid2 = "";
    $('#tbl_role >tbody :radio').each(function () {
        if (this.checked) {
            var index = $(this).closest("tr").attr("id");
            var fkroleid2 = roleJson[index].PK_ROLE_ID;
            if (roleid2 == "")
                roleid2 = fkroleid2
            else
                roleid2 = roleid2 + "," + fkroleid2;

        }
    });

    if (roleid2 == undefined || roleid2 == "") {
        $.jAlert({
            'content': 'Please Select AnyOne Role.',
            'onClose': function () {
                $("#loading-div-background").hide();
            }
        });

        return false;
    }


    var pageacces2 = "";
    $('#tbl_user >tbody :checkbox').each(function () {
        if (this.checked) {
            var index = $(this).closest("tr").attr("id");
            var fkaccessid2 = EmployeeDetail[index].PK_USER_ID;
            if (pageacces2 == "")
                pageacces2 = fkaccessid2
            else
                pageacces2 = pageacces2 + "," + fkaccessid2;

        }
    });

    if (pageacces2 == undefined || pageacces2 == "") {
        $.jAlert({
            'content': 'Please Select Anyone User.',
            'onClose': function () {
                $("#loading-div-background").hide();
            }
        });

        return false;
    }

    var PageRole = [];

    // var xmlsave;
    // xmlsave = "<ROWSET>";
    fkroleid = $('input[name=roleRadio]:checked').attr('chkval');
    //fkroleid = isempty(fkroleid) ? 0 : parseInt(fkroleid);

    $('#tbl_user >tbody :checkbox:checked').each(function () {
        var index = $(this).closest("tr").attr("id");
        var fkuserid = EmployeeDetail[index].LOGIN_NAME;

        //xmlsave += "<ROW>";
        //xmlsave += "<USER_ADID>" + fkuserid + "</USER_ADID>";
        //xmlsave += "<ACCESS_ROLE_ID>" + fkroleid + "</ACCESS_ROLE_ID>";
        //xmlsave += "<IS_ACTIVE>1</IS_ACTIVE>";
        //xmlsave += "<AMD_BY>" + $("#txt_UserID").val() + "</AMD_BY>"
        //xmlsave += "<AMD_DATE>$date$</AMD_DATE>";
        //xmlsave += "</ROW>";

        var HdrData1 = {
            "USER_ADID": fkuserid, "ACCESS_ROLE_ID": fkroleid, "IS_ACTIVE": "1", "AMD_BY": $("#txt_UserID").val(),
        };
        PageRole.push(HdrData1);

    });

    //  xmlsave += "</ROWSET>";
    // xmlsave = replaceSpecialChar(xmlsave);
    $('#txt_role_srch,#txt_user_srch').val('');
    $('#txt_role_srch,#txt_user_srch').trigger('keyup');
    var transactionData = { "hdrData": PageRole };
    PageMethods.saveData(JSON.stringify(transactionData), saveType, function (response) {
        $.jAlert({
            'content': response,
            'onClose': function () {
                onLoad();
                $("#loading-div-background").hide();

            }
        });
        //setEmployees([]);
        // getRoleData();
        // getEmpData();
        // getAccessData();
    }, function (error) {
        $.jAlert({
            'content': error,
            'onClose': function () {
                $("#loading-div-background").hide();
            }
        });
    });
}

$("#btn_Cancel").click(function () {
    setEmployees([]);
    getRoleData();
    getEmpData();
    getAccessData();
});

function GetEmpData() {
    var ser = $("#txt_user_srch").val();
    if (ser.length >= 3) {
        $("#loading-div-background").show();
        setEmployees();
        UserstoCheck = _.uniq(_.pluck(_.flatten(filterdaccessJson), "PK_USER_ID"));
        checkUsers(UserstoCheck);
    }
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

//$('body').on('click', '.click_Delete', function () {
function deletePK(id) {
    var $row = $(this).closest('tr');
    var table = $('#tbl_SubCatagory').DataTable();
    var data = table.row($row).data();

    role_PKID = $("#txt_PKID_" + id).val();//accessJson[parseInt(data[0]) - 1].PK_USER_ROLE_ACCESS_ID;
    saveType = "DELETE";
    //$("#btn_Save").html('Delete');
    $.jAlert({ 'type': 'confirm', 'onConfirm': function () { DeleteData() } });
};//);

function DeleteData() {
    var Role = [];

    var objLocation;

    objLocation = {
        "PK_USER_ROLE_ACCESS_ID": role_PKID, "AMD_BY": $('#txt_UserID').val()
    };

    Role.push(objLocation);
    var transactionData = { "hdrData": Role };
    PageMethods.saveData(JSON.stringify(transactionData), saveType, function (response) {
        $.jAlert({
            'content': response,
            'onClose': function () {
                onLoad();
                $("#loading-div-background").hide();
            }
        });
        onLoad();
    }, function (error) {
        $.jAlert({
            'content': error,
            'onClose': function () {
                $("#loading-div-background").hide();
            }
        });
    });
}

$("#btn_show").click(function () {
    addmodalpopup();
    $("#loading-div-background").show();
    getAccessData();
    displayData(accessJson);
    $("#loading-div-background").hide();
});


