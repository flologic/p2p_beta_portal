﻿
var EmployeeDetail = '';
var gradeJson = "", Grade;
var CostcenterJson = "", CostCenterName;
var departmentJson = "", DepartmentName;
var VerticalJson = "", VerticalName;
var designationJson = "", DesignationName;
var branchJson = "", BranchName;
var EmpPKid, Designationid, DesignationName, Gradeid, Grade, Costcenterid, CostCenterName, Verticalid, VerticalName, Departmentid, DepartmentName, Branchid, BranchName;
var saveType;

$(document).ready(function () {
    onLoad();

});

function onLoad() {
    $('#form1').trigger("reset");
    saveType = "SAVE";
    $("#btn_Save").html('Save');
    addmodalpopup();
    $("#loading-div-background").show();
    bindGrade();
    bindCostcenter();
    bindVertical();
    bindDepartment();
    bindDesignation();
    bindBranch();
    //GetEmployeeData();
    $("#loading-div-background").hide();
}

$(document).on('focus', '.datepicker-rtl', function () {
    $(this).datepicker({
        dateFormat: 'dd-M-yy', autoclose: true, todayBtn: 'linked', maxDate: 0, //maybe you want something like this
        //showButtonPanel: true,
        onClose: function () {
            $('.datepicker-rtl').removeClass('hasDatepicker');
            // or 'destroy' or $('.datepicker').remove(); or $(this).datepick('remove');
        }
    });
});

function bindGrade() {
    PageMethods.getGradedata(function (response) {
        gradeJson = JSON.parse(response[0]);
        BindGrade();
    },
        function (error) {
        });
}

function BindGrade() {
    $("#txt_grade").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: gradeJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#txt_grade").val(ui.item.label);
            Gradeid = ui.item.id;
            Grade = ui.item.label;
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function bindCostcenter() {
    PageMethods.getCostCenterData(function (response) {
        CostcenterJson = JSON.parse(response[0]);
        BindCostCenter();
    },
        function (error) {
        });
}

function BindCostCenter() {
    $("#txt_Costcenter").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: CostcenterJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#txt_Costcenter").val(ui.item.label);
            Costcenterid = ui.item.id;
            CostCenterName = ui.item.label;
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function bindVertical() {
    PageMethods.getVerticalsData(function (response) {
        VerticalJson = JSON.parse(response[0]);
        BindVerticalData();
    },
        function (error) {
        });
}

function BindVerticalData() {
    $("#txt_vertical").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: VerticalJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#txt_vertical").val(ui.item.label);
            Verticalid = ui.item.id;
            VerticalName = ui.item.label;
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function bindDepartment() {
    PageMethods.getDepartmentsData(function (response) {
        departmentJson = JSON.parse(response[0]);
        BindDepartmentData();
    },
        function (error) {
        });
}

function BindDepartmentData() {
    $("#txt_Dept_name").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: departmentJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#txt_Dept_name").val(ui.item.label);
            Departmentid = ui.item.id;
            DepartmentName = ui.item.label;
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function bindDesignation() {
    PageMethods.getdesignationData(function (response) {
        designationJson = JSON.parse(response[0]);
        BindDesignation();
    },
        function (error) {
        });
}

function BindDesignation() {
    $("#txt_designation").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: designationJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#txt_designation").val(ui.item.label);
            Designationid = ui.item.id;
            DesignationName = ui.item.label;
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function GetEmployeeData() {
    PageMethods.getEmployeeAllData(function (response) {
        if (response) {
            EmployeeDetail = JSON.parse(response);
            displayEmpData();
        }
    }, function (error) {
    });
}

function displayEmpData() {
    var ser = capitalizeFirstLetter($("#txt_Subject").val());
    var EmployeeDetail1 = getResult("EMP_NAME", ser);
    var EmployeeDetail2 = getResult("EMP_CODE", ser);

    var EmployeeDetail = EmployeeDetail1.concat(EmployeeDetail2);
    if (EmployeeDetail) {
        var tableHeader = "";
        var tableBody = "";
        tableHeader += "<th style='width: 3%'>#</th>";
        tableHeader += "<th>Employee Name</th>";
        tableHeader += "<th>Employee Code</th>";
        tableHeader += "<th>Mobile No</th>";
        tableHeader += "<th>Address</th>";
        tableHeader += "<th>Email Id</th>";
        tableHeader += "<th>Designation</th>";
        tableHeader += "<th>Branch</th>";
        tableHeader += "<th>Department</th>";
        tableHeader += "<th>Date of Joining</th>";
        tableHeader += "<th>Date of Birth</th>";
        tableHeader += "<th>Date of last working Day</th>";
        tableHeader += "<th>Vertical</th>";
        tableHeader += "<th>Cost Center</th>";
        tableHeader += "<th>IFSC Code</th>";
        tableHeader += "<th>Grade</th>";
        tableHeader += "<th style='width: 5%'>Edit</th>";
        tableHeader += "<th style='width: 5%'>Delete</th>";

        $.each(EmployeeDetail, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.EMP_NAME + "<input class='form-control input-sm' type='text' id='txt_emp_Name" + (key + 1) + "' value='" + val.EMP_NAME + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.EMP_CODE + "<input class='form-control input-sm' type='text' id='txt_emp_Code" + (key + 1) + "' value='" + val.EMP_CODE + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.MOBILE_NO + "<input class='form-control input-sm' type='text' id='txt_mobno" + (key + 1) + "' value='" + val.MOBILE_NO + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.ADDRESS + "<input class='form-control input-sm' type='text' id='txt_add" + (key + 1) + "' value='" + val.ADDRESS + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.EMAILID + "<input class='form-control input-sm' type='text' id='txt_emailid" + (key + 1) + "' value='" + val.EMAILID + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.DESIGNATION_NAME + "<input class='form-control input-sm' type='text' id='txt_degnname" + (key + 1) + "' value='" + val.DESIGNATION_NAME + "' style='display:none'><input class='form-control input-sm' type='text' id='txt_designtn" + (key + 1) + "' value='" + val.FK_DESIGNATION_ID + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.BRANCH_NAME + "<input class='form-control input-sm' type='text' id='txt_brnchname" + (key + 1) + "' value='" + val.BRANCH_NAME + "' style='display:none'><input class='form-control input-sm' type='text' id='txt_brnch" + (key + 1) + "' value='" + val.FK_BRANCH_ID + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.DEPARTMENT_NAME + "<input class='form-control input-sm' type='text' id='txt_deptname" + (key + 1) + "' value='" + val.DEPARTMENT_NAME + "' style='display:none'><input class='form-control input-sm' type='text' id='txt_deprt" + (key + 1) + "' value='" + val.FK_DEPARTMENT_ID + "' style='display:none'></input></td>";

            tableBody += "<td>" + val.DATE_JOINING + "<input class='form-control input-sm' type='text' id='txt_doj" + (key + 1) + "' value='" + val.DATE_JOINING + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.DATE_BIRTH + "<input class='form-control input-sm' type='text' id='txt_dob" + (key + 1) + "' value='" + val.DATE_BIRTH + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.LAST_WORKING_DATE + "<input class='form-control input-sm' type='text' id='txt_Lwd" + (key + 1) + "' value='" + val.LAST_WORKING_DATE + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.VERTICAL_NAME + "<input class='form-control input-sm' type='text' id='txt_vertname" + (key + 1) + "' value='" + val.VERTICAL_NAME + "' style='display:none'><input class='form-control input-sm' type='text' id='txt_vert" + (key + 1) + "' value='" + val.FK_VERTICAL_ID + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.COST_CENTER_NAME + "<input class='form-control input-sm' type='text' id='txt_costname" + (key + 1) + "' value='" + val.COST_CENTER_NAME + "' style='display:none'><input class='form-control input-sm' type='text' id='txt_cost" + (key + 1) + "' value='" + val.FK_COST_CENTRE_ID + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.IFSCODE + "<input class='form-control input-sm' type='text' id='txt_Ifsc" + (key + 1) + "' value='" + val.IFSCODE + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.GRADE + "<input class='form-control input-sm' type='text' id='grade" + (key + 1) + "' value='" + val.GRADE + "' style='display:none'><input class='form-control input-sm' type='text' id='txt_grade" + (key + 1) + "' value='" + val.FK_GRADE_ID + "' style='display:none'></input></td>";
            tableBody += "<td class='click_Edit'><img src='../../assets/images/edit.png' title='Edit' alt='Edit'/></td>";
            tableBody += "<td class='click_Delete'><img src='../../assets/images/delete.png' title='Delete' alt='Delete'/></td>";
            tableBody += "</tr>";
        })
        var table = "<table class='display nowrap' id='tbl_Employee'>" +
              "<thead><tr>" + tableHeader + "</tr></thead>" +
              "<tbody>" + tableBody + "</tbody>" +
              "</table>";
        $("#div_Employee").empty();
        $("#div_Employee").html(table);

        $("#tbl_Employee").DataTable({
            //dom: 'lfBrtip',
            //buttons: [
            //    { extend: 'print', exportOptions: { columns: ':visible' } },
            //    { extend: 'copy', exportOptions: { columns: ':visible' } },
            //    { extend: 'csv', exportOptions: { columns: ':visible' } },
            //    { extend: 'excel', exportOptions: { columns: ':visible' } },
            //    { extend: 'pdf', exportOptions: { columns: ':visible' } },
            //    'colvis'
            //]
        });
        $("#loading-div-background").hide();
    }
}

function getResult(keyToFilter, valueStartsWith) {
    return _.filter(EmployeeDetail, function (d) { return d[keyToFilter].startsWith(valueStartsWith); })
}

function bindBranch() {
    PageMethods.getbranchData(function (response) {
        branchJson = JSON.parse(response[0]);
        BindBranch();
    },
        function (error) {
        });
}

function BindBranch() {
    $("#txt_branchnm").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: branchJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#txt_branchnm").val(ui.item.label);
            Branchid = ui.item.id;
            BranchName = ui.item.label;
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

$("#btn_Save").click(function () {
    SaveData();
});

function SaveData() {
    var Employee = [];
    var EmpName = $("#txt_Empname").val();
    var Empcode = $("#txt_empcode").val();
    var MobNo = $("#txt_Mobno").val();
    var Addrss = $("#txt_address").val();
    var emailId = $("#txt_emailid").val();
    //var company = $("#txt_company").val();
    var dtofJoin = $("#txt_dojin").val();
    var dtofbirth = $("#txt_dob").val();
    var dtofLWD = $("#txt_lwd").val();
    var IfscCode = $("#txt_ifsccode").val();

    if (EmpName == "") {
        $.jAlert({
            'content': 'Please Enter Employee Name.',
            'onClose': function () {
                $('#txt_Empname').focus();
            }
        });
        return false;
    }
    else if (Empcode == "") {
        $.jAlert({
            'content': 'Please Enter Employee Code.',
            'onClose': function () {
                $('#txt_empcode').focus();
            }
        });
        return false;
    }
    else if (MobNo == "") {
        $.jAlert({
            'content': 'Please Enter Mobile Number.',
            'onClose': function () {
                $('#txt_Mobno').focus();
            }
        });
        return false;
    }
    else if (Addrss == "") {
        $.jAlert({
            'content': 'Please Enter Employee Address.',
            'onClose': function () {
                $('#txt_address').focus();
            }
        });
        return false;
    }
    else if (emailId == "") {
        $.jAlert({
            'content': 'Please Enter Email Id.',
            'onClose': function () {
                $('#txt_emailid').focus();
            }
        });
        return false;
    }

    else if (Designationid == "" || Designationid == "0" || Designationid == undefined) {
        $.jAlert({
            'content': 'Please Select Designation Name.',
            'onClose': function () {
                $('#txt_designation').focus();
            }
        });
        return false;
    }
    else if (Branchid == "" || Branchid == "0" || Branchid == undefined) {
        $.jAlert({
            'content': 'Please Select Branch.',
            'onClose': function () {
                $('#txt_branchnm').focus();
            }
        });
        return false;
    }
    else if (Departmentid == "" || Departmentid == "0" || Departmentid == undefined) {
        $.jAlert({
            'content': 'Please Select Department.',
            'onClose': function () {
                $('#txt_Dept_name').focus();
            }
        });
        return false;
    }
    else if (dtofJoin == "") {
        $.jAlert({
            'content': 'Please Enter Date of Joining.',
            'onClose': function () {
                $('#txt_dojin').focus();
            }
        });
        return false;
    }
    else if (dtofbirth == "") {
        $.jAlert({
            'content': 'Please Enter Date of Birth.',
            'onClose': function () {
                $('#txt_dob').focus();
            }
        });
        return false;
    }
    else if (dtofLWD == "") {
        $.jAlert({
            'content': 'Please Enter Last working Day.',
            'onClose': function () {
                $('#txt_lwd').focus();
            }
        });
        return false;
    }
    else if (Verticalid == "" || Verticalid == "0" || Verticalid == undefined) {
        $.jAlert({
            'content': 'Please Select Vertical Name.',
            'onClose': function () {
                $('#txt_vertical').focus();
            }
        });
        return false;
    }
    else if (Costcenterid == "" || Costcenterid == "0" || Costcenterid == undefined) {
        $.jAlert({
            'content': 'Please Select Cost Center Name.',
            'onClose': function () {
                $('#txt_Costcenter').focus();
            }
        });
        return false;
    }

    else if (IfscCode == "") {
        $.jAlert({
            'content': 'Please Enter IFSC code.',
            'onClose': function () {
                $('#txt_ifsccode').focus();
            }
        });
        return false;
    }
    else if (Gradeid == "" || Gradeid == "0" || Gradeid == undefined) {
        $.jAlert({
            'content': 'Please Select Grade.',
            'onClose': function () {
                $('#txt_grade').focus();
            }
        });
        return false;
    }

    var objLocation;
    if (saveType == "SAVE") {
        objLocation = {
            "EMP_NAME": EmpName, "EMP_CODE": Empcode, "MOBILE_NO": MobNo, "ADDRESS": Addrss, "EMAILID": emailId, "FK_DESIGNATION_ID": Designationid, "FK_BRANCH_ID": Branchid, "FK_DEPARTMENT_ID": Departmentid, "DATE_JOINING": dtofJoin, "DATE_BIRTH": dtofbirth, "LAST_WORKING_DATE": dtofLWD, "FK_VERTICAL_ID": Verticalid, "FK_COST_CENTRE_ID": Costcenterid, "IFSCODE": IfscCode, "FK_GRADE_ID": Gradeid, "IS_ACTIVE": "1", "CREATED_BY": $('#txt_UserID').val()
        };
    }
    else {
        objLocation = {
            "PK_EMP_ID": EmpPKid, "EMP_NAME": EmpName, "EMP_CODE": Empcode, "MOBILE_NO": MobNo, "ADDRESS": Addrss, "EMAILID": emailId, "FK_DESIGNATION_ID": Designationid, "FK_BRANCH_ID": Branchid, "FK_DEPARTMENT_ID": Departmentid, "DATE_JOINING": dtofJoin, "DATE_BIRTH": dtofbirth, "LAST_WORKING_DATE": dtofLWD, "FK_VERTICAL_ID": Verticalid, "FK_COST_CENTRE_ID": Costcenterid, "IFSCODE": IfscCode, "FK_GRADE_ID": Gradeid, "IS_ACTIVE": "1", "CREATED_BY": $('#txt_UserID').val()
        };
    }
    //alert(objLocation);
    Employee.push(objLocation);
    PageMethods.saveData(JSON.stringify(Employee), saveType, function (response) {
        $.jAlert({
            'content': response,
        });
        onLoad();
    }, function (error) {
        $.jAlert({
            'content': error,
        });
    });
}

$('body').on('click', '.click_Edit', function () {
    var $row = $(this).closest('tr');
    var table = $('#tbl_Employee').DataTable();
    var data = table.row($row).data();
    $("#txt_Empname").val(EmployeeDetail[parseInt(data[0]) - 1].EMP_NAME);
    $("#txt_empcode").val(EmployeeDetail[parseInt(data[0]) - 1].EMP_CODE);
    $("#txt_Mobno").val(EmployeeDetail[parseInt(data[0]) - 1].MOBILE_NO);
    $("#txt_address").val(EmployeeDetail[parseInt(data[0]) - 1].ADDRESS);
    $("#txt_emailid").val(EmployeeDetail[parseInt(data[0]) - 1].EMAILID);
    Designationid = EmployeeDetail[parseInt(data[0]) - 1].FK_DESIGNATION_ID;
    $("#txt_designation").val(EmployeeDetail[parseInt(data[0]) - 1].DESIGNATION_NAME);
    Branchid = EmployeeDetail[parseInt(data[0]) - 1].FK_BRANCH_ID;
    $("#txt_branchnm").val(EmployeeDetail[parseInt(data[0]) - 1].BRANCH_NAME);
    Departmentid = EmployeeDetail[parseInt(data[0]) - 1].FK_DEPARTMENT_ID;
    $("#txt_Dept_name").val(EmployeeDetail[parseInt(data[0]) - 1].DEPARTMENT_NAME);
    $("#txt_dojin").val(EmployeeDetail[parseInt(data[0]) - 1].DATE_JOINING);
    $("#txt_dob").val(EmployeeDetail[parseInt(data[0]) - 1].DATE_BIRTH);
    $("#txt_lwd").val(EmployeeDetail[parseInt(data[0]) - 1].LAST_WORKING_DATE);
    Verticalid = EmployeeDetail[parseInt(data[0]) - 1].FK_VERTICAL_ID;
    $("#txt_vertical").val(EmployeeDetail[parseInt(data[0]) - 1].VERTICAL_NAME);
    Costcenterid = EmployeeDetail[parseInt(data[0]) - 1].FK_COST_CENTRE_ID;
    $("#txt_Costcenter").val(EmployeeDetail[parseInt(data[0]) - 1].COST_CENTER_NAME);
    $("#txt_ifsccode").val(EmployeeDetail[parseInt(data[0]) - 1].IFSCODE);
    Gradeid = EmployeeDetail[parseInt(data[0]) - 1].FK_GRADE_ID;
    $("#txt_grade").val(EmployeeDetail[parseInt(data[0]) - 1].GRADE);
    EmpPKid = EmployeeDetail[parseInt(data[0]) - 1].PK_EMP_ID;
    saveType = "EDIT";
    $("#btn_Save").html('Update');
});

$('body').on('click', '.click_Delete', function () {
    var $row = $(this).closest('tr');
    var table = $('#tbl_Employee').DataTable();
    var data = table.row($row).data();
    $("#txt_Empname").val(EmployeeDetail[parseInt(data[0]) - 1].EMP_NAME);
    $("#txt_empcode").val(EmployeeDetail[parseInt(data[0]) - 1].EMP_CODE);
    $("#txt_Mobno").val(EmployeeDetail[parseInt(data[0]) - 1].MOBILE_NO);
    $("#txt_address").val(EmployeeDetail[parseInt(data[0]) - 1].ADDRESS);
    $("#txt_emailid").val(EmployeeDetail[parseInt(data[0]) - 1].EMAILID);
    Designationid = EmployeeDetail[parseInt(data[0]) - 1].FK_DESIGNATION_ID;
    $("#txt_designation").val(EmployeeDetail[parseInt(data[0]) - 1].DesignationName);
    Branchid = EmployeeDetail[parseInt(data[0]) - 1].FK_BRANCH_ID;
    $("#txt_branchnm").val(EmployeeDetail[parseInt(data[0]) - 1].BranchName);
    Departmentid = EmployeeDetail[parseInt(data[0]) - 1].FK_DEPARTMENT_ID;
    $("#txt_Dept_name").val(EmployeeDetail[parseInt(data[0]) - 1].DepartmentName);
    $("#txt_dojin").val(EmployeeDetail[parseInt(data[0]) - 1].DATE_JOINING);
    $("#txt_dob").val(EmployeeDetail[parseInt(data[0]) - 1].DATE_BIRTH);
    $("#txt_lwd").val(EmployeeDetail[parseInt(data[0]) - 1].LAST_WORKING_DATE);
    Verticalid = EmployeeDetail[parseInt(data[0]) - 1].FK_VERTICAL_ID;
    $("#txt_vertical").val(EmployeeDetail[parseInt(data[0]) - 1].VerticalName);
    Costcenterid = EmployeeDetail[parseInt(data[0]) - 1].FK_COST_CENTRE_ID;
    $("#txt_Costcenter").val(EmployeeDetail[parseInt(data[0]) - 1].CostCenterName);
    $("#txt_ifsccode").val(EmployeeDetail[parseInt(data[0]) - 1].IFSCODE);
    Gradeid = EmployeeDetail[parseInt(data[0]) - 1].FK_GRADE_ID;
    $("#txt_grade").val(EmployeeDetail[parseInt(data[0]) - 1].Grade);
    EmpPKid = EmployeeDetail[parseInt(data[0]) - 1].PK_EMP_ID;
    saveType = "DELETE";
    $("#btn_Save").html('Delete');
    $.jAlert({ 'type': 'confirm', 'onConfirm': function () { SaveData() } });
});

$("#btn_Cancel").click(function () {
    window.location.replace("../../HomePage.aspx");
});

function ValidateEmail(email) {
    var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    return expr.test(email);
};

$("#btn_Show").click(function () {
    var ser = $("#txt_Subject").val();
    $("#loading-div-background").show();
    GetEmployeeData();
});

//$(document).on('', '#txt_Subject', function () {
//    var ser = $("#txt_Subject").val();
//    $("#loading-div-background").show();
//    GetEmployeeData();

//});

function GetEmpData() {
    var ser = $("#txt_Subject").val();
    $("#loading-div-background").show();
    GetEmployeeData();
}
