﻿var Client_ID = '', Master_API = '';
var UserJson = [], UserID, ZoneID;
var CatDtl = '';

$(document).ready(function () {
    Client_ID = $("#txt_Client_ID").val();
    Master_API = $("#txt_Master_URL").val();
    addmodalpopup();
    $("#loading-div-background").show();
    GetRegionData();
    getZoneData();
});


function getZoneData() {
    PageMethods.GetZoneData(Client_ID, Master_API, function (response) {
        ItemJson = JSON.parse(JSON.parse(response[0]));
        BindZone();
    },
        function (error) {
        });
}

function BindZone() {
    $("#Sel_Zone_Name").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: ItemJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#Sel_Zone_Name").val(ui.item.label);
            ZoneID = ui.item.id;
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}


function GetRegionData() {
    PageMethods.GetRegionData(Client_ID, Master_API, function (response) {
        CatDtl = JSON.parse(JSON.parse(response[0]));
        if (CatDtl.Message == "Data Not Found") {
            alert("Region Data Not Found");
            $("#loading-div-background").hide();
        }
        else {
            RegionOnSuccess();
        }
    },
        function (error) {
        });
}

function RegionOnSuccess() {
    if (CatDtl) {
        var tableHeader = "";
        var tableBody = "";
        tableHeader += "<th style='width: 2%;'>#</th>";
        tableHeader += "<th>Zone Name</th>";
        tableHeader += "<th>Region Name</th>";
        tableHeader += "<th>Region Head</th>";
        tableHeader += "<th>Edit</th>";
        tableHeader += "<th>Delete</th>";
        $.each(CatDtl, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.ZONENAME + "<input class='form-control input-sm' type='text' id='txt_ZoneName" + (key + 1) + "' value='" + val.ZONENAME + "' style='display:none'><input class='form-control input-sm' type='text' id='txt_ZoneId" + (key + 1) + "' value='" + val.FK_ZONEID + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.label + "<input class='form-control input-sm' type='text' id='txt_Region" + (key + 1) + "' value='" + val.label + "' style='display:none'></input>";
            tableBody += "<td>" + val.REGION_HEAD + "<input class='form-control input-sm' type='text' id='txt_RegionHead" + (key + 1) + "' value='" + val.REGION_HEAD + "' style='display:none'></input>";
            tableBody += "<td><a><img src='../../assets/images/edit.png' onclick='return bindData(" + (key + 1) + ")' title='Add New Row' /></a><input class='form-control input-sm' type='text' id='txt_Pk" + (key + 1) + "' value='" + val.id + "' style='display:none'></input></td>";
            tableBody += "<td><a><img src='../../assets/images/delete.png' onclick='return DeleteData(" + (key + 1) + ")' title='delete Row' /></a></td>";
            tableBody += "</tr>";
        });
        var table = "<table ID='CatTable' class='display nowrap dataTable'>" +
              "<thead><tr>" + tableHeader + "</tr></thead>" +
              "<tbody>" + tableBody + "</tbody>" +
              "</table>";
        $("#div_displayDtl").empty();
        $("#div_displayDtl").html(table);
        $("#CatTable").dataTable({
            dom: 'Bfrtip', "bSort": true,
            buttons: [
                'excel'
            ]
        });
    }
    $("#loading-div-background").hide();
}

$("#btn_Save").click(function () {
    if ($("#Sel_Zone_Name").val() == "") {
        $.jAlert({
            'content': 'Please Select Zone Name..!'
        });
        return false;
    }
    if ($("#txt_Region_Name").val() == "") {
        $.jAlert({
            'content': 'Please Enter Region Name..!'
        });
        return false;
    }
    if ($("#txt_Region_Head").val() == "") {
        $.jAlert({
            'content': 'Please Enter Region Head..!'
        });
        return false;
    }
    PageMethods.SaveData(ZoneID, $("#txt_Region_Name").val(), $("#txt_Region_Head").val(), $("#txt_UserName").val(), Client_ID, Master_API, function (response) {
        if (JSON.parse(response) == "OK") {
            $.jAlert({
                'content': 'Data Saved Successfully..!'
            });
            GetRegionData();
            $("#Sel_Zone_Name").val("");
            $("#txt_Region_Name").val("");
            $("#txt_Region_Head").val("");
        }
        else if (JSON.parse(response) == "NotFound") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(response) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Saved Due To Internal Server Error..!'
            });
        }
    }, function (error) {
    });
})

function bindData(index) {
    $("#btn_Save").hide();
    $("#btn_Upd").show();
    ZoneID = $("#txt_ZoneId" + index).val();
    $("#Sel_Zone_Name").val($("#txt_ZoneName" + index).val());
    $("#txt_Region_Name").val($("#txt_Region" + index).val());
    $("#txt_Region_Head").val($("#txt_RegionHead" + index).val());
    $("#txt_PK_RegionID").val($("#txt_Pk" + index).val());
}

$("#btn_Upd").click(function () {
    if ($("#Sel_Zone_Name").val() == "") {
        $.jAlert({
            'content': 'Please Select Zone Name..!'
        });
        return false;
    }
    if ($("#txt_Region_Name").val() == "") {
        $.jAlert({
            'content': 'Please Enter Region Name..!'
        });
        return false;
    }
    if ($("#txt_Region_Head").val() == "") {
        $.jAlert({
            'content': 'Please Enter Region Head..!'
        });
        return false;
    }
    PageMethods.UpdateData($("#txt_PK_RegionID").val(), ZoneID, $("#txt_Region_Name").val(), $("#txt_Region_Head").val(), Client_ID, Master_API, function (response) {
        if (JSON.parse(response) == "OK") {
            $.jAlert({
                'content': 'Data Edited Successfully..!'
            });
            GetRegionData();
            ZoneID = "";
            $("#Sel_Zone_Name").val("");
            $("#txt_Region_Name").val("");
            $("#txt_Region_Head").val("");
            $("#btn_Save").show();
            $("#btn_Upd").hide();
        }
        else if (JSON.parse(response) == "NotFound") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(response) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Saved Due To Internal Server Error..!'
            });
        }
    }, function (error) {
    });
});

function DeleteData(index) {
    $("#txt_PK_RegionID").val($("#txt_Pk" + index).val());
    PageMethods.Deletedata($("#txt_PK_RegionID").val(), Client_ID, Master_API, function (response) {
        if (JSON.parse(response) == "OK") {
            $.jAlert({
                'content': 'Data Deleted Successfully..!'
            });
            GetRegionData();
        }
        else {
            $.jAlert({
                'content': 'Data Not Deleted..!'
            });
        }
    }, function (error) {
    });
}

$("#btn_Cancel").click(function () {
    window.location.replace("../../HomePage.aspx");
});