﻿var catJson, subCatJson, itmJson, partJson, makeJson, modelJson, templateJson, templateJsonbck, templatedtlJson, templatedtlJsonbck;
var catid, subcatid, itmid, templatename, templatedesc, saveType;
var catid2, subcatid2, itmid2, UpdatefltrdtlJson;

$(document).ready(function () {
    onLoad();

});

function onLoad() {
    $("#form1").trigger("reset");
    saveType = "SAVE";
    addmodalpopup();
    // $("#loading-div-background").show();
    Client_ID = $("#txt_Client_ID").val();
    Master_API = $("#txt_Master_URL").val();
    getCategoryddl();
    getMakeDetail();
    getAllModelData();
    getpart();
    gettemplatedtlData();

    $('#btn_Save').attr('disabled', 'disabled');
    $('#btn_Update').attr('disabled', 'disabled');
    $('#btn_Save').removeClass('hidden');
    $('#btn_Update').addClass('hidden');

}

function getCategoryddl() {
    PageMethods.getCatData(Client_ID, Master_API, function (response) {
        catJson = JSON.parse(JSON.parse(response[0]));
        assignCat();

    }, function (error) {
        alert(error);
    });
}

function assignCat() {
    $("#sel_Category").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: catJson,
        select: function (event, ui) {
            $("#sel_Category").val(ui.item.label);
            catid = ui.item.id;
            $("#sel_SubCategoryName").val("");
            $("#sel_ItemName").val("");
            getsubCategoryddl();
            return false;
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $("#sel_Category").val("");
            }
            else {

            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function getpart() {
    PageMethods.getPartDetails(Client_ID, Master_API, function (response) {
        partJson = JSON.parse(JSON.parse(response[0]));

    });
}

function getsubCategoryddl() {
    PageMethods.getSubCatData(Client_ID, Master_API, catid, function (response) {
        subCatJson = JSON.parse(JSON.parse(response[0]));
        assignsubcat();
        //setDropdown("#sel_SubCategoryName", subCatJson, "");
    }, function (error) {
        alert(error);
    });
}

function assignsubcat() {
    $("#sel_SubCategoryName").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: subCatJson,
        select: function (event, ui) {
            $("#sel_SubCategoryName").val(ui.item.label);
            subcatid = ui.item.id;
            $("#sel_ItemName").val("");
            getallitm();
            return false;
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $("#sel_SubCategoryName").val("");
            }
            else {

            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function getallitm() {
    PageMethods.getallitm(Client_ID, Master_API, catid, subcatid, function (response) {
        itmJson = JSON.parse(JSON.parse(response[0]));
        assignItm();

    }, function (error) {
        alert(error);
    });
}

function assignItm() {
    $("#sel_ItemName").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: itmJson,
        select: function (event, ui) {
            $("#sel_ItemName").val(ui.item.label);
            itmid = ui.item.id;
            getPartDetails();
            return false;
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $("#sel_ItemName").val("");
            }
            else {

            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function getPartDetails() {


    //var curitmid = $(this).val();
    //var itmtypid = _.find(itmJson, function (item) { return item.id == curitmid; }) == undefined ? "0" : _.find(itmJson, function (item) { return item.id == curitmid; }).FK_ITMTYPID;
    //var typeReq = _.find(itmJson, function (item) { return item.id == curitmid; }) == undefined ? "NO" : _.find(itmJson, function (item) { return item.id == curitmid; }).TYPE_REQ.trim();
    var object = {}
    object[String("FK_ITMTYPEID")] = parseInt(subcatid);
    var filteredList = _.where(partJson, object);
    //var filteredList = _.filter(partJson, function (item) {
    //    var temp = "";
    //    return (item.FK_ITMTYPEID == parseInt(itmtypid) || item.PK_PRTID == 31 || item.PRTNAME ==32);
    //});

    if (filteredList) {
        var html = "";
        $("#div_parts").empty();
        html += "<table class='table color-bordered-table info-bordered-table' id='tbl_template'>";
        html += "<thead>";
        html += "<tr>";
        html += "<th style='width: 3%'>#</th>";
        html += "<th style='width: 3%'>Select <input type='checkbox' name='checkall'></th>";
        html += "<th>Details</th>";
        html += "<th>Description</th>";
        html += "</tr>";
        html += "</thead>";
        html += "<tbody>";
        $.each(filteredList, function (key, val) {
            html += "<tr id=" + (key + 1) + " name='" + val.FK_PARTID + "'>";
            html += "<td>" + (key + 1) + "</td>";
            html += "<td><input id='chk" + (key + 1) + "' name='" + val.FK_PARTID + "' type='checkbox' checked /></td>";
            html += "<td id='partname" + (key + 1) + "' name='partname" + (key + 1) + "'>" + val.PART + "</td>";
            if (val.FK_PARTID == 31) {
                if (makeJson != []) {
                    html += "<td id='makedesc" + (key + 1) + "'><select id='mksel_" + (key + 1) + "' class='form-control input-sm'><option value='0'>---SELECT ONE---</option>";
                    $.each(makeJson, function (key, val) {
                        html += "<option value='" + val.id + "'>" + val.label + "</option>";
                    });
                    html += "</select></td>";
                }
                else
                    html += "<td id='makedesc" + (key + 1) + "'><select id='mksel_" + (key + 1) + "' class='form-control input-sm' ><option value='0'>---SELECT ONE---</option></select></td>";
            }
            else if (val.FK_PARTID == 32) {
                var catid = $("#sel_Category").val();
                var subcatid = $("#sel_SubCategoryName").val();
                //var object1 = {}
                //object1[String("catid")] = parseInt(catid);
                //object1[String("subcatid")] = parseInt(subcatid);
                //var filteredmodelList = _.where(modelJson, object1);
                var filteredmodelList = modelJson

                if (filteredmodelList != []) {
                    html += "<td id='modeldesc" + (key + 1) + "'><select id='mdsel_" + (key + 1) + "' class='form-control input-sm'><option value='0'>---SELECT ONE---</option>";
                    $.each(filteredmodelList, function (key, val) {
                        html += "<option value='" + val.PK_MODELID + "'>" + val.MODEL + "</option>";
                    });
                    html += "</select></td>";
                }
                else
                    html += "<td id='modeldesc" + (key + 1) + "'><select id='mdsel_" + (key + 1) + "' class='form-control input-sm'><option value='0'>---SELECT ONE---</option></select></td>";
            }

            else {
                if (parseInt(val.ISNUMBERS) == "1")
                    html += "<td><input class='form-control input-sm' name='numeric' placeholder='only numbers allowed' id='txtdescr" + (key + 1) + "' /></td>";
                else
                    html += "<td><input class='form-control input-sm' id='txtdescr" + (key + 1) + "' /></td>";
                // html += "<td id='txt_sel_desc" + (key + 1) + "'><select id='numsel_" + (key + 1) + "' class='form-control input-sm' ><option value='0'>---SELECT ONE---</option><option value='1GB'>1GB</option><option value='1GB'>1GB</option><option value='2GB'>2GB</option></select></td>";
                html += "</tr>";

            }



        });


        html += " </tbody>";
        html += "</table>";

        $("#div_parts").html(html);


    }

}
function getMakeDetail() {
    PageMethods.getMakeDetail(Client_ID, Master_API, function (response) {
        makeJson = JSON.parse(JSON.parse(response[0]));

    }, function (error) {
        alert(error);
    });
}

function getAllModelData() {
    PageMethods.getAllModelData(Client_ID, Master_API, function (response) {
        modelJson = JSON.parse(JSON.parse(response[0]));
    }, function (error) {
        alert(error);
    });
}

function gettemplateData() {
    PageMethods.gettemplateData(Client_ID, Master_API, function (response) {
        templateJson = JSON.parse(JSON.parse(response[0]));
        templateJsonbck = templateJson;
        if (templatedtlJson.Message != "Data Not Found") {
            displayData(templateJson);
        }

    }, function (error) {
        alert(error);
    });
}

function gettemplatedtlData() {
    PageMethods.gettemplatedtlData(Client_ID, Master_API, function (response) {

        templatedtlJson = JSON.parse(JSON.parse(response[0]));
        if (templatedtlJson.Message != "Data Not Found") {
            gettemplateData();
        }
        // displayData(templateJson);

    }, function (error) {
        alert(error);
    });
}


$(document).on('click', '#btn_generate', function (e) {
    var temp = getcheckeddata();
    if (temp && (!isempty(temp))) {
        var object = {}
        object[String("ischecked")] = "1";
        var filteredList = _.where(temp, object);
        var descstring = "";
        var MAKENAME = "";
        var MODELNAME = "";


        if (filteredList && filteredList.length > 0) {
            var makeobj = _.filter(filteredList, function (item) {
                return (item.makeddl != "not found" && item.makeddltxt != "not found" && item.makeddltxt != "")
            });
            if (makeobj && makeobj.length > 0) {
                makeobj[0].makeddltxt = makeobj[0].makeddltxt == '---SELECT ONE---' ? 'NA' : makeobj[0].makeddltxt;
                MAKENAME = makeobj[0].partname + ":" + makeobj[0].makeddltxt + ',';
            }

            var modalobj = _.filter(filteredList, function (item) {
                return (item.modelddl != "not found" && item.modelddltxt != "not found" && item.modelddltxt != "")

            });
            if (modalobj && modalobj.length > 0) {
                modalobj[0].modelddltxt = modalobj[0].modelddltxt == '---SELECT ONE---' ? 'NA' : modalobj[0].modelddltxt;
                MODELNAME = modalobj[0].partname + ":" + modalobj[0].modelddltxt + ',';
            }

            filteredList = _.filter(filteredList, function (item) {
                return (item.fkpartid != "31" && item.fkpartid != "32");
            })

            if (!isempty(MAKENAME)) {
                descstring += MAKENAME;
                descstring.trim();
            }
            if (!isempty(MODELNAME)) {
                descstring += MODELNAME
                descstring.trim();
            }

            $.each(filteredList, function (key, val) {

                descstring += filteredList[key].partname + ':' + (filteredList[key].txtbox == 'not found' ? 'NA' : filteredList[key].txtbox) + ','


            });
            descstring = String(descstring).trim();
            descstring = descstring.substring(0, descstring.length - 1).toUpperCase();
            $("#txt_TemplateDescription").val(descstring);
            $("#txt_TemplateDescription").trigger('change');

        }
        else {
            descstring = "";
            $("#txt_TemplateDescription").val(descstring);
            $("#txt_TemplateDescription").trigger('change');
        }


    }

    var a = "";
});
$(document).on('keydown', 'input[name=numeric]', function (e) {


    //    $("input[name=numeric]").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
        // Allow: Ctrl/cmd+A
        (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+C
        (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+X
        (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});
$("#txt_TemplateDescription").change(function () {
    if (!isempty($(this).val())) {
        $('#btn_Save').removeAttr('disabled');
        $('#btn_Update').removeAttr('disabled');
    }
    else {
        $('#btn_Save').attr('disabled', 'disabled');
        $('#btn_Update').attr('disabled', 'disabled');
    }
});


$("#btn_Save").click(function () {
    var saveob = getcheckeddata();
    if (saveob != undefined && saveob != [] && saveob != "") {

        var TmplHdr = [];
        var TmplDtl = [];
        var objTmplHdr;
        var objTmplDtl;

        objTmplHdr = {
            "FK_ITMID": itmid, "FK_ITMSUBCATID": subcatid, "TMPLNAME": templatename, "TMPLPODESC": templatedesc, "IS_ACTIVE": "1", "CREATED_BY": $('#txt_UserID').val(), "FK_CLIENTID": Client_ID
        };

        TmplHdr.push(objTmplHdr);

        $.each(saveob, function (key, val) {
            hirar = val.hirar;
            hirar = (isempty(hirar) || hirar == 'not found') ? '' : hirar

            objTmplDtl = {
                "FK_PRTID": val.fkpartid, "TMPLVAL": hirar, "NOS": "0", "CHECKED": val.ischecked, "FK_CLIENTID": Client_ID
            };

            TmplDtl.push(objTmplDtl);

        })

        var transactionData = { "TmplHdrData": TmplHdr, "TmplDtlData": TmplDtl }

        PageMethods.saveData(JSON.stringify(transactionData), Master_API, saveType, function (response) {
            $.jAlert({
                'content': JSON.parse(JSON.parse(response)),
            });
            location.reload(true);
        }, function (error) {
            $.jAlert({
                'content': error,
            });
        });

    }

});


$("#btn_Update").click(function () {
    var updateob = getcheckeddata();
    var pkId = $("#pkID").val();
    saveType = "EDIT";
    var TmplHdr = [];
    var TmplDtl = [];
    var objTmplHdr;
    var objTmplDtl;
    //catid2 = $("#sel_Category").val();
    //subcatid2 = $("#sel_SubCategoryName").val();
    //itmid2 = $("#sel_ItemName").val();
    templatename2 = $("#txt_TemplateName").val().trim();
    templatedesc2 = $("#txt_TemplateDescription").val().trim();

    objTmplHdr = {
        "PK_TMPLHDRID": pkId, "FK_ITMID": itmid2, "FK_ITMSUBCATID": subcatid2, "TMPLNAME": templatename2, "TMPLPODESC": templatedesc2, "CREATED_BY": $('#txt_UserID').val()
    };

    TmplHdr.push(objTmplHdr);

    $.each(updateob, function (key, val) {
        hirar = val.hirar;
        hirar = (isempty(hirar) || hirar == 'not found') ? '' : hirar

        objTmplDtl = {
            "PK_TMPLDTLID": val.pkdtlid, "FK_TMPLHDRID": pkId, "FK_PRTID": val.fkpartid, "TMPLVAL": hirar, "NOS": "0", "CHECKED": val.ischecked
        };

        TmplDtl.push(objTmplDtl);

    })

    var transactionData = { "TmplHdrData": TmplHdr, "TmplDtlData": TmplDtl }

    PageMethods.saveData(JSON.stringify(transactionData), Master_API, saveType, function (response) {
        $.jAlert({
            'content': JSON.parse(JSON.parse(response)),
        });
        location.reload(true);
    }, function (error) {
        $.jAlert({
            'content': error,
        });
    });




});



function setmodel(id) {
    // alert(ddlid, ddlval());
    alert(id);
    alert($("#" + id).val());
}

$('#Button11').click(function () {
    //var $row = $(this).closest("tr");    // Find the row
    //var $text = $row.find(".nr").text(); // Find the text


    var data = {
        firstName: [], lastName: []
    }

    //loop through the tr's
    $('#div_parts table tbody tr').each(function () {
        var d = $(this).data();

        //look for the fields firstName and lastName in the tr
        //get their values and push into storage        
        data.firstName.push($('.form-control input-sm', this).val());
        data.lastName.push($('.form-control input-sm', this).val());
    });



    var table = $("#tbl_template").closest("table").DataTable();
    var ids = $.map(table.rows('.selected').data(), function (item) {
        return item[0]
    });
    console.log(ids)
    alert(table.rows('.selected').data().length + ' row(s) selected');




});



function getcheckeddata() {
    if (validation()) {
        var values = $("#div_parts table tbody tr").map(function () {   //input:checked//:checked
            row = $(this).closest("tr");
            var rowid = row.index();
            var rowname = row.attr("name");
            //partname = "partname" + rowid;
            return {
                chkid: (rowid + 1),
                ischecked: $(row).find("input[id='chk" + (rowid + 1) + "']").is(':checked') == true ? "1" : "0",
                partname: $(row).find("td[id='partname" + (rowid + 1) + "']").text() || "not found",
                fkpartid: rowname,
                txtbox: $(row).find("input[id='txtdescr" + (rowid + 1) + "']").val() || "not found",
                makeddl: $(row).find("select[id='mksel_" + (rowid + 1) + "']").val() || "not found",
                makeddltxt: $('#' + $(row).find("select[id='mksel_" + (rowid + 1) + "' ]").attr('id') + ' option:selected').text() || "not found",
                modelddl: $(row).find("select[id='mdsel_" + (rowid + 1) + "']").val() || "not found",
                modelddltxt: $('#' + $(row).find("select[id='mdsel_" + (rowid + 1) + "' ]").attr('id') + ' option:selected').text() || "not found",
                numddl: $(row).find("select[id='numsel_" + (rowid + 1) + "']").val() || "not found",
                hirar: $(row).find("input[id='txtdescr" + (rowid + 1) + "']").val() || $(row).find("select[id='mksel_" + (rowid + 1) + "']").val() || $(row).find("select[id='mdsel_" + (rowid + 1) + "']").val() || $(row).find("select[id='numsel_" + (rowid + 1) + "']").val() || "not found",
                pkdtlid: $(row).find("input[id='pkdtlid" + (rowid + 1) + "']").val() || "not found",
                //txtdescr: $(row).find("input[id='txtdescr" + (rowid + 1) + "']").val() || "blank",
                //makedesc: $(row).find("input[id='makedesc" + (rowid + 1) + "']").val() || "not found",
                //desc: $(row).find("input[id='sel_" + (rowid + 1) + "']").val() || $(row).find("input[id='txtdescr" + (rowid + 1) + "']").val() || "blank",
                //checkbox_id: $(this).val(),
                //hidden_id: $(row).find("input[name=id]").val(),
                //address: $(row).find("input[name=address]").val(),
                //radio: $(row).find('input[type=radio]:checked').val() || "not selected"
            }
        }).get();
        //console.log(values);
        return values;
    }
    else {
        return "";
    }
}


function validation() {
    // var catid, subcatid, itmid, templatename, templatedesc;
    catid1 = $("#sel_Category").val();
    subcatid1 = $("#sel_SubCategoryName").val();
    itmid1 = $("#sel_ItemName").val();
    templatename = $("#txt_TemplateName").val().trim();
    templatedesc = $("#txt_TemplateDescription").val().trim();

    if (catid1 == "0" || catid1 == "" || catid1 == undefined) {
        $.jAlert({
            'content': 'Please select Category',
            'onClose': function () {
                $("#sel_Category").focus();
            }
        });

        return false;
    }

    else if (subcatid1 == "0" || subcatid1 == "" || subcatid1 == undefined) {
        $.jAlert({
            'content': 'Please select Sub-Category',
            'onClose': function () {
                $("#sel_SubCategoryName").focus();
            }
        });

        return false;
    }

    else if (itmid1 == "0" || itmid1 == "" || itmid1 == undefined) {
        $.jAlert({
            'content': 'Please select Item',
            'onClose': function () {
                $("#sel_ItemName").focus();
            }
        });

        return false;
    }

    else if (templatename == "" || templatename == undefined) {
        $.jAlert({
            'content': 'Please Enter Template Name',
            'onClose': function () {
                $("#txt_TemplateName").focus();
            }
        });

        return false;
    }
        //else if (templatedesc == "" || templatedesc == undefined) {
        //    alert("Please Enter Template Description");
        //    $("#txt_TemplateDescription").focus();
        //    return false;
        //}
    else {
        return true;
    }

}


function displayData(obj) {
    if (obj) {
        var tableHeader = "";
        var tableBody = "";

        tableHeader += " <th style='width: 3%'>#</th>";
        tableHeader += "<th>Category Name</th>";
        tableHeader += "<th>Sub-Category Name</th>";
        tableHeader += "<th>Item</th>";
        tableHeader += "<th>Template Name</th>";
        tableHeader += "<th>Template Description</th>";
        tableHeader += "<th style='width: 3%'>Edit</th>";
        tableHeader += "<th style='width: 3%'>Delete</th>";

        $.each(obj, function (key, val) {

            tableBody += "<tr id=" + key + ">";
            tableBody += "<td><a data-toggle='collapse' data-parent='#accordion' href='#collapseOne" + key + "' aria-controls='collapseOne" + key + "' class='btn btn-linkedin waves-effect btn-circle waves-light'>";
            tableBody += "<i class='fa fa-fw fa-plus ' ></i></a></td>";
            tableBody += "<td>" + val.ITEMCATNAME + "</td>";
            tableBody += "<td>" + val.ITEMSUBCATNAME + "</td>";
            tableBody += "<td>" + val.ITEMNAME + "</td>";
            tableBody += "<td>" + val.TMPLNAME + "</td>";
            tableBody += "<td>" + val.TMPLPODESC + "</td>";
            tableBody += "<td class='text-center click_Edit'><button class='btn btn-facebook waves-effect btn-circle waves-light' type='button'><i class='fa fa-edit'></i></button></td>";
            tableBody += "<td class='text-center click_Delete'><button class='btn btn-googleplus waves-effect btn-circle waves-light' type='button'><i class='fa fa-trash-o'></i></button></td>";
            tableBody += "</tr>";

            var object = {}
            object[String("FK_TMPLHDRID")] = parseInt(val.PK_TMPLHDRID);
            object[String("CHECKED")] = parseInt(1);
            var filterdtlJson = _.where(templatedtlJson, object);


        })
        var table = "<table class='table table-striped table-bordered table-hover' id='tbl_alltemplatedtl' runat='server'>" +
              "<thead><tr>" + tableHeader + "</tr></thead>" +
              "<tbody>" + tableBody + "</tbody>" +
              "</table>";
        // $("#div_ItemTemplate").empty();
        //$("#div_ItemTemplate").html(table);

        var table = $("#tbl_alltemplatedtl").DataTable({
            //"bDestroy": true,

            'fnCreatedRow': function (nRow, aData, iDataIndex) {

                $(nRow).attr('id', iDataIndex); // or whatever you choose to set as the id
            },
            data: obj,
            columns: [
            {
                className: 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": '',
                title: "#", render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }

            },
            { title: "Category Name", data: "ITEMCATNAME" },
            { title: "Sub-Category Name", data: "ITEMSUBCATNAME" },
            { title: "Item", data: "ITEMNAME" },
            { title: "Template Name", data: "TMPLNAME" },
            { title: "Template Description", data: "TMPLPODESC" },
              {
                  title: "Edit", className: 'text-center click_Edit', render: function (data, type, full, meta) {
                      return "<td class='text-center click_Edit'><button class='btn btn-facebook waves-effect btn-circle waves-light' type='button'><i class='fa fa-edit'></i></button></td>";
                  }
              },
            {
                title: "Delete", className: 'text-center click_Delete', render: function (data, type, full, meta) {
                    return "<td class='text-center click_Delete'><button class='btn btn-googleplus waves-effect btn-circle waves-light' type='button'><i class='fa fa-trash-o'></i></button></td>";
                }
            }
            ],
            // "order": [[1, 'asc']],
            dom: 'lBfrtip',
            buttons: [
               // { extend: 'print', exportOptions: { columns: ':visible' } },
              //  { extend: 'copy', exportOptions: { columns: ':visible' } },
               // { extend: 'csv', exportOptions: { columns: ':visible' } },
               // { extend: 'excel', exportOptions: { columns: ':visible' } },
               // { extend: 'pdf', exportOptions: { columns: ':visible' } },
             //   'colvis'
            ]


        });


        $("#tbl_alltemplatedtl tbody").on("click", "td.details-control", function () {
            var tr = $(this).closest("tr");
            var row = table.row(tr);

            if (row.child.isShown()) {
                row.child.hide();
                tr.removeClass("shown");
            }
            else {
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        })




        $("#loading-div-background").hide();


        function format(d) {
            // `d` is the original data object for the row
            var object = {}
            object[String("id")] = parseInt(d.id);
            object[String("CHECKED")] = parseInt(1);
            var filterdtlJson = _.where(templatedtlJson, object);

            var tableBody;

            //tableBody += "<tr id='collapseOne"+key+"' class='panel-collapse collapse' role='tabpanel' aria-labelledby='headingOne'>";
            tableBody += "<tr>";
            tableBody += "<td>";
            tableBody += "<table class='table color-bordered-table info-bordered-table'>";
            tableBody += "<thead>";
            tableBody += "<tr>";
            tableBody += "<th class='active'>#</th>";
            tableBody += "<th class='active'>Part Name</th>";
            tableBody += "<th class='active'>Value</th>";
            tableBody += "</tr>";
            tableBody += "</thead>";
            tableBody += "<tbody>";
            $.each(filterdtlJson, function (key, val) {
                tableBody += "<tr class='danger'>";

                tableBody += "<td>" + (key + 1) + "</td>";
                tableBody += "<td>" + val.PART_NAME + "</td>";
                if (val.FK_PRTID == 31 || val.FK_PRTID == 32) {
                    tableBody += "<td>" + val.NAME + "</td>";
                }
                else {
                    tableBody += "<td>" + val.TMPLVAL + "</td>";
                }


                tableBody += "</tr>";

            })

            tableBody += "</tbody>";
            tableBody += "</table>";
            tableBody += "</td>";
            tableBody += "</tr>";
            return tableBody;


        }

    }
}

$('body').on('click', '.click_Edit', function () {
    //templateJson = templateJsonbck;
    var index = parseInt($(this).parent('tr').attr('id'));
    if (index != undefined && index != null) {
        var pkid = templateJson[index].PK_TMPLHDRID;

        $("#sel_Category").val(templateJson[index].ITEMCATNAME);
        catid2 = templateJson[index].PK_ITMCATID;
        $("#sel_SubCategoryName").val(templateJson[index].ITEMSUBCATNAME);
        subcatid2 = templateJson[index].FK_ITMSUBCATID;
        $("#sel_ItemName").val(templateJson[index].ITEMNAME);
        itmid2 = templateJson[index].FK_ITMID;
        $("#txt_TemplateName").val(templateJson[index].TMPLNAME);
        $("#txt_TemplateDescription").val(templateJson[index].TMPLPODESC);

        /////////////////////////////////////////////////////////////////////


        var curitmid = $("#sel_ItemName").val();
        var itmtypid = parseInt(templateJson[index].FK_ITMSUBCATID); //_.find(itmJson, function (item) { return item.id == curitmid; }) == undefined ? "0" : _.find(itmJson, function (item) { return item.id == curitmid; }).FK_ITMSUBCATID;
        //  var typeReq = _.find(itmJson, function (item) { return item.id == curitmid; }) == undefined ? "NO" : _.find(itmJson, function (item) { return item.id == curitmid; }).TYPE_REQ.trim();
        var object = {}
        object[String("FK_ITMTYPEID")] = parseInt(itmtypid);
        var filteredList = _.where(partJson, object);

        var object2 = {};
        object2[String("FK_TMPLHDRID")] = parseInt(pkid);
        var filteredDtlList = _.where(templatedtlJson, object2);


        if (filteredList) {
            var html = "";
            $("#div_parts").empty();
            html += "<table class='table color-bordered-table info-bordered-table' id='tbl_template'>";
            html += "<thead>";
            html += "<tr>";
            html += "<th style='width: 3%'>#</th>";
            html += "<th style='width: 3%'>Select<input type='checkbox' name='checkall'></th>";
            html += "<th>Details</th>";
            html += "<th>Description</th>";
            html += "</tr>";
            html += "</thead>";
            html += "<tbody>";
            $.each(filteredList, function (key, val) {

                html += "<tr id=" + (key + 1) + " name='" + val.FK_PARTID + "'>";
                html += "<td>" + (key + 1) + "</td>";
                html += "<td><input id='chk" + (key + 1) + "' name='" + val.FK_PARTID + "' type='checkbox' checked /><input type='hidden' id='pkdtlid" + (key + 1) + "' name='pkdtlid" + (key + 1) + "' value=" + filteredDtlList[key].PK_TMPLDTLID + " /></td>";
                html += "<td id='partname" + (key + 1) + "' name='partname" + (key + 1) + "'>" + val.PART + "</td>";
                //html += "<td type='hidden' id='pkdtlid" + (key + 1) + "' name='pkdtlid" + (key + 1) + "' value=" + filteredDtlList[key].PK_TMPLDTLID + "></td>";
                if (val.FK_PARTID == 31) {
                    if (makeJson != []) {
                        html += "<td id='makedesc" + (key + 1) + "'><select id='mksel_" + (key + 1) + "' class='form-control input-sm'><option value='0'>---SELECT ONE---</option>";
                        $.each(makeJson, function (key, val) {
                            html += "<option value='" + val.id + "'>" + val.label + "</option>";
                        });
                        html += "</select></td>";
                    }
                    else
                        html += "<td id='makedesc" + (key + 1) + "'><select id='mksel_" + (key + 1) + "' class='form-control input-sm' ><option value='0'>---SELECT ONE---</option></select></td>";
                }
                else if (val.FK_PARTID == 32) {
                    var catid = $("#sel_Category").val();
                    var subcatid = $("#sel_SubCategoryName").val();

                    filteredmodelList = modelJson;

                    if (filteredmodelList != []) {
                        html += "<td id='modeldesc" + (key + 1) + "'><select id='mdsel_" + (key + 1) + "' class='form-control input-sm'><option value='0'>---SELECT ONE---</option>";
                        $.each(filteredmodelList, function (key, val) {
                            html += "<option value='" + val.PK_MODELID + "'>" + val.MODEL + "</option>";
                        });
                        html += "</select></td>";
                    }
                    else
                        html += "<td id='modeldesc" + (key + 1) + "'><select id='mdsel_" + (key + 1) + "' class='form-control input-sm'><option value='0'>---SELECT ONE---</option></select></td>";
                }

                else {
                    if (parseInt(val.ISNUMBERS) == "1")
                        html += "<td><input class='form-control input-sm' name='numeric' placeholder='only numbers allowed' id='txtdescr" + (key + 1) + "' /></td>";
                    else
                        html += "<td><input class='form-control input-sm' id='txtdescr" + (key + 1) + "' /></td>";
                    // html += "<td id='txt_sel_desc" + (key + 1) + "'><select id='numsel_" + (key + 1) + "' class='form-control input-sm' ><option value='0'>---SELECT ONE---</option><option value='1GB'>1GB</option><option value='1GB'>1GB</option><option value='2GB'>2GB</option></select></td>";
                    html += "</tr>";

                }



            });


            html += " </tbody>";
            html += "</table>";

            $("#div_parts").html(html);


        }


        ////////////////////////////////////////////////////////////////
        var k = 0;
        $("#div_parts input[type=checkbox]").not("input[name='checkall']").each(function (i) {

            if (filteredDtlList && filteredDtlList.length > 0) {

                if (filteredDtlList[i].CHECKED == "1") {
                    $(this.checked = true);

                }
                else {
                    $(this.checked = false);
                }

                if (filteredDtlList[i].FK_PRTID == "31") {
                    $("#mksel_" + (i + 1)).val(filteredDtlList[i].TMPLVAL);
                }
                else if (filteredDtlList[i].FK_PRTID == "32") {
                    $("#mdsel_" + (i + 1)).val(filteredDtlList[i].TMPLVAL);
                }
                else {
                    $("#txtdescr" + (i + 1)).val(filteredDtlList[i].TMPLVAL);
                }
            }
        });
        $("#pkID").val(parseInt(pkid));
        $('#btn_Update').removeClass('hidden');
        $('#btn_Save').addClass('hidden');

        templateJson = templateJson.filter(function (item) {
            return item.id !== parseInt(pkid)
        });

    }

});
$('body').on('click', '.click_Delete', function () {
    saveType = "DELETE";
    templateJson = templateJsonbck;
    var index = parseInt($(this).parent('tr').attr('id'));
    if (index != undefined && index != null && index != NaN) {
        var pkid = templateJson[index].PK_TMPLHDRID;
        if (pkid != null && pkid != undefined && pkid != "") {
            $.jAlert({
                'type': 'confirm', 'onConfirm': function () {
                    PageMethods.saveData(pkid, Master_API, saveType, function (response) {

                        $.jAlert({
                            'content': JSON.parse(JSON.parse(response)),
                        });
                        location.reload(true);
                        //onLoad();

                    }, function (error) {
                        location.reload(true);
                    });

                }

            });
        }
    }
});

$("#btn_Clear").click(function () {
    location.reload(true);
    //onLoad();
});

$(document).on('change', '[name=checkall]', function () {
    $('input:checkbox').not(this).prop('checked', this.checked);
});

