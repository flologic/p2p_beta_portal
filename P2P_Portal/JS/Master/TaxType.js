﻿var Client_ID = '', Master_API = '';
var UserJson = [], UserID;
var TaxTypeDtl = '';

$(document).ready(function () {
    Client_ID = $("#txt_Client_ID").val();
    Master_API = $("#txt_Master_URL").val();    
    GetTaxTypeData();

});


function GetTaxTypeData() {
    PageMethods.GetTaxTypeData(Client_ID, Master_API, function (response) {
        TaxTypeDtl = JSON.parse(JSON.parse(response[0]));
        if (TaxTypeDtl.Message == "Data Not Found") {
            $.jAlert({
                'content': 'Tax Type Data Not Found..!'
            });
        }
        else {
            TaxTypeOnSuccess();
        }
    },
        function (error) {
        });
}

function TaxTypeOnSuccess() {
    if (TaxTypeDtl) {
        var tableHeader = "";
        var tableBody = "";
        tableHeader += "<th style='width: 2%;'>#</th>";
        tableHeader += "<th>Tax Type</th>";        
        tableHeader += "<th>Edit</th>";
        tableHeader += "<th>Delete</th>";
        $.each(TaxTypeDtl, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.label + "<input class='form-control input-sm' type='text' id='txt_taxtype_name" + (key + 1) + "' value='" + val.label + "' style='display:none'></input></td>";           
            tableBody += "<td><a><img src='../../assets/images/edit.png' onclick='return bindData(" + (key + 1) + ")' title='Add New Row' /></a><input class='form-control input-sm' type='text' id='txt_Pk" + (key + 1) + "' value='" + val.id + "' style='display:none'></input></td>";
            tableBody += "<td><a><img src='../../assets/images/delete.png' onclick='return DeleteData(" + (key + 1) + ")' title='delete Row' /></a></td>";
            tableBody += "</tr>";
        });
        var table = "<table ID='TaxTypeTable' class='display nowrap dataTable'>" +
              "<thead><tr>" + tableHeader + "</tr></thead>" +
              "<tbody>" + tableBody + "</tbody>" +
              "</table>";
        $("#div_displayDtl").empty();
        $("#div_displayDtl").html(table);
        $("#TaxTypeTable").dataTable({
            dom: 'Bfrtip', "bSort": true,
            buttons: [
                'excel'
            ]
        });
    }
}

$("#btn_Save").click(function () {
    if ($("#Sel_TaxType_Name").val() == "") {
        $.jAlert({
            'content': 'Please Enter Tax Type..!'
        });
        return false;
    }
    
    PageMethods.SaveData(Client_ID, Master_API, $("#Sel_TaxType_Name").val(), $("#txt_Username").val(), function (response) {
        if (JSON.parse(JSON.parse(response)) == "Save") {
            $.jAlert({
                'content': 'Data Saved Successfully..!'
            });
            GetTaxTypeData();
            $("#Sel_TaxType_Name").val("");           
        }
        else if (JSON.parse(JSON.parse(response)) == "Duplicate") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(JSON.parse(response)) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Saved Due To Internal Server Error..!'
            });
        }
    }, function (error) {
    });
});

function bindData(index) {
    $("#btn_Save").hide();
    $("#btn_Upd").show();
    $("#Sel_TaxType_Name").val($("#txt_taxtype_name" + index).val());   
    $("#txt_PK_TAXTYPEID").val($("#txt_Pk" + index).val());
}

$("#btn_Upd").click(function () {
    PageMethods.UpdateData(Client_ID, Master_API, $("#txt_PK_TAXTYPEID").val(), $("#Sel_TaxType_Name").val(), $("#txt_Username").val(), function (response) {
        if (JSON.parse(JSON.parse(response)) == "Update") {
            $.jAlert({
                'content': 'Data Updated Successfully..!'
            });

            GetTaxTypeData();
            $("#Sel_TaxType_Name").val("");
            $("#btn_Save").show();
            $("#btn_Upd").hide();
        }
        else if (JSON.parse(JSON.parse(response)) == "Duplicate") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(JSON.parse(response)) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Update Due To Internal Server Error..!'
            });
        }

    }, function (error) {
    });
});

function DeleteData(index) {
    $.jAlert({
        'title': 'Confirmation',
        'content': 'Are you sure to delete this record..?', 'type': 'confirm',
        'onConfirm': function () {
            $("#txt_PK_TAXTYPEID").val($("#txt_Pk" + index).val());
            PageMethods.Deletedata(Client_ID, Master_API, $("#txt_PK_TAXTYPEID").val(), $("#txt_Username").val(), function (response) {
                if (JSON.parse(JSON.parse(response)) == "Delete") {
                    $.jAlert({
                        'content': 'Data Deleted Successfully..!'
                    });

                    GetTaxTypeData();
                }
                else if (JSON.parse(response) == "NotFound") {
                    $.jAlert({
                        'content': 'Data Not Delete..!'
                    });
                }
                else if (JSON.parse(JSON.parse(response)) == "InternalServerError") {
                    $.jAlert({
                        'content': 'Data Not Delete Due To Internal Server Error..!'
                    });
                }
            }, function (error) {
            });
        }
    });
}