﻿var Client_ID = '', Master_API = '';
var TypeJson = [], TypeID;
var TaxDtl = '';

$(document).ready(function () {
    //bindZone();
    Client_ID = $("#txt_Client_ID").val();
    Master_API = $("#txt_Master_URL").val();
    bindTaxType();
    GetTaxData();

});

function bindTaxType() {
    PageMethods.GetType(Client_ID, Master_API, function (response) {
        TypeJson = JSON.parse(JSON.parse(response[0]));
        if (TypeJson.Message == "Data Not Found") {
            $.jAlert({
                'content': 'Tax Type Data Not Found..!'
            });
        }
        else {
            BindTaxtype();
        }
    },
        function (error) {
        });
}

function BindTaxtype() {//response
    // var UserJson = JSON.parse(JSON.parse(response.d));
    $("#txt_taxType").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: TypeJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#txt_taxType").val(ui.item.label);
            TypeID = ui.item.id;
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function GetTaxData() {
    PageMethods.GetTaxData(Client_ID, Master_API, function (response) {
        TaxDtl = JSON.parse(JSON.parse(response[0]));
        if (TaxDtl.Message == "Data Not Found") {
            $.jAlert({
                'content': 'Tax Data Not Found..!'
            });
        }
        else {
            TaxOnSuccess();
        }
    },
        function (error) {
        });
}

function TaxOnSuccess() {//response
    //var ZoneDtl = JSON.parse(JSON.parse(response.d));
    if (TaxDtl) {
        var tableHeader = "";
        var tableBody = "";
        tableHeader += "<th style='width: 2%;'>#</th>";
        tableHeader += "<th>Tax Type</th>";
        tableHeader += "<th>Tax Name</th>";
        tableHeader += "<th>Tax Rate</th>";
        tableHeader += "<th>Edit</th>";
        tableHeader += "<th>Delete</th>";
        $.each(TaxDtl, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.Taxtype + "<input class='form-control input-sm' type='text' id='txt_tax_type" + (key + 1) + "' value='" + val.Taxtype + "' style='display:none'></input>";
            tableBody += "<input class='form-control input-sm' type='text' id='txt_taxTypeId" + (key + 1) + "' value='" + val.FK_TaxTypeId + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.label + "<input class='form-control input-sm' type='text' id='txt_tax_name" + (key + 1) + "' value='" + val.label + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.TaxRate + "<input class='form-control input-sm' type='text' id='txt_tax_rate" + (key + 1) + "' value='" + val.TaxRate + "' style='display:none'></input></td>";
            tableBody += "<td><a><img src='../../assets/images/edit.png' onclick='return bindData(" + (key + 1) + ")' title='Add New Row' /></a><input class='form-control input-sm' type='text' id='txt_Pk" + (key + 1) + "' value='" + val.id + "' style='display:none'></input></td>";
            tableBody += "<td><a><img src='../../assets/images/delete.png' onclick='return DeleteData(" + (key + 1) + ")' title='delete Row' /></a></td>";
            tableBody += "</tr>";
        });
        var table = "<table ID='zoneTable' class='display nowrap dataTable'>" +
              "<thead><tr>" + tableHeader + "</tr></thead>" +
              "<tbody>" + tableBody + "</tbody>" +
              "</table>";
        $("#div_displayDtl").empty();
        $("#div_displayDtl").html(table);
        $("#zoneTable").dataTable({
            dom: 'Bfrtip', "bSort": true,
            buttons: [
                'excel'
            ]
        });
    }
}

$("#btn_Save").click(function () {
    if ($("#txt_taxType").val() == "") {
        $.jAlert({
            'content': 'Please select Tax Type..!'
        });
        return false;
    }
    if ($("#txt_taxName").val() == "") {
        $.jAlert({
            'content': 'Please Enter Tax Name..!'
        });
        return false;
    }

    if ($("#txt_taxRate").val() == "") {
        $.jAlert({
            'content': 'Please Enter Tax Rate..!'
        });
        return false;
    }
    PageMethods.SaveData(Client_ID, Master_API, TypeID, $("#txt_taxName").val(), $("#txt_taxRate").val(), $("#txt_Username").val(), function (response) {
        if (response == "Save") {
            $.jAlert({
                'content': 'Data Saved Successfully..!'
            });
            GetTaxData();
            $("#Sel_Zone_Name").val("");
            $("#txt_Zone_Head").val("");
        }
        else if (response == "Duplicate") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (response == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Saved Due To Internal Server Error..!'
            });
        }
    }, function (error) {
    });
});

function bindData(index) {
    $("#btn_Save").hide();
    $("#btn_Upd").show();
    $("#txt_taxType").val($("#txt_tax_type" + index).val());
    TypeID = $("#txt_taxTypeId" + index).val()
    $("#txt_taxName").val($("#txt_tax_name" + index).val());
    $("#txt_taxRate").val($("#txt_tax_rate" + index).val());
    $("#txt_PK_TaxID").val($("#txt_Pk" + index).val());
}

$("#btn_Upd").click(function () {
    PageMethods.UpdateData(Client_ID, Master_API, $("#txt_PK_TaxID").val(), TypeID, $("#txt_taxName").val(), $("#txt_taxRate").val(), $("#txt_Username").val(), function (response) {
        if (response == "Update") {
            $.jAlert({
                'content': 'Data Updated Successfully..!'
            });

            GetTaxData();
            $("#txt_taxType").val("");
            $("#txt_taxName").val("");
            $("#txt_taxRate").val("");
            $("#btn_Save").show();
            $("#btn_Upd").hide();
        }
        else if (response == "Duplicate") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (response == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Update Due To Internal Server Error..!'
            });
        }

    }, function (error) {
    });
});

function DeleteData(index) {
    $.jAlert({
        'title': 'Confirmation',
        'content': 'Are you sure to delete this record..?', 'type': 'confirm',
        'onConfirm': function () {
            $("#txt_PK_TaxID").val($("#txt_Pk" + index).val());
            PageMethods.Deletedata(Client_ID, Master_API, $("#txt_PK_TaxID").val(), $("#txt_Username").val(), function (response) {
                if (response == "Delete") {
                    $.jAlert({
                        'content': 'Data Deleted Successfully..!'
                    });

                    GetTaxData();
                }
                else if (response == "InternalServerError") {
                    $.jAlert({
                        'content': 'Data Not Delete Due To Internal Server Error..!'
                    });
                }
            }, function (error) {
            });
        }
    });
}

$("#btn_Cancel").click(function () {
    window.location.replace("../../HomePage.aspx");
});


//function bindUser() {
//    var obj = {};
//    obj.name = $.trim($("[id*=txt_Master_URL]").val());
//    $.ajax({
//        type: "POST",
//        url: "Zone_Master.aspx/GetUser",
//        //JSON.stringify(obj),//{ 'URL': $("#txt_Master_URL").val() },//"URL": $("#txt_Master_URL").val(),
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: BindUser,
//        failure: function (response) {
//            alert(response.d);
//        }
//    });
//}

//function GetZoneData() {
//    $.ajax({
//        type: "POST",
//        url: "Zone_Master.aspx/GetZoneData",
//        data: {},//'{name: "' + $("#<%=txtUserName.ClientID%>")[0].value + '" }'
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: ZoneOnSuccess,
//        failure: function (response) {
//            alert(response.d);
//        }
//    });
//}



