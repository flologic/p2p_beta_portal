﻿var Client_ID = '', Master_API = '';
var ItemJson = "", ItemCatId, ItemSubCatId;
var BindAllDetail = '';

$(document).ready(function () {
    Client_ID = $("#txt_Client_ID").val();
    Master_API = $("#txt_Master_URL").val();
    // $('#form1').trigger("reset");
    addmodalpopup();
    $("#loading-div-background").show();
    GetItemData();
    getItemCat();
    getItemSubCat();
});

function GetItemData() {
    PageMethods.GetItemData(Client_ID, Master_API, function (response) {
        BindAllDetail = JSON.parse(JSON.parse(response[0]));
        if (BindAllDetail.Message == "Data Not Found") {
            alert("Item Data Not Found");
            $("#loading-div-background").hide();
        }
        else {
            ItemOnSuccess();
        }
    });
}

function getItemCat() {
    PageMethods.GetItemCatData(Client_ID, Master_API, function (response) {
        ItemJson = JSON.parse(JSON.parse(response[0]));
        BindItemCat();
    },
        function (error) {
        });
}

function BindItemCat() {
    $("#txt_cat_Id").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: ItemJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#txt_cat_Id").val(ui.item.label);
            ItemCatId = ui.item.id;
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function getItemSubCat() {
    PageMethods.GetItemSubCatData(Client_ID, Master_API, function (response) {
        ItemJson = JSON.parse(JSON.parse(response[0]));
        BindItemSubCat();
    },
        function (error) {
        });
}

function BindItemSubCat() {
    $("#txt_itmsbcat_name").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: ItemJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#txt_itmsbcat_name").val(ui.item.label);
            ItemSubCatId = ui.item.id;
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function ItemOnSuccess() {
    if (BindAllDetail) {
        var tableHeader = "";
        var tableBody = "";
        tableHeader += "<th style='width: 2%;'>#</th>";
        tableHeader += "<th>Item Category</th>";
        tableHeader += "<th>Item Sub-Category</th>";
        tableHeader += "<th>Item Name</th>";
        tableHeader += "<th>Base Rate</th>";
        tableHeader += "<th>HSN Code</th>";
        tableHeader += "<th>Edit</th>";
        tableHeader += "<th>Delete</th>";
        $.each(BindAllDetail, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.ITMCATNAME + "<input class='form-control input-sm' type='text' id='txt_itmCatname" + (key + 1) + "' value='" + val.ITMCATNAME + "' style='display:none'/><input class='form-control input-sm' type='text' id='txt_Itemctid" + (key + 1) + "' value='" + val.FK_ITMCATID + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.ITMSUBCATNAME + "<input class='form-control input-sm' type='text' id='txt_itmSubCatname" + (key + 1) + "' value='" + val.ITMSUBCATNAME + "' style='display:none'/><input class='form-control input-sm' type='text' id='txt_ItemSubctid" + (key + 1) + "' value='" + val.FK_ITMSUBCATID + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.label + "<input class='form-control input-sm' type='text' id='txt_itmnm" + (key + 1) + "' value='" + val.label + "' style='display:none'/><input class='form-control input-sm' type='text' id='txt_Itemid" + (key + 1) + "' value='" + val.id + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.BASE_RATE + "<input class='form-control input-sm'  type='text' id='txt_baserate" + (key + 1) + "' value='" + val.BASE_RATE + "' style='display:none'/></td>";
            tableBody += "<td>" + val.HSN_CODE + "<input class='form-control input-sm'  type='text' id='txt_HSN_Code" + (key + 1) + "' value='" + val.HSN_CODE + "' style='display:none'/></td>";
            tableBody += "<td><a><img src='../../assets/images/edit.png' onclick='return bindData(" + (key + 1) + ")' title='Add New Row' /></a><input class='form-control input-sm' type='text' id='txt_Pk_" + (key + 1) + "' value='" + val.id + "' style='display:none'></input></td>";
            tableBody += "<td><a><img src='../../assets/images/delete.png' onclick='return DeleteData(" + (key + 1) + ")' title='delete Row' /></a></td>";
            tableBody += "</tr>";
        });

        var table = "<table ID='itemsbcatTable' class='display nowrap dataTable'>" +
              "<thead><tr>" + tableHeader + "</tr></thead>" +
              "<tbody>" + tableBody + "</tbody>" +
              "</table>";
        $("#div_displayDtl").empty();
        $("#div_displayDtl").html(table);
        $("#itemsbcatTable").dataTable({
            dom: 'Bfrtip', "bSort": true,
            buttons: [
                'excel'
            ]
        });
       
    }
    $("#loading-div-background").hide();
}

$("#btn_Save").click(function () {
    if ($("#txt_cat_Id").val() == "") {
        $.jAlert({
            'content': 'Please Select Item Category..!'
        });
        return false;
    }
    if ($("#txt_itmsbcat_name").val() == "") {
        $.jAlert({
            'content': 'Please Enter Item Subcategory..!'
        });
        return false;
    }
    if ($("#txt_Item").val() == "") {
        $.jAlert({
            'content': 'Please Enter Item..!'
        });
        return false;
    }
    if ($("#txt_Base_Rate").val() == "") {
        $.jAlert({
            'content': 'Please Enter Base Rate..!'
        });
        return false;
    }
    if ($("#txt_HSN_Code").val() == "") {
        $.jAlert({
            'content': 'Please Enter HSN Code..!'
        });
        return false;
    }
    PageMethods.SaveData(ItemCatId, ItemSubCatId, $("#txt_Item").val(), $("#txt_Base_Rate").val(), $("#txt_HSN_Code").val(), $("#txt_UserName").val(), Client_ID, Master_API, function (response) {
        if (JSON.parse(response) == "OK") {
            $.jAlert({
                'content': 'Data Saved Successfully..!'
            });
            GetItemData();
            $("#txt_cat_Id").val("");
            $("#txt_itmsbcat_name").val("");
            $("#txt_Item").val("");
            $("#txt_Base_Rate").val("");
            $("#txt_HSN_Code").val("");
        }
        else if (JSON.parse(response) == "NotFound") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(response) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Saved Due To Internal Server Error..!'
            });
        }
    }, function (error) {
    });
})

function bindData(index) {
    $("#btn_Save").hide();
    $("#btn_Upd").show();
    ItemCatId = $("#txt_Itemctid" + index).val();
    ItemSubCatId = $("#txt_ItemSubctid" + index).val();
    $("#txt_cat_Id").val($("#txt_itmCatname" + index).val());
    $("#txt_itmsbcat_name").val($("#txt_itmSubCatname" + index).val());
    $("#txt_Item").val($("#txt_itmnm" + index).val());
    $("#txt_Base_Rate").val($("#txt_baserate" + index).val());
    $("#txt_HSN_Code").val($("#txt_HSN_Code" + index).val());
    $("#txt_PK_ITEMID").val($("#txt_Pk_" + index).val());
}

$("#btn_Upd").click(function () {
     if ($("#txt_cat_Id").val() == "") {
        $.jAlert({
            'content': 'Please Select Item Category..!'
        });
        return false;
    }
    if ($("#txt_itmsbcat_name").val() == "") {
        $.jAlert({
            'content': 'Please Enter Item Subcategory..!'
        });
        return false;
    }
    if ($("#txt_Item").val() == "") {
        $.jAlert({
            'content': 'Please Enter Item..!'
        });
        return false;
    }
    if ($("#txt_Base_Rate").val() == "") {
        $.jAlert({
            'content': 'Please Enter Base Rate..!'
        });
        return false;
    }
    if ($("#txt_HSN_Code").val() == "") {
        $.jAlert({
            'content': 'Please Enter HSN Code..!'
        });
        return false;
    }
    PageMethods.UpdateData($("#txt_PK_ITEMID").val(), ItemCatId, ItemSubCatId, $("#txt_Item").val(), $("#txt_Base_Rate").val(), $("#txt_HSN_Code").val(), Client_ID, Master_API, function (response) {
        if (JSON.parse(response) == "OK") {
            $.jAlert({
                'content': 'Data Edited Successfully..!'
            });

            GetItemData();
            ItemCatId = "";
            ItemSubCatId = "";
            $("#txt_cat_Id").val("");
            $("#txt_itmsbcat_name").val("");
            $("#txt_Item").val("");
            $("#txt_Base_Rate").val("");
            $("#txt_HSN_Code").val("");
            $("#btn_Save").show();
            $("#btn_Upd").hide();
        }
        else if (JSON.parse(response) == "NotFound") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(response) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Saved Due To Internal Server Error..!'
            });
        }
    }, function (error) {
    });
});

function DeleteData(index) {
    $("#txt_PK_ITEMID").val($("#txt_Pk_" + index).val());
    PageMethods.Deletedata($("#txt_PK_ITEMID").val(), Client_ID, Master_API, function (response) {
        if (JSON.parse(response) == "OK") {
            $.jAlert({
                'content': 'Data Deleted Successfully..!'
            });
            GetItemData();
        }
        else {
            $.jAlert({
                'content': 'Data Not Deleted..!'
            });
        }
    }, function (error) {
    });
}

$("#btn_Cancel").click(function () {
    window.location.replace("../../HomePage.aspx");
});