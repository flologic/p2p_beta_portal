﻿var Client_ID = '', Master_API = '';
var SuppJson = '';//, SuppID;
var TypeJson = [{ "label": "RFQ" }, { "label": "RENT" }, { "label": "OTHER" }];

$(document).ready(function () {
    Client_ID = $("#txt_Client_ID").val();
    Master_API = $("#txt_Master_URL").val();
    GetSuppType();
    GetSupplierData();
});

function GetSuppType() {
    $("#txt_Supptype").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: TypeJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#txt_Supptype").val(ui.item.label);
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function GetSupplierData() {
    PageMethods.GetSuppHDR(Client_ID, Master_API, function (response) {
        SuppJson = JSON.parse(JSON.parse(response[0]));
        if (SuppJson.Message == "Data Not Found") {
            $.jAlert({
                'content': 'Supplier Data Not Found..!'
            });
        }
        else {
            SupplierOnSuccess();
        }
    },
        function (error) {
        });
}

function SupplierOnSuccess() {
    if (SuppJson) {
        var tableHeader = "";
        var tableBody = "";
        tableHeader += "<th style='width: 2%;'>#</th>";
        tableHeader += "<th>Supplier Name</th>";
        tableHeader += "<th>Supplier Code</th>";
        tableHeader += "<th>Supplier Type</th>"
        tableHeader += "<th>Supplier ShortName</th>"
        tableHeader += "<th>Edit</th>";
        tableHeader += "<th>Delete</th>";
        $.each(SuppJson, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.label + "<input class='form-control input-sm' type='text' id='txt_SuppName" + (key + 1) + "' value='" + val.label + "' style='display:none'>";
            tableBody += "<td>" + val.Supp_Code + "<input class='form-control input-sm' type='text' id='txt_SuppCode" + (key + 1) + "' value='" + val.Supp_Code + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.Supp_Type + "<input class='form-control input-sm' type='text' id='txt_SuppType" + (key + 1) + "' value='" + val.Supp_Type + "' style='display:none'>";
            tableBody += "<td>" + val.Short_Name + "<input class='form-control input-sm' type='text' id='txt_SuppShortName" + (key + 1) + "' value='" + val.Short_Name + "' style='display:none'>";
            tableBody += "<td><a><img src='../../assets/images/edit.png' onclick='return bindData(" + (key + 1) + ")' title='Add New Row' /></a><input class='form-control input-sm' type='text' id='txt_PK_ID" + (key + 1) + "' value='" + val.id + "' style='display:none'></input></td>";
            tableBody += "<td><a><img src='../../assets/images/delete.png' onclick='return DeleteData(" + (key + 1) + ")' title='delete Row' /></a></td>";
            tableBody += "</tr>";
        });
        var table = "<table ID='SuppTable' class='display nowrap dataTable'>" +
              "<thead><tr>" + tableHeader + "</tr></thead>" +
              "<tbody>" + tableBody + "</tbody>" +
              "</table>";
        $("#div_supplier").empty();
        $("#div_supplier").html(table);
        $("#SuppTable").dataTable({
            dom: 'Bfrtip', "bSort": true,
            buttons: [
                'excel'
            ]
        });
        $("#loading-div-background").hide();
    }
}

$("#btn_Save").click(function () {
    if ($("#txt_Supplier").val() == "") {
        $.jAlert({
            'content': 'Please Enter Supplier Name..!'
        });
        return false;
    }
    if ($("#txt_Suppcode").val() == "") {
        $.jAlert({
            'content': 'Please Enter Supplier Code..!'
        });
        return false;
    }
    if ($("#txt_Supptype").val() == "") {
        $.jAlert({
            'content': 'Please Select Supplier Type..!'
        });
        return false;
    }
    PageMethods.SaveData(Client_ID, Master_API, $("#txt_Supplier").val(), $("#txt_Suppcode").val(), $("#txt_Supptype").val(), $("#txt_ShortName").val(), $("#txt_Username").val(), function (response) {
        if (JSON.parse(JSON.parse(response)) == "Save") {
            $.jAlert({
                'content': 'Data Saved Successfully..!'
            });
            GetSupplierData();
            $("#txt_Supplier").val("");
            $("#txt_Suppcode").val("");
            $("#txt_Supptype").val("");
            $("#txt_ShortName").val("");
        }
        else if (JSON.parse(JSON.parse(response)) == "Duplicate") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(JSON.parse(response)) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Saved Due To Internal Server Error..!'
            });
        }
    }, function (error) {
    });
});

function bindData(index) {
    $("#btn_Save").hide();
    $("#btn_Upd").show();
    $("#txt_Supplier").val($("#txt_SuppName" + index).val());
    $("#txt_Suppcode").val($("#txt_SuppCode" + index).val());
    $("#txt_Supptype").val($("#txt_SuppType" + index).val());
    $("#txt_ShortName").val($("#txt_SuppShortName" + index).val());
    $("#txt_PKID").val($("#txt_PK_ID" + index).val());
}

$("#btn_Upd").click(function () {
    if ($("#txt_Supplier").val() == "") {
        $.jAlert({
            'content': 'Please Enter Supplier Name..!'
        });
        return false;
    }
    if ($("#txt_Suppcode").val() == "") {
        $.jAlert({
            'content': 'Please Enter Supplier Code..!'
        });
        return false;
    }
    if ($("#txt_Supptype").val() == "") {
        $.jAlert({
            'content': 'Please Select Supplier Type..!'
        });
        return false;
    }
    PageMethods.UpdateData(Client_ID, Master_API, $("#txt_PKID").val(), $("#txt_Supplier").val(), $("#txt_Suppcode").val(), $("#txt_Supptype").val(), $("#txt_ShortName").val(), $("#txt_Username").val(), function (response) {
        if (JSON.parse(JSON.parse(response)) == "Update") {
            $.jAlert({
                'content': 'Data Updated Successfully..!'
            });

            GetSupplierData();
            $("#txt_Supplier").val("");
            $("#txt_Suppcode").val("");
            $("#txt_Supptype").val("");
            $("#txt_ShortName").val("");
            $("#btn_Save").show();
            $("#btn_Upd").hide();
        }
        else if (JSON.parse(JSON.parse(response)) == "Duplicate") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(JSON.parse(response)) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Update Due To Internal Server Error..!'
            });
        }

    }, function (error) {
    });
});

function DeleteData(index) {
    $.jAlert({
        'title': 'Confirmation',
        'content': 'Are you sure to delete this record..?', 'type': 'confirm',
        'onConfirm': function () {
            $("#txt_PKID").val($("#txt_PK_ID" + index).val());
            PageMethods.Deletedata(Client_ID, Master_API, $("#txt_PKID").val(), $("#txt_Username").val(), function (response) {
                if (JSON.parse(JSON.parse(response)) == "Delete") {
                    $.jAlert({
                        'content': 'Data Deleted Successfully..!'
                    });
                    GetSupplierData();
                }
                else if (JSON.parse(JSON.parse(response)) == "InternalServerError") {
                    $.jAlert({
                        'content': 'Data Not Delete Due To Internal Server Error..!'
                    });
                }
            }, function (error) {
            });
        }
    });
}

$("#btn_Cancel").click(function () {
    window.location.replace("../../HomePage.aspx");
});






