﻿var Client_ID = '', Master_API = '';
var PaymntJson = '';

$(document).ready(function () {
    Client_ID = $("#txt_Client_ID").val();
    Master_API = $("#txt_Master_URL").val();
    getBillPaymntData();

});

function getBillPaymntData() {
    PageMethods.GetPaymentData(Client_ID, Master_API, function (response) {
        PaymntJson = JSON.parse(JSON.parse(response[0]));
        if (PaymntJson.Message == "Data Not Found") {
            $.jAlert({
                'content': 'Bill Payment Mode Data Not Found..!'
            });
        }
        else {
            bindBillPaymnt();
        }

    },
     function (error) {
     });
}

function bindBillPaymnt() {
    if (PaymntJson) {
        var tableheader = "";
        var tableBody = "";
        tableheader += "<th style='width: 2%;'>#</th>";
        tableheader += "<th>Bill Payment Mode</th>";
        tableheader += "<th>Edit</th>";
        tableheader += "<th>Delete</th>";

        $.each(PaymntJson, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.label + "<input class='form-control input-sm' type='text' id='txt_Paymnt_mode" + (key + 1) + "' value='" + val.label + "' style='display:none'></input>";
            tableBody += "<td><a><img src='../../assets/images/edit.png' onclick='return bindData(" + (key + 1) + ")' title='Add New Row' /></a><input class='form-control input-sm' type='text' id='txt_Pk" + (key + 1) + "' value='" + val.id + "' style='display:none'></input></td>";
            tableBody += "<td><a><img src='../../assets/images/delete.png' onclick='return DeleteData(" + (key + 1) + ")' title='delete Row' /></a></td>";
            tableBody += "</tr>";
        });
        var table = "<table ID='BillPaymnttable' class='display nowrap dataTable'>" +
           "<thead><tr>" + tableheader + "</tr></thead>" +
           "<tbody>" + tableBody + "<tbody>" +
        "</table>";
        $("#div_displayDtl").empty();
        $("#div_displayDtl").html(table);
        $("#BillPaymnttable").dataTable({
            dom: 'Bfrtip', "bSort": true,
            buttons: [
                'excel'
            ]
        });
    }
}


function bindData(index) {
    $("#btn_Save").hide();
    $("#btn_Upd").show();
    $("#Sel_payment_Mode").val($("#txt_Paymnt_mode" + index).val());
    $("#txt_PK_Billpaymnt_ID").val($("#txt_Pk" + index).val());
}

$("#btn_Save").click(function () {
    if ($("#Sel_payment_Mode").val() == "") {
        $.jAlert({
            'content': 'Please Enter Payment Mode ..!'
        });
        return false;
    }
   

    PageMethods.SaveData(Client_ID, Master_API, $("#Sel_payment_Mode").val(),$("#txt_Username").val(), function (response) {
        if (JSON.parse(response) == "Save") {
            $.jAlert({
                'content': 'Data Saved Successfully..!'
            });
            getBillPaymntData();
            $("#Sel_payment_Mode").val("");
          
        }
        else if (JSON.parse(response) == "Duplicate") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(response) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Saved Due To Internal Server Error..!'
            });
        }
    }, function (error) {
    });
});

$("#btn_Upd").click(function () {
    PageMethods.UpdateData(Client_ID, Master_API, $("#txt_PK_Billpaymnt_ID").val(), $("#Sel_payment_Mode").val(), $("#txt_Username").val(), function (response) {
        if (JSON.parse(response) == "Update") {
            $.jAlert({
                'content': 'Data Updated Successfully..!'
            });

            getBillPaymntData();
            $("#Sel_payment_Mode").val("");
            $("#btn_Save").show();
            $("#btn_Upd").hide();
        }
        else if (JSON.parse(response) == "Duplicate") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(response) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Update Due To Internal Server Error..!'
            });
        }

    }, function (error) {
    });
});

function DeleteData(index) {
    $.jAlert({
        'title': 'Confirmation',
        'content': 'Are you sure to delete this record..?', 'type': 'confirm',
        'onConfirm': function () {
            $("#txt_PK_Billpaymnt_ID").val($("#txt_Pk" + index).val());
            PageMethods.Deletedata(Client_ID, Master_API, $("#txt_PK_Billpaymnt_ID").val(), $("#txt_Username").val(), function (response) {
                if (JSON.parse(response) == "Delete") {
                    $.jAlert({
                        'content': 'Data Deleted Successfully..!'
                    });

                    getBillPaymntData();
                }
                else if (JSON.parse(response) == "NotFound") {
                    $.jAlert({
                        'content': 'Data Not Delete..!'
                    });
                }
                else if (JSON.parse(response) == "InternalServerError") {
                    $.jAlert({
                        'content': 'Data Not Delete Due To Internal Server Error..!'
                    });
                }
            }, function (error) {
            });
        }
    });
}

$("#btn_Cancel").click(function () {
    window.location.replace("../../HomePage.aspx");
});
