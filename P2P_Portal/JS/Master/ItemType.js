﻿var Client_ID = '', Master_API = '';
var UserJson = [], UserID;
var CatDtl = '';

$(document).ready(function () {
    Client_ID = $("#txt_Client_ID").val();
    Master_API = $("#txt_Master_URL").val();
    addmodalpopup();
    $("#loading-div-background").show();
    GetItemType();
   
});

function GetItemType() {
    PageMethods.GetItemType(Client_ID, Master_API, function (response) {
        CatDtl = JSON.parse(JSON.parse(response[0]));
        if (CatDtl.Message == "Data Not Found") {
            alert("Item Type Data Not Found");
            $("#loading-div-background").hide();
        }
        else {
            ItemTypeOnSuccess();
        }
    },
        function (error) {
        });
}

function ItemTypeOnSuccess() {
    if (CatDtl) {
        var tableHeader = "";
        var tableBody = "";
        tableHeader += "<th style='width: 2%;'>#</th>";
        tableHeader += "<th>Item Type</th>";
        tableHeader += "<th>Edit</th>";
        tableHeader += "<th>Delete</th>";
        $.each(CatDtl, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.label + "<input class='form-control input-sm' type='text' id='txt_ItemType" + (key + 1) + "' value='" + val.label + "' style='display:none'></input></td>";
            tableBody += "<td><a><img src='../../assets/images/edit.png' onclick='return bindData(" + (key + 1) + ")' title='Add New Row' /></a><input class='form-control input-sm' type='text' id='txt_Pk" + (key + 1) + "' value='" + val.id + "' style='display:none'></input></td>";
            tableBody += "<td><a><img src='../../assets/images/delete.png' onclick='return DeleteData(" + (key + 1) + ")' title='delete Row' /></a></td>";
            tableBody += "</tr>";
        });
        var table = "<table ID='UnitTable' class='display nowrap dataTable'>" +
              "<thead><tr>" + tableHeader + "</tr></thead>" +
              "<tbody>" + tableBody + "</tbody>" +
              "</table>";
        $("#div_displayDtl").empty();
        $("#div_displayDtl").html(table);
        $("#UnitTable").dataTable({
            dom: 'Bfrtip', "bSort": true,
            buttons: [
                'excel'
            ]
        });
    }
    $("#loading-div-background").hide();
}

$("#btn_Save").click(function () {
    if ($("#Sel_ItemType").val() == "") {
        $.jAlert({
            'content': 'Please Enter Item Type..!'
        });
        return false;
    }
    PageMethods.SaveData($("#Sel_ItemType").val(), $("#txt_UserName").val(), Client_ID, Master_API, function (response) {
        if (JSON.parse(response) == "OK") {
            $.jAlert({
                'content': 'Data Saved Successfully..!'
            });
            GetItemType();
            $("#Sel_ItemType").val("");
        }
        else if (JSON.parse(response) == "NotFound") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(response) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Saved Due To Internal Server Error..!'
            });
        }
    }, function (error) {
    });
});

function bindData(index) {
    $("#btn_Save").hide();
    $("#btn_Upd").show();
    $("#Sel_ItemType").val($("#txt_ItemType" + index).val());
    $("#txt_PK_ItemTypeID").val($("#txt_Pk" + index).val());
}

$("#btn_Upd").click(function () {
    PageMethods.UpdateData($("#txt_PK_ItemTypeID").val(), $("#Sel_ItemType").val(), Client_ID, Master_API, function (response) {
        if (JSON.parse(response) == "OK") {
            $.jAlert({
                'content': 'Data Edited Successfully..!'
            });
            GetItemType();
            $("#Sel_ItemType").val("");
            $("#btn_Save").show();
            $("#btn_Upd").hide();
        }
        else if (JSON.parse(response) == "NotFound") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(response) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Saved Due To Internal Server Error..!'
            });
        }
    }, function (error) {
    });
});

function DeleteData(index) {
    $("#txt_PK_ItemTypeID").val($("#txt_Pk" + index).val());
    PageMethods.Deletedata($("#txt_PK_ItemTypeID").val(), Client_ID, Master_API, function (response) {
        if (JSON.parse(response) == "OK") {
            $.jAlert({
                'content': 'Data Deleted Successfully..!'
            });
            GetItemType();
        }
        else {
            $.jAlert({
                'content': 'Data Not Deleted..!'
            });
        }
    }, function (error) {
    });
}

$("#btn_Cancel").click(function () {
    window.location.replace("../../HomePage.aspx");
});