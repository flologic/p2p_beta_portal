﻿var AccessRoleJson;
var role_PKID;
var saveType;

$(document).ready(function () {
    onLoad();
});

function onLoad() {
    $('#form1').trigger("reset");
    saveType = "SAVE";
    $("#btn_Save").html('Save');
    $("#txt_Role").val("");
    $('#txt_Role').attr('readonly', false);
    GetRoleData();
}

function GetRoleData() {
    PageMethods.getRoleAllData(function (response) {
        if (response) {
            AccessRoleJson = JSON.parse(response);
            displayData();
        }
    }, function (error) {
    });
}

function displayData() {
    if (AccessRoleJson) {
        var tableHeader = "";
        var tableBody = "";

        tableHeader += "<th style='width: 3%'>#</th>";
        tableHeader += "<th>Role Name</th>";
        tableHeader += "<th style='width: 5%'>Edit</th>";
        tableHeader += "<th style='width: 5%'>Delete</th>";

        $.each(AccessRoleJson, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.ROLE_NAME + "</td>";
            tableBody += "<td class='click_Edit'><img src='../../assets/images/edit.png' title='Edit' alt='Edit'/></td>";
            tableBody += "<td class='click_Delete'><img src='../../assets/images/delete.png' title='Delete' alt='Delete'/></td>";
            tableBody += "</tr>";
        })
        var table = "<table class='display nowrap' id='tbl_Location'>" +
              "<thead><tr>" + tableHeader + "</tr></thead>" +
              "<tbody>" + tableBody + "</tbody>" +
              "</table>";
        $("#div_Role").empty();
        $("#div_Role").html(table);

        $("#tbl_Location").dataTable({
            dom: 'Bfrtip', "bSort": true,
            buttons: [
                'excel'
            ]
        });
    }
}

$("#btn_Save").click(function () {
    SaveData();
});

function SaveData() {
    var Role = [];
    var RoleName = $("#txt_Role").val();
    if (RoleName == "") {
        $.jAlert({
            'content': 'Please Enter Role Name.',
            'onClose': function () {
                $('#txt_Role').focus();
            }
        });
        return false;
    }

    var objLocation;
    if (saveType == "SAVE") {
        objLocation = {
            "ROLE_NAME": RoleName, "IS_ACTIVE": "1", "AMD_BY": $('#txt_UserID').val()
        };
    }
    else {
        objLocation = {
            "PK_ROLE_ID": role_PKID, "ROLE_NAME": RoleName, "IS_ACTIVE": "1", "AMD_BY": $('#txt_UserID').val()
        };
    }
    Role.push(objLocation);
    PageMethods.saveData(JSON.stringify(Role), saveType, function (response) {
        $.jAlert({
            'content': response,
        });
        onLoad();
    }, function (error) {
        $.jAlert({
            'content': error,
        });
    });
}

$("#btn_Cancel").click(function () {
    onLoad();
});


$('body').on('click', '.click_Edit', function () {
    var $row = $(this).closest('tr');
    var table = $('#tbl_Location').DataTable();
    var data = table.row($row).data();
    $("#txt_Role").val(AccessRoleJson[parseInt(data[0]) - 1].ROLE_NAME);
    role_PKID = AccessRoleJson[parseInt(data[0]) - 1].PK_ROLE_ID;
    saveType = "EDIT";
    $("#btn_Save").html('Update');
});


$('body').on('click', '.click_Delete', function () {
    var $row = $(this).closest('tr');
    var table = $('#tbl_Location').DataTable();
    var data = table.row($row).data();
    $("#txt_Role").val(AccessRoleJson[parseInt(data[0]) - 1].ROLE_NAME);
    $('#txt_Role').attr('readonly', true);
    role_PKID = AccessRoleJson[parseInt(data[0]) - 1].PK_ROLE_ID;
    saveType = "DELETE";
    $("#btn_Save").html('Delete');
    $.jAlert({ 'type': 'confirm', 'onConfirm': function () { SaveData() } });
});
