﻿var Client_ID = '', Master_API = '';
var UserJson = [], UserID;
var CompanyDtl = '';

$(document).ready(function () {
    Client_ID = $("#txt_Client_ID").val();
    Master_API = $("#txt_Master_URL").val();
    GetCompanyData();

});



function GetCompanyData() {
    PageMethods.GetCompanyData(Client_ID, Master_API, function (response) {
        CompanyDtl = JSON.parse(JSON.parse(response[0]));
        if (CompanyDtl.Message == "Data Not Found") {
            $.jAlert({
                'content': 'Department Data Not Found..!'
            });
        }
        else {
            CompanyOnSuccess();
        }
    },
        function (error) {
        });
}

function CompanyOnSuccess() {
    if (CompanyDtl) {
        var tableHeader = "";
        var tableBody = "";
        tableHeader += "<th style='width: 2%;'>#</th>";
        tableHeader += "<th>Company Name</th>";
        tableHeader += "<th>Company Description</th>";
        tableHeader += "<th>Edit</th>";
        tableHeader += "<th>Delete</th>";
        $.each(CompanyDtl, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.label + "<input class='form-control input-sm' type='text' id='txt_company_name" + (key + 1) + "' value='" + val.label + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.COMPANY_DESC + "<input class='form-control input-sm' type='text' id='txt_company_description" + (key + 1) + "' value='" + val.COMPANY_DESC + "' style='display:none'></input>";
            tableBody += "<td><a><img src='../../assets/images/edit.png' onclick='return bindData(" + (key + 1) + ")' title='Add New Row' /></a><input class='form-control input-sm' type='text' id='txt_Pk" + (key + 1) + "' value='" + val.id + "' style='display:none'></input></td>";
            tableBody += "<td><a><img src='../../assets/images/delete.png' onclick='return DeleteData(" + (key + 1) + ")' title='delete Row' /></a></td>";
            tableBody += "</tr>";
        });
        var table = "<table ID='CompanyTable' class='display nowrap dataTable'>" +
              "<thead><tr>" + tableHeader + "</tr></thead>" +
              "<tbody>" + tableBody + "</tbody>" +
              "</table>";
        $("#div_displayDtl").empty();
        $("#div_displayDtl").html(table);
        $("#CompanyTable").dataTable({
            dom: 'Bfrtip', "bSort": true,
            buttons: [
                'excel'
            ]
        });
    }
}

$("#btn_Save").click(function () {
    if ($("#Sel_Company_Name").val() == "") {
        $.jAlert({
            'content': 'Please Enter Company Name..!'
        });
        return false;
    }
    if ($("#txt_Company_Description").val() == "") {
        $.jAlert({
            'content': 'Please Enter Company Description..!'
        });
        return false;
    }
    PageMethods.SaveData(Client_ID, Master_API, $("#Sel_Company_Name").val(), $("#txt_Company_Description").val(), $("#txt_Username").val(), function (response) {
        if (JSON.parse(response) == "Save") {
            $.jAlert({
                'content': 'Data Saved Successfully..!'
            });
            GetCompanyData();
            $("#Sel_Company_Name").val("");
            $("#txt_Company_Description").val("");
        }
        else if (JSON.parse(response) == "Duplicate") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(response) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Saved Due To Internal Server Error..!'
            });
        }
    }, function (error) {
    });
});

function bindData(index) {
    $("#btn_Save").hide();
    $("#btn_Upd").show();
    $("#Sel_Company_Name").val($("#txt_company_name" + index).val());
    $("#txt_Company_Description").val($("#txt_company_description" + index).val());
    $("#txt_PK_COMPANYID").val($("#txt_Pk" + index).val());
}

$("#btn_Upd").click(function () {
    PageMethods.UpdateData(Client_ID, Master_API, $("#txt_PK_COMPANYID").val(), $("#Sel_Company_Name").val(), $("#txt_Company_Description").val(), $("#txt_Username").val(), function (response) {
        if (JSON.parse(response) == "Update") {
            $.jAlert({
                'content': 'Data Updated Successfully..!'
            });

            GetCompanyData();
            $("#Sel_Company_Name").val("");
            $("#txt_Company_Description").val("");
            $("#btn_Save").show();
            $("#btn_Upd").hide();
        }
        else if (JSON.parse(response) == "Duplicate") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(response) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Update Due To Internal Server Error..!'
            });
        }

    }, function (error) {
    });
});

function DeleteData(index) {
    $.jAlert({
        'title': 'Confirmation',
        'content': 'Are you sure to delete this record..?', 'type': 'confirm',
        'onConfirm': function () {
            $("#txt_PK_COMPANYID").val($("#txt_Pk" + index).val());
            PageMethods.Deletedata(Client_ID, Master_API, $("#txt_PK_COMPANYID").val(), $("#txt_Username").val(), function (response) {
                if (JSON.parse(response) == "Delete") {
                    $.jAlert({
                        'content': 'Data Deleted Successfully..!'
                    });

                    GetCompanyData();
                }
                else if (JSON.parse(response) == "NotFound") {
                    $.jAlert({
                        'content': 'Data Not Delete..!'
                    });
                }
                else if (JSON.parse(response) == "InternalServerError") {
                    $.jAlert({
                        'content': 'Data Not Delete Due To Internal Server Error..!'
                    });
                }
            }, function (error) {
            });
        }
    });
}

$("#btn_Cancel").click(function () {
    window.location.replace("../../HomePage.aspx");
});

