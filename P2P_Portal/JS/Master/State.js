﻿var Client_ID = '', Master_API = '';
var UserJson = [], UserID;
var StateDtl = '';

$(document).ready(function () {
    Client_ID = $("#txt_Client_ID").val();
    Master_API = $("#txt_Master_URL").val();
    GetStateData();
    bindUser();
});

function bindUser() {
    PageMethods.GetUser(Client_ID, Master_API, function (response) {
        UserJson = JSON.parse(JSON.parse(response[0]));
        if (UserJson.Message == "Data Not Found") {
            $.jAlert({
                'content': 'State Admin Data Not Found..!'
            });
        }
        else {
            BindUser();
        }
    },
        function (error) {
        });
}

function BindUser() {//response
    // var UserJson = JSON.parse(JSON.parse(response.d));
    $("#txt_State_Admin").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: UserJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#txt_State_Admin").val(ui.item.label);
            UserID = ui.item.id;
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function GetStateData() {
    PageMethods.GetStateData(Client_ID, Master_API, function (response) {
        StateDtl = JSON.parse(JSON.parse(response[0]));
        if (StateDtl.Message == "Data Not Found") {
            $.jAlert({
                'content': 'State Data Not Found..!'
            });
        }
        else {
            StateOnSuccess();
        }
    },
        function (error) {
        });
}

function StateOnSuccess() {
    if (StateDtl) {
        var tableHeader = "";
        var tableBody = "";
        tableHeader += "<th style='width: 2%;'>#</th>";
        tableHeader += "<th>State Name</th>";
        tableHeader += "<th>State Admin</th>";
        tableHeader += "<th>Edit</th>";
        tableHeader += "<th>Delete</th>";
        $.each(StateDtl, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.label + "<input class='form-control input-sm' type='text' id='txt_state_name" + (key + 1) + "' value='" + val.label + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.STATE_ADMIN + "<input class='form-control input-sm' type='text' id='txt_state_admin" + (key + 1) + "' value='" + val.STATE_ADMIN + "' style='display:none'></input>";
            tableBody += "<input class='form-control input-sm' type='text' id='txt_state_adminid" + (key + 1) + "' value='" + val.STATE_ADMINID + "' style='display:none'></input></td>";
            tableBody += "<td><a><img src='../../assets/images/edit.png' onclick='return bindData(" + (key + 1) + ")' title='Add New Row' /></a><input class='form-control input-sm' type='text' id='txt_Pk" + (key + 1) + "' value='" + val.id + "' style='display:none'></input></td>";
            tableBody += "<td><a><img src='../../assets/images/delete.png' onclick='return DeleteData(" + (key + 1) + ")' title='delete Row' /></a></td>";
            tableBody += "</tr>";
        });
        var table = "<table ID='StateTable' class='display nowrap dataTable'>" +
              "<thead><tr>" + tableHeader + "</tr></thead>" +
              "<tbody>" + tableBody + "</tbody>" +
              "</table>";
        $("#div_displayDtl").empty();
        $("#div_displayDtl").html(table);
        $("#StateTable").dataTable({
            dom: 'Bfrtip', "bSort": true,
            buttons: [
                'excel'
            ]
        });
    }
}

function bindData(index) {
    $("#btn_Save").hide();
    $("#btn_Upd").show();
    $("#Sel_State_Name").val($("#txt_state_name" + index).val());
    $("#txt_State_Admin").val($("#txt_state_admin" + index).val());
    UserID = $("#txt_state_adminid" + index).val()
    $("#txt_PK_STATEID").val($("#txt_Pk" + index).val());

}

$("#btn_Save").click(function () {
    if ($("#Sel_State_Name").val() == "") {
        $.jAlert({
            'content': 'Please Enter State Name..!'
        });
        return false;
    }
   
    PageMethods.SaveData(Client_ID, Master_API, $("#Sel_State_Name").val(), $("#txt_Username").val(), UserID, function (response) {
        if (JSON.parse(response) == "Save") {
            $.jAlert({
                'content': 'Data Saved Successfully..!'
            });
            GetStateData();
            $("#Sel_State_Name").val("");
            $("#txt_State_Admin").val("");
           
        }
        else if (JSON.parse(response) == "Duplicate") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(response) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Saved Due To Internal Server Error..!'
            });
        }
    }, function (error) {
    });
});



$("#btn_Upd").click(function () {
    PageMethods.UpdateData(Client_ID, Master_API, $("#txt_PK_STATEID").val(), $("#Sel_State_Name").val(), $("#txt_Username").val(), UserID, function (response) {
        if (JSON.parse(response) == "Update") {
            $.jAlert({
                'content': 'Data Updated Successfully..!'
            });

            GetStateData();
            $("#Sel_State_Name").val("");
            $("#txt_State_Admin").val("");
            $("#btn_Save").show();
            $("#btn_Upd").hide();
        }
        else if (JSON.parse(response) == "Duplicate") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(response) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Update Due To Internal Server Error..!'
            });
        }

    }, function (error) {
    });
});

function DeleteData(index) {
    $.jAlert({
        'title': 'Confirmation',
        'content': 'Are you sure to delete this record..?', 'type': 'confirm',
        'onConfirm': function () {
            $("#txt_PK_STATEID").val($("#txt_Pk" + index).val());
            PageMethods.Deletedata(Client_ID, Master_API, $("#txt_PK_STATEID").val(), $("#txt_Username").val(), function (response) {
                if (JSON.parse(response) == "Delete") {
                    $.jAlert({
                        'content': 'Data Deleted Successfully..!'
                    });

                    GetStateData();
                }
                else if (JSON.parse(response) == "NotFound") {
                    $.jAlert({
                        'content': 'Data Not Delete..!'
                    });
                }
                else if (JSON.parse(response) == "InternalServerError") {
                    $.jAlert({
                        'content': 'Data Not Delete Due To Internal Server Error..!'
                    });
                }
            }, function (error) {
            });
        }
    });
}

$("#btn_Cancel").click(function () {
    window.location.replace("../../HomePage.aspx");
});

