﻿var Client_ID = '', Master_API = '';
var SuppJson = '';
var SupplierJson = "", SupplierID = 0;

$(document).ready(function () {
    Client_ID = $("#txt_Client_ID").val();
    Master_API = $("#txt_Master_URL").val();
    GetSupplierData();
});

function GetSupplier() {
    var supp = $("#txt_supplier").val();
    CapLetters("txt_supplier");
    if (supp.length >= 3) {
        PageMethods.GetSupp(Client_ID, Master_API, supp, function (response) {
            SupplierJson = JSON.parse(JSON.parse(response[0]));
            if (SupplierJson.Message == "Data Not Found") {
                $.jAlert({
                    'content': 'Supplier Data Not Found..!'
                });
            }
            else {
                BindSupplier();
            }
        },
            function (error) {
            });
    }
}

function BindSupplier() {
    $("#txt_supplier").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: SupplierJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#txt_supplier").val(ui.item.label);
            SupplierID = ui.item.id;
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function GetSupplierData() {
    PageMethods.GetSuppDTL(Client_ID, Master_API, function (response) {
        SuppJson = JSON.parse(JSON.parse(response[0]));
        if (SuppJson.Message == "Data Not Found") {
            $.jAlert({
                'content': 'Supplier Data Not Found..!'
            });
        }
        else {
            SupplierOnSuccess();
        }
    },
        function (error) {
        });
}

function SupplierOnSuccess() {
    if (SuppJson) {
        var tableHeader = "";
        var tableBody = "";
        tableHeader += "<th style='width: 2%;'>#</th>";
        tableHeader += "<th>Supplier Name</th>";
        tableHeader += "<th>Branch</th>";
        tableHeader += "<th>Contact Person</th>";
        tableHeader += "<th>Contact Email</th>"
        tableHeader += "<th>Address</th>";
        tableHeader += "<th>City</th>";
        tableHeader += "<th>Pin Code</th>";
        tableHeader += "<th>Tel.NO</th>";
        tableHeader += "<th>Fax</th>";
        tableHeader += "<th>Mob.NO</th>";
        tableHeader += "<th>PAN.NO</th>";
        tableHeader += "<th>GST</th>";
        tableHeader += "<th>Edit</th>";
        tableHeader += "<th>Delete</th>";
        $.each(SuppJson, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "<input type='text' id='txt_PK_ID" + (key + 1) + "' runat='server' value='" + val.PK_SUPPLIER_DTL_ID + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.SUPPLIER_NAME + "</td>";
            tableBody += "<td>" + val.BRANCH + "</td>";
            tableBody += "<td>" + val.CONTACT_PERSON + "</td>";
            tableBody += "<td>" + val.CONTACT_EMAIL + "</td>";
            tableBody += "<td>" + val.ADDRESS + "</td>";
            tableBody += "<td>" + val.CITY + "</td>";
            tableBody += "<td>" + val.PINCODE + "</td>";
            tableBody += "<td>" + val.TELNO + "</td>";
            tableBody += "<td>" + val.FAX + "</td>";
            tableBody += "<td>" + val.MBLNO + "</td>";
            tableBody += "<td>" + val.PANNO + "</td>";
            tableBody += "<td>" + val.GST + "</td>";
            tableBody += "<td class='click_Edit'><img src='../../assets/images/edit.png' title='Edit' alt='Edit'/></td>";
            tableBody += "<td class='click_Delete'><img src='../../assets/images/delete.png' title='Delete' alt='Delete' onclick='return DeleteData(" + (key + 1) + ")'/></td>";
            tableBody += "</tr>";
        });
        var table = "<table ID='SuppTable' class='display nowrap dataTable'>" +
              "<thead><tr>" + tableHeader + "</tr></thead>" +
              "<tbody>" + tableBody + "</tbody>" +
              "</table>";
        $("#div_supplier").empty();
        $("#div_supplier").html(table);
        $("#SuppTable").dataTable({
            dom: 'Bfrtip', "bSort": true,
            buttons: [
                'excel'
            ]
        });
        $("#loading-div-background").hide();
    }
}

$("#btn_Save").click(function () {
    if ($("#txt_supplier").val() == "") {
        $.jAlert({
            'content': 'Please Select Supplier Name..!'
        });
        return false;
    }
    if ($("#txt_branch").val() == "") {
        $.jAlert({
            'content': 'Please Enter Branch Name..!'
        });
        return false;
    }
    if ($("#txt_city").val() == "") {
        $.jAlert({
            'content': 'Please Enter City Name..!'
        });
        return false;
    }
    if ($("#txt_cnctperson").val() == "") {
        $.jAlert({
            'content': 'Please Enter Contact Person Name..!'
        });
        return false;
    }
    if ($("#txt_contemail").val() == "") {
        $.jAlert({
            'content': 'Please Enter Email..!'
        });
        return false;
    }
    if ($("#txt_Address").val() == "") {
        $.jAlert({
            'content': 'Please Enter Address..!'
        });
        return false;
    }
    if ($("#txt_fax").val() == "") {
        $.jAlert({
            'content': 'Please Enter Fax Number..!'
        });
        return false;
    }
    if ($("#txt_pincode").val() == "") {
        $.jAlert({
            'content': 'Please Enter Pincode..!'
        });
        return false;
    }
    if ($("#txt_telno").val() == "") {
        $.jAlert({
            'content': 'Please Enter Telephone Number..!'
        });
        return false;
    }
    if ($("#txt_GST").val() == "") {
        $.jAlert({
            'content': 'Please Enter GST Number..!'
        });
        return false;
    }
    if ($("#txt_mobno").val() == "") {
        $.jAlert({
            'content': 'Please Enter Mobile Number..!'
        });
        return false;
    }
    if ($("#txt_panno").val() == "") {
        $.jAlert({
            'content': 'Please Enter Pan Number..!'
        });
        return false;
    }
    PageMethods.SaveData(Client_ID, Master_API, SupplierID, $("#txt_branch").val(), $("#txt_city").val(), $("#txt_cnctperson").val(), $("#txt_contemail").val(), $("#txt_Address").val(), $("#txt_fax").val(), $("#txt_pincode").val(), $("#txt_telno").val(), $("#txt_GST").val(), $("#txt_mobno").val(), $("#txt_panno").val(), function (response) {
        if (JSON.parse(JSON.parse(response)) == "Save") {
            $.jAlert({
                'content': 'Data Saved Successfully..!'
            });
            GetSupplierData();
            $("#txt_supplier").val("");
            $("#txt_branch").val("");
            $("#txt_city").val("");
            $("#txt_cnctperson").val("");
            $("#txt_contemail").val("");
            $("#txt_Address").val("");
            $("#txt_fax").val("");
            $("#txt_pincode").val("");
            $("#txt_telno").val("");
            $("#txt_GST").val("");
            $("#txt_mobno").val("");
            $("#txt_panno").val("");
        }
        else if (JSON.parse(JSON.parse(response)) == "Duplicate") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(JSON.parse(response)) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Saved Due To Internal Server Error..!'
            });
        }
    }, function (error) {
    });
});

$('body').on('click', '.click_Edit', function () {
    var $row = $(this).closest('tr');
    var table = $('#SuppTable').DataTable();
    var data = table.row($row).data();

    $("#txt_supplier").val(SuppJson[parseInt(data[0]) - 1].SUPPLIER_NAME)
    $("#txt_supplier").attr("disabled", true);

    SupplierID = SuppJson[parseInt(data[0]) - 1].FK_SUPPLIER_HDR_ID;
    $("#txt_branch").val(SuppJson[parseInt(data[0]) - 1].BRANCH);
    $("#txt_city").val(SuppJson[parseInt(data[0]) - 1].CITY);
    $("#txt_cnctperson").val(SuppJson[parseInt(data[0]) - 1].CONTACT_PERSON);
    $("#txt_contemail").val(SuppJson[parseInt(data[0]) - 1].CONTACT_EMAIL);
    $("#txt_Address").val(SuppJson[parseInt(data[0]) - 1].ADDRESS);
    $("#txt_pincode").val(SuppJson[parseInt(data[0]) - 1].PINCODE);
    $("#txt_telno").val(SuppJson[parseInt(data[0]) - 1].TELNO);
    $("#txt_fax").val(SuppJson[parseInt(data[0]) - 1].FAX);
    $("#txt_mobno").val(SuppJson[parseInt(data[0]) - 1].MBLNO);
    $("#txt_panno").val(SuppJson[parseInt(data[0]) - 1].PANNO);
    $("#txt_GST").val(SuppJson[parseInt(data[0]) - 1].GST);
    $("#txt_PKID").val(SuppJson[parseInt(data[0]) - 1].PK_ID);
    Model_PKID = SuppJson[parseInt(data[0]) - 1].PK_ID;
});

$("#btn_Upd").click(function () {
    if ($("#txt_supplier").val() == "") {
        $.jAlert({
            'content': 'Please Select Supplier Name..!'
        });
        return false;
    }
    if ($("#txt_branch").val() == "") {
        $.jAlert({
            'content': 'Please Enter Branch Name..!'
        });
        return false;
    }
    if ($("#txt_city").val() == "") {
        $.jAlert({
            'content': 'Please Enter City Name..!'
        });
        return false;
    }
    if ($("#txt_cnctperson").val() == "") {
        $.jAlert({
            'content': 'Please Enter Contact Person Name..!'
        });
        return false;
    }
    if ($("#txt_contemail").val() == "") {
        $.jAlert({
            'content': 'Please Enter Email..!'
        });
        return false;
    }
    if ($("#txt_Address").val() == "") {
        $.jAlert({
            'content': 'Please Enter Address..!'
        });
        return false;
    }
    if ($("#txt_fax").val() == "") {
        $.jAlert({
            'content': 'Please Enter Fax Number..!'
        });
        return false;
    }
    if ($("#txt_pincode").val() == "") {
        $.jAlert({
            'content': 'Please Enter Pincode..!'
        });
        return false;
    }
    if ($("#txt_telno").val() == "") {
        $.jAlert({
            'content': 'Please Enter Telephone Number..!'
        });
        return false;
    }
    if ($("#txt_GST").val() == "") {
        $.jAlert({
            'content': 'Please Enter GST Number..!'
        });
        return false;
    }
    if ($("#txt_mobno").val() == "") {
        $.jAlert({
            'content': 'Please Enter Mobile Number..!'
        });
        return false;
    }
    if ($("#txt_panno").val() == "") {
        $.jAlert({
            'content': 'Please Enter Pan Number..!'
        });
        return false;
    }
    PageMethods.UpdateData(Client_ID, Master_API, $("#txt_PKID").val(), SupplierID, $("#txt_branch").val(), $("#txt_city").val(), $("#txt_cnctperson").val(), $("#txt_contemail").val(), $("#txt_Address").val(), $("#txt_fax").val(), $("#txt_pincode").val(), $("#txt_telno").val(), $("#txt_GST").val(), $("#txt_mobno").val(), $("#txt_panno").val(), function (response) {
        if (JSON.parse(JSON.parse(response)) == "Update") {
            $.jAlert({
                'content': 'Data Updated Successfully..!'
            });

            GetSupplierData();
            $("#txt_supplier").val("");
            $("#txt_branch").val("");
            $("#txt_city").val("");
            $("#txt_cnctperson").val("");
            $("#txt_contemail").val("");
            $("#txt_Address").val("");
            $("#txt_fax").val("");
            $("#txt_pincode").val("");
            $("#txt_telno").val("");
            $("#txt_GST").val("");
            $("#txt_mobno").val("");
            $("#txt_panno").val("");
            $("#btn_Save").show();
            $("#btn_Upd").hide();
        }
        else if (JSON.parse(JSON.parse(response)) == "Duplicate") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(JSON.parse(response)) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Update Due To Internal Server Error..!'
            });
        }

    }, function (error) {
    });
});

function DeleteData(index) {
    $.jAlert({
        'title': 'Confirmation',
        'content': 'Are you sure to delete this record..?', 'type': 'confirm',
        'onConfirm': function () {
            $("#txt_PKID").val($("#txt_PK_ID" + index).val());
            PageMethods.Deletedata(Client_ID, Master_API, $("#txt_PKID").val(), function (response) {
                if (JSON.parse(JSON.parse(response)) == "Delete") {
                    $.jAlert({
                        'content': 'Data Deleted Successfully..!'
                    });
                    GetSupplierData();
                }
                else if (JSON.parse(JSON.parse(response)) == "InternalServerError") {
                    $.jAlert({
                        'content': 'Data Not Delete Due To Internal Server Error..!'
                    });
                }
            }, function (error) {
            });
        }
    });
}

$("#btn_Cancel").click(function () {
    window.location.replace("../../HomePage.aspx");
});






