﻿var Client_ID = '', Master_API = '';
var UserJson = [], UserID;
var ZoneDtl = '';

$(document).ready(function () {
    //bindZone();
    Client_ID = $("#txt_Client_ID").val();
    Master_API = $("#txt_Master_URL").val();
   // bindUser();
    GetZoneData();

});

function bindUser() {
    var Desig = $("#txt_Zone_Head").val();
    CapLetters("txt_Zone_Head");
    if (Desig.length >= 3) {
        PageMethods.GetUser(Client_ID, Master_API, Desig, function (response) {
            UserJson = JSON.parse(JSON.parse(response[0]));
            if (UserJson.Message == "Data Not Found") {
                $.jAlert({
                    'content': 'Zone Head Data Not Found..!'
                });
            }
            else {
                BindUser();
            }
        },
            function (error) {
            });
    }
}

function BindUser() {//response
    // var UserJson = JSON.parse(JSON.parse(response.d));
    $("#txt_Zone_Head").autocomplete({
        autoFocus: true,
        minLength: 3,
        source: UserJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#txt_Zone_Head").val(ui.item.label);
            UserID = ui.item.id;
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function GetZoneData() {
    PageMethods.GetZoneData(Client_ID, Master_API, function (response) {
        ZoneDtl = JSON.parse(JSON.parse(response[0]));
        if (ZoneDtl.Message == "Data Not Found") {
            $.jAlert({
                'content': 'Zone Data Not Found..!'
            });
        }
        else {
            ZoneOnSuccess();
        }
    },
        function (error) {
        });
}

function ZoneOnSuccess() {//response
    //var ZoneDtl = JSON.parse(JSON.parse(response.d));
    if (ZoneDtl) {
        var tableHeader = "";
        var tableBody = "";
        tableHeader += "<th style='width: 2%;'>#</th>";
        tableHeader += "<th>Zone Name</th>";
        tableHeader += "<th>Zone Head</th>";
        tableHeader += "<th>Edit</th>";
        tableHeader += "<th>Delete</th>";
        $.each(ZoneDtl, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.label + "<input class='form-control input-sm' type='text' id='txt_zone_name" + (key + 1) + "' value='" + val.label + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.ZONE_HEAD + "<input class='form-control input-sm' type='text' id='txt_zone_headid" + (key + 1) + "' value='" + val.ZONE_HEADID + "' style='display:none'></input>";
            tableBody += "<input class='form-control input-sm' type='text' id='txt_zone_head" + (key + 1) + "' value='" + val.ZONE_HEAD + "' style='display:none'></input></td>";
            tableBody += "<td><a><img src='../../assets/images/edit.png' onclick='return bindData(" + (key + 1) + ")' title='Add New Row' /></a><input class='form-control input-sm' type='text' id='txt_Pk" + (key + 1) + "' value='" + val.id + "' style='display:none'></input></td>";
            tableBody += "<td><a><img src='../../assets/images/delete.png' onclick='return DeleteData(" + (key + 1) + ")' title='delete Row' /></a></td>";
            tableBody += "</tr>";
        });
        var table = "<table ID='zoneTable' class='display nowrap dataTable'>" +
              "<thead><tr>" + tableHeader + "</tr></thead>" +
              "<tbody>" + tableBody + "</tbody>" +
              "</table>";
        $("#div_displayDtl").empty();
        $("#div_displayDtl").html(table);
        $("#zoneTable").dataTable({
            dom: 'Bfrtip', "bSort": true,
            buttons: [
                'excel'
            ]
        });
    }
}

$("#btn_Save").click(function () {
    if ($("#Sel_Zone_Name").val() == "") {
        $.jAlert({
            'content': 'Please Enter Zone Name..!'
        });
        return false;
    }
    if ($("#txt_Zone_Head").val() == "") {
        $.jAlert({
            'content': 'Please Enter Zone Head..!'
        });
        return false;
    }
    PageMethods.SaveData(Client_ID, Master_API, $("#Sel_Zone_Name").val(), UserID, $("#txt_Username").val(), function (response) {
        if (response == "OK") {
            $.jAlert({
                'content': 'Data Saved Successfully..!'
            });
            GetZoneData();
            $("#Sel_Zone_Name").val("");
            $("#txt_Zone_Head").val("");
        }
        else if (response == "Not Found") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (response == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Saved Due To Internal Server Error..!'
            });
        }
    }, function (error) {
    });
});

function bindData(index) {
    $("#btn_Save").hide();
    $("#btn_Upd").show();
    $("#Sel_Zone_Name").val($("#txt_zone_name" + index).val());
    UserID = $("#txt_zone_headid" + index).val()
    $("#txt_Zone_Head").val($("#txt_zone_head" + index).val());
    $("#txt_PK_ZONEID").val($("#txt_Pk" + index).val());
}

$("#btn_Upd").click(function () {
    if ($("#Sel_Zone_Name").val() == "") {
        $.jAlert({
            'content': 'Please Enter Zone Name..!'
        });
        return false;
    }
    if ($("#txt_Zone_Head").val() == "") {
        $.jAlert({
            'content': 'Please Enter Zone Head..!'
        });
        return false;
    }
    PageMethods.UpdateData(Client_ID, Master_API, $("#txt_PK_ZONEID").val(), $("#Sel_Zone_Name").val(), UserID, $("#txt_Username").val(), function (response) {
        if (response == "OK") {
            $.jAlert({
                'content': 'Data Updated Successfully..!'
            });

            GetZoneData();
            $("#Sel_Zone_Name").val("");
            $("#txt_Zone_Head").val("");
            $("#btn_Save").show();
            $("#btn_Upd").hide();
        }
        else if (response == "Not Found") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (response == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Update Due To Internal Server Error..!'
            });
        }

    }, function (error) {
    });
});

function DeleteData(index) {
    $.jAlert({
        'title': 'Confirmation',
        'content': 'Are you sure to delete this record..?', 'type': 'confirm',
        'onConfirm': function () {
            $("#txt_PK_ZONEID").val($("#txt_Pk" + index).val());
            PageMethods.Deletedata(Client_ID, Master_API, $("#txt_PK_ZONEID").val(), $("#txt_Username").val(), function (response) {
                if (JSON.parse(response) == "OK") {
                    $.jAlert({
                        'content': 'Data Deleted Successfully..!'
                    });

                    GetZoneData();
                }
                else if (JSON.parse(response) == "NotFound") {
                    $.jAlert({
                        'content': 'Data Not Delete..!'
                    });
                }
                else if (JSON.parse(response) == "InternalServerError") {
                    $.jAlert({
                        'content': 'Data Not Delete Due To Internal Server Error..!'
                    });
                }
            }, function (error) {
            });
        }
    });
}

$("#btn_Cancel").click(function () {
    window.location.replace("../../HomePage.aspx");
});


//function bindUser() {
//    var obj = {};
//    obj.name = $.trim($("[id*=txt_Master_URL]").val());
//    $.ajax({
//        type: "POST",
//        url: "Zone_Master.aspx/GetUser",
//        //JSON.stringify(obj),//{ 'URL': $("#txt_Master_URL").val() },//"URL": $("#txt_Master_URL").val(),
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: BindUser,
//        failure: function (response) {
//            alert(response.d);
//        }
//    });
//}

//function GetZoneData() {
//    $.ajax({
//        type: "POST",
//        url: "Zone_Master.aspx/GetZoneData",
//        data: {},//'{name: "' + $("#<%=txtUserName.ClientID%>")[0].value + '" }'
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: ZoneOnSuccess,
//        failure: function (response) {
//            alert(response.d);
//        }
//    });
//}



