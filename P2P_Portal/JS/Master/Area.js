﻿var Client_ID = '', Master_API = '';
var UserJson = [], UserID;
var RegionJson = [], RegionID;
var AreaDtl = '';

$(document).ready(function () {
    //bindZone();
    Client_ID = $("#txt_Client_ID").val();
    Master_API = $("#txt_Master_URL").val();
    bindUser();
    GetAreaData();

});

function bindUser() {
    PageMethods.GetUser(Client_ID, Master_API, function (response) {
        UserJson = JSON.parse(JSON.parse(response[0]));
        if (UserJson.Message == "Data Not Found") {
            $.jAlert({
                'content': 'Area Head Data Not Found..!'
            });
        }
        else {
            BindUser();
        }
        bindRegion();
    },
        function (error) {
        });
}

function BindUser() {//response
    // var UserJson = JSON.parse(JSON.parse(response.d));
    $("#txt_AreahdId").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: UserJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#txt_AreahdId").val(ui.item.label);
            UserID = ui.item.id;
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function bindRegion() {
    PageMethods.GetRegion(Client_ID, Master_API, function (response) {
        RegionJson = JSON.parse(JSON.parse(response[0]));
        if (RegionJson.Message == "Data Not Found") {
            $.jAlert({
                'content': 'Region Data Not Found..!'
            });
        }
        else {
            BindRegion();
        }
    },
        function (error) {
        });
}

function BindRegion() {//response
    // var UserJson = JSON.parse(JSON.parse(response.d));
    $("#txt_Region").autocomplete({
        autoFocus: true,
        minLength: 0,
        source: RegionJson,
        select: function (event, ui) {
            event.preventDefault();
            $("#txt_Region").val(ui.item.label);
            RegionID = ui.item.id;
            return false;
        },
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
            }
        }
    }).focus(function () {
        $(this).autocomplete("search", "");
    });
}

function GetAreaData() {
    PageMethods.GetAreaData(Client_ID, Master_API, function (response) {
        AreaDtl = JSON.parse(JSON.parse(response[0]));
        if (AreaDtl.Message == "Data Not Found") {
            $.jAlert({
                'content': 'Zone Data Not Found..!'
            });
        }
        else {
            AreaOnSuccess();
        }
    },
        function (error) {
        });
}

function AreaOnSuccess() {//response
    //var ZoneDtl = JSON.parse(JSON.parse(response.d));
    if (AreaDtl) {
        var tableHeader = "";
        var tableBody = "";
        tableHeader += "<th style='width: 2%;'>#</th>";
        tableHeader += "<th>Region</th>";
        tableHeader += "<th>Area</th>";
        tableHeader += "<th>Area Head</th>"
        tableHeader += "<th>Edit</th>";
        tableHeader += "<th>Delete</th>";
        $.each(AreaDtl, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.REGION + "<input class='form-control input-sm' type='text' id='txt_region" + (key + 1) + "' value='" + val.REGION + "' style='display:none'>";
            tableBody += "<input class='form-control input-sm' type='text' id='txt_region_id" + (key + 1) + "' value='" + val.FK_REGIONID + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.label + "<input class='form-control input-sm' type='text' id='txt_Areadesc" + (key + 1) + "' value='" + val.label + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.AREA_HEAD + "<input class='form-control input-sm' type='text' id='txt_Area_Head" + (key + 1) + "' value='" + val.AREA_HEAD + "' style='display:none'>";
            tableBody += "<input class='form-control input-sm' type='text' id='txt_areahdid" + (key + 1) + "' value='" + val.AREA_HEADID + "' style='display:none'></input></td>";
            tableBody += "<td><a><img src='../../assets/images/edit.png' onclick='return bindData(" + (key + 1) + ")' title='Add New Row' /></a><input class='form-control input-sm' type='text' id='txt_PK_AREA" + (key + 1) + "' value='" + val.id + "' style='display:none'></input></td>";
            tableBody += "<td><a><img src='../../assets/images/delete.png' onclick='return DeleteData(" + (key + 1) + ")' title='delete Row' /></a></td>";
            tableBody += "</tr>";
        });
        var table = "<table ID='AreaTable' class='display nowrap dataTable'>" +
              "<thead><tr>" + tableHeader + "</tr></thead>" +
              "<tbody>" + tableBody + "</tbody>" +
              "</table>";
        $("#div_displayDtl").empty();
        $("#div_displayDtl").html(table);
        $("#AreaTable").dataTable({
            dom: 'Bfrtip', "bSort": true,
            buttons: [
                'excel'
            ]
        });
        $("#loading-div-background").hide();
    }
}

$("#btn_Save").click(function () {
    if ($("#txt_Region").val() == "") {
        $.jAlert({
            'content': 'Please Selecct Region Name..!'
        });
        return false;
    }
    if ($("#txt_Area").val() == "") {
        $.jAlert({
            'content': 'Please Enter Area..!'
        });
        return false;
    }
    if ($("#txt_AreahdId").val() == "") {
        $.jAlert({
            'content': 'Please Select Area Head..!'
        });
        return false;
    }
    PageMethods.SaveData(Client_ID, Master_API, RegionID,$("#txt_Area").val(), UserID, $("#txt_Username").val(), function (response) {
        if (JSON.parse(response) == "OK") {
            $.jAlert({
                'content': 'Data Saved Successfully..!'
            });
            GetAreaData();
            $("#txt_Region").val("");
            $("#txt_Area").val("");
            $("#txt_AreahdId").val("");
            $("#div_displayDtl").empty();
        }
        else if (JSON.parse(response) == "Not Found") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(response) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Saved Due To Internal Server Error..!'
            });
        }
    }, function (error) {
    });
});

function bindData(index) {
    $("#btn_Save").hide();
    $("#btn_Upd").show();
    RegionID = $("#txt_region_id" + index).val(); 
    $("#txt_Region").val($("#txt_region" + index).val());
    $("#txt_Area").val($("#txt_Areadesc" + index).val());
    UserID = $("#txt_areahdid" + index).val()
    $("#txt_AreahdId").val($("#txt_Area_Head" + index).val());
    $("#txt_PK_AREAID").val($("#txt_PK_AREA" + index).val());
}

$("#btn_Upd").click(function () {
    if ($("#txt_Region").val() == "") {
        $.jAlert({
            'content': 'Please Selecct Region Name..!'
        });
        return false;
    }
    if ($("#txt_Area").val() == "") {
        $.jAlert({
            'content': 'Please Enter Area..!'
        });
        return false;
    }
    if ($("#txt_AreahdId").val() == "") {
        $.jAlert({
            'content': 'Please Select Area Head..!'
        });
        return false;
    }
    PageMethods.UpdateData(Client_ID, Master_API, $("#txt_PK_AREAID").val(), RegionID, $("#txt_Area").val(), UserID, $("#txt_Username").val(), function (response) {
        if (JSON.parse(response) == "OK") {
            $.jAlert({
                'content': 'Data Updated Successfully..!'
            });

            GetAreaData();
            $("#txt_Region").val("");
            $("#txt_Area").val("");
            $("#txt_AreahdId").val("");
            $("#btn_Save").show();
            $("#btn_Upd").hide();
            $("#div_displayDtl").empty();
        }
        else if (JSON.parse(response) == "NotFound") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(response) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Update Due To Internal Server Error..!'
            });
        }

    }, function (error) {
    });
});

function DeleteData(index) {
    $.jAlert({
        'title': 'Confirmation',
        'content': 'Are you sure to delete this record..?', 'type': 'confirm',
        'onConfirm': function () {
            $("#txt_PK_AREAID").val($("#txt_PK_AREA" + index).val());
            PageMethods.Deletedata(Client_ID, Master_API, $("#txt_PK_AREAID").val(), $("#txt_Username").val(), function (response) {
                if (JSON.parse(response) == "OK") {
                    $.jAlert({
                        'content': 'Data Deleted Successfully..!'
                    });

                    GetAreaData();
                    $("#txt_Region").val("");
                    $("#txt_Area").val("");
                    $("#txt_AreahdId").val("");
                    $("#btn_Save").show();
                    $("#btn_Upd").hide();
                    $("#div_displayDtl").empty();
                }
                else if (JSON.parse(response) == "NotFound") {
                    $.jAlert({
                        'content': 'Data Not Delete..!'
                    });
                }
                else if (JSON.parse(response) == "InternalServerError") {
                    $.jAlert({
                        'content': 'Data Not Delete Due To Internal Server Error..!'
                    });
                }
            }, function (error) {
            });
        }
    });
}

$("#btn_Cancel").click(function () {
    window.location.replace("../../HomePage.aspx");
});


//function bindUser() {
//    var obj = {};
//    obj.name = $.trim($("[id*=txt_Master_URL]").val());
//    $.ajax({
//        type: "POST",
//        url: "Zone_Master.aspx/GetUser",
//        //JSON.stringify(obj),//{ 'URL': $("#txt_Master_URL").val() },//"URL": $("#txt_Master_URL").val(),
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: BindUser,
//        failure: function (response) {
//            alert(response.d);
//        }
//    });
//}

//function GetZoneData() {
//    $.ajax({
//        type: "POST",
//        url: "Zone_Master.aspx/GetZoneData",
//        data: {},//'{name: "' + $("#<%=txtUserName.ClientID%>")[0].value + '" }'
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: ZoneOnSuccess,
//        failure: function (response) {
//            alert(response.d);
//        }
//    });
//}



