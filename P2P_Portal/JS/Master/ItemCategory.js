﻿var Client_ID = '', Master_API = '';
var UserJson = [], UserID;
var CatDtl = '';

$(document).ready(function () {
    Client_ID = $("#txt_Client_ID").val();
    Master_API = $("#txt_Master_URL").val();
    addmodalpopup();
    $("#loading-div-background").show();
    GetItemCatData();
   

});

function GetItemCatData() {
    PageMethods.GetItemCatData(Client_ID, Master_API, function (response) {
        CatDtl = JSON.parse(JSON.parse(response[0]));
        if (CatDtl.Message == "Data Not Found") {
            alert("Item Category Data Not Found");
            $("#loading-div-background").hide();
        }
        else {
            CatOnSuccess();
        }
    },
        function (error) {
        });
}

function CatOnSuccess() {
    if (CatDtl) {
        var tableHeader = "";
        var tableBody = "";
        tableHeader += "<th style='width: 2%;'>#</th>";
        tableHeader += "<th>Item Category</th>";
        tableHeader += "<th>Category Code</th>";
        tableHeader += "<th>Edit</th>";
        tableHeader += "<th>Delete</th>";
        $.each(CatDtl, function (key, val) {
            tableBody += "<tr>";
            tableBody += "<td>" + (key + 1) + "</td>";
            tableBody += "<td>" + val.label + "<input class='form-control input-sm' type='text' id='txt_CatName" + (key + 1) + "' value='" + val.label + "' style='display:none'></input></td>";
            tableBody += "<td>" + val.ItmCatCode + "<input class='form-control input-sm' type='text' id='txt_CatCode" + (key + 1) + "' value='" + val.ItmCatCode + "' style='display:none'></input>";
            tableBody += "<td><a><img src='../../assets/images/edit.png' onclick='return bindData(" + (key + 1) + ")' title='Add New Row' /></a><input class='form-control input-sm' type='text' id='txt_Pk" + (key + 1) + "' value='" + val.id + "' style='display:none'></input></td>";
            tableBody += "<td><a><img src='../../assets/images/delete.png' onclick='return DeleteData(" + (key + 1) + ")' title='delete Row' /></a></td>";
            tableBody += "</tr>";
        });
        var table = "<table ID='CatTable' class='display nowrap dataTable'>" +
              "<thead><tr>" + tableHeader + "</tr></thead>" +
              "<tbody>" + tableBody + "</tbody>" +
              "</table>";
        $("#div_displayDtl").empty();
        $("#div_displayDtl").html(table);
        $("#CatTable").dataTable({
            dom: 'Bfrtip', "bSort": true,
            buttons: [
                'excel'
            ]
        });
    }
    $("#loading-div-background").hide();
}

$("#btn_Save").click(function () {
    if ($("#Sel_Cat_Name").val() == "") {
        $.jAlert({
            'content': 'Please Enter Item Category..!'
        });
        return false;
    }
    if ($("#txt_Cat_Code").val() == "") {
        $.jAlert({
            'content': 'Please Enter Category Code..!'
        });
        return false;
    }
    PageMethods.SaveData($("#Sel_Cat_Name").val(), $("#txt_Cat_Code").val(), $("#txt_UserName").val(), Client_ID, Master_API, function (response) {
        if (JSON.parse(response) == "OK") {
            $.jAlert({
                'content': 'Data Saved Successfully..!'
            });

            GetItemCatData();
            $("#Sel_Cat_Name").val("");
            $("#txt_Cat_Code").val("");
        }
        else if (JSON.parse(response) == "NotFound") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(response) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Saved Due To Internal Server Error..!'
            });
        }
    }, function (error) {
    });
});

function bindData(index) {
    $("#btn_Save").hide();
    $("#btn_Upd").show();
    $("#Sel_Cat_Name").val($("#txt_CatName" + index).val());
    $("#txt_Cat_Code").val($("#txt_CatCode" + index).val());
    $("#txt_PK_CatID").val($("#txt_Pk" + index).val());
}

$("#btn_Upd").click(function () {
    PageMethods.UpdateData($("#txt_PK_CatID").val(),$("#Sel_Cat_Name").val(), $("#txt_Cat_Code").val(),Client_ID, Master_API, function (response) {
        if (JSON.parse(response) == "OK") {
            $.jAlert({
                'content': 'Data Edited Successfully..!'
            });

            GetItemCatData();
            $("#Sel_Cat_Name").val("");
            $("#txt_Cat_Code").val("");
            $("#btn_Save").show();
            $("#btn_Upd").hide();
        }
        else if (JSON.parse(response) == "NotFound") {
            $.jAlert({
                'content': 'Duplicate Entry Found..!'
            });
        }
        else if (JSON.parse(response) == "InternalServerError") {
            $.jAlert({
                'content': 'Data Not Saved Due To Internal Server Error..!'
            });
        }
    }, function (error) {
    });
});

function DeleteData(index) {
    $("#txt_PK_CatID").val($("#txt_Pk" + index).val());
    PageMethods.Deletedata($("#txt_PK_CatID").val(),Client_ID, Master_API, function (response) {
        if (JSON.parse(response) == "OK") {
            $.jAlert({
                'content': 'Data Deleted Successfully..!'
            });
            GetItemCatData();
        }
        else {
            $.jAlert({
                'content': 'Data Not Deleted..!'
            });
        }
    }, function (error) {
    });
}

$("#btn_Cancel").click(function () {
    window.location.replace("../../HomePage.aspx");
});