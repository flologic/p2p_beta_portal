﻿using System;
using System.Web.Services;
using FSL.Controller;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Web;
using System.Configuration;
using System.Web.Script.Serialization;
using System.Net.Http;
using System.Text;
using System.Collections;

namespace P2P_Portal
{
    public partial class WorkItem : System.Web.UI.Page
    {
        HttpClient client = new HttpClient();
        string apiUrl = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            txt_Master_URL.Text = System.Configuration.ConfigurationManager.AppSettings["MasterAPI"];
            apiUrl = txt_Master_URL.Text + "Authenticate";
            //AjaxPro.Utility.RegisterTypeForAjax(typeof(WorkItem));
            ActionController.DisablePageCaching(this);
            if (ActionController.IsSessionExpired(Page))
                ActionController.RedirctToLogin(Page);
            else
            {
                if (!IsPostBack)
                {
                    string Result = string.Empty;
                    var input = new
                    {
                        SaveType = "GET",
                        UserName = Convert.ToString(Session["User_ADID"]),
                        newSessionID = Session.SessionID,
                        Tokenid = ""
                    };
                    string inputJson = (new JavaScriptSerializer()).Serialize(input);
                    HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(apiUrl + "/DataTransaction", inputContent).Result;
                    Result = response.Content.ReadAsStringAsync().Result;
                    if (Result == "\"false\"")  //Session["User_ADID"] != null
                    {
                        ActionController.SetControlAttributes(Page);
                        txt_User_id.Text = Convert.ToString(Session["User_ADID"]);
                        span_User_Name.InnerHtml = Convert.ToString(Session["USER_NAME"]);
                        app_Path.Text = HttpContext.Current.Request.ApplicationPath;
                        sessionid.Text = Session.SessionID;
                        hiddenTimeout.Text = Convert.ToString(HttpContext.Current.Session.Timeout);
                        ALERTMSG.Text = Convert.ToString(ConfigurationManager.AppSettings["ALERTMSG"]);
                        Hashtable LoggedUserHash = (Hashtable)Application["LOGGEDUSERHASH"];
                        LoggedUserHash.Clear();
                        if (LoggedUserHash.ContainsKey(Convert.ToString(Session["User_ADID"])))
                        {
                            string ActionStatus = string.Empty;
                            ((Hashtable)Application["LOGGEDUSERHASH"]).Remove(Convert.ToString(Session["User_ADID"]));

                            var input2 = new
                            {
                                SaveType = "UPDATE",
                                UserName = Convert.ToString(Session["User_ADID"]),
                                newSessionID = Session.SessionID,
                                Tokenid = ""
                            };
                            string inputJson2 = (new JavaScriptSerializer()).Serialize(input2);
                            HttpContent inputContent2 = new StringContent(inputJson2, Encoding.UTF8, "application/json");
                            HttpResponseMessage response2 = client.PostAsync(apiUrl + "/DataTransaction", inputContent2).Result;
                            string Result2 = response.Content.ReadAsStringAsync().Result;
                            Logger1.Activity("Login Details Updated");
                            LoggedUserHash.Add(Convert.ToString(Session["User_ADID"]), Session.SessionID);

                            var input1 = new
                            {
                                SaveType = "SAVE",
                                UserName = Convert.ToString(Session["User_ADID"]),
                                newSessionID = Session.SessionID,
                                Tokenid = ""
                            };
                            string inputJson1 = (new JavaScriptSerializer()).Serialize(input1);
                            HttpContent inputContent1 = new StringContent(inputJson1, Encoding.UTF8, "application/json");
                            HttpResponseMessage response1 = client.PostAsync(apiUrl + "/DataTransaction", inputContent1).Result;
                            string Result1 = response1.Content.ReadAsStringAsync().Result;
                            Logger1.Activity("Login Details Inserted");
                        }
                        else
                        {
                            string ActionStatus = string.Empty;
                            string Tokenid = string.IsNullOrEmpty(Request.Params.Get("tokenId")) ? string.Empty : Request.Params.Get("tokenId");
                            LoggedUserHash.Add(Convert.ToString(Session["User_ADID"]), Session.SessionID);

                            //string Result = P2P_Portal_Helper.Portal.Authenticate.DataTransaction("SAVE", UserName, Session.SessionID, Tokenid);
                            var input3 = new
                            {
                                SaveType = "SAVE",
                                UserName = Convert.ToString(Session["User_ADID"]),
                                newSessionID = Session.SessionID,
                                Tokenid = Tokenid
                            };
                            string inputJson3 = (new JavaScriptSerializer()).Serialize(input3);
                            HttpContent inputContent3 = new StringContent(inputJson3, Encoding.UTF8, "application/json");
                            HttpResponseMessage response3 = client.PostAsync(apiUrl + "/DataTransaction", inputContent3).Result;
                            string Result3 = response.Content.ReadAsStringAsync().Result;
                            Logger1.Activity("Login Details Inserted");
                        }
                        Application["LOGGEDUSERHASH"] = LoggedUserHash;
                    }
                    else
                    {
                        Response.Redirect("Login.aspx?Message=" + "User Already Logged In", false);
                    }
                    //ActionController.SetControlAttributes(Page);
                    //txt_User_id.Text = Convert.ToString(Session["User_ADID"]);
                    //span_User_Name.InnerHtml = Convert.ToString(Session["USER_NAME"]);
                    //app_Path.Text = HttpContext.Current.Request.ApplicationPath;
                    //sessionid.Text = Session.SessionID;
                    //hiddenTimeout.Text = Convert.ToString(HttpContext.Current.Session.Timeout);
                    //ALERTMSG.Text = Convert.ToString(ConfigurationManager.AppSettings["ALERTMSG"]);
                }
            }
        }

        protected void btn_Logout(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("Login.aspx");
        }

        [WebMethod]
        public static string[] headerDetail(string userid, string Master_API)
        {
            string[] jsonArray = new string[3];
            var isInsert = string.Empty;

            //string apiUrl = "http://192.168.0.120:81/api/Workitem";
            var input1 = new
            {
                OBJ_TYPE = "Masters",
                AD_ID = userid,
            };
            var input2 = new
            {
                OBJ_TYPE = "Process",
                AD_ID = userid,
            };
            var input3 = new
            {
                OBJ_TYPE = "Reports",
                AD_ID = userid,
            };
            string[] inputJson = new string[3];

            inputJson[0] = (new JavaScriptSerializer()).Serialize(input1);
            inputJson[1] = (new JavaScriptSerializer()).Serialize(input2);
            inputJson[2] = (new JavaScriptSerializer()).Serialize(input3);
            HttpClient client = new HttpClient();
            HttpContent inputContent1 = new StringContent(inputJson[0], Encoding.UTF8, "application/json");
            HttpContent inputContent2 = new StringContent(inputJson[1], Encoding.UTF8, "application/json");
            HttpContent inputContent3 = new StringContent(inputJson[2], Encoding.UTF8, "application/json");
            HttpResponseMessage response1 = client.PostAsync(Master_API + "/GetUsersMenu", inputContent1).Result;
            HttpResponseMessage response2 = client.PostAsync(Master_API + "/GetUsersMenu", inputContent2).Result;
            HttpResponseMessage response3 = client.PostAsync(Master_API + "/GetUsersMenu", inputContent3).Result;

            List<clsMenu> ls1 = (new JavaScriptSerializer()).Deserialize<List<clsMenu>>(response1.Content.ReadAsStringAsync().Result);
            var ds1 = JsonConvert.SerializeObject(ls1, Formatting.Indented);

            List<clsMenu> ls2 = (new JavaScriptSerializer()).Deserialize<List<clsMenu>>(response2.Content.ReadAsStringAsync().Result);
            var ds2 = JsonConvert.SerializeObject(ls2, Formatting.Indented);

            List<clsMenu> ls3 = (new JavaScriptSerializer()).Deserialize<List<clsMenu>>(response3.Content.ReadAsStringAsync().Result);
            var ds3 = JsonConvert.SerializeObject(ls3, Formatting.Indented);


            jsonArray[0] = ds1;
            jsonArray[1] = ds2;
            jsonArray[2] = ds3;

            return jsonArray;
        }

        [WebMethod]
        public static string updateSession(int time)
        {
            HttpContext.Current.Session.Timeout = time;
            return "";

        }
    }

    public class clsMenu
    {
        public int OBJ_ID { get; set; }
        public string OBJ_NAME { get; set; }
        public int OBJ_PARENT_ID { get; set; }
        public string OBJ_TYPE { get; set; }
        public string OBJ_URL { get; set; }
        public int PANEL_ID { get; set; }
        public string PROCESS_NAME { get; set; }
        public string AD_ID { get; set; }
    }
}