﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using FSL.Controller;
using System.Net.Http;
using System.Web.Script.Serialization;
using System.Text;


namespace P2P_Portal
{
    public partial class MyTask : System.Web.UI.Page
    {

        public string userid;
        protected void Page_Load(object sender, EventArgs e)
        {
            ActionController.DisablePageCaching(this);
            if (ActionController.IsSessionExpired(Page))
                ActionController.RedirctToLogin(Page);
            else
            {
                if (!IsPostBack)
                {
                    ActionController.SetControlAttributes(Page);
                    sessionadid.Value = ((string)Session["User_ADID"]);
                    if (Request.QueryString["ST_ID"] != null)
                    {
                        txt_hide_steps.Text = Convert.ToString(Request.QueryString["ST_ID"]);
                    }
                    txt_Master_Url.Value = System.Configuration.ConfigurationManager.AppSettings["MasterAPI"];
                    txt_P2P_Url.Value = System.Configuration.ConfigurationManager.AppSettings["P2PAPI"];
                    txt_Wfe_Url.Value = System.Configuration.ConfigurationManager.AppSettings["WFEAPI"];
                }
            }
        }

        [WebMethod]
        public static string[] GetTaskList(string userid, string MasterUrl, string ProcessUrl, string WfeUrl)
        {
            string[] jsonArray = new string[1];
            HttpClient client = new HttpClient();
            string MasterUrl1 = MasterUrl + "Mytask";
            var input = new
            {
                AD_ID = userid,
            };
            string inputJson = (new JavaScriptSerializer()).Serialize(input);
            HttpContent inputContent = new StringContent(inputJson, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync(MasterUrl1 + "/GetPersonalqitem", inputContent).Result;

            List<WorkQueueItems> output = (new JavaScriptSerializer()).Deserialize<List<WorkQueueItems>>(response.Content.ReadAsStringAsync().Result);
            if ((output != null) && (output.ToString() != "[]"))
            {
                jsonArray[0] = JsonConvert.SerializeObject(output, Formatting.Indented);
            }
            return jsonArray;
        }
    }

    public class WorkQueueItems
    {
        public string EMPLOYEE_NAME { get; set; }
        public string AD_ID { get; set; }
        public string PAGE { get; set; }
        public int PK_TRANSID { get; set; }

        public int PK_PROCESSID { get; set; }
        public string PROCESS_NAME { get; set; }
        public int INSTANCE_ID { get; set; }
        public string STEP_NAME { get; set; }
        public string ACCESS_ROLE_NAME { get; set; }
        public int FK_STEPID { get; set; }
        public string HEADER_INFO { get; set; }
        public string ASSIGN_DATE { get; set; }
        public string TARGET_DATE { get; set; }
    }
}