﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="MEF.ChangePassword" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Change Password</title>
    <link href="../../assets/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../assets/plugins/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet" />
    <link href="../../assets/css/style.css" rel="stylesheet" />
    <link href="../../assets/css/jquery-ui.css" rel="stylesheet" />
    <link href="../../assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="../../assets/css/jAlert.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ToolkitScriptManager>
        <div class="row bg-title">
            <div class="col-md-12">
                <div id="Div1" class="panel panel-default panel-border-top" runat="server">
                    <div class="panel-heading">
                        Change Password
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="Div2" class="panel panel-default panel-border-top" runat="server">
                                    <div class="panel-heading">
                                        Change Password                                   
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Current Password:</label>
                                                <div class="col-md-2">
                                                    <input class="form-control input-sm mandatory" type="text" name="fname" id="txt_Currentpwd" maxlength="100" />
                                                </div>
                                                <label class="col-md-2 control-label">New Password:</label>
                                                <div class="col-md-2">
                                                    <input class="form-control input-sm mandatory" type="text" name="fname" id="txt_newpwd" maxlength="200" />
                                                </div>
                                                <label class="col-md-2 control-label">Confirm New Password:</label>
                                                <div class="col-md-2">
                                                    <input class="form-control input-sm mandatory" type="text" name="fname" id="txt_confnewpwd" maxlength="200" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button type="button" class="fcbtn btn btn-info btn-outline btn-1b" id="btnSubmit">Submit</button>
                                <button type="button" class="fcbtn btn btn-info btn-outline btn-1b" id="btn_Cancel" onclick="showSideMap1('Workitem.aspx');">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div style="display: none">
                <asp:TextBox ID="txtCreatedBy" runat="server"></asp:TextBox>
                <asp:TextBox ID="sessionid" runat="server"></asp:TextBox>
            </div>
        </div>
    </form>
    <script lang="JavaScript" type="text/javascript" src="../../assets/js/jquery.min.js"></script>
    <script lang="JavaScript" type="text/javascript" src="../../assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    <script lang="JavaScript" type="text/javascript" src="../../assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script lang="JavaScript" type="text/javascript" src="../../assets/js/jquery-ui.js"></script>
    <script lang="JavaScript" type="text/javascript" src="../../assets/js/jAlert.js"></script>
    <script lang="JavaScript" type="text/javascript" src="../../assets/js/jAlert-functions.js"></script>
    <script src="JS/Portal/ChangePassword.js"></script>
</body>
</html>
