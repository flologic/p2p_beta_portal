﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WorkItem.aspx.cs" Inherits="P2P_Portal.WorkItem" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>P2P Portal Login</title>
    <link rel="icon" href="assets/images/favicon.ico" />
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/plugins/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet" />
    <link href="assets/css/animate.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet" />
    <link href="assets/css/colors/blue.css" id="theme" rel="stylesheet" />
    <link href="assets/css/jAlert.css" rel="stylesheet" />
</head>
<body style="overflow: hidden">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"></asp:ScriptManager>
        <div id="wrapper">
            <nav class="navbar navbar-default navbar-static-top m-b-0">
                <div class="navbar-header">
                    <a class="navbar-toggle hidden-sm hidden-md hidden-lg top-left-part" href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse">
                        <i class="ti-menu"></i>
                    </a>
                    <div class="top-left-part">
                        <a class="logo" href="#" onclick="showSideMap('HomePage.aspx');">
                            <img src="assets/images/cholamslogo.jpg" style="height: 47px;" />
                        </a>
                    </div>

                    <ul class="nav navbar-top-links navbar-right pull-right">
                        <li>
                            <a>
                                <input type="hidden" id="txt_SessionResetFlag" runat="server" value="" />
                                Session Duration Remaining: 
                         <asp:Label ID="Lbl_SessionState" Font-Size="Small" Font-Bold="true" Style="vertical-align: bottom;"
                             ForeColor="Red" runat="server"></asp:Label>
                                <asp:TextBox ID="TextBox1" runat="server" Style="display: none"></asp:TextBox>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#">
                                <img src="assets/images/user.png" alt="user-img" width="36" class="img-circle">
                            </a>
                            <ul class="dropdown-menu dropdown-user animated flipInY">
                                <li><a href="#">User : <b><span id="span_User_Name" runat="server"></span></b></a></li>
                                <li role="separator" class="divider"></li>
                                <%-- <img src="images/forgotpassword.png" />--%>
                                <%--  <li><a href="ChangePassword.aspx" class="text-muted">Change Password?</a></li>
                                <li role="separator" class="divider"></li>--%>
                                <li><a><span style="margin-left: 0%;"><i class="fa fa-power-off"></i>
                                    <asp:Button ID="btnLogout" runat="server" Text="Logout" OnClick="btn_Logout" Style="margin-left: 1px; background-color: transparent; border: none" /></span></a> </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href='#' class='waves-effect' onclick="showSideMap('HomePage.aspx');"><i class='linea-icon linea-basic fa-fw' data-icon='v'></i>
                                <span class='hide-menu'><b>Home</b></span>
                            </a>
                        </li>
                        <li id="liprocessul">
                            <a href='#' class='waves-effect'><i class='linea-icon linea-basic fa-fw' data-icon='v'></i>
                                <span class='hide-menu'><b>Process</b><span class='fa arrow'></span></span>
                            </a>
                            <ul class='nav nav-second-level' id="processul">
                            </ul>
                        </li>

                        <li id="litask">
                            <a href='#' class='waves-effect'><i class='linea-icon linea-basic fa-fw' data-icon='v'></i>
                                <span class='hide-menu'><b>My Task</b><span class='fa arrow'></span></span>
                            </a>
                            <ul class='nav nav-second-level' id="taskul">
                                <li onclick="showSideMap('MyTask.aspx')"><a href="#"><b>My Task</b></a> </li>
                            </ul>
                        </li>
                        <li id="lireport">
                            <a href='inbox.html' class='waves-effect'>
                                <i class='linea-icon linea-basic fa-fw'></i>
                                <span class='hide-menu'><b>Report</b> <span class='fa arrow'></span></span>
                            </a>
                            <ul class='nav nav-second-level' id="reportul">
                            </ul>
                        </li>
                        <li id="liMaster">
                            <a href='inbox.html' class='waves-effect'>
                                <i class='linea-icon linea-basic fa-fw'></i>
                                <span class='hide-menu'><b>Master</b> <span class='fa arrow'></span></span>
                            </a>
                            <ul class='nav nav-second-level' id="masterul">
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>

            <div>
                <iframe id="frmset_WorkArea" name="frmset_WorkArea" style="width: 100%; border: 0px solid none;" runat="server" frameborder="0"></iframe>
            </div>

        </div>
        <asp:TextBox ID="txt_User_id" runat="server" Style="display: none"></asp:TextBox>
        <asp:TextBox ID="app_Path" runat="server" Style="display: none"></asp:TextBox>
        <asp:TextBox ID="sessionid" runat="server" Style="display: none"></asp:TextBox>
        <asp:TextBox ID="hiddenTimeout" runat="server" Text="0" Style="display: none"></asp:TextBox>
        <asp:TextBox ID="ALERTMSG" runat="server" Style="display: none"></asp:TextBox>
        <asp:TextBox ID="txt_Master_URL" runat="server"></asp:TextBox>
    </form>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/underscore.js"></script>
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/plugins/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <script src="assets/plugins/raphael/raphael-min.js"></script>
    <script src="assets/plugins/morrisjs/morris.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/custom.min.js"></script>
    <script src="assets/js/widget.js"></script>
    <script src="assets/js/jAlert.js"></script>
    <script src="assets/js/jAlert-functions.js"></script>
    <script src="JS/Portal/Workitem.js"></script>

</body>
</html>
