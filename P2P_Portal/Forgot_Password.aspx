﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Forgot_Password.aspx.cs" Inherits="MEF.Forgot_Password" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8" />
    <title>Forgot_Password</title>

    <link href="../../assets/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../assets/plugins/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet" />
    <link href="../../assets/css/style.css" rel="stylesheet" />
    <link href="../../assets/css/jquery-ui.css" rel="stylesheet" />
    <link href="../../assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="../../assets/css/jAlert.css" rel="stylesheet" />
    <style type="text/css">
        .sample {
            background-color: #DC5807;
            border: 1px solid black;
            border-collapse: collapse;
            color: White;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ToolkitScriptManager>
        <div class="row bg-title">
            <div class="col-md-12">
                <div id="Div1" class="panel panel-default panel-border-top" runat="server">
                    <div class="panel-heading">
                        Forgot Password
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="Div2" class="panel panel-default panel-border-top" runat="server">
                                    <div class="panel-heading">
                                        Forgot Password                                   
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Login Id:</label>
                                                <div class="col-md-2">
                                                    <input class="form-control input-sm mandatory" type="text" name="fname" id="txt_loginid" maxlength="100" />
                                                </div>
                                                <label class="col-md-2 control-label">Email Id:</label>
                                                <div class="col-md-2">
                                                    <input class="form-control input-sm mandatory" type="text" name="fname" id="txt_emailid" maxlength="200" onchange="validateEmail1(this)" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button type="button" class="fcbtn btn btn-info btn-outline btn-1b" id="btnSubmit">Submit</button>
                                <button type="button" class="fcbtn btn btn-info btn-outline btn-1b" id="btn_Cancel">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div style="display: none">
                <asp:TextBox ID="txtCreatedBy" runat="server"></asp:TextBox>
            </div>
        </div>

        <script lang="JavaScript" type="text/javascript" src="../../assets/js/jquery.min.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/js/jquery-ui.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/js/jAlert.js"></script>
        <script lang="JavaScript" type="text/javascript" src="../../assets/js/jAlert-functions.js"></script>
        <script src="JS/Portal/Forgot_Password.js"></script>
    </form>
</body>
</html>
