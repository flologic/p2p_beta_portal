﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HomePage.aspx.cs" Inherits="P2P_Portal.HomePage" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Home Page</title>
    <link href="assets/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/plugins/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet" />
    <script lang="JavaScript" type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script src="assets/js/amcharts.js" type="text/javascript"></script>
    <script src="assets/js/serial.js" type="text/javascript"></script>
    <script src="assets/js/pie.js" type="text/javascript"></script>
    <link href="assets/css/jquery-ui.css" rel="stylesheet" />
    <link href="assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ToolkitScriptManager>
        <%--  <div class="row bg-title">
            <div class="col-md-12">
                <div class="panel panel-default panel-border-top">
                    <div class="panel-heading">
                        Dashboard					
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12 row bg-title" id="">
                            <div class="col-md-3" id="div_pndgreq" style="display: none">
                                <div class="panel panel-default text-center">
                                    <div class="panel-heading">
                                        Requisition Raised Count
                                    </div>
                                    <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                            <div class="form-horizontal ">
                                                <a href="Report/Asset Cycle Reports/Req_To_Asset_Cycle_Report.aspx?FLAG=Stock" id="reqraisedcnt" style="font-weight: bold">0</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3" id="div_pndgreqappr" style="display: none">
                                <div class="panel panel-default text-center">
                                    <div class="panel-heading">
                                        Requisition Pending for Approval
                                    </div>
                                    <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                            <div class="form-horizontal ">
                                                <a href="MyTask.aspx?ST_ID=1" id="req_pending" style="font-weight: bold">0</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3" id="div_pndgpo" style="display: none">
                                <div class="panel panel-default text-center">
                                    <div class="panel-heading">
                                        PO Entry Pending
                                    </div>
                                    <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                            <div class="form-horizontal ">
                                                <a href="Report/Requisition Reports/Req_To_Asset_Cycle_Report.aspx?FLAG=P" id="reqcomplpopendig" style="font-weight: bold">0</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3" id="div_pndgpoappr" style="display: none">
                                <div class="panel panel-default text-center">
                                    <div class="panel-heading">
                                        PO Pending for Approval
                                    </div>
                                    <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                            <div class="form-horizontal ">
                                                <a href="MyTask.aspx?ST_ID=2" id="po_pending" style="font-weight: bold">0</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12 row bg-title">

                            <div class="col-md-3" id="div_pndggrn" style="display: none">
                                <div class="panel panel-default text-center">
                                    <div class="panel-heading">
                                        GRN Entry Pending
                                    </div>
                                    <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                            <div class="form-horizontal ">
                                                <a href="Report/PO Reports/PO_View_All.aspx?FLAG=P" id="entry_pending" style="font-weight: bold">0</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3" id="div_pndggrnappr" style="display: none">
                                <div class="panel panel-default text-center">
                                    <div class="panel-heading">
                                        GRN Pending for Approval
                                    </div>
                                    <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                            <div class="form-horizontal ">
                                                <a href="MyTask.aspx?ST_ID=3" id="grn_pending" style="font-weight: bold">0</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3" id="div_pndgbill" style="display: none">
                                <div class="panel panel-default text-center">
                                    <div class="panel-heading">
                                        Bill Passing Pending
                                    </div>
                                    <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                            <div class="form-horizontal ">
                                                <a href="Report/Bill Passing Reports/BillPass_Report.aspx" id="bill_passing" style="font-weight: bold">0</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3" id="div_pndgbillappr" style="display: none">
                                <div class="panel panel-default text-center">
                                    <div class="panel-heading">
                                        Bill Passing Pending For Approval
                                    </div>
                                    <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                            <div class="form-horizontal ">
                                                <a href="MyTask.aspx?ST_ID=4" id="bill_approval" style="font-weight: bold">0</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-12 row bg-title">
                            <div class="col-md-3" id="div_pndgalct" style="display: none">
                                <div class="panel panel-default text-center">
                                    <div class="panel-heading">
                                        Asset Pending for Allocation
                                    </div>
                                    <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                            <div class="form-horizontal ">
                                                <a href="Report/All Asset Reports/Asset_Details_Report.aspx?FLAG=Stock" id="asset_stock" style="font-weight: bold">0</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3" id="div_asset_allocaion" style="display: none">
                                <div class="panel panel-default text-center">
                                    <div class="panel-heading">
                                        Asset Pending for Approval
                                    </div>
                                    <div class="panel-wrapper collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                            <div class="form-horizontal ">
                                                <a href="MyTask.aspx?ST_ID=10" id="asset_allo_pending" style="font-weight: bold">0</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>--%>
        <%--  <div class=" row bg-title">
            <div class="col-md-4" id="div_ownast" style="display: none">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        User Assets
                    </div>
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <div id="div_OwnAssetDtl">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5" id="div_stocksubcat" style="display: none">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Assets Stock
                    </div>
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <div id="div_Stockcntsubcat">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6" id="div_brnchast" style="display: none">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <label class="form-control input-sm label-color" id="lbl_brnch"></label>
                    </div>
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <div id="div_BranchAssetDtl">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>--%>
        <div id="conditionButton" style="display: none">
            <asp:TextBox ID="txt_UserName" runat="server"></asp:TextBox>
            <asp:TextBox ID="txt_desgid" runat="server"></asp:TextBox>
            <asp:TextBox ID="txt_branchid" runat="server"></asp:TextBox>
        </div>

        <script lang="JavaScript" type="text/javascript" src="assets/js/jquery.min.js"></script>
        <script lang="JavaScript" type="text/javascript" src="assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
        <script lang="JavaScript" type="text/javascript" src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script lang="JavaScript" type="text/javascript" src="assets/js/jquery-ui.js"></script>
    </form>

    <script type="text/javascript">
        var AssetDtlownJson, AssetDtlbranchJson, SubcatwiseastcntJson, USrRoleJson;
        $(document).ready(function () {
            //bindDashboard();
            //bindAssetData();
            //bindStockData();

        });

        function bindDashboard()
        {
            PageMethods.getReportDetail($("#txt_UserName").val(), function (response) {
                var DashboardJson = JSON.parse(response[0]);
                USrRoleJson = JSON.parse(response[1]);

                $.each(USrRoleJson, function (key, val) {
                    if (val.ACCESS_ROLE_ID == 2) //normal user
                    {
                        $("#div_pndgreq").show();
                        $("#reqraisedcnt").html(DashboardJson[0].reqraisedCount);

                        $("#div_pndgreqappr").show();
                        $("#req_pending").html(DashboardJson[0].ReqCount);
                    }

                    if (val.ACCESS_ROLE_ID == 7 || val.ACCESS_ROLE_ID == 11 || val.ACCESS_ROLE_ID == 12 || val.ACCESS_ROLE_ID == 13) //IT Procurement head & IT Head & CTO & CFO
                    {
                        $("#div_pndgreq").show();
                        $("#reqraisedcnt").html(DashboardJson[0].reqraisedCount);

                        $("#div_pndgreqappr").show();
                        $("#req_pending").html(DashboardJson[0].ReqCount);

                        $("#div_pndgpo").show();
                        $("#reqcomplpopendig").html(DashboardJson[0].reqraispopendgCount);

                        $("#div_pndgpoappr").show();
                        $("#po_pending").html(DashboardJson[0].PoCount);
                    }
                    if (val.ACCESS_ROLE_ID == 14 || val.ACCESS_ROLE_ID == 15) //Finance heads & Finance team members
                    {
                        $("#div_pndgbill").show();
                        $("#bill_passing").html(DashboardJson[0].BillpenCount);

                        $("#div_pndgbillappr").show();
                        $("#bill_approval").html(DashboardJson[0].BillCount);
                    }
                    if (val.ACCESS_ROLE_ID == 1 || val.ACCESS_ROLE_ID == 6)   //portal admin & IT PROCUREMNET TEAM
                    {
                        $("#div_pndgreq").show();
                        $("#reqraisedcnt").html(DashboardJson[0].reqraisedCount);

                        $("#div_pndgreqappr").show();
                        $("#req_pending").html(DashboardJson[0].ReqCount);

                        $("#div_pndgpo").show();
                        $("#reqcomplpopendig").html(DashboardJson[0].reqraispopendgCount);

                        $("#div_pndgpoappr").show();
                        $("#po_pending").html(DashboardJson[0].PoCount);

                        $("#div_pndggrn").show();
                        $("#entry_pending").html(DashboardJson[0].GrnEntryCount);

                        $("#div_pndggrnappr").show();
                        $("#grn_pending").html(DashboardJson[0].GrnCount);

                        $("#div_pndgbill").show();
                        $("#bill_passing").html(DashboardJson[0].BillpenCount);

                        $("#div_pndgbillappr").show();
                        $("#bill_approval").html(DashboardJson[0].BillCount);

                        $("#div_pndgalct").show();
                        $("#asset_stock").html(DashboardJson[0].AssetCount);
                    }
                    $("#div_asset_allocaion").show();
                    $("#asset_allo_pending").html(DashboardJson[0].AllocationCount);
                });

            }, function (error) {
            });
        }

        function bindAssetData() {
            PageMethods.GetAssetDtl($("#txt_UserName").val(), $("#txt_desgid").val(), $("#txt_branchid").val(), function (response) {
                AssetDtlownJson = JSON.parse(response[0]);
                AssetDtlbranchJson = JSON.parse(response[1]);
                displayownData(AssetDtlownJson);
                displaybranchData(AssetDtlbranchJson);
            }, function (error) {
            });
        }

        function bindStockData() {
            PageMethods.GetAStCntSubCatWise($("#txt_UserName").val(), function (response) {
                SubcatwiseastcntJson = JSON.parse(response[0]);
                displaySubcatwiseastcntData(SubcatwiseastcntJson);
            }, function (error) {
            });
        }

        function displayownData(AssetDtlownJson) {
            if (AssetDtlownJson != "") {
                if (AssetDtlownJson) {
                    var tableHeader = "";
                    var tableBody = "";

                    tableHeader += "<th style='width: 3%'>#</th>";
                    tableHeader += "<th>Sub-Category</th>";
                    tableHeader += "<th>Asset No</th>";
                    tableHeader += "<th>Serial No</th>";
                    tableHeader += "<th>Make</th>";
                    tableHeader += "<th>Model</th>";
                    tableHeader += "<th>Remark</th>";

                    $.each(AssetDtlownJson, function (key, val) {
                        tableBody += "<tr>";
                        tableBody += "<td>" + (key + 1) + "</td>";
                        tableBody += "<td>" + val.subcat + "</td>";
                        tableBody += "<td>" + val.assetno + "</td>";
                        tableBody += "<td>" + val.serialno + "</td>";
                        if (val.makename != null) {
                            tableBody += "<td>" + val.makename + "</td>";
                        }
                        else {
                            tableBody += "<td></td>";
                        }
                        if (val.modelname != null) {
                            tableBody += "<td>" + val.modelname + "</td>";
                        }
                        else {
                            tableBody += "<td></td>";
                        }
                        tableBody += "<td>" + val.requestRemark + "</td>";
                        tableBody += "</tr>";
                    })
                    var table = "<table class='display nowrap' id='tbl_ownassetdtl'>" +
                          "<thead><tr>" + tableHeader + "</tr></thead>" +
                          "<tbody>" + tableBody + "</tbody>" +
                          "</table>";
                    $("#div_OwnAssetDtl").empty();
                    $("#div_OwnAssetDtl").html(table);
                    $("#tbl_ownassetdtl").dataTable({ "dom": 'ftipr' });
                    $("#div_ownast").show();
                }
            }
        }

        function displaybranchData(AssetDtlbranchJson) {
            if (AssetDtlbranchJson != "") {
                //how to call :  SetLargeDatatable(ChargeJson, 'tbl_chrg_data', 'div_chrg_data');
                //SetLargeDatatable(AssetDtlbranchJson, 'tbl_branchassetdtl', 'div_BranchAssetDtl');
                if (AssetDtlbranchJson) {
                    var tableHeader = "";
                    var tableBody = "";

                    tableHeader += "<th style='width: 3%'>#</th>";
                    tableHeader += "<th>Sub-Category</th>";
                    tableHeader += "<th>Asset No</th>";
                    tableHeader += "<th>Employee Name</th>";
                    $("#lbl_brnch").text("Branch Assets For " + AssetDtlbranchJson[0].branchname);
                    $.each(AssetDtlbranchJson, function (key, val) {
                        tableBody += "<tr>";
                        tableBody += "<td>" + (key + 1) + "</td>";
                        tableBody += "<td>" + val.subcat + "</td>";
                        tableBody += "<td>" + val.assetno + "</td>";
                        tableBody += "<td>" + val.empname + "</td>";
                        tableBody += "</tr>";
                    })
                    var table = "<table class='display nowrap' id='tbl_branchassetdtl'>" +
                          "<thead><tr>" + tableHeader + "</tr></thead>" +
                          "<tbody>" + tableBody + "</tbody>" +
                          "</table>";
                    $("#div_BranchAssetDtl").empty();
                    $("#div_BranchAssetDtl").html(table);
                    $("#tbl_branchassetdtl").dataTable({ "dom": 'ftipr' });
                    $("#div_brnchast").show();
                }
            }
        }

        function displaySubcatwiseastcntData(SubcatwiseastcntJson) {
            if (SubcatwiseastcntJson != "") {
                if (SubcatwiseastcntJson) {
                    var tableHeader = "";
                    var tableBody = "";

                    tableHeader += "<th style='width: 3%'>#</th>";
                    tableHeader += "<th>Category</th>";
                    tableHeader += "<th>Sub-Category</th>";
                    tableHeader += "<th>Stock</th>";
                    tableHeader += "<th>Transit</th>";
                    tableHeader += "<th>Formatting</th>";

                    $.each(SubcatwiseastcntJson, function (key, val) {
                        tableBody += "<tr>";
                        tableBody += "<td>" + (key + 1) + "</td>";
                        tableBody += "<td>" + val.cat + "</td>";
                        tableBody += "<td>" + val.subcat + "</td>";
                        tableBody += "<td>" + val.astcnt + "</td>";
                        tableBody += "<td>" + val.asttrnstcnt + "</td>";
                        tableBody += "<td>" + val.astfrmtcnt + "</td>";
                        tableBody += "</tr>";
                    })
                    var table = "<table class='display nowrap' id='tbl_subcatastcnt'>" +
                          "<thead><tr>" + tableHeader + "</tr></thead>" +
                          "<tbody>" + tableBody + "</tbody>" +
                          "</table>";
                    $("#div_Stockcntsubcat").empty();
                    $("#div_Stockcntsubcat").html(table);
                    $("#tbl_subcatastcnt").dataTable({ "dom": 'ftipr' });
                    $("#div_stocksubcat").show();
                }
            }
        }

        function SetLargeDatatable(obj, tblid, divid) {


            // This function is created to bind large Datatable using json
            //how to call :  SetLargeDatatable(ChargeJson, 'tbl_chrg_data', 'div_chrg_data');
            var tableHeader = "";
            var tableBody = "";
            var headobj = obj;
            var keys = [];

            for (var k in headobj[0]) keys.push(k);
            $.each(keys, function (key, val) {
                tableHeader += "<th>" + val + "</th>";
            });

            var data = [];

            $.each(headobj, function (key, val) {
                var labl = keys[key]
                var tempdata = [];
                $.each(val, function (key, val) {
                    if (isempty(val))
                        val = "";
                    //            var value = val[labl]
                    tempdata.push(val);

                });
                data.push(tempdata);


            });
            var table = "<table class='table color-bordered-table info-bordered-table' id='" + tblid + "' >" +
        "<thead><tr>" + tableHeader + "</tr></thead>" +
        //"<tbody>" + tableBody + "</tbody>" +
        "</table>";
            $("#" + divid).empty();
            $("#" + divid).html(table);

            var newdom = '<"wrapper"lBi<"' + tblid + 'toolbar1">tp >';
            var Otable2 = $('#' + tblid).DataTable(

                  {
                      data: data,
                      deferRender: true,
                      "processing": true,
                      "dom": newdom, "bSort": true,
                      buttons: [
                          //{ extend: 'copy', text: 'Copy', className: 'paginate_button', exportOptions: { columns: ':visible' } },
                          { extend: 'excel', exportOptions: { columns: ':visible' } },
                          { extend: 'print', exportOptions: { columns: ':visible' } },
                          //'colvis',

                      ],
                      columnDefs: [

                      ],

                  });



        }

        function isempty(v) // to check values is blank,undefined or null
        {
            var type = typeof v;

            if (type === 'undefined')
                return true;
                //if (type === 'boolean')
                //    if (v === false)
                //  return true;
            else if (v === null)
                return true;
            else if (v == undefined)
                return true;
            else if (String(v).trim() == "")
                return true;
            //if (type === 'array') {
            //     if (v.length < 1)
            // return true;
            //  }
            // else if (type === 'string') {
            //     if (v.length < 1)
            //        return true;
            //    else if (parseInt(v) === 0)
            //         return true;
            // }
            // else if (type === 'object') {
            //     if (Object.keys(v).length < 1)
            //        return true;
            //}
            //else if (type === 'number') {
            //    if (v === 0)
            // return true;
            // }
            return false;
        }

    </script>
</body>
</html>
