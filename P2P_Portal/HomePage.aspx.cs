﻿using FSL.Controller;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace P2P_Portal
{
    public partial class HomePage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AjaxPro.Utility.RegisterTypeForAjax(typeof(HomePage));
            ActionController.DisablePageCaching(this);
            if (ActionController.IsSessionExpired(Page))
                ActionController.RedirctToLogin(Page);
            else
            {
                if (Session["User_ADID"] == null)
                    Response.Write("<script language='javascript'>self.parent.location='../Login.aspx';</script>");
                else
                {
                    txt_UserName.Text = Convert.ToString(Session["User_ADID"]);
                    txt_desgid.Text = (Session["FK_DESGID"]).ToString();
                    txt_branchid.Text = (Session["FK_BRANCHID"]).ToString();
                }
            }
        }


        [WebMethod]
        public static string[] getReportDetail(string userId)
        {
            string[] jsonArray = new string[2];
            //List<P2P_Portal_Helper.GetReqCount> objvehiType = P2P_Portal_Helper.Common.getReqCountData(userId);
            //jsonArray[0] = JsonConvert.SerializeObject(objvehiType, Formatting.Indented);

            //List<P2P_Portal_Helper.UsrWiseRole> objvehiType1 = P2P_Portal_Helper.Common.getUserWiseRole(userId);
            //jsonArray[1] = JsonConvert.SerializeObject(objvehiType1, Formatting.Indented);

            return jsonArray;
        }

        [WebMethod]
        public static string[] GetAssetDtl(string userId, int desgid, int branchid)
        {
            string[] jsonArray = new string[2];
            //List<P2P_Portal_Helper.AssetCount> objvehiType = P2P_Portal_Helper.Common.GetOwnAssetDtl(userId, desgid, branchid);
            //jsonArray[0] = JsonConvert.SerializeObject(objvehiType, Formatting.Indented);
            //List<P2P_Portal_Helper.AssetCount> objvehiType1 = P2P_Portal_Helper.Common.GetBranchAssetDtl(userId, desgid, branchid);
            //jsonArray[1] = JsonConvert.SerializeObject(objvehiType1, Formatting.Indented);
            return jsonArray;
        }

        [WebMethod]
        public static string[] GetAStCntSubCatWise(string usrid)
        {
            string[] jsonArray = new string[1];
          //  List<P2P_Portal_Helper.SubCatAssetCount> objvehiType = P2P_Portal_Helper.Common.getSubCatWiseAstCnt(usrid);
          //  jsonArray[0] = JsonConvert.SerializeObject(objvehiType, Formatting.Indented);
            return jsonArray;
        }
    }
}