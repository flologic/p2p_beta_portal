﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyTask.aspx.cs" Inherits="P2P_Portal.MyTask" %>

<!DOCTYPE html>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>My Task</title>
    <link href="../../assets/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../assets/css/style.css" rel="stylesheet" />
    <link href="../../assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"></asp:ScriptManager>
        <div class="row bg-title">
            <div class="col-md-12">
                <div class="panel panel-default panel-border-top" runat="server">
                    <div class="panel-heading">
                        <label id="lbl_Page" runat="server"></label>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <div id="div_MyTask">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="conditionButton" style="display: none">
            <asp:TextBox ID="txt_UserName" runat="server"></asp:TextBox>
            <asp:TextBox ID="txt_pk_category_id" runat="server"></asp:TextBox>
            <input type="text" id="sessionadid" runat="server" />
            <asp:TextBox ID="txt_hide_steps" runat="server"></asp:TextBox>
            <input type="text" id="txt_Master_Url" runat="server" />
            <input type="text" id="txt_P2P_Url" runat="server" />
            <input type="text" id="txt_Wfe_Url" runat="server" />

        </div>
    </form>
    <script lang="JavaScript" type="text/javascript" src="../../assets/js/jquery.min.js"></script>
    <script lang="JavaScript" type="text/javascript" src="../../assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    <script lang="JavaScript" type="text/javascript" src="../../assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#lbl_Page").text("My Task");
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1);
            if (hashes == "ST_ID=1") {
                $("#lbl_Page").text("Requisition Pending for Approval");
            }
            else if (hashes == "ST_ID=2") {
                $("#lbl_Page").text("PO Pending for Approval");
            }
            else if (hashes == "ST_ID=3") {
                $("#lbl_Page").text("GRN Pending for Approval");
            }
            else if (hashes == "ST_ID=4") {
                $("#lbl_Page").text("Bill Passing Pending For Approval");
            }
            else if (hashes == "ST_ID=10") {
                $("#lbl_Page").text("Asset Pending for Approval");
            }
            getDetails();
        });

        function getDetails() {
            var userid = $("#sessionadid").val();
            PageMethods.GetTaskList(userid, $("#txt_Master_Url").val(), $("#txt_P2P_Url").val(), $("#txt_Wfe_Url").val(), function (response) {
                SaveData_CallBack(response);
            }, function (error) {
                //alert(error);
            });
            $('#tbl_MyTask').DataTable();
        }

        function SaveData_CallBack(response) {
            var TaskListJson = JSON.parse(response[0]);

            if (TaskListJson) {
                var tableHeader = "";
                var tableBody = "";

                tableHeader += "<th style='width:3%'>Sr.No</th>";
                tableHeader += "<th>Initiator</th>";
                tableHeader += "<th>Header Info</th>";
                tableHeader += "<th>Process</th>";
                tableHeader += "<th>Step</th>";
                tableHeader += "<th>Assign Date</th>";
                tableHeader += "<th>Target Date</th>";

                var flag = true;
                var arr_ele = new Array();
                if ($("#txt_hide_steps").val() != "") {
                    flag = false;
                    arr_ele = $("#txt_hide_steps").val().split(",");
                }
                var count = 0;
                $.each(TaskListJson, function (key, val) {

                    flag = false;
                    for (var ind = 0; ind < arr_ele.length; ind++) {
                        if (parseInt(arr_ele[ind]) == parseInt(val.PK_PROCESSID)) {
                            flag = true;
                        }
                    }
                    if (flag || (flag == false && arr_ele.length == 0)) {
                        count = parseInt(count) + 1;
                        tableBody += "<tr>";
                        tableBody += "<td>" + count + "</td>";
                        tableBody += "<td>" + val.EMPLOYEE_NAME + "</td>";
                        tableBody += "<td><a href='../Process/" + val.PROCESS_NAME + "/" + val.PAGE + "?trasid=" + val.PK_TRANSID + "&processid=" + val.PK_PROCESSID + "&instanceid=" + val.INSTANCE_ID + "&step=" + val.STEP_NAME + "&rolename=" + val.ACCESS_ROLE_NAME + "&stepid=" + val.FK_STEPID + "&wiid=" + val.PK_TRANSID + "'>" + val.HEADER_INFO + "</a></td>";
                        tableBody += "<td>" + val.PROCESS_NAME + "</td>";
                        tableBody += "<td>" + val.STEP_NAME + "</td>";
                        tableBody += "<td>" + val.ASSIGN_DATE + "</td>";
                        tableBody += "<td>" + val.TARGET_DATE + "</td>";
                        tableBody += "</tr>";
                    }
                })
                var table = "<table class='display nowrap dataTable' id='tbl_MyTask'>" +
                    "<thead><tr>" + tableHeader + "</tr></thead>" +
                    "<tbody>" + tableBody + "</tbody>" +
                    "</table>";
                $("#div_MyTask").html(table);
                $("#tbl_MyTask").dataTable();
            }
        }
    </script>
</body>
</html>
